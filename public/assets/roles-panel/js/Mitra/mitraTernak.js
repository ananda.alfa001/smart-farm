let lastCollapse = 0;
let first = true;
let chart1 = "";
let chart2 = "";
let chart3 = "";
let chart4 = "";
$(document).ready(function () {


});
$(".cek").click(function(){
    let nPeternakan_id = $(this).data("id");
    let nRow = $(this).data("row");
    if(nRow != lastCollapse){
        if(first == false){
            chart1.destroy();
            chart2.destroy();
            chart3.destroy();
            chart4.destroy();
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        $.ajax({
            url: '/kemitraan/MitraTernak/GetOnePeternak',
            type: 'GET',
            data: {
                peteranakan: nPeternakan_id,
            },
            success: function(diagram) {
                chart1 = new Chart(document.querySelector('#doughnutChart' + nRow), {
                    type: 'doughnut',
                    data: {
                        labels: [
                            'Sudah Vaksin',
                            'Belum Vaksin',
                            'Sakit',
                            'Sehat',
                        ],
                        datasets: [{
                            label: 'Kesehatan Sapi',
                            data: [diagram.sudahVaksin, diagram.belumVaksin, diagram.sakit, diagram.sehat],
                            backgroundColor: [
                                '#12FF51',
                                '#FFF009',
                                '#FF0909',
                                'rgb(54, 162, 235)',
                            ],
                            hoverOffset: 4
                        }]
                    }
                });

                chart2 = new Chart(document.querySelector('#pieChart' + nRow), {
                    type: 'pie',
                    data: {
                        labels: [
                            'Jantan',
                            'Betina',
                        ],
                        datasets: [{
                            label: 'My First Dataset',
                            data: [diagram.jantan, diagram.betina],
                            backgroundColor: [
                                'rgb(111, 217, 20, 1)',
                                'rgb(54, 162, 235)'
                            ],
                            hoverOffset: 4
                        }]
                    }
                });

                chart3 = new Chart(document.querySelector('#polarAreaChart' + nRow), {
                    type: 'polarArea',
                    data: {
                        labels: [
                            'Fresh',
                            'Open',
                            'Preg',
                            'Bred',
                            'No Bred',
                            'Dry',
                            'Death',
                            'Sold',
                            'Belum Ada Status',
                        ],
                        datasets: [{
                            label: 'Status Sapi',
                            data: [diagram.fresh, diagram.open, diagram.preg, diagram.bred, diagram.nobred, diagram.dry, diagram.death, diagram.sold,diagram.belumada],
                            backgroundColor: [
                                'rgb(73, 122, 235)',
                                'rgb(111, 103, 2',
                                'rgb(193, 88, 6)',
                                'rgb(129, 149, 1 )',
                                'rgb(45, 35, 0)',
                                'rgb(125, 86, 65)',
                                'rgb(45, 35, 0)',
                                'rgb(182, 15, 31)',
                                'rgb(142, 42, 34)',
                                'rgb(122, 1, 1)',
                            ]
                        }]
                    }
                });

                chart4 = new Chart(document.querySelector('#lineChart' + nRow), {
                    type: 'line',
                    data: {
                        labels: diagram.tgl_produksi.reverse(),
                        datasets: [
                            {
                            label: 'ADM (Ltr/Sapi)',
                            data: diagram.ADM.reverse(),
                            borderColor: ['#8BEC68'],
                            backgroundColor: ['#8BEC68'],
                            hoverOffset: 4
                            },
                            {
                            label: 'Susu (Ltr)',
                            data: diagram.production.reverse(),
                            borderColor: ['#A1D6F3'],
                            backgroundColor: ['#A1D6F3'],
                            hoverOffset: 4
                            }
                        ]
                    }
                });
            },
            error: function(jqXhr, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
        lastCollapse = nRow;

    }
    first = false;
  });
