function PEN() {
    //HASIL PILIHAN
    $log1 = document.getElementById("1").value;
    $log2 = document.getElementById("2").value;
    $log3 = document.getElementById("3").value;
    $log4 = document.getElementById("4").value;
    $log5 = document.getElementById("5").value;
    // FORM 1
    if ($log1 == "PEN") {
        //drop
        $('option.hiddens1').prop('hidden', true);
        //form
        $('#nilai_1').prop('hidden', true);
        $('#nilai_1i').prop('hidden', false);
        $('#nilai_1ii').prop('hidden', true);

    } else if ($log1 == "STATUS") {
        $('option.hiddens1').prop('hidden', true);
        //form
        $('#nilai_1').prop('hidden', true);
        $('#nilai_1i').prop('hidden', true);
        $('#nilai_1ii').prop('hidden', false);
    } else {
        //drop
        $('option.hiddens1').prop('hidden', false);
        //form
        $('#nilai_1').prop('hidden', false);
        $('#nilai_1i').prop('hidden', true);
        $('#nilai_1ii').prop('hidden', true);
    }

    // FORM 2
    if ($log2 == "PEN") {
        //drop
        $('option.hiddens2').prop('hidden', true);
        //form
        $('#nilai_2').prop('hidden', true);
        $('#nilai_2i').prop('hidden', false);
        $('#nilai_2ii').prop('hidden', true);
    } else if ($log2 == "STATUS") {
        $('option.hiddens2').prop('hidden', true);
        //form
        $('#nilai_2').prop('hidden', true);
        $('#nilai_2i').prop('hidden', true);
        $('#nilai_2ii').prop('hidden', false);
    } else {
        $('option.hiddens2').prop('hidden', false);
        $('#nilai_2').prop('hidden', false);
        $('#nilai_2i').prop('hidden', true);
        $('#nilai_2ii').prop('hidden', true);
    }

    // FORM 3
    if ($log3 == "PEN") {
        //drop
        $('option.hiddens3').prop('hidden', true);
        //form
        $('#nilai_3').prop('hidden', true);
        $('#nilai_3i').prop('hidden', false);
        $('#nilai_3ii').prop('hidden', true);
    } else if ($log3 == "STATUS") {
        $('option.hiddens3').prop('hidden', true);
        //form
        $('#nilai_3').prop('hidden', true);
        $('#nilai_3i').prop('hidden', true);
        $('#nilai_3ii').prop('hidden', false);
    } else {
        $('option.hiddens3').prop('hidden', false);
        $('#nilai_3').prop('hidden', false);
        $('#nilai_3i').prop('hidden', true);
        $('#nilai_3ii').prop('hidden', true);
    }

    // FORM 4
    if ($log4 == "PEN") {
        //drop
        $('option.hiddens4').prop('hidden', true);
        //form
        $('#nilai_4').prop('hidden', true);
        $('#nilai_4i').prop('hidden', false);
        $('#nilai_4ii').prop('hidden', true);
    } else if ($log4 == "STATUS") {
        $('option.hiddens4').prop('hidden', true);
        //form
        $('#nilai_4').prop('hidden', true);
        $('#nilai_4i').prop('hidden', true);
        $('#nilai_4ii').prop('hidden', false);
    } else {
        $('option.hiddens4').prop('hidden', false);
        $('#nilai_4').prop('hidden', false);
        $('#nilai_4i').prop('hidden', true);
        $('#nilai_4ii').prop('hidden', true);
    }

    // FORM 5
    if ($log5 == "PEN") {
        //drop
        $('option.hiddens5').prop('hidden', true);
        //form
        $('#nilai_5').prop('hidden', true);
        $('#nilai_5i').prop('hidden', false);
        $('#nilai_5ii').prop('hidden', true);
    } else if ($log5 == "STATUS") {
        $('option.hiddens5').prop('hidden', true);
        //form
        $('#nilai_5').prop('hidden', true);
        $('#nilai_5i').prop('hidden', true);
        $('#nilai_5ii').prop('hidden', false);
    } else {
        $('option.hiddens5').prop('hidden', false);
        $('#nilai_5').prop('hidden', false);
        $('#nilai_5i').prop('hidden', true);
        $('#nilai_5ii').prop('hidden', true);
    }
}
