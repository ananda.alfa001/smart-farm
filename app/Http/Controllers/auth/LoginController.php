<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Alert;

class LoginController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required',
        ]);
        $remember = $request->input('remember');
        if (Auth::attempt($credentials, $remember)) {
            $request->session()->regenerate();
            DIM();
            token_all_generate();
            return redirect()->intended('/Auth-Roles');
        }else{
            Alert::warning('Username atau Password Salah!', 'Coba Lagi!');
            return back();
        }
    }
    public function logout(Request $request){
        DIM();
        token_all_generate();
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        Alert::success('Logout Success', 'Good Bye!');
        return redirect('/');
    }

}

