<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Alert;

class GuardController extends Controller
{
    //first login
    public function AuthRoles(request $request)
    {
        if (Auth::check()) {
            $name = Auth::user()->name;
            $roles = Auth::user()->roles;
            if ($roles == "peternakan") {
                $check = DB::table('peternakan')
                    ->where('user_id', Auth::user()->id)
                    ->value('id');
                if ($check != null) {
                    DIM();
                    token_all_generate();
                    session(['idUserPeternakan' => Auth::user()->id]);
                    session(['idPeternakan' => $check]);
                    Alert::success('Login Success', 'Have a Nice Work Peternakan!');
                    return redirect('/peternakan');
                } else {
                    Alert::Info('Info', $name . ' Please complete the form first!');
                    return redirect('/register/form/first');
                }
            } elseif ($roles == "teknisi") {
                $check = DB::table('teknisi')
                    ->where('user_id', Auth::user()->id)
                    ->value('id');
                if ($check != null) {
                    DIM();
                    token_all_generate();
                    session(['idTeknisi' =>  $check]);
                    session(['idUserTeknisi' => Auth::user()->id]);
                    Alert::success('Login Success', 'Have a Nice Work Teknisi!');
                    return redirect('/teknisi');
                } else {
                    Alert::Info('Info', $name . ' Lengkapi Data Dokter/Teknisi Dahulu!');
                    return redirect('/register-teknisi/form/first');
                }
            } elseif ($roles == "kemitraan") {
                $check = DB::table('mitra')
                    ->where('user_id', Auth::user()->id)
                    ->value('id');
                if ($check != null) {
                    DIM();
                    token_all_generate();
                    session(['idKemitraan' => $check]);
                    session(['idUserKemitraan' => Auth::user()->id]);
                    Alert::success('Login Success', 'Have a Nice Work Kemitraan!');
                    return redirect('kemitraan');
                } else {
                    Alert::Info('Info', $name . ' Lengkapi Data KUD/Kemitraan Dahulu!');
                    return redirect('/register-kemitraan/form/first');
                }
            } elseif($roles == "admin"){
                return redirect('/dashboard/farm');
            }else {
                Auth::logout();
                request()->session()->invalidate();
                request()->session()->regenerateToken();
                Alert::error('Login Gagal', 'Coba Kembali!');
                return redirect('login');
            }
        } else {
            Auth::logout();
            request()->session()->invalidate();
            request()->session()->regenerateToken();
            Alert::error('Login Gagal', 'Coba Kembali!');
            return redirect('login');
        }
    }

    public function AuthGuard(request $request)
    {
        if (Auth::check()) {
            $name = Auth::user()->name;
            if (Auth::user()->roles == 'peternakan') {
                $check = DB::table('peternakan')
                    ->where('user_id', Auth::user()->id)
                    ->value('id');
                if ($check != null) {
                    DIM();
                    token_all_generate();
                    session(['idUserPeternakan' => Auth::user()->id]);
                    session(['idPeternakan' => $check]);
                    Alert::success('Login Success', 'Have a Nice Work Peternakan!');
                    return redirect('/peternakan');
                } else {
                    Alert::Info('Info', $name . ' Please complete the form first!');
                    return redirect('/register/form/first');
                }
            } elseif (Auth::user()->roles == 'teknisi') {
                $check = DB::table('teknisi')
                    ->where('user_id', Auth::user()->id)
                    ->value('id');
                if ($check != null) {
                    DIM();
                    token_all_generate();
                    session(['idTeknisi' =>  $check]);
                    session(['idUserTeknisi' => Auth::user()->id]);
                    Alert::success('Login Success', 'Have a Nice Work Teknisi!');
                    return redirect('/teknisi');
                } else {
                    Alert::Info('Info', $name . ' Lengkapi Data Dokter/Teknisi Dahulu!');
                    return redirect('/register-teknisi/form/first');
                }
            } elseif (Auth::user()->roles == "kemitraan") {
                $check = DB::table('mitra')
                    ->where('user_id', Auth::user()->id)
                    ->value('id');
                if ($check != null) {
                    DIM();
                    token_all_generate();
                    session(['idKemitraan' => $check]);
                    session(['idUserKemitraan' => Auth::user()->id]);
                    Alert::success('Login Success', 'Have a Nice Work Kemitraan!');
                    return redirect('kemitraan');
                } else {
                    Alert::Info('Info', $name . ' Lengkapi Data KUD/Kemitraan Dahulu!');
                    return redirect('/register-kemitraan/form/first');
                }
            }elseif(Auth::user()->roles == "admin"){
                return redirect('/dashboard/farm');
            }else {
                Auth::logout();
                DIM();
                token_all_generate();
                request()->session()->invalidate();
                request()->session()->regenerateToken();
                Alert::error('Login Failed', 'Try Again!');
                return redirect('login');
            }
        } else {
            Alert::error('You are not logged in', 'Please Login First!');
            return redirect('login');
        }
    }
}
