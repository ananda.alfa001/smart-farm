<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use Carbon\Carbon;
use Alert;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    protected $redirectTo = RouteServiceProvider::HOME;

    //REGISTER USER
    protected function store(request $request)
    {
        if(uniqueuser($request->email) == null){
            try{
                $validate = Validator::make($request->all(), [
                    'name' => 'required', 'string', 'max:255',
                    'email' => 'required', 'string', 'max:255', 'unique:users',
                    'roles' => 'required',
                    'password' => 'required','string',
                ]);
                if ($validate->fails()) {
                    Alert::info('Info', $validate->errors()->first());
                    return redirect()->back();
                } else {
                    $post = DB::transaction(function () use ($request) {
                        try {
                            $validate = $request->validate([
                                'name' => ['required'],
                                'email' => ['required'],
                                'roles' => ['required'],
                                'password' => ['required'],
                            ]);
                            $validate['password'] = Hash::make($validate['password']);
                            $user = User::create($validate);
                            Auth::login($user);
                            //input detil roles
                            $name = Auth::user()->name;
                            if(Auth::user()->roles == "peternakan" ){
                                Alert::Success('Success', $name.' Berhasil Terdaftar Sebagai Peternakan! Silahkan lengkapi detil form berikut.');
                                return view('auth.register-roles.register-peternakan');
                            }elseif($validate['roles'] == "kemitraan" ){
                                Alert::Success('Success', $name.' Berhasil Terdaftar Sebagai Kemitraan! Silahkan lengkapi detil form berikut.');
                                return view('auth.register-roles.register-kemitraan');
                            }elseif($validate['roles'] == "teknisi" ){
                                Alert::Success('Success', $name.' Berhasil Terdaftar Sebagai Teknisi! Silahkan lengkapi detil form berikut.');
                                return view('auth.register-roles.register-teknisi');
                            }else{
                                Auth::logout();
                                request()->session()->invalidate();
                                request()->session()->regenerateToken();
                                Alert::error('Error', 'Bagian Tidak Terdaftar, Anda Logout!');
                                return redirect()->back();
                            }
                        }catch (\Exception $e) {
                            DB::rollback();
                            throw new \Exception($e->getMessage());
                        }
                    });
                    return $post;
                }
            }catch (\Exception $e) {
                Alert::error('Error', $e->getMessage());
                return redirect()->back();
            }
        }else{
            Alert::warning('Email Telah Terpakai!!', "Silahkan Masukan Email yang Lainnya!");
            return redirect()->back();
        }
    }

    //REGISTER ROLES PETERNAKAN
    //------------REGISTER USER DETIL PETERNAKAN (form_1)
    protected function form_1(){
        return view('auth.register-roles.register-peternakan');
    }
    //------------STORE DB REGISTER USER DETIL PETERNAKAN (form_1_up)
    protected function form_1_up(request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required', 'string', 'max:255',
            'year' => 'required', 'date',
            'farm_owner' => 'required', 'string', 'max:255',
            'cage' => 'required','number',
            'address' => 'required','string', 'max:255',
            'telephone_number' => 'required','number',
            // 'file' => 'required|mimes:doc,docx,pdf,jpg,jpeg,png',
        ]);
        if ($validate->fails()) {
            Alert::info('Info', $validate->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $peternakan_id = DB::table('peternakan')->insertGetId([
                        'user_id' => Auth::user()->id,
                        'nama_peternakan' => $request->name,
                        'tahun_berdiri' => $request->year,
                        'nama_pemilik' => $request->farm_owner,
                        'alamat' => $request->address,
                        'no_hp' => $request->telephone_number,
                        'created_at' => Carbon::now()
                    ]);
                    // $uploadedFile = $request->file('file');
                    // $path = $uploadedFile->store('public/files/persetujuan/'.$peternakan_id);
                    foreach($request->cage as $key => $value)
                    {
                        db::table('kandang')->insert([
                            'peternakan_id' => $peternakan_id,
                            'nama_kandang' => $value,
                            'created_at' => Carbon::now()
                        ]);
                    }
                    // DB::table('peternakan')
                    // ->where('id',$peternakan_id)
                    // ->update([
                    //     'lembar_persetujuan' => $path,
                    // ]);
                    session(['idPeternakan' => $peternakan_id]);
                    Alert::success('Detil Farm Succes!');
                    return redirect('/peternakan');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
            }
    }

    //REGISTER ROLES TEKNISI
    //------------REGISTER USER DETIL TEKNISI (form_teknisi_1)
    protected function form_teknisi_1(){
        return view('auth.register-roles.register-teknisi');
    }
    //------------STORE DB REGISTER USER DETIL TEKNISI (form_teknisi_1_up)
    protected function form_teknisi_1_up(request $request)
    {
        $validate = Validator::make($request->all(), [
            'name_instansi' => 'required', 'string', 'max:255',
            'profesi' => 'required', 'string', 'max:255',
            'tanggal' => 'required', 'date',
            'alamat' => 'required','string', 'max:255',
            'nomor_telp' => 'required','number',
        ]);
        if ($validate->fails()) {
            Alert::info('Info', $validate->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $teknisi_id = DB::table('teknisi')->insertGetId([
                        'user_id' => Auth::user()->id,
                        'nama_instansi' => $request->name_instansi,
                        'profesi' => $request->profesi,
                        'tgl_lahir' => $request->tanggal,
                        'alamat' => $request->alamat,
                        'no_hp' => $request->nomor_telp,
                        'created_at' => Carbon::now(),
                    ]);
                    // $uploadedFile = $request->file('file');
                    // $path = $uploadedFile->store('public/files/persetujuan/'.$peternakan_id);

                    // DB::table('teknisi')
                    // ->where('id',$peternakan_id)
                    // ->update([
                    //     'lembar_persetujuan' => $path,
                    // ]);
                    session(['idUserTeknisi' => Auth::user()->id]);
                    Alert::success('Detil Dokter/Teknisi Succes!');
                    return redirect('/teknisi');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
        }
    }

    //REGISTER USER KEMITRAAN
    //------------REGISTER USER DETIL TEKNISI (form_teknisi_1)
    protected function form_kemitraan_1(){
        return view('auth.register-roles.register-kemitraan');
    }
    //------------STORE DB REGISTER USER DETIL TEKNISI (form_teknisi_1_up)
    protected function form_kemitraan_1_up(request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required', 'string', 'max:255',
            'address' => 'required','string', 'max:255',
            'skala' => 'required','string',
            'no_telp' => 'required','number',
        ]);
        if ($validate->fails()) {
            Alert::info('Info', $validate->errors()->first());
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $id_kemitraan = DB::table('mitra')->insertGetId([
                        'user_id' => Auth::user()->id,
                        'nama_instansi' => $request->name,
                        'alamat' => $request->address,
                        'file_notaris' => 'null',
                        'file_identitas' => 'null',
                        'file_pernyataan' => 'null',
                        'skala' => $request->skala,
                        'no_telp' => $request->nomor_telp,
                        'created_at' => Carbon::now(),
                    ]);
                    // $uploadedFile = $request->file('file');
                    // $path = $uploadedFile->store('public/files/persetujuan/'.$peternakan_id);

                    // DB::table('teknisi')
                    // ->where('id',$peternakan_id)
                    // ->update([
                    //     'lembar_persetujuan' => $path,
                    // ]);
                    session(['idUserKemitraan' => Auth::user()->id]);
                    Alert::success('Detil KUD/Teknisi Succes!');
                    return redirect('kemitraan');
                }catch (\Exception $e) {
                    DB::rollback();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
        }
    }


    //------------REGISTER USER DETIL PETERNAKAN FORM-2
	public function Upload_KemitraanMicro(){
		return view('auth.register-roles.upload.upload-micro');
	}
	public function Upload_KemitraanMacro(){
		return view('auth.register-roles.upload.upload-macro');
	}
}
