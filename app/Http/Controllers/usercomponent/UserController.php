<?php

namespace App\Http\Controllers\usercomponent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Middleware\kemitraan;
use Carbon\Carbon;
use Alert;

use App\Models\Cowcard;
use App\Models\Peternakan;
use App\Models\Death;
use App\Models\Dmi;
use app\Models\HistoryEvent;
use App\Models\HistoryInspect;
use App\Models\Kandang;
use App\Models\MilkProduction;
use App\Models\Mitra;
use App\Models\Penyakit;
use App\Models\Program;
use App\Models\Sold;
use App\Models\StatusBred;
use App\Models\StatusDry;
use App\Models\StatusFresh;
use App\Models\StatusNobred;
use App\Models\StatusOpen;
use App\Models\StatusPreg;
use App\Models\StatusVaksin;
use App\Models\Syncron;
use App\Models\Teknisi;
use App\Models\Tempcowcount;
use App\Models\User;

class UserController extends Controller
{
    public function navbar_1()
    {
        return view('main.components.navbar.header_1');
    }
    public function navbar_2()
    {
        return view('main.components.navbar.header_2');
    }
    public function navbar_3()
    {
        return view('main.components.navbar.header_3');
    }
    /* #region  Index */
    public function User()
    {
        switch (Auth::user()->roles) {
            case "peternakan":
                $peternakan = DB::table('peternakan')
                    ->where('user_id', Auth::user()->id)
                    ->get();
                $id_pet = DB::table('peternakan')
                    ->where('user_id', Auth::user()->id)
                    ->value('id');
                $kandang = DB::table('kandang')
                    ->where('peternakan_id', $id_pet)
                    ->get();
                $view = view('main.user-component.peternakan.users-profile', compact('peternakan', 'kandang', 'id_pet'));
                break;
            case "teknisi":
                $teknisi = DB::table('teknisi')
                    ->where('user_id', Auth::user()->id)
                    ->get();
                $view = view('main.user-component.teknisi.users-profile', compact('teknisi'));
                break;
            case "kemitraan":
                $kemitraan = Mitra::where('user_id', Auth::user()->id)->first();
                $view = view('main.user-component.kemitraan.users-profile', compact('kemitraan'));
                break;
        }
        return $view;
    }
    /* #endregion */

    /* #region  Update */
    public function Update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => ['required', 'text'],
            'email' => ['required', 'email'],
        ]);
        if ($validate->failed()) {
            Alert::error('Error', $validate);
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    User::where('id', Auth::user()->id)
                        ->Update([
                            'name' => $request->name,
                            'email' => $request->email
                        ]);
                    switch (Auth::user()->roles) {
                        case "peternakan":
                            $peternakan = DB::transaction(function () use ($request) {
                                Peternakan::where('user_id', Auth::user()->id)
                                    ->Update([
                                        'nama_peternakan' => $request->nama_peternakan,
                                        'no_hp' => $request->no_hp,
                                        'tahun_berdiri' => $request->tahun_berdiri,
                                        'nama_pemilik' => $request->nama_pemilik
                                    ]);
                                Alert::success('success',  'Bio Berhasil Ter-Update');
                                return redirect('/User-detils');
                            });
                            return $peternakan;
                            break;
                        case "teknisi":
                            $teknisi = DB::transaction(function () use ($request) {
                                Teknisi::where('user_id', Auth::user()->id)
                                    ->Update([
                                        'nama_instansi'=>$request->nama_instansi,
                                        'no_telp'=>$request->no_telp,
                                        'alamat'=>$request->alamat,
                                        'skala'=>$request->skala,
                                    ]);
                                Alert::success('success',  'Bio Berhasil Ter-Update');
                                return redirect('/User-detils');
                            });
                            return $teknisi;
                            break;
                        case "kemitraan":
                            $kemitraan = DB::transaction(function () use ($request) {
                                Mitra::where('user_id', Auth::user()->id)
                                    ->update([
                                        'nama_instansi'=>$request->nama_instansi,
                                        'no_telp'=>$request->no_telp,
                                        'alamat'=>$request->alamat,
                                        'skala'=>$request->skala,
                                    ]);
                                Alert::success('success',  'Bio Berhasil Ter-Update');
                                return redirect('/User-detils');
                            });
                            return $kemitraan;
                            break;
                    }
                } catch (\Exception $e) {
                    DB::rollBack();
                    Alert::error('Error', $e->getMessage());
                    return redirect('/User-detils');
                }

            });
            return $post;
        }
    }
    /* #endregion */

    /* #region Change Password */
    public function ChangePassword(Request $request)
    {
        $Check = $request->newPassword;
        $ReCheck = $request->renewPassword;
        $OldCheck = $request->oldPassword;
        if ($Check == $ReCheck) {
            $hashPassword = DB::table('users')
                ->where('password', Auth::user()->password)
                ->value('password');
            $hashOld = hash::check($OldCheck, $hashPassword);
            if ($hashOld) {
                $hashNew = Hash::make($request->newPassword);
                try {
                    DB::table('users')
                        ->where('id', Auth::user()->id)
                        ->Update([
                            'password' => $hashNew
                        ]);
                    Alert::success('success',  'Password telah di ubah');
                } catch (\Exception $e) {
                    Alert::error('Error', $e->getMessage());
                    DB::rollBack();
                }
            } else {
                Alert::warning('warning',  'Password lama salah');
            }
        } else {
            Alert::warning('warning',  'Password baru tidak sinkron');
        }
        return redirect('/User-detils');
    }
    /* #endregion */

    /* #region  Tambah Kandang */
    public function TambahKandang(Request $request)
    {
        $post = DB::transaction(function () use ($request) {
            try {
                DB::table('kandang')->insert([
                    'peternakan_id' => $request->id_pet,
                    'nama_kandang' => $request->namaKandang,
                    'created_at' => Carbon::now('+07:00')
                ]);
                Alert::success('Success', 'Kandang Berhasil Ditambahkan');
                return redirect('/User-detils');
            } catch (\Exception $error) {
                DB::rollback();
                Alert::error('Error',  $error->getMessage());
                return redirect()->back();
            }
        });
        return $post;
    }
    /* #endregion */

    public function Approve_Kemitraan($id)
    {

    }
}
