<?php

namespace App\Http\Controllers\peternakan;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Cowcard;
use App\Models\Peternakan;
use App\Models\Death;
use App\Models\Dmi;
use app\Models\HistoryEvent;
use App\Models\HistoryInspect;
use App\Models\Kandang;
use App\Models\MilkProduction;
use App\Models\Mitra;
use App\Models\Penyakit;
use App\Models\Program;
use App\Models\Sold;
use App\Models\StatusBred;
use App\Models\StatusDry;
use App\Models\StatusFresh;
use App\Models\StatusNobred;
use App\Models\StatusOpen;
use App\Models\StatusPreg;
use App\Models\StatusVaksin;
use App\Models\Syncron;
use App\Models\Teknisi;
use App\Models\Tempcowcount;
use App\Models\MitraPeternakanInvited;

use Alert;
use App\Exports\ProgramExport;
use App\Models\MitraPeternakan as ModelsMitraPeternakan;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use MitraPeternakan;

class PeternakanController extends Controller
{

    public function index(Request $request)
    {
        if (Session('idPeternakan') != null) {
            DIM();
            DIH();
            token_all_generate();
            $idPeternakan = Session('idPeternakan');
            //KESEHATAN
            $cow = cow_card(Session('idPeternakan'));
            $activity = activity(Session('idPeternakan'));
            $pen = DB::table('kandang as a')
                ->select('a.id', 'a.nama_kandang', 'b.PEN')
                ->leftJoin('cowcard as b', 'b.kandang_id', 'a.id')
                ->where('a.peternakan_id', $idPeternakan)
                ->distinct()
                ->orderBy('a.id', 'ASC')
                ->get();
            return view('user.peternakan.Dashboard', compact('activity', 'pen'));
        } else {
            Alert::warning('warning',  'Anda Bukan Peternakan!');
            return redirect()->back();
        }
    }
    public function dashboard()
    {
        $idPeternakan = Session('idPeternakan');
        $sudahVaksin = DB::table('cowcard as a')
            ->select('a.status_vaksin')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.status_vaksin', "Sudah Vaksin")
            ->where('c.id', $idPeternakan)
            ->count();
        $belumVaksin = DB::table('cowcard as a')
            ->select('a.status_vaksin')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.status_vaksin', "Belum Vaksin")
            ->where('c.id', $idPeternakan)
            ->count();
        $sakit = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.sakit', "Sakit")
            ->where('c.id', $idPeternakan)
            ->count();
        $sehat = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.sakit', "Sehat")
            ->where('c.id', $idPeternakan)
            ->count();
        $jantan = DB::table('cowcard as a')
            ->select('a.SEX')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.SEX', "Jantan")
            ->where('c.id', $idPeternakan)
            ->count();
        $betina = DB::table('cowcard as a')
            ->select('a.SEX')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.SEX', "Betina")
            ->where('c.id', $idPeternakan)
            ->count();
        //STATUS
        $fresh = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Fresh")
            ->where('c.id', $idPeternakan)
            ->count();
        $open = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Open")
            ->where('c.id', $idPeternakan)
            ->count();
        $preg = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Preg")
            ->where('c.id', $idPeternakan)
            ->count();
        $bred = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Bred")
            ->where('c.id', $idPeternakan)
            ->count();
        $nobred = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "No Bred")
            ->where('c.id', $idPeternakan)
            ->count();
        $dry = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Dry")
            ->where('c.id', $idPeternakan)
            ->count();
        $death = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Death")
            ->where('c.id', $idPeternakan)
            ->count();
        $sold = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Sold")
            ->where('c.id', $idPeternakan)
            ->count();
        $belumada = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Belum Ada Status")
            ->where('c.id', $idPeternakan)
            ->count();
        $ADM = array();
        $production = array();
        $tgl_produksi = array();
        $produksi = DB::table('milk_production')->select('ADM', 'production', 'created_at')->where('peternakan_id', $idPeternakan)->orderBy('created_at', 'desc')->limit(30)->get();
        foreach ($produksi as $p) {
            array_push($ADM, $p->ADM);
            array_push($production, $p->production);
            array_push($tgl_produksi, Carbon::parse($p->created_at)->format('d/m'));
        };
        return response()->JSON([
            'sehat' => $sehat, 'sakit' => $sakit, 'sudahVaksin' => $sudahVaksin,
            'belumVaksin' => $belumVaksin, 'jantan' => $jantan, 'betina' => $betina, 'fresh' => $fresh, 'open' => $open, 'preg' => $preg, 'bred' => $bred,
            'nobred' => $nobred, 'dry' => $dry, 'death' => $death, 'sold' => $sold, 'belumada' => $belumada,
            'ADM' => $ADM, 'production' => $production, 'tgl_produksi' => $tgl_produksi
        ]);
    }
    public function cow_card(Request $request)
    {
        if (Session('idPeternakan') != null) {
            DIM();
            token_all_generate();
            $cow = cow_card(Session('idPeternakan'));
            $activity = activity(Session('idPeternakan'));
            $kandangs = DB::table('kandang')
                ->where('peternakan_id', Session('idPeternakan'))
                ->get();
            $pens = DB::table('cowcard as a')
                ->select('a.PEN')
                ->leftJoin('kandang as b', 'b.id', 'a.kandang_id')
                ->leftJoin('peternakan as c', 'c.id', 'b.peternakan_id')
                ->where('b.peternakan_id', Session('idPeternakan'))
                ->distinct()
                ->get();
            return view('user.peternakan.Cow_Card', compact('cow', 'activity', 'kandangs', 'pens'));
        } else {
            Alert::warning('warning',  'Anda Bukan Peternakan!');
            return redirect()->back();
        }
    }
    // redirect ke view registrasi cowcard
    public function redirecttoregister()
    {
        $CowCount = DB::table('cowcard as a')
            ->select('a.PEN')
            ->leftJoin('kandang as b', 'b.id', 'a.kandang_id')
            ->leftJoin('peternakan as c', 'c.id', 'b.peternakan_id')
            ->where('b.peternakan_id', Session('idPeternakan'))
            ->distinct()
            ->count();
        $limit = DB::table('peternakan')
            ->where('id', Session('idPeternakan'))
            ->value('limit');
        if ($CowCount < $limit) {
            $kandang = DB::table('kandang')->where('peternakan_id', Session('idPeternakan'))->get();
            $IndukSapi = DB::table('kandang')
                ->leftJoin('cowcard', 'kandang.id', '=', 'cowcard.kandang_id')
                ->select('cowcard.id', 'cowcard.kode_cowcard')
                ->where('peternakan_id', Session('idPeternakan'))->get();
            return view('user.peternakan.RegSapi', compact('kandang', 'IndukSapi'));
        } else {
            Alert::Info('Maksimal Cow Card Anda ' . $limit . '!', 'Hubungi Contact Center untuk menambah Cow Card!');
            return redirect()->back();
        }
    }
     // menambahkan cowcard baru
     public function signcow(Request $request)
     {
         $messsages = array(
             'id_sapi.required' => 'ID Sapi Kosong',
             'kandang.required' => 'Harap Isi Kandang Sapi!',
             'pen.required' => 'Harap Isi PEN!',
             'tgl_lhr.required' => 'Harap Isi Tanggal Lahir Sapi!',
             'calf.required' => 'Harap Isi CALF!',
             'jenis_kelamin.required' => 'Harap Isi Jenis Kelamin Sapi!',
             'LACT.required' => 'Harap Isi LACT!',
             'bb.required' => 'Harap Isi Berat Bdan Sapi!',
         );
         $validator = Validator::make($request->all(), [
             'id_sapi' => 'required',
             'kandang' => 'required', 'string', 'max:255',
             'pen' => 'required', 'string',
             'tgl_lhr' => 'required',
             'calf' => 'required',
             'jenis_kelamin' => 'required',
             'LACT' => 'required',
             'bb' => 'required'
         ], $messsages);
         if ($validator->fails()) {
             Alert::error('Error', $validator->errors()->first());
             return redirect()->back();
         } else {

             //Cek Cow
             $CowCount = DB::table('cowcard as a')
                 ->select('a.PEN')
                 ->leftJoin('kandang as b', 'b.id', 'a.kandang_id')
                 ->leftJoin('peternakan as c', 'c.id', 'b.peternakan_id')
                 ->where('b.peternakan_id', Session('idPeternakan'))
                 ->distinct()
                 ->count();
             $limit = DB::table('peternakan')->where('id', Session('idPeternakan'))->value('limit');
             $limit = DB::table('peternakan')
                 ->where('id', Session('idPeternakan'))
                 ->value('limit');
             if ($CowCount < $limit) {
                 //next
                 $post = DB::transaction(function () use ($request) {
                     try {
                         //Check duplicate kode_cowcard
                         $TempCowCardCount = DB::table('cowcard as a')
                             ->select('a.kode_cowcard','a.kandang_id', 'b.nama_kandang', 'b.peternakan_id')
                             ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                             ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                             ->where('c.id', Session('idPeternakan'))
                             ->where('a.kode_cowcard', $request->id_sapi)
                             ->whereNull('a.deleted_at')
                             ->count();

                         if($TempCowCardCount == 0){
                             $idcow = DB::table('cowcard')->insertGetId([
                                 'kode_cowcard' => $request->id_sapi,
                                 'induk_sapi_id' => $request->induk_sapi,
                                 'kandang_id' => $request->kandang,
                                 'PEN' => $request->pen,
                                 'tanggal_lahir' => $request->tgl_lhr,
                                 'CALF' => $request->calf,
                                 'SEX' => $request->jenis_kelamin,
                                 'LACT' => $request->LACT,
                                 'berat' => $request->bb,
                                 'Status' => "Belum Ada Status",
                                 'DIM' => 0,
                                 'sakit' => "Sehat",
                                 'created_at' => Carbon::now('+07:00'),
                             ]);
                             if ($request->session()->has('idUserTeknisi')) {
                                 $input = Session('idUserTeknisi');
                             } else {
                                 $input = Session('idUserPeternakan');
                             };
                             if ($request->jenis_kelamin == 'Jantan') {
                                 $request['id'] = $idcow;
                                 $request['EASE'] = 1;
                                 $request['tgl_fresh'] = Carbon::now();
                                 $request['selectKandang'] = $request->kandang;
                                 $request['PEN'] = $request->pen;
                                 setstatusfresh($request);
                                 DB::table('cowcard')->where('id', $idcow)->update([
                                     'Status' => "Bull",
                                     'created_at' => Carbon::now('+07:00')
                                 ]);
                                 $notifikasi_id = DB::table('history_event')->insertGetId([
                                     'kode_cowcard' => $request->id_sapi,
                                     'activity' => 'Add Cow',
                                     'ket_1' => 'Bull',
                                     'peternakan_id' => Session('idPeternakan'),
                                     'input_by' => $input,
                                     'created_at' => Carbon::now('+07:00')
                                 ]);
                             } else {
                                 $notifikasi_id = DB::table('history_event')->insertGetId([
                                     'kode_cowcard' => $request->id_sapi,
                                     'activity' => 'Add Cow',
                                     'peternakan_id' => Session('idPeternakan'),
                                     'input_by' => $input,
                                     'created_at' => Carbon::now('+07:00')
                                 ]);
                             }
                             notifible(Session('idPeternakan'), 2, $notifikasi_id);
                             Alert::success('Success', 'Cow Card Berhasil Di Input dan Terdaftar dengan Kode Sapi ' . $request->id_sapi . '!!');
                             return redirect('/peternakan/cowcard');
                         }else{
                            Alert::warning('Failed', 'Kode Cowcard '.$request->id_sapi.' sudah Terdaftar! buat Kode Cowcard yang lain. ');
                            return redirect('/peternakan/regcowcard');
                         }
                     } catch (\Exception $error) {
                         DB::rollback();
                         Alert::error('Error', $error->getMessage());
                         return redirect()->back();
                     }
                 });
                 return $post;
             } else {
                 Alert::Info('Maksimal Cow Card Anda ' . $limit . '!', 'Hubungi Contact Center untuk menambah Cow Card!');
                 return redirect()->back();
             }
         }
     }
    // Set Status
    public function set_bull(Request $request)
    {
        try {
            $request['EASE'] = 1;
            setstatusfresh($request);
            DB::table('cowcard')->where('id', $request->id)->update([
                'Status' => "Bull",
                'created_at' => Carbon::now('+07:00')
            ]);
            if ($request->session()->has('idUserTeknisi')) {
                $input = Session('idUserTeknisi');
            } else {
                $input = Session('idUserPeternakan');
            };
            $notifikasi_id = DB::table('history_event')->insertGetId([
                'kode_cowcard' => $request->kode,
                'activity' => 'Bull',
                'peternakan_id' => Session('idPeternakan'),
                'input_by' => $input,
                'created_at' => Carbon::now('+07:00')
            ]);
            notifible(Session('idPeternakan'), 2, $notifikasi_id);
            Alert::success('Success', 'Status Berhasil Ter-Update!!');
            return redirect()->back();
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function set_bred(Request $request)
    {
        try {
            setstatusbred($request);
            Alert::success('Success', 'Status Berhasil Ter-Update!!');
            return redirect()->back();
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function set_dry(Request $request)
    {
        try {
            setstatusdry($request);
            Alert::success('Success', 'Status Berhasil Ter-Update!!');
            return redirect()->back();
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function set_fresh(Request $request)
    {
        try {
            setstatusfresh($request);
            Alert::success('Success', 'Status Berhasil Ter-Update!!');
            return redirect()->back();
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function set_nobred(Request $request)
    {
        try {
            setstatusnobred($request);
            Alert::success('Success', 'Status Berhasil Ter-Update!!');
            return redirect()->back();
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function set_open(Request $request)
    {
        try {
            setstatusopen($request);
            Alert::success('Success', 'Status Berhasil Ter-Update!!');
            return redirect()->back();
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function set_preg(Request $request)
    {
        try {
            setstatuspreg($request);
            Alert::success('Success', 'Status Berhasil Ter-Update!!');
            return redirect()->back();
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function ProgramResult($id)
    {
        $shares = programresult($id);
        $data = DB::table('program')->where('id', $id)->get();
        $sync = DB::table('program')->where('id', $id)->value('previous_program');
        foreach ($data as $q) {
            $StringProgram1 = ($q->program_1 != null ? $q->program_1 . " " : "");
            $StringProgram2 = ($q->program_2 != null ? "& " . $q->program_2 . " " : "");
            $StringProgram3 = ($q->program_3 != null ? "& " . $q->program_3 . " " : "");
            $StringProgram4 = ($q->program_4 != null ? "& " . $q->program_4 . " " : "");
            $StringProgram5 = ($q->program_5 != null ? "& " . $q->program_5 . " " : "");
            $StringProgram6 = ($q->program_6 != null ? "& " . $q->program_6 . " " : "");
            $StringProgram7 = ($q->program_7 != null ? "& " . $q->program_7 . " " : "");
        }
        $StringProgram = "$StringProgram1$StringProgram2$StringProgram3$StringProgram4$StringProgram5$StringProgram6$StringProgram7";
        if ($sync == null) {
            return view('user.peternakan.View_Program', compact('id', 'StringProgram', 'shares'));
        } else {
            $curprog = $id;
            $newshares=[];
            $temp = 0;
            foreach ($shares as $tosync) {
                $sapi_id = $tosync->id;
                $temp = DB::table('syncron')->where('sapi_id', '=', $sapi_id)->orderBy('created_at', 'desc')->first();
                if ($temp == null) {
                    DB::table('syncron')->insert([
                        'sapi_id' => $sapi_id,
                        'curent_prog' => $curprog,
                        'created_at' => Carbon::now()
                    ]);
                } else {
                    $tempid = DB::table('syncron')->where('sapi_id', '=', $sapi_id)->where('curent_prog', '=', $curprog)->orderBy('created_at', 'desc')->first();
                    $id_sapi_available = ($tempid==null) ? null : $tempid->sapi_id;
                    $prog_available = ($tempid==null) ? null : $tempid->curent_prog;
                    if ($id_sapi_available == $sapi_id && $prog_available == $curprog) {
                        $newshares[$temp]=$tosync;
                    } else {
                    }
                }
                $temp++;
            }
            $shares=$newshares;
            return view('user.peternakan.sync.View_Program_sync', compact('id', 'StringProgram', 'shares'));
        }
    }
    // View Status Details
    public function view_status_details($id)
    {
        $cow = DB::table('cowcard')->where('id', $id)->get();
        $kodesapi = DB::table('cowcard')->where('id', $id)->value('kode_cowcard');
        $fresh = DB::table('status_fresh as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();
        $bred = DB::table('status_bred as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();
        $dry = DB::table('status_dry as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();
        $nobred = DB::table('status_nobred as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();
        $open = DB::table('status_open as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();
        $preg = DB::table('status_preg as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();

        $DCC = DB::table('status_preg as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->limit(1)->value('DCC');
        $DSLH = DB::table('status_bred as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->limit(1)->value('DSLH');
        $DUDRY = DB::table('status_dry as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->limit(1)->value('DUDRY');
        $TBRD = DB::table('status_bred as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->limit(1)->value('TBRD');
        $DDAT = DB::table('status_bred as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->limit(1)->value('tanggal');
        $activity = DB::table('history_event as a')
            ->where('kode_cowcard', $kodesapi)
            ->orderBy('a.id', 'DESC')
            ->get();
        return view('user.peternakan.Status_Details', compact('cow', 'fresh', 'bred', 'dry', 'nobred', 'open', 'preg', 'DCC', 'DSLH', 'DUDRY', 'TBRD', 'DDAT', 'activity', 'kodesapi'));
    }
    // View Status
    public function view_selection($id)
    {
        $cowcard = DB::table('cowcard')->where('id', $id)->value('Status');
        if ($cowcard == "Bred") {
            return redirect(route("view-status-bred", $id));
        } elseif ($cowcard == "Dry") {
            return redirect(route("view-status-dry", $id));
        } elseif ($cowcard == "Fresh") {
            return redirect(route("view-status-fresh", $id));
        } elseif ($cowcard == "No Bred") {
            return redirect(route("view-status-nobred", $id));
        } elseif ($cowcard == "Open") {
            return redirect(route("view-status-open", $id));
        } elseif ($cowcard == "Preg") {
            return redirect(route("view-status-preg", $id));
        } else {
            return redirect('/peternakan');
        }
    }
    public function view_bred($id)
    {
        $cow = DB::table('cowcard as a')->where('a.id', $id)
            ->select('a.kandang_id', 'a.PEN', 'a.tanggal_lahir', 'a.SEX', 'a.LACT', 'a.Status', 'a.DIM', 'b.sapi_id', 'b.tanggal', 'b.CDAT', 'b.TBRD', 'b.created_at')
            ->leftJoin('status_bred as b', 'b.sapi_id', 'a.id')
            ->orderBy('b.created_at', 'desc')
            ->limit(1)->get();
        return view('user.peternakan.Status_Bred', compact('cow'));
    }

    public function view_dry($id)
    {
        $cow = DB::table('cowcard as a')
            ->where('a.id', $id)
            ->select('a.kandang_id', 'a.PEN', 'a.tanggal_lahir', 'a.SEX', 'a.LACT', 'a.Status', 'a.DIM', 'b.sapi_id', 'b.tanggal', 'b.DUDRY', 'b.DDRY', 'b.created_at')
            ->leftJoin('status_dry as b', 'b.sapi_id', 'a.id')
            ->orderBy('b.created_at', 'desc')
            ->limit(1)
            ->get();
        return view('user.peternakan.Status_Dry', compact('cow'));
    }
    public function view_fresh($id)
    {
        $cow = DB::table('cowcard as a')->where('a.id', $id)
            ->select('a.kandang_id', 'a.PEN', 'a.tanggal_lahir', 'a.SEX', 'a.LACT', 'a.Status', 'a.DIM', 'b.sapi_id', 'b.tanggal', 'b.EASE', 'b.created_at')
            ->leftJoin('status_fresh as b', 'b.sapi_id', 'a.id')
            ->orderBy('b.created_at', 'desc')
            ->limit(1)->get();
        return view('user.peternakan.Status_Fresh', compact('cow'));
    }
    public function view_nobred($id)
    {
        $cow = DB::table('cowcard as a')->where('a.id', $id)
            ->select('a.kandang_id', 'a.PEN', 'a.tanggal_lahir', 'a.SEX', 'a.LACT', 'a.Status', 'a.DIM', 'b.sapi_id', 'b.tanggal', 'b.created_at')
            ->leftJoin('status_nobred as b', 'b.sapi_id', 'a.id')
            ->orderBy('b.created_at', 'desc')
            ->limit(1)->get();
        return view('user.peternakan.Status_No_Bred', compact('cow'));
    }
    public function view_open($id)
    {
        $cow = DB::table('cowcard as a')->where('a.id', $id)
            ->select('a.kandang_id', 'a.PEN', 'a.tanggal_lahir', 'a.SEX', 'a.LACT', 'a.Status', 'a.DIM', 'b.sapi_id', 'b.tanggal', 'b.DOPN', 'b.ABDAT', 'b.created_at')
            ->leftJoin('status_open as b', 'b.sapi_id', 'a.id')
            ->orderBy('b.created_at', 'desc')
            ->limit(1)->get();
        return view('user.peternakan.Status_Open', compact('cow'));
    }
    public function view_preg($id)
    {
        if (Session('idPeternakan') != null) {
            $cow = DB::table('cowcard as a')->where('a.id', $id)
                ->select('a.kandang_id', 'a.PEN', 'a.tanggal_lahir', 'a.SEX', 'a.LACT', 'a.Status', 'a.DIM', 'b.sapi_id', 'b.tanggal', 'b.DCC', 'b.DCCD', 'b.HDAT', 'b.DSLH', 'b.SYNC', 'b.SDESC', 'b.created_at')
                ->leftJoin('status_preg as b', 'b.sapi_id', 'a.id')
                ->orderBy('b.created_at', 'desc')
                ->limit(1)->get();
            return view('user.peternakan.Status_Preg', compact('cow'));
        } else {
            Alert::warning('warning',  'Anda Bukan Peternakan!');
            return redirect()->back();
        }
    }

    //PROGRAM
    public function gotoprogram()
    {
        if (Session('idPeternakan') != null) {
            $idpeternakan = Session('idPeternakan');
            $program = DB::table('program')
                ->where('peternakan_id', $idpeternakan)
                ->where('deleted_at', null)
                ->get();
            return view('user.peternakan.Program', compact('idpeternakan', 'program'));
        } else {
            Alert::warning('warning',  'Anda Bukan Peternakan!');
            return redirect()->back();
        }
    }
    public function set_program($id)
    {
        $id = Session('idPeternakan');
        $data = DB::table('program')
            ->where('peternakan_id', $id)
            ->get();
        $pen = DB::table('cowcard as a')
            ->select('a.PEN')
            ->leftJoin('kandang as b', 'b.id', 'a.kandang_id')
            ->leftJoin('peternakan as c', 'c.id', 'b.peternakan_id')
            ->where('b.peternakan_id', $id)
            ->distinct()
            ->get();
        $status = collect(["Belum Ada Status", "Fresh", "Open", "Preg", "Bred", "No Bred", "Dry", "Death", "Sold"]);
        return view('user.peternakan.Make_Program', compact('id', 'data', 'pen', 'status'));
    }
    public function insert_program(Request $request)
    {
        $post = DB::transaction(function () use ($request) {
            try {
                $pgm1 = null;
                $pgm2 = null;
                $pgm3 = null;
                $pgm4 = null;
                $pgm5 = null;
                $pgm6 = null;
                $pgm7 = null;

                //FORM 1
                if ($request->nilai_1i == null && $request->nilai_1ii == null) { //get - 1
                    if ($request->variabel_1 != null && $request->operator_1 != null && $request->nilai_1 != null) {
                        $pgm1 = $request->variabel_1 . " " . $request->operator_1 . " " . $request->nilai_1;
                    }
                } else if ($request->nilai_1 == null && $request->nilai_1ii == null) { //get - 2
                    if ($request->variabel_1 != null && $request->operator_1 != null && $request->nilai_1i != null) {
                        $pgm1 = $request->variabel_1 . " " . $request->operator_1 . " " . $request->nilai_1i;
                    }
                } else if ($request->nilai_1i == null && $request->nilai_1 == null) { //get - 3
                    if ($request->variabel_1 != null && $request->operator_1 != null && $request->nilai_1ii != null) {
                        $pgm1 = $request->variabel_1 . " " . $request->operator_1 . " " . $request->nilai_1ii;
                    }
                }

                //FORM 2
                if ($request->nilai_2i == null && $request->nilai_2ii == null) { //get - 1
                    if ($request->variabel_2 != null && $request->operator_2 != null && $request->nilai_2 != null) {
                        if ($pgm1 == null) {
                            $pgm1 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2;
                        } else {
                            $pgm2 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2;
                        }
                    }
                } else if ($request->nilai_2 == null && $request->nilai_2ii == null) { //get - 2
                    if ($request->variabel_2 != null && $request->operator_2 != null && $request->nilai_2i != null) {
                        if ($pgm1 == null) {
                            $pgm1 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2i;
                        } else {
                            $pgm2 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2i;
                        }
                    }
                } else if ($request->nilai_2 == null && $request->nilai_2i == null) { //get - 3
                    if ($request->variabel_2 != null && $request->operator_2 != null && $request->nilai_2ii != null) {
                        if ($pgm1 == null) {
                            $pgm1 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2ii;
                        } else {
                            $pgm2 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2ii;
                        }
                    }
                }

                //FORM 3
                if ($request->nilai_3i == null && $request->nilai_3ii == null) { //get - 1
                    if ($request->variabel_3 != null && $request->operator_3 != null && $request->nilai_3 != null) {
                        if ($pgm2 == null) {
                            $pgm2 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3;
                        } else {
                            $pgm3 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3;
                        }
                    }
                } else if ($request->nilai_3 == null && $request->nilai_3ii == null) { //get - 2
                    if ($request->variabel_3 != null && $request->operator_3 != null && $request->nilai_3i != null) {
                        if ($pgm2 == null) {
                            $pgm2 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3i;
                        } else {
                            $pgm3 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3i;
                        }
                    }
                } else if ($request->nilai_3 == null && $request->nilai_3i == null) { //get - 3
                    if ($request->variabel_3 != null && $request->operator_3 != null && $request->nilai_3ii != null) {
                        if ($pgm2 == null) {
                            $pgm2 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3ii;
                        } else {
                            $pgm3 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3ii;
                        }
                    }
                }

                //FORM 4
                if ($request->nilai_4i == null && $request->nilai_4ii == null) { //get - 1
                    if ($request->variabel_4 != null && $request->operator_4 != null && $request->nilai_4 != null) {
                        if ($pgm3 == null) {
                            $pgm3 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4;
                        } else {
                            $pgm4 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4;
                        }
                    }
                } else if ($request->nilai_4 == null && $request->nilai_4ii == null) { //get - 2
                    if ($request->variabel_4 != null && $request->operator_4 != null && $request->nilai_4i != null) {
                        if ($pgm3 == null) {
                            $pgm3 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4i;
                        } else {
                            $pgm4 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4i;
                        }
                    }
                } else if ($request->nilai_4 == null && $request->nilai_4i == null) { //get - 3
                    if ($request->variabel_4 != null && $request->operator_4 != null && $request->nilai_4ii != null) {
                        if ($pgm3 == null) {
                            $pgm3 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4ii;
                        } else {
                            $pgm4 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4ii;
                        }
                    }
                }

                //FORM 5
                if ($request->nilai_5i == null && $request->nilai_5ii == null) { //get - 1
                    if ($request->variabel_5 != null && $request->operator_5 != null && $request->nilai_5 != null) {
                        if ($pgm4 == null) {
                            $pgm4 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5;
                        } else {
                            $pgm5 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5;
                        }
                    }
                } else if ($request->nilai_5 == null && $request->nilai_5ii == null) { //get - 2
                    if ($request->variabel_5 != null && $request->operator_5 != null && $request->nilai_5i != null) {
                        if ($pgm4 == null) {
                            $pgm4 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5i;
                        } else {
                            $pgm5 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5i;
                        }
                    }
                } else if ($request->nilai_5 == null && $request->nilai_5i == null) { //get - 3
                    if ($request->variabel_5 != null && $request->operator_5 != null && $request->nilai_5ii != null) {
                        if ($pgm4 == null) {
                            $pgm4 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5ii;
                        } else {
                            $pgm5 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5ii;
                        }
                    }
                }

                if ($request->variabel_6 != null && $request->operator_6 != null && $request->nilai_6 != null) {
                    $pgm6 = $request->variabel_6 . " " . $request->operator_6 . " " . $request->nilai_6;
                }
                if ($request->variabel_7 != null && $request->operator_7 != null && $request->nilai_7 != null) {
                    if ($pgm6 == null) {
                        $pgm6 = $request->variabel_7 . " " . $request->operator_7 . " " . $request->nilai_7;
                    } else {
                        $pgm7 = $request->variabel_7 . " " . $request->operator_7 . " " . $request->nilai_7;
                    }
                }

                DB::table('program')->insert([
                    'peternakan_id' => Session('idPeternakan'),
                    'namaprogram' => $request->nprog,
                    'program_1' => $pgm1,
                    'program_2' => $pgm2,
                    'program_3' => $pgm3,
                    'program_4' => $pgm4,
                    'program_5' => $pgm5,
                    'program_6' => $pgm6,
                    'program_7' => $pgm7,
                    'created_at' => Carbon::now('+07:00')
                ]);
                if ($request->session()->has('idUserTeknisi')) {
                    $input = Session('idUserTeknisi');
                } else {
                    $input = Session('idUserPeternakan');
                };
                DB::table('history_event')->insert([
                    'kode_cowcard' => '-',
                    'activity' => 'Add Program',
                    'ket_1' => "Add Program name " . $request->nprog,
                    'input_by' => $input,
                    'peternakan_id' => Session('idPeternakan'),
                    'created_at' => Carbon::now('+07:00')
                ]);
                Alert::success('Success', 'Program Berhasil Di Input!!');
                return redirect('/peternakan/program');
            } catch (\Exception $error) {
                DB::rollback();
                Alert::error('Error',  $error->getMessage());
                return redirect()->back();
            }
        });
        return $post;
    }
    public function delete_program($id)
    {
        DB::table('program')->where('id', $id)
            ->update(['deleted_at' => Carbon::now()]);
        Alert::success('Success', 'Program Berhasil Di Hapus!!');
        return redirect()->back();
    }
    public function edit_program($id)
    {
        $data = DB::table('program')->where('id', $id)->first();
        $nilai1 = ExplodeNilai(DB::table('program')->where('id', $id)->value('program_1'));
        $nilai2 = ExplodeNilai(DB::table('program')->where('id', $id)->value('program_2'));
        $nilai3 = ExplodeNilai(DB::table('program')->where('id', $id)->value('program_3'));
        $nilai4 = ExplodeNilai(DB::table('program')->where('id', $id)->value('program_4'));
        $nilai5 = ExplodeNilai(DB::table('program')->where('id', $id)->value('program_5'));
        $nilai6 = ExplodeNilai(DB::table('program')->where('id', $id)->value('program_6'));
        $nilai7 = ExplodeNilai(DB::table('program')->where('id', $id)->value('program_7'));

        $pen = DB::table('cowcard as a')
            ->select('a.PEN')
            ->leftJoin('kandang as b', 'b.id', 'a.kandang_id')
            ->leftJoin('peternakan as c', 'c.id', 'b.peternakan_id')
            ->where('b.peternakan_id', $id)
            ->distinct()
            ->get();
        $status = collect(["Belum Ada Status", "Fresh", "Open", "Preg", "Bred", "No Bred", "Dry", "Death", "Sold"]);

        return view('user.peternakan.Edit_Program', compact('data', 'pen', 'status', 'nilai1', 'nilai2', 'nilai3', 'nilai4', 'nilai5', 'nilai6', 'nilai7'));
    }
    public function insert_edited_program(Request $request)
    {
        $post = DB::transaction(function () use ($request) {
            try {
                $pgm1 = null;
                $pgm2 = null;
                $pgm3 = null;
                $pgm4 = null;
                $pgm5 = null;
                $pgm6 = null;
                $pgm7 = null;

                //FORM 1
                if ($request->nilai_1i == null && $request->nilai_1ii == null) { //get - 1
                    if ($request->variabel_1 != null && $request->operator_1 != null && $request->nilai_1 != null) {
                        $pgm1 = $request->variabel_1 . " " . $request->operator_1 . " " . $request->nilai_1;
                    }
                } else if ($request->nilai_1 == null && $request->nilai_1ii == null) { //get - 2
                    if ($request->variabel_1 != null && $request->operator_1 != null && $request->nilai_1i != null) {
                        $pgm1 = $request->variabel_1 . " " . $request->operator_1 . " " . $request->nilai_1i;
                    }
                } else if ($request->nilai_1i == null && $request->nilai_1 == null) { //get - 3
                    if ($request->variabel_1 != null && $request->operator_1 != null && $request->nilai_1ii != null) {
                        $pgm1 = $request->variabel_1 . " " . $request->operator_1 . " " . $request->nilai_1ii;
                    }
                }

                //FORM 2
                if ($request->nilai_2i == null && $request->nilai_2ii == null) { //get - 1
                    if ($request->variabel_2 != null && $request->operator_2 != null && $request->nilai_2 != null) {
                        if ($pgm1 == null) {
                            $pgm1 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2;
                        } else {
                            $pgm2 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2;
                        }
                    }
                } else if ($request->nilai_2 == null && $request->nilai_2ii == null) { //get - 2
                    if ($request->variabel_2 != null && $request->operator_2 != null && $request->nilai_2i != null) {
                        if ($pgm1 == null) {
                            $pgm1 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2i;
                        } else {
                            $pgm2 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2i;
                        }
                    }
                } else if ($request->nilai_2 == null && $request->nilai_2i == null) { //get - 3
                    if ($request->variabel_2 != null && $request->operator_2 != null && $request->nilai_2ii != null) {
                        if ($pgm1 == null) {
                            $pgm1 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2ii;
                        } else {
                            $pgm2 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2ii;
                        }
                    }
                }

                //FORM 3
                if ($request->nilai_3i == null && $request->nilai_3ii == null) { //get - 1
                    if ($request->variabel_3 != null && $request->operator_3 != null && $request->nilai_3 != null) {
                        if ($pgm2 == null) {
                            $pgm2 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3;
                        } else {
                            $pgm3 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3;
                        }
                    }
                } else if ($request->nilai_3 == null && $request->nilai_3ii == null) { //get - 2
                    if ($request->variabel_3 != null && $request->operator_3 != null && $request->nilai_3i != null) {
                        if ($pgm2 == null) {
                            $pgm2 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3i;
                        } else {
                            $pgm3 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3i;
                        }
                    }
                } else if ($request->nilai_3 == null && $request->nilai_3i == null) { //get - 3
                    if ($request->variabel_3 != null && $request->operator_3 != null && $request->nilai_3ii != null) {
                        if ($pgm2 == null) {
                            $pgm2 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3ii;
                        } else {
                            $pgm3 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3ii;
                        }
                    }
                }

                //FORM 4
                if ($request->nilai_4i == null && $request->nilai_4ii == null) { //get - 1
                    if ($request->variabel_4 != null && $request->operator_4 != null && $request->nilai_4 != null) {
                        if ($pgm3 == null) {
                            $pgm3 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4;
                        } else {
                            $pgm4 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4;
                        }
                    }
                } else if ($request->nilai_4 == null && $request->nilai_4ii == null) { //get - 2
                    if ($request->variabel_4 != null && $request->operator_4 != null && $request->nilai_4i != null) {
                        if ($pgm3 == null) {
                            $pgm3 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4i;
                        } else {
                            $pgm4 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4i;
                        }
                    }
                } else if ($request->nilai_4 == null && $request->nilai_4i == null) { //get - 3
                    if ($request->variabel_4 != null && $request->operator_4 != null && $request->nilai_4ii != null) {
                        if ($pgm3 == null) {
                            $pgm3 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4ii;
                        } else {
                            $pgm4 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4ii;
                        }
                    }
                }

                //FORM 5
                if ($request->nilai_5i == null && $request->nilai_5ii == null) { //get - 1
                    if ($request->variabel_5 != null && $request->operator_5 != null && $request->nilai_5 != null) {
                        if ($pgm4 == null) {
                            $pgm4 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5;
                        } else {
                            $pgm5 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5;
                        }
                    }
                } else if ($request->nilai_5 == null && $request->nilai_5ii == null) { //get - 2
                    if ($request->variabel_5 != null && $request->operator_5 != null && $request->nilai_5i != null) {
                        if ($pgm4 == null) {
                            $pgm4 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5i;
                        } else {
                            $pgm5 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5i;
                        }
                    }
                } else if ($request->nilai_5 == null && $request->nilai_5i == null) { //get - 3
                    if ($request->variabel_5 != null && $request->operator_5 != null && $request->nilai_5ii != null) {
                        if ($pgm4 == null) {
                            $pgm4 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5ii;
                        } else {
                            $pgm5 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5ii;
                        }
                    }
                }

                if ($request->variabel_6 != null && $request->operator_6 != null && $request->nilai_6 != null) {
                    $pgm6 = $request->variabel_6 . " " . $request->operator_6 . " " . $request->nilai_6;
                }
                if ($request->variabel_7 != null && $request->operator_7 != null && $request->nilai_7 != null) {
                    if ($pgm6 == null) {
                        $pgm6 = $request->variabel_7 . " " . $request->operator_7 . " " . $request->nilai_7;
                    } else {
                        $pgm7 = $request->variabel_7 . " " . $request->operator_7 . " " . $request->nilai_7;
                    }
                }
                DB::table('program')->where('id', $request->id)->update([
                    'peternakan_id' => Session('idPeternakan'),
                    'namaprogram' => $request->nprog,
                    'program_1' => $pgm1,
                    'program_2' => $pgm2,
                    'program_3' => $pgm3,
                    'program_4' => $pgm4,
                    'program_5' => $pgm5,
                    'program_6' => $pgm6,
                    'program_7' => $pgm7,
                    'created_at' => Carbon::now('+07:00')
                ]);
                if ($request->session()->has('idUserTeknisi')) {
                    $input = Session('idUserTeknisi');
                } else {
                    $input = Session('idUserPeternakan');
                };
                DB::table('history_event')->insert([
                    'kode_cowcard' => '-',
                    'activity' => 'Edit Program',
                    'ket_1' => "Add Program name " . $request->nprog,
                    'input_by' => $input,
                    'peternakan_id' => Session('idPeternakan'),
                    'created_at' => Carbon::now('+07:00')
                ]);
                Alert::success('Success', 'Program Berhasil Di Ter-Edit!!');
                return redirect('/peternakan/program');
            } catch (\Exception $error) {
                DB::rollback();
                Alert::error('Error',  $error->getMessage());
                return redirect()->back();
            }
        });
        return $post;
    }

    //SOLD DEATH
    public function sold(Request $request)
    {
        $cowcard = DB::table('cowcard')->where('id', $request->id)->get();
        $dim = 0;
        $status = 'null';
        $idsapi = 'null';
        foreach ($cowcard as $p) {
            $dim =  $p->DIM;
            $status = $p->Status;
            $idsapi = $p->kode_cowcard;
        }
        status_update($status, $request->id);
        DB::table('cowcard')->where('id', $request->id)->update([
            'Status' => "Sold",
            'updated_at' => Carbon::now('+07:00')
        ]);

        DB::table('sold')->insert([
            'sapi_id' => $request->id,
            'date' => $request->solddate,
            'price' => $request->soldprice,
            'created_at' => Carbon::now('+07:00')
        ]);
        if ($request->session()->has('idUserTeknisi')) {
            $input = Session('idUserTeknisi');
        } else {
            $input = Session('idUserPeternakan');
        };
        $notifikasi_id = DB::table('history_event')->insertGetId([
            'kode_cowcard' => $idsapi,
            'activity' => 'Sold',
            'ket_1' => $request->solddate,
            'ket_2' => $request->soldprice,
            'DIM' => $dim,
            'input_by' => $input,
            'peternakan_id' => Session('idPeternakan'),
            'created_at' => Carbon::now('+07:00')
        ]);
        notifible(Session('idPeternakan'), 2, $notifikasi_id);
        Alert::success('Success', 'Status Berhasil Ter-Jual!!');
        return redirect()->back();
    }
    public function death(Request $request)
    {
        $cowcard = DB::table('cowcard')->where('id', $request->id)->get();
        $dim = 0;
        $status = 'null';
        $idsapi = 'null';
        foreach ($cowcard as $p) {
            $dim =  $p->DIM;
            $status = $p->Status;
            $idsapi = $p->kode_cowcard;
        }
        status_update($status, $request->id);
        DB::table('cowcard')->where('id', $request->id)->update([
            'Status' => "Death",
            'updated_at' => Carbon::now('+07:00')
        ]);
        DB::table('death')->insert([
            'sapi_id' => $request->id,
            'date' => $request->deathdate,
            'reason' => $request->deathreason,
            'created_at' => Carbon::now('+07:00')
        ]);
        if ($request->session()->has('idUserTeknisi')) {
            $input = Session('idUserTeknisi');
        } else {
            $input = Session('idUserPeternakan');
        };
        $notifikasi_id = DB::table('history_event')->insertGetId([
            'kode_cowcard' => $idsapi,
            'activity' => 'Death',
            'ket_1' => $request->deathdate,
            'ket_2' => $request->deathreason,
            'DIM' => $dim,
            'peternakan_id' => Session('idPeternakan'),
            'input_by' => $input,
            'created_at' => Carbon::now('+07:00')
        ]);
        notifible(Session('idPeternakan'), 2, $notifikasi_id);
        Alert::success('Success', 'Status Berhasil Ter-Update!!');
        return redirect()->back();
    }
    //FUNC SAPI SAKIT
    public function penyakit($idsapi, Request $request)
    {
        $post = DB::transaction(function () use ($idsapi, $request) {
            try {
                $cowcard = DB::table('cowcard')
                    ->where('id', $idsapi)
                    ->get();
                $dim = 0;
                $LACT = 0;
                $string = null;
                $set_kandang = null;
                $penfirst = null;
                $ket_4 = null;
                $ket_5 = null;
                foreach ($cowcard as $p) {
                    $kodecowcard = $p->kode_cowcard;
                    $kandang_id = $p->kandang_id;
                    $PEN = $p->PEN;
                    $dim =  $p->DIM;
                    $LACT =  $p->LACT;
                    $kesehatan = $p->sakit;
                }
                //INPUT
                if ($request->session()->has('idUserTeknisi')) {
                    $input = Session('idUserTeknisi');
                } else {
                    $input = Session('idUserPeternakan');
                };
                //KANDANG
                if ($kandang_id == $request->selectKandang) {
                    $set_kandang = $kandang_id;
                } else {
                    $set_kandang = $request->selectKandang;
                }
                //PEN
                if ($PEN == $request->PEN) {
                    $penfirst = $PEN;
                } else {
                    $penfirst = $request->PEN;
                }
                DB::table('cowcard')->where('id', $idsapi)->update([
                    'sakit' => "Sakit",
                    'kandang_id' => $set_kandang,
                    'PEN' => $penfirst,
                    'DIH' => 0,
                    'updated_at' => Carbon::now('+07:00')
                ]);
                if ($request->has('narray')) {
                    $count = count($request->narray);
                    $str = "";
                    for ($i = 0; $i < $count; $i++) {
                        $str = $str . $request->narray[$i] . " ";
                    }
                    $string = $request->diagnosis . " " . $str;
                    DB::table('penyakit')->insert([
                        'sapi_id' => $request->id,
                        'tglsakit' => $request->tsakit,
                        'gejala' => $request->gejala,
                        'diagnosis' => $request->string,
                        'penanganan' => $request->penanganan,
                        'input_by' => $input,
                        'LACT' => $LACT,
                        'dim_masuk' => $dim,
                        'created_at' => Carbon::now('+07:00')
                    ]);
                } else {
                    $string  =  $request->diagnosis;
                    DB::table('penyakit')->insert([
                        'sapi_id' => $idsapi,
                        'tglsakit' => $request->tsakit,
                        'gejala' => $request->gejala,
                        'diagnosis' => $request->diagnosis,
                        'penanganan' => $request->penanganan,
                        'input_by' => $input,
                        'LACT' => $LACT,
                        'dim_masuk' => $dim,
                        'created_at' => Carbon::now('+07:00')
                    ]);
                }
                if ($kesehatan == "Sehat") {
                    $ket_4 = "Kandang Awal: " . $kandang_id;
                    $ket_5 = "PEN Awal: " . $PEN;
                } else {
                    $ket_4 = null;
                    $ket_5 = null;
                }
                $notifikasi_id = DB::table('history_event')->insertGetId([
                    'kode_cowcard' => $kodecowcard,
                    'activity' => 'Sakit',
                    'ket_1' => "LACT : " . strval($LACT),
                    'ket_2' => "Gejala : " . strval($request->gejala),
                    'ket_3' => "Diagnosis : " . strval($string),
                    'ket_4' => $ket_4,
                    'ket_5' => $ket_5,
                    'DIM' => $dim,
                    'input_by' => $input,
                    'peternakan_id' => Session('idPeternakan'),
                    'created_at' => Carbon::now('+07:00')
                ]);
                notifible(Session('idPeternakan'), 2, $notifikasi_id);
                Alert::success('Success', 'Data Berhasil Di Simpan!!');
                return redirect()->back();
            } catch (\Exception $error) {
                DB::rollback();
                Alert::error('Error',  $error->getMessage());
                return redirect()->back();
            }
        });
        return $post;
    }
    public function vaksin(Request $request)
    {
        try {
            $cowcard = DB::table('cowcard')->where('id', $request->id)->get();
            $dim = 0;
            $LACT = 0;
            $status = 'null';
            $idsapi = 'null';
            foreach ($cowcard as $p) {
                $dim =  $p->DIM;
                $LACT =  $p->LACT;
                $status = $p->Status;
                $idsapi = $p->kode_cowcard;
            }
            // Status sebelumnya tetap aktif hingga diubah status
            // status_update($status, $request->id);
            DB::table('cowcard')->where('id', $request->id)->update([
                'status_vaksin' => 'Sudah Vaksin',
                'updated_at' => Carbon::now('+07:00')
            ]);
            if ($request->session()->has('idUserTeknisi')) {
                $input = Session('idUserTeknisi');
            } else {
                $input = Session('idUserPeternakan');
            };
            if ($request->has('narray')) {
                $count = count($request->narray);
                $str = "";
                for ($i = 0; $i < $count; $i++) {
                    $str = $str . $request->narray[$i] . " ";
                }
                $string = $request->nvaksin . " " . $str;
                DB::table('status_vaksin')->insert([
                    'sapi_id' => $request->id,
                    'tanggal' => $request->tvaksin,
                    'nama_vaksin' => $string,
                    'keterangan' => $request->keterangan,
                    'input_by' => $input,
                    'LACT' => $LACT,
                    'dim_masuk' => $dim,
                    'created_at' => Carbon::now('+07:00')
                ]);
            } else {
                DB::table('status_vaksin')->insert([
                    'sapi_id' => $request->id,
                    'tanggal' => $request->tvaksin,
                    'nama_vaksin' => $request->nvaksin,
                    'keterangan' => $request->keterangan,
                    'input_by' => $input,
                    'LACT' => $LACT,
                    'dim_masuk' => $dim,
                    'created_at' => Carbon::now('+07:00')
                ]);
            }
            if ($request->session()->has('idUserTeknisi')) {
                $input = Session('idUserTeknisi');
            } else {
                $input = Session('idUserPeternakan');
            };
            $notifikasi_id = DB::table('history_event')->insertGetId([
                'kode_cowcard' => $idsapi,
                'activity' => 'Vaksin',
                'ket_1' => "LACT : " . strval($LACT),
                'ket_2' => "Nama Vaksin : " . strval($request->nvaksin),
                'ket_3' => "Keterangan : " . strval($request->keterangan),
                'DIM' => $dim,
                'input_by' => $input,
                'peternakan_id' => Session('idPeternakan'),
                'created_at' => Carbon::now('+07:00')
            ]);
            notifible(Session('idPeternakan'), 2, $notifikasi_id);
            Alert::success('Success', 'Data Berhasil Di Simpan!!');
            return redirect()->back();
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function riwayatpenyakit($id)
    {
        $kodesapi = DB::table('cowcard')->where('id', $id)->value('kode_cowcard');
        $penyakit = DB::table('penyakit')
            ->where('sapi_id', $id)
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('user.peternakan.RiwayatSakitSapi', compact('penyakit', 'kodesapi'));
    }
    public function hospitalize($id)
    {
        $kodesapi = DB::table('cowcard')
            ->where('id', $id)
            ->value('kode_cowcard');
        $PEN = DB::table('cowcard')
            ->where('id', $id)
            ->value('PEN');
        $idkandang = DB::table('cowcard')
            ->where('id', $id)
            ->value('kandang_id');
        $penyakit = DB::table('penyakit')
            ->where('sapi_id', $id)
            ->orderBy('created_at', 'DESC')
            ->get();
        $id_sapi = $id;
        $tgl_sakit = DB::table('penyakit as a')
            ->where('a.sapi_id', $id)
            ->orderBy('a.created_at', 'desc')
            ->limit(1)
            ->value('tglsakit');
        $gejala = DB::table('penyakit as a')->where('a.sapi_id', $id)
            ->orderBy('a.created_at', 'desc')
            ->limit(1)
            ->value('gejala');
        $diagnosis = DB::table('penyakit as a')
            ->where('a.sapi_id', $id)
            ->orderBy('a.created_at', 'desc')
            ->limit(1)
            ->value('diagnosis');
        $penanganan = DB::table('penyakit as a')
            ->where('a.sapi_id', $id)
            ->orderBy('a.created_at', 'desc')
            ->limit(1)
            ->value('penanganan');
        $inputby = DB::table('penyakit as a')
            ->where('a.sapi_id', $id)
            ->orderBy('a.created_at', 'desc')
            ->limit(1)
            ->value('input_by');
        return view('user.peternakan.Hospital', compact('penyakit', 'PEN', 'idkandang', 'kodesapi', 'id_sapi', 'tgl_sakit', 'gejala', 'diagnosis', 'penanganan', 'inputby'));
    }
    public function sembuh(Request $request, $id)
    {
        $post = DB::transaction(function () use ($request, $id) {
            try {
                $cowcard = DB::table('cowcard')
                    ->where('id', $id)
                    ->get();
                $dim = 0;
                $LACT = 0;
                $idsapi = 'null';
                foreach ($cowcard as $p) {
                    $dim =  $p->DIM;
                    $LACT =  $p->LACT;
                    $idsapi = $p->kode_cowcard;
                }
                // Ambil status yang masih aktif
                $status_before = getactivestatus($id);
                // Ambil History Kandang
                $check1 = DB::table('history_event')
                    ->where('kode_cowcard', $idsapi)
                    ->whereNotNull('ket_4')
                    ->orderBy('created_at', 'DESC')
                    ->limit(1)
                    ->value('ket_4');
                // Ambil History PEN
                $check2 = DB::table('history_event')
                    ->where('kode_cowcard', $idsapi)
                    ->whereNotNull('ket_5')
                    ->orderBy('created_at', 'DESC')
                    ->limit(1)
                    ->value('ket_5');
                $sub1 = explode(" ", $check1);
                $sub2 = explode(" ", $check2);
                DB::table('cowcard')->where('id', $id)->update([
                    'kandang_id' => $sub1[2],
                    'PEN' => $sub2[2],
                    'Status' => $status_before,
                    'sakit' => 'Sehat',
                    'updated_at' => Carbon::now('+07:00')
                ]);
                if ($request->session()->has('idUserTeknisi')) {
                    $input = Session('idUserTeknisi');
                } else {
                    $input = Session('idUserPeternakan');
                };
                DB::table('penyakit')->insert([
                    'sapi_id' => $id,
                    'tglsakit' => Carbon::now('+07:00'),
                    'gejala' => 'Sembuh',
                    'diagnosis' => 'Sembuh',
                    'penanganan' => 'Sembuh',
                    'input_by' => $input,
                    'LACT' => $LACT,
                    'dim_masuk' => $dim,
                    'created_at' => Carbon::now('+07:00')
                ]);
                $notifikasi_id = DB::table('history_event')->insertGetId([
                    'kode_cowcard' => $idsapi,
                    'activity' => 'Sembuh',
                    'ket_1' => "LACT : " . strval($LACT),
                    'ket_2' => "Sembuh",
                    'DIM' => $dim,
                    'peternakan_id' => Session('idPeternakan'),
                    'input_by' => $input,
                    'created_at' => Carbon::now('+07:00')
                ]);
                notifible(Session('idPeternakan'), 2, $notifikasi_id);
                Alert::success('Success', 'Sapi Berhasil Sembuh!!');
                return redirect('/peternakan/cowcard');
            } catch (\Exception $error) {
                DB::rollback();
                Alert::error('Error',  $error->getMessage());
                return redirect()->back();
            }
        });
        return $post;
    }
    public function riwayatvaksin($id)
    {
        $kodesapi = DB::table('cowcard')->where('id', $id)->value('kode_cowcard');
        $vaksin = DB::table('status_vaksin')
            ->where('sapi_id', $id)
            ->get();
        return view('user.peternakan.RiwayatVaksin', compact('vaksin', 'kodesapi'));
    }
    public function token_show(Request $request)
    {
        $token = DB::table('peternakan')
            ->where('id', Session('idPeternakan'))
            ->value('token');
        if ($token == null) {
            Alert::error('Error',  'Token Tidak Tersedia! Silahkan Coba Log In Kembali!');
            return redirect()->back();
        } else {
            return view('user.peternakan.token', compact('token'));
        }
    }
    public function token_generate(Request $request)
    {
        try {
            if ($request->session()->has('idUserTeknisi')) {
                $input = Session('idUserTeknisi');
            } else {
                $input = Session('idUserPeternakan');
            };
            token_generate($input, Session('idPeternakan'));
            Alert::success('Success', 'Token Berhasil Di Perbaharui!');
            return redirect()->back();
        } catch (\Exception $error) {
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function registerbatch(Request $request)
    {
        $CowCount = DB::table('cowcard as a')
            ->select('a.PEN')
            ->leftJoin('kandang as b', 'b.id', 'a.kandang_id')
            ->leftJoin('peternakan as c', 'c.id', 'b.peternakan_id')
            ->where('b.peternakan_id', Session('idPeternakan'))
            ->distinct()
            ->count();
        $limit = DB::table('peternakan')->value('limit');
        if ($CowCount < $limit) {
            $temp = DB::table('tempcowcount')->where('peternakan_id', $request->id_Peternakan)->count();
            if ($temp == 0) {
                try {
                    DB::table('tempcowcount')->insert([
                        'peternakan_id' => $request->id_Peternakan,
                        'current' => 0,
                        'max' => $request->max,
                        'Status' => $request->Status,
                        'created_at' => Carbon::now('+07:00')
                    ]);
                    Alert::success('Success', 'Memulai batch data baru');
                    return redirect('/peternakan/batch/second');
                } catch (\Exception $error) {
                    Alert::error('Error',  "Tidak ada informasi batch untuk dijalankan");
                    return redirect('/peternakan');
                }
            } else {
                Alert::success('Success', 'Melanjutkan batch sebelumnya');
                return redirect('/peternakan/batch/second');
            }
        } else {
            Alert::Info('Maksimal Cow Card Anda ' . $limit . '!', 'Hubungi Contact Center untuk menambah Cow Card!');
            return redirect()->back();
        }
    }
    public function startbatch(Request $request)
    {

        $kandang = DB::table('kandang')->where('peternakan_id', Session('idPeternakan'))->get();
        $current = DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->value('current');
        $max = DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->value('max');
        $status = DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->value('Status');
        if ($current < $max) {
            switch ($status) {
                case ('Bred'):
                    return view('user.peternakan.BatchBred', compact('kandang', 'current', 'max'));
                    break;

                case ('Dry'):
                    return view('user.peternakan.BatchDry', compact('kandang'));
                    break;

                case ('Fresh'):
                    return view('user.peternakan.BatchFresh', compact('kandang'));
                    break;

                case ('No Bred'):
                    return view('user.peternakan.BatchNoBred', compact('kandang'));
                    break;

                case ('Open'):
                    return view('user.peternakan.BatchOpen', compact('kandang'));
                    break;

                case ('Preg'):
                    return view('user.peternakan.BatchPreg', compact('kandang'));
                    break;

                default:
                    return redirect()->back();
            }
        } else {
            DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->delete();
            return redirect('/peternakan');
        }
    }
    public function insertbatch(Request $request)
    {
        try {
            $status = DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->value('Status');
            $idcow = DB::table('cowcard')->insertGetId([
                'kode_cowcard' => $request->id_sapi,
                'kandang_id' => $request->kandang,
                'PEN' => $request->pen,
                'tanggal_lahir' => $request->tgl_lhr,
                'CALF' => $request->calf,
                'berat' => $request->bb,
                'SEX' => $request->jenis_kelamin,
                'LACT' => $request->LACT,
                'Status' => "Belum Ada Status",
                'DIM' => 0,
                'created_at' => Carbon::now('+07:00')
            ]);
            if ($request->session()->has('idUserTeknisi')) {
                $input = Session('idUserTeknisi');
            } else {
                $input = Session('idUserPeternakan');
            };
            $notifikasi_id = DB::table('history_event')->insertGetId([
                'kode_cowcard' => $request->id_sapi,
                'activity' => 'Add Cow',
                'input_by' => $input,
                'peternakan_id' => Session('idPeternakan'),
                'created_at' => Carbon::now('+07:00')
            ]);
            notifible(Session('idPeternakan'), 2, $notifikasi_id);
            $request['id'] = $idcow;
            switch ($status) {
                case ('Bred'):
                    setstatusbred($request);
                    DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->increment('current');
                    break;

                case ('Dry'):
                    setstatusdry($request);
                    DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->increment('current');
                    break;

                case ('Fresh'):
                    setstatusfresh($request);
                    DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->increment('current');
                    break;

                case ('No Bred'):
                    setstatusnobred($request);
                    DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->increment('current');
                    break;

                case ('Open'):
                    setstatusopen($request);
                    DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->increment('current');
                    break;

                case ('Preg'):
                    setstatuspreg($request);
                    DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->increment('current');
                    break;

                default:
                    return redirect()->back();
            }
            $current = DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->value('current');
            $max = DB::table('tempcowcount')->where('peternakan_id', session('idPeternakan'))->value('max');
            if ($current != $max) {
                return redirect('/peternakan/batch/second');
            } else {
                return redirect('/peternakan');
            }
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }

    public function ubahberat(Request $request)
    {
        try {
            DB::table('cowcard')->where('id', $request->id)->update([
                'berat' => $request->berat,
                'updated_at' => Carbon::now('+07:00')
            ]);
            if ($request->session()->has('idUserTeknisi')) {
                $input = Session('idUserTeknisi');
            } else {
                $input = Session('idUserPeternakan');
            };
            $notifikasi_id =  DB::table('history_event')->insertGetId([
                'activity' => 'Ubah berat badan',
                'kode_cowcard' => $request->kode,
                'ket_1' => 'BB :' . strval($request->berat),
                'input_by' => $input,
                'peternakan_id' => Session('idPeternakan'),
                'updated_at' => Carbon::now('+07:00')
            ]);
            notifible(Session('idPeternakan'), 2, $notifikasi_id);
            Alert::success('Success', 'Berhasil mengubah berat badan');
            return redirect('/peternakan/cowcard');
        } catch (\Exception $error) {
            Alert::error('Error',  "Gagal mengubah berat badan");
            return redirect('/peternakan');
        }
    }

    public function remark(Request $request)
    {
        try {
            if ($request->session()->has('idUserTeknisi')) {
                $input = Session('idUserTeknisi');
            } else {
                $input = Session('idUserPeternakan');
            };
            $notifikasi_id = DB::table('history_event')->insertGetId([
                'activity' => $request->remarktitle,
                'kode_cowcard' => $request->kode,
                'ket_1' => $request->tremark,
                'ket_2' => $request->keterangan,
                'input_by' => $input,
                'peternakan_id' => Session('idPeternakan'),
                'updated_at' => Carbon::now('+07:00')
            ]);
            notifible(Session('idPeternakan'), 2, $notifikasi_id);
            Alert::success('Success', 'Berhasil menambahkan remark');
            return redirect('/peternakan/cowcard');
        } catch (\Exception $error) {
            Alert::error('Error',  "Gagal menambahkan remark");
            return redirect('/peternakan');
        }
    }

    public function feeder(Request $request)
    {
        try {
            DB::table('cowcard')->where('id', $request->id)->update([
                'pakan' => $request->pakan,
                'DMI' => $request->DMI,
                'updated_at' => Carbon::now('+07:00')
            ]);
            DB::table('dmi')->insert([
                'peternakan_id' => Session('idPeternakan'),
                'id_sapi' => $request->id,
                'pakan' => $request->pakan,
                'DMI' => $request->DMI,
                'created_at' => Carbon::now('+07:00')
            ]);
            if ($request->session()->has('idUserTeknisi')) {
                $input = Session('idUserTeknisi');
            } else {
                $input = Session('idUserPeternakan');
            };
            $notifikasi_id = DB::table('history_event')->insertGetId([
                'activity' => 'Rubah pakan',
                'kode_cowcard' => $request->kode,
                'ket_1' => "Pakan : " . $request->pakan,
                'peternakan_id' => Session('idPeternakan'),
                'input_by' => $input,
                'updated_at' => Carbon::now('+07:00')
            ]);
            notifible(Session('idPeternakan'), 2, $notifikasi_id);
            Alert::success('Success', 'Berhasil mengganti jenis pakan');
            return redirect('/peternakan/cowcard');
        } catch (\Exception $error) {
            Alert::error('Error',  $error->getMessage());
            return redirect('/peternakan');
        }
    }

    public function cek()
    {
        $kandang = DB::table('kandang')->where('peternakan_id', Session('idPeternakan'))->get();
        return view('user.peternakan.BatchBred', compact('kandang'));
    }

    /* #region  Pindah Kandang */
    public function pindahKandang(Request $request)
    {
        try {
            /* #region  Query Update */
            $cowcard = DB::table('cowcard')->where('id', $request->id)->get();
            $dim = 0;
            $LACT = 0;
            $status = 'null';
            $idsapi = 'null';
            foreach ($cowcard as $p) {
                $dim =  $p->DIM;
                $LACT =  $p->LACT;
                $status = $p->Status;
                $idsapi = $p->kode_cowcard;
            }
            status_update($status, $request->id);
            DB::table('cowcard')->where('id', $request->id)->update([
                'DIM' => 0,
                'kandang_id' => $request->selectKandang,
                'PEN' => $request->PEN,
                'updated_at' => Carbon::now('+07:00')
            ]);
            if ($request->session()->has('idUserTeknisi')) {
                $input = Session('idUserTeknisi');
            } else {
                $input = Session('idUserPeternakan');
            };
            $notifikasi_id = DB::table('history_event')->insertGetId([
                'kode_cowcard' => $idsapi,
                'activity' => 'Pindah Kandang',
                'ket_1' => "kandang_id : " . strval($request->selectKandang),
                'ket_2' => "PEN : " . strval($request->PEN),
                'DIM' => $dim,
                'input_by' => $input,
                'peternakan_id' => Session('idPeternakan'),
                'created_at' => Carbon::now('+07:00')
            ]);
            notifible(Session('idPeternakan'), 2, $notifikasi_id);
            /* #endregion */
            Alert::success('Success', 'Sapi berhasil di pindahkan!!');
            return redirect()->back();
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function export($id)
    {
        return Excel::download(new ProgramExport($id), 'program.xlsx');
    }
    /* #endregion */
    public function gotoproduksi()
    {
        if (Session('idPeternakan') != null) {
            $idpeternakan = Session('idPeternakan');
            $produksusu = DB::table('milk_production')
                ->where('peternakan_id', $idpeternakan)
                ->get();
            return view('user.peternakan.Produksi', compact('idpeternakan', 'produksusu'));
        } else {
            Alert::warning('warning',  'Anda Bukan Peternakan!');
            return redirect()->back();
        }
    }
    public function produksisusu(Request $request)
    {
        try {
            $tmp = DB::table('milk_production')
                ->where('peternakan_id', Session('idPeternakan'))
                ->orderBy('created_at', 'desc')
                ->limit(1)
                ->value('created_at');
            $created_at = Carbon::now()->format('d-m-y');
            $TMP = Carbon::parse($tmp)->format('d-m-y');
            if ($tmp == null || $TMP  != $created_at) {
                $idpeternakan = Session('idPeternakan');
                $cowcard = DB::table('cowcard as a')
                    ->select('a.*')
                    ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                    ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                    ->where('c.id', $idpeternakan)
                    ->count();
                $death = DB::table('cowcard as a')
                    ->select('a.Status')
                    ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                    ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                    ->where('a.Status', "Death")
                    ->where('c.id', $idpeternakan)
                    ->count();
                $sold = DB::table('cowcard as a')
                    ->select('a.Status')
                    ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                    ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                    ->where('a.Status', "Sold")
                    ->where('c.id', $idpeternakan)
                    ->count();
                $dry = DB::table('cowcard as a')
                    ->select('a.Status')
                    ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                    ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                    ->where('a.Status', "Dry")
                    ->where('c.id', $idpeternakan)
                    ->count();
                $bull = DB::table('cowcard as a')
                    ->select('a.Status')
                    ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                    ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                    ->where('a.Status', "Bull")
                    ->where('c.id', $idpeternakan)
                    ->count();
                $lact = DB::table('cowcard as a')
                    ->select('a.Status')
                    ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                    ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                    ->where('a.LACT', "0")
                    ->where('c.id', $idpeternakan)
                    ->count();
                $pembagi = $cowcard - $dry - $bull - $death - $sold - $lact;
                $liter = $request->susu;
                if ($pembagi == 0) {
                    $ADM = 0;
                    Alert::success('Success', 'Produksi susu berhasil dimasukkan, tetapi Sapi Anda Tidak Produksi Hari ini !!');
                } else {
                    $ADM = $liter / $pembagi;
                    Alert::success('Success', 'Produksi susu berhasil dimasukkan !!');
                }
                DB::table('milk_production')->insert([
                    'peternakan_id' => $idpeternakan,
                    'production' => $liter,
                    'ADM' => $ADM,
                    'created_at' => Carbon::now('+07:00')
                ]);
                return redirect()->back();
            } else {
                Alert::warning('Maksimal 1 susu perhari',  'Anda telah memasukkan produksi hari ini !!');
                return redirect()->back();
            }
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }
    public function editproduksisusu(Request $request)
    {
        try {
            $idpeternakan = Session('idPeternakan');
            $cowcard = DB::table('cowcard as a')
                ->select('a.*')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('c.id', $idpeternakan)
                ->count();
            $death = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Death")
                ->where('c.id', $idpeternakan)
                ->count();
            $sold = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Sold")
                ->where('c.id', $idpeternakan)
                ->count();
            $dry = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Dry")
                ->where('c.id', $idpeternakan)
                ->count();
            $bull = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Bull")
                ->where('c.id', $idpeternakan)
                ->count();
            $lact = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.LACT', "0")
                ->where('c.id', $idpeternakan)
                ->count();
            $pembagi = $cowcard - $dry - $bull - $death - $sold - $lact;
            $liter = $request->susu;
            if ($pembagi == 0) {
                $ADM = 0;
                Alert::success('Success', 'Produksi susu berhasil dimasukkan, tetapi Sapi Anda Tidak Produksi Hari ini !!');
            } else {
                $ADM = $liter / $pembagi;
                Alert::success('Success', 'Produksi susu berhasil dimasukkan !!');
            }
            DB::table('milk_production')->where('id', $request->id)->update([
                'production' => $liter,
                'ADM' => $ADM,
            ]);
            return redirect()->back();
        } catch (\Exception $error) {
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }

    /* #region  Index Ternak */
    public function indexKemitraan()
    {
        $MitraPeternakan = MitraPeternakanInvited::with('mitra')
            ->where('peternakan_id', Session('idPeternakan'))
            ->get();
        return view('user.peternakan.kemitraan.Index', compact('MitraPeternakan'));
    }
    /* #endregion */

    /* #region  Put Tolak Mitra */
    public function TolakMitra(Request $req)
    {
        try{
            $mitraPeternakanInvitedModel = MitraPeternakanInvited::find($req->id);
            $mitraPeternakanInvitedModel->decline = 1;
            $mitraPeternakanInvitedModel->desc_decline = $req->Desc;
            $mitraPeternakanInvitedModel->status = "Pengajuan DiTolak";
            $mitraPeternakanInvitedModel->save();

            Alert::success('success',  'Mitra '.$req->name.' Telah Berhasil Di Tolak');
            return redirect()->back();
        }catch(\Exception $ex){
            DB::rollBack();
            Alert::error('Error', $ex->getMessage());
            return redirect()->back();
        }
    }
    /* #endregion */
    public function TerimaMitra($ids, $mitra_id, $Peternakan_id)
    {
        try{
            $mitraPeternakanInvitedModel = MitraPeternakanInvited::find($ids);
            $mitraPeternakanInvitedModel->approval = 1;
            $mitraPeternakanInvitedModel->status = "Sudah DiSetujui";
            $mitraPeternakanInvitedModel->save();

            $mitraPeternakan = new ModelsMitraPeternakan();
            $mitraPeternakan->mitra_id = $mitra_id;
            $mitraPeternakan->peternakan_id = $Peternakan_id;
            $mitraPeternakan->status = 1;
            $mitraPeternakan->input_by = Auth::user()->id;
            $mitraPeternakan->save();

            Alert::success('success',  ' Pengajuan Berhasil Disetujui!');
            return redirect()->back();
        }catch(\Exception $ex){
            DB::rollBack();
            Alert::error('Error', $ex->getMessage());
            return redirect()->back();
        }
    }
    /* #endregion */
}
