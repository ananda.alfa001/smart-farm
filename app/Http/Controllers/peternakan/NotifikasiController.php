<?php

namespace App\Http\Controllers\peternakan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Cowcard;
use App\Models\Peternakan;
use App\Models\Death;
use App\Models\Dmi;
use app\Models\HistoryEvent;
use App\Models\HistoryInspect;
use App\Models\Kandang;
use App\Models\MilkProduction;
use App\Models\Mitra;
use App\Models\Penyakit;
use App\Models\Program;
use App\Models\Sold;
use App\Models\StatusBred;
use App\Models\StatusDry;
use App\Models\StatusFresh;
use App\Models\StatusNobred;
use App\Models\StatusOpen;
use App\Models\StatusPreg;
use App\Models\StatusVaksin;
use App\Models\Syncron;
use App\Models\Teknisi;
use App\Models\Tempcowcount;

use Alert;
use App\Exports\ProgramExport;
use App\Models\MitraPeternakanInvited;
use App\Models\Notifible;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
class NotifikasiController extends Controller
{
    public function notifikasi()
    {
        $sessid = null;
        if(Session::has('idPeternakan')){
            $sessid = Session('idPeternakan');
        }elseif(Session::has('your_key')){
            $sessid = Session('idTeknisi');
        }elseif(Session::has('idKemitraan')){
            $sessid = Session('idKemitraan');
        }
        $unread = DB::table('notifible')
                ->where('roles_target_id',$sessid)
                ->orderBy('created_at', 'DESC')
                ->get();
        return view('user.peternakan.notifikasi.notifikasiall', compact('unread'));
    }
}
