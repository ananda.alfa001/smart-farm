<?php

namespace App\Http\Controllers\peternakan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Alert;
use App\Exports\ProgramExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

use function PHPUnit\Framework\isNull;

class SycnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = DB::transaction(function () use ($request) {
            try {
                $pgm1 = null;
                $pgm2 = null;
                $pgm3 = null;
                $pgm4 = null;
                $pgm5 = null;
                $pgm6 = null;
                $pgm7 = null;

                //FORM 1
                if ($request->nilai_1i == null && $request->nilai_1ii == null) { //get - 1
                    if ($request->variabel_1 != null && $request->operator_1 != null && $request->nilai_1 != null) {
                        $pgm1 = $request->variabel_1 . " " . $request->operator_1 . " " . $request->nilai_1;
                    }
                } else if ($request->nilai_1 == null && $request->nilai_1ii == null) { //get - 2
                    if ($request->variabel_1 != null && $request->operator_1 != null && $request->nilai_1i != null) {
                        $pgm1 = $request->variabel_1 . " " . $request->operator_1 . " " . $request->nilai_1i;
                    }
                } else if ($request->nilai_1i == null && $request->nilai_1 == null) { //get - 3
                    if ($request->variabel_1 != null && $request->operator_1 != null && $request->nilai_1ii != null) {
                        $pgm1 = $request->variabel_1 . " " . $request->operator_1 . " " . $request->nilai_1ii;
                    }
                }

                //FORM 2
                if ($request->nilai_2i == null && $request->nilai_2ii == null) { //get - 1
                    if ($request->variabel_2 != null && $request->operator_2 != null && $request->nilai_2 != null) {
                        if ($pgm1 == null) {
                            $pgm1 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2;
                        } else {
                            $pgm2 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2;
                        }
                    }
                } else if ($request->nilai_2 == null && $request->nilai_2ii == null) { //get - 2
                    if ($request->variabel_2 != null && $request->operator_2 != null && $request->nilai_2i != null) {
                        if ($pgm1 == null) {
                            $pgm1 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2i;
                        } else {
                            $pgm2 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2i;
                        }
                    }
                } else if ($request->nilai_2 == null && $request->nilai_2i == null) { //get - 3
                    if ($request->variabel_2 != null && $request->operator_2 != null && $request->nilai_2ii != null) {
                        if ($pgm1 == null) {
                            $pgm1 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2ii;
                        } else {
                            $pgm2 = $request->variabel_2 . " " . $request->operator_2 . " " . $request->nilai_2ii;
                        }
                    }
                }

                //FORM 3
                if ($request->nilai_3i == null && $request->nilai_3ii == null) { //get - 1
                    if ($request->variabel_3 != null && $request->operator_3 != null && $request->nilai_3 != null) {
                        if ($pgm2 == null) {
                            $pgm2 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3;
                        } else {
                            $pgm3 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3;
                        }
                    }
                } else if ($request->nilai_3 == null && $request->nilai_3ii == null) { //get - 2
                    if ($request->variabel_3 != null && $request->operator_3 != null && $request->nilai_3i != null) {
                        if ($pgm2 == null) {
                            $pgm2 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3i;
                        } else {
                            $pgm3 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3i;
                        }
                    }
                } else if ($request->nilai_3 == null && $request->nilai_3i == null) { //get - 3
                    if ($request->variabel_3 != null && $request->operator_3 != null && $request->nilai_3ii != null) {
                        if ($pgm2 == null) {
                            $pgm2 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3ii;
                        } else {
                            $pgm3 = $request->variabel_3 . " " . $request->operator_3 . " " . $request->nilai_3ii;
                        }
                    }
                }

                //FORM 4
                if ($request->nilai_4i == null && $request->nilai_4ii == null) { //get - 1
                    if ($request->variabel_4 != null && $request->operator_4 != null && $request->nilai_4 != null) {
                        if ($pgm3 == null) {
                            $pgm3 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4;
                        } else {
                            $pgm4 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4;
                        }
                    }
                } else if ($request->nilai_4 == null && $request->nilai_4ii == null) { //get - 2
                    if ($request->variabel_4 != null && $request->operator_4 != null && $request->nilai_4i != null) {
                        if ($pgm3 == null) {
                            $pgm3 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4i;
                        } else {
                            $pgm4 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4i;
                        }
                    }
                } else if ($request->nilai_4 == null && $request->nilai_4i == null) { //get - 3
                    if ($request->variabel_4 != null && $request->operator_4 != null && $request->nilai_4ii != null) {
                        if ($pgm3 == null) {
                            $pgm3 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4ii;
                        } else {
                            $pgm4 = $request->variabel_4 . " " . $request->operator_4 . " " . $request->nilai_4ii;
                        }
                    }
                }

                //FORM 5
                if ($request->nilai_5i == null && $request->nilai_5ii == null) { //get - 1
                    if ($request->variabel_5 != null && $request->operator_5 != null && $request->nilai_5 != null) {
                        if ($pgm4 == null) {
                            $pgm4 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5;
                        } else {
                            $pgm5 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5;
                        }
                    }
                } else if ($request->nilai_5 == null && $request->nilai_5ii == null) { //get - 2
                    if ($request->variabel_5 != null && $request->operator_5 != null && $request->nilai_5i != null) {
                        if ($pgm4 == null) {
                            $pgm4 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5i;
                        } else {
                            $pgm5 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5i;
                        }
                    }
                } else if ($request->nilai_5 == null && $request->nilai_5i == null) { //get - 3
                    if ($request->variabel_5 != null && $request->operator_5 != null && $request->nilai_5ii != null) {
                        if ($pgm4 == null) {
                            $pgm4 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5ii;
                        } else {
                            $pgm5 = $request->variabel_5 . " " . $request->operator_5 . " " . $request->nilai_5ii;
                        }
                    }
                }

                if ($request->variabel_6 != null && $request->operator_6 != null && $request->nilai_6 != null) {
                    $pgm6 = $request->variabel_6 . " " . $request->operator_6 . " " . $request->nilai_6;
                }
                if ($request->variabel_7 != null && $request->operator_7 != null && $request->nilai_7 != null) {
                    if ($pgm6 == null) {
                        $pgm6 = $request->variabel_7 . " " . $request->operator_7 . " " . $request->nilai_7;
                    } else {
                        $pgm7 = $request->variabel_7 . " " . $request->operator_7 . " " . $request->nilai_7;
                    }
                }
                //DIM + (x)
                $parse = explode(" ",$pgm1);
                // DB::table('program')->insert([
                //     'peternakan_id' => Session('idPeternakan'),
                //     'namaprogram' => $request->nprog,
                //     'program_1' => $pgm1,
                //     'program_2' => $pgm2,
                //     'program_3' => $pgm3,
                //     'program_4' => $pgm4,
                //     'program_5' => $pgm5,
                //     'program_6' => $pgm6,
                //     'program_7' => $pgm7,
                //     'created_at' => Carbon::now('+07:00')
                // ]);
                if ($request->session()->has('idUserTeknisi')) {
                    $input = Session('idUserTeknisi');
                } else {
                    $input = Session('idUserPeternakan');
                };
                DB::table('history_event')->insert([
                    'kode_cowcard' => '-',
                    'activity' => 'Add Program sinkron',
                    'ket_1' => "Add Program name " . $request->nprog,
                    'input_by' => $input,
                    'peternakan_id' => Session('idPeternakan'),
                    'created_at' => Carbon::now('+07:00')
                ]);



                Alert::success('Success', 'Program Berhasil Di Input!!');
                return redirect('/peternakan/program');
            } catch (\Exception $error) {
                DB::rollback();
                Alert::error('Error',  $error->getMessage());
                return redirect()->back();
            }
        });
        return $post;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $idPeternakan = Session('idPeternakan');
        $data = DB::table('program')
            ->where('id', '=', $id)
            ->where('previous_program', '!=', null)
            ->first();
        $idprev = $id;
        $idmain = ($data == null)? $id : $data->main_prog;
        // DD($data);
        $pen = DB::table('cowcard as a')
            ->select('a.PEN')
            ->leftJoin('kandang as b', 'b.id', 'a.kandang_id')
            ->leftJoin('peternakan as c', 'c.id', 'b.peternakan_id')
            ->where('b.peternakan_id', $idPeternakan)
            ->distinct()
            ->get();
        $status = collect(["Belum Ada Status", "Fresh", "Open", "Preg", "Bred", "No Bred", "Dry", "Death", "Sold"]);
        return view('user.peternakan.sync.Make_Sync',compact('idprev','idmain','idPeternakan', 'pen', 'status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function updatetindakan($id,$idprog)
    {
        $kode_cow = DB::table('cowcard')->where('id', $id)->value('kode_cowcard');
        $DIM_cow = DB::table('cowcard')->where('id', $id)->value('DIM');
        DB::table('syncron')->where('sapi_id', $id)->where('curent_prog', $idprog)->update([
            'curent_prog'=>null,
            'DIM'=>$DIM_cow,
            'previous_prog'=>$idprog,
            'updated_at'=>Carbon::now()
        ]);
        if (Session('idUserTeknisi')!= null) {
            $input = Session('idUserPeternakan');
        } else {
            $input = Session('idUserTeknisi');
        };
        DB::table('history_event')->insert([
            'kode_cowcard' => $kode_cow,
            'activity' => 'Tindakan sinkron',
            'input_by' => $input,
            'peternakan_id' => Session('idPeternakan'),
            'created_at' => Carbon::now('+07:00')
        ]);
        return redirect()->back();
    }
}
