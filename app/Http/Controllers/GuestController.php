<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Cowcard;
use App\Models\Kandang;
use App\Models\Peternakan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GuestController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function login()
    {
        return view('auth.login');
    }
    public function register()
    {
        return view('auth.register');
    }

    public function homepage()
    {
        $cowcard = Cowcard::count();
        $peternakan = Peternakan::count();
        $kandang = Kandang::count();
        return view('front-page.index',compact('cowcard','peternakan','kandang'));
    }

}
