<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Session\Session;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\Models\User;
use Alert;
use App\Http\Middleware\peternakan;
use Carbon\Carbon;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }
    public function farm()
    {
        $cowcard = DB::table('cowcard as a')
                ->select('a.id','a.kode_cowcard','a.SEX','a.DIM','a.berat','a.sakit','c.nama_peternakan')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->orderBy('a.id', 'DESC')
                ->get();
        $peternakan = DB::table('peternakan')
                    ->get();
        $activity = DB::table('history_event as a')
                    ->select('a.*', 'b.kode_cowcard')
                    ->leftJoin('cowcard as b', 'a.kode_cowcard', 'b.kode_cowcard')
                    ->leftJoin('kandang as c', 'b.kandang_id', 'c.id')
                    ->leftJoin('peternakan as d', 'c.peternakan_id', 'd.id')
                    ->orderBy('a.id', 'DESC')
                    ->get();
        return view('user.admin.dashboard-peternakan',compact('peternakan','cowcard','activity'));
    }
    public function user()
    {
        $user = DB::table('users')
                ->get();
        return view('user.admin.dashboard-user',compact('user'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cow = DB::table('cowcard')->where('id', $id)->get();
        $kodesapi = DB::table('cowcard')->where('id', $id)->value('kode_cowcard');
        $fresh = DB::table('status_fresh as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();
        $bred = DB::table('status_bred as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();
        $dry = DB::table('status_dry as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();
        $nobred = DB::table('status_nobred as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();
        $open = DB::table('status_open as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();
        $preg = DB::table('status_preg as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->get();

        $DCC = DB::table('status_preg as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->limit(1)->value('DCC');
        $DSLH = DB::table('status_bred as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->limit(1)->value('DSLH');
        $DUDRY = DB::table('status_dry as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->limit(1)->value('DUDRY');
        $TBRD = DB::table('status_bred as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->limit(1)->value('TBRD');
        $DDAT = DB::table('status_bred as a')->where('a.sapi_id', $id)->orderBy('a.created_at', 'desc')->limit(1)->value('tanggal');
        $activity = DB::table('history_event as a')
            ->where('kode_cowcard', $kodesapi)
            ->orderBy('a.id', 'DESC')
            ->get();
            return view('user.admin.peternakan.cowcard.CowCard', compact('cow', 'fresh', 'bred', 'dry', 'nobred', 'open', 'preg', 'DCC', 'DSLH', 'DUDRY', 'TBRD', 'DDAT', 'activity', 'kodesapi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Change Limit Cow Card
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'limit' => ['required'],
        ]);
        if ($validate->failed()) {
            return redirect()->back()
            ->withErrors($validate)
            ->withInput();
        } else {
            try {
                DB::table('peternakan')
                    ->where('id', $id)
                    ->update([
                            'limit' => $request->limit
                        ]);
                        Alert::success('Success', 'Limit Berhasil Bertambah!!');
                        return redirect()->back();
            } catch (\Exception $error) {
                Alert::error('Error',  $error->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
