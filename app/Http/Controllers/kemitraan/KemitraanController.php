<?php

namespace App\Http\Controllers\kemitraan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Middleware\kemitraan;
use Carbon\Carbon;
use Alert;
use App\Models\MitraPeternakanInvited;
use Illuminate\Contracts\Session\Session;
use App\Models\Cowcard;
use App\Models\Peternakan;
use App\Models\Death;
use App\Models\Dmi;
use app\Models\HistoryEvent;
use App\Models\HistoryInspect;
use App\Models\Kandang;
use App\Models\MilkProduction;
use App\Models\Mitra;
use App\Models\Penyakit;
use App\Models\Program;
use App\Models\Sold;
use App\Models\StatusBred;
use App\Models\StatusDry;
use App\Models\StatusFresh;
use App\Models\StatusNobred;
use App\Models\StatusOpen;
use App\Models\StatusPreg;
use App\Models\StatusVaksin;
use App\Models\Syncron;
use App\Models\Teknisi;
use App\Models\Tempcowcount;
use App\Models\MitraPeternakan;
use PhpParser\Node\Stmt\Foreach_;

class KemitraanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /* #region  Dashboard View */
    public function index()
    {
        $MitraPeternakan = MitraPeternakan::with('Peternakan')
            ->where('mitra_id', Session('idKemitraan'))
            ->where('status', 1)
            ->get();
        return view('user.kemitraan.Index', compact('MitraPeternakan'));
    }
    /* #endregion */

    /* #region  Dashboard GetAllPeternak */
    public function GetAllPeternak()
    {
        $MitraPeternakan = MitraPeternakan::with('Peternakan')
            ->where('mitra_id', Session('idKemitraan'))
            ->where('status', 1)
            ->get();
        $nSudahVaksin = 0;
        $nBelumVaksin = 0;
        $nSakit = 0;
        $nSehat = 0;
        $nJantan = 0;
        $nBetina = 0;
        $nFresh = 0;
        $nopen = 0;
        $nPreg = 0;
        $nBred = 0;
        $nNobred = 0;
        $nDry = 0;
        $nDeath = 0;
        $nSold = 0;
        $nBelumAda = 0;
        foreach ($MitraPeternakan as $vPerternakan) {
            $idPeternakan = $vPerternakan->peternakan_id;
            $sudahVaksin = DB::table('cowcard as a')
                ->select('a.status_vaksin')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.status_vaksin', "Sudah Vaksin")
                ->where('c.id', $idPeternakan)
                ->count();
            $nSudahVaksin += $sudahVaksin;
            $belumVaksin = DB::table('cowcard as a')
                ->select('a.status_vaksin')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.status_vaksin', "Belum Vaksin")
                ->where('c.id', $idPeternakan)
                ->count();
            $nBelumVaksin += $belumVaksin;
            $sakit = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.sakit', "Sakit")
                ->where('c.id', $idPeternakan)
                ->count();
            $nSakit += $sakit;
            $sehat = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.sakit', "Sehat")
                ->where('c.id', $idPeternakan)
                ->count();
            $nSehat += $sehat;
            $jantan = DB::table('cowcard as a')
                ->select('a.SEX')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.SEX', "Jantan")
                ->where('c.id', $idPeternakan)
                ->count();
            $nJantan += $jantan;
            $betina = DB::table('cowcard as a')
                ->select('a.SEX')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.SEX', "Betina")
                ->where('c.id', $idPeternakan)
                ->count();
            $nBetina += $betina;
            //STATUS
            $fresh = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Fresh")
                ->where('c.id', $idPeternakan)
                ->count();
            $nFresh += $fresh;
            $open = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Open")
                ->where('c.id', $idPeternakan)
                ->count();
            $nopen += $open;
            $preg = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Preg")
                ->where('c.id', $idPeternakan)
                ->count();
            $nPreg += $preg;
            $bred = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Bred")
                ->where('c.id', $idPeternakan)
                ->count();
            $nBred += $bred;
            $nobred = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "No Bred")
                ->where('c.id', $idPeternakan)
                ->count();
            $nNobred += $nobred;
            $dry = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Dry")
                ->where('c.id', $idPeternakan)
                ->count();
            $nDry += $dry;
            $death = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Death")
                ->where('c.id', $idPeternakan)
                ->count();
            $nDeath += $death;
            $sold = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Sold")
                ->where('c.id', $idPeternakan)
                ->count();
            $nSold += $sold;
            $belumada = DB::table('cowcard as a')
                ->select('a.Status')
                ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
                ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
                ->where('a.Status', "Belum Ada Status")
                ->where('c.id', $idPeternakan)
                ->count();
            $nBelumAda += $belumada;
        }

        $lADM = array();
        $lProduction = array();
        $lTgl_produksi = array();
        $ADM = array();
            $production = array();
            $tgl_produksi = array();
        foreach ($MitraPeternakan as $vPerternakan) {
            $idPeternakan = $vPerternakan->peternakan_id;
            $produksi = DB::table('milk_production')->select('ADM', 'production', 'created_at')->where('peternakan_id', $idPeternakan)->orderBy('created_at', 'desc')->limit(30)->get();
            foreach ($produksi as $p) {
            array_push($ADM, $p->ADM);
            array_push($production, $p->production);
            array_push($tgl_produksi, Carbon::parse($p->created_at)->format('d/m'));
        };
        }
        return response()->JSON([
            'sehat' => $nSehat, 'sakit' => $nSakit, 'sudahVaksin' => $nSudahVaksin,
            'belumVaksin' => $nBelumVaksin, 'jantan' => $nJantan, 'betina' => $nBetina, 'fresh' => $nFresh, 'open' => $nopen, 'preg' => $nPreg, 'bred' => $nBred,
            'nobred' => $nNobred, 'dry' => $nDry, 'death' => $nDeath, 'sold' => $nSold, 'belumada' => $nBelumAda,
            'ADM' => $ADM, 'production' => $production, 'tgl_produksi' => $tgl_produksi
        ]);


    }
    /* #endregion */

    /* #region  Daftar Kemitraan View */
    public function DaftarKemitraan()
    {
        $MitraPeternakan = MitraPeternakan::with('Peternakan')
            ->where('mitra_id', Session('idKemitraan'))
            ->where('status', 1)
            ->get();
        return view('user.kemitraan.ternak.MitraTernak', compact('MitraPeternakan'));
    }
    /* #endregion */

    /* #region  Daftar Kemitraan Post Dashboard Mitra Ternak */
    public function GetOnePeternak(Request $request)
    {
        $idPeternakan = $request->peteranakan;
        $sudahVaksin = DB::table('cowcard as a')
            ->select('a.status_vaksin')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.status_vaksin', "Sudah Vaksin")
            ->where('c.id', $idPeternakan)
            ->count();
        $belumVaksin = DB::table('cowcard as a')
            ->select('a.status_vaksin')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.status_vaksin', "Belum Vaksin")
            ->where('c.id', $idPeternakan)
            ->count();
        $sakit = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.sakit', "Sakit")
            ->where('c.id', $idPeternakan)
            ->count();
        $sehat = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.sakit', "Sehat")
            ->where('c.id', $idPeternakan)
            ->count();
        $jantan = DB::table('cowcard as a')
            ->select('a.SEX')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.SEX', "Jantan")
            ->where('c.id', $idPeternakan)
            ->count();
        $betina = DB::table('cowcard as a')
            ->select('a.SEX')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.SEX', "Betina")
            ->where('c.id', $idPeternakan)
            ->count();
        //STATUS
        $fresh = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Fresh")
            ->where('c.id', $idPeternakan)
            ->count();
        $open = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Open")
            ->where('c.id', $idPeternakan)
            ->count();
        $preg = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Preg")
            ->where('c.id', $idPeternakan)
            ->count();
        $bred = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Bred")
            ->where('c.id', $idPeternakan)
            ->count();
        $nobred = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "No Bred")
            ->where('c.id', $idPeternakan)
            ->count();
        $dry = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Dry")
            ->where('c.id', $idPeternakan)
            ->count();
        $death = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Death")
            ->where('c.id', $idPeternakan)
            ->count();
        $sold = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Sold")
            ->where('c.id', $idPeternakan)
            ->count();
        $belumada = DB::table('cowcard as a')
            ->select('a.Status')
            ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
            ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
            ->where('a.Status', "Belum Ada Status")
            ->where('c.id', $idPeternakan)
            ->count();
        $ADM = array();
        $production = array();
        $tgl_produksi = array();
        $produksi = DB::table('milk_production')->select('ADM', 'production', 'created_at')->where('peternakan_id', $idPeternakan)->orderBy('created_at', 'desc')->limit(30)->get();
        foreach ($produksi as $p) {
            array_push($ADM, $p->ADM);
            array_push($production, $p->production);
            array_push($tgl_produksi, Carbon::parse($p->created_at)->format('d/m'));
        };
        return response()->JSON([
            'sehat' => $sehat, 'sakit' => $sakit, 'sudahVaksin' => $sudahVaksin,
            'belumVaksin' => $belumVaksin, 'jantan' => $jantan, 'betina' => $betina, 'fresh' => $fresh, 'open' => $open, 'preg' => $preg, 'bred' => $bred,
            'nobred' => $nobred, 'dry' => $dry, 'death' => $death, 'sold' => $sold, 'belumada' => $belumada,
            'ADM' => $ADM, 'production' => $production, 'tgl_produksi' => $tgl_produksi
        ]);
    }
    /* #endregion */

    /* #region  Status Kemitraan View*/
    public function IndexKemitraan()
    {
        $MitraPeternakan = MitraPeternakan::with('Peternakan')
            ->where('mitra_id', Session('idKemitraan'))
            ->get();
        return view('user.kemitraan.daftar_kemitraan.index', compact('MitraPeternakan'));
    }
    /* #endregion */

    /* #region  Put BerhentiBermitra */
    public function BerhentiBermitra(Request $req)
    {
        try {
            $mitraPeternakanModel = MitraPeternakan::find($req->id);
            $mitraPeternakanModel->status = 0;
            $mitraPeternakanModel->Desc = $req->Desc;
            $mitraPeternakanModel->save();

            notifible($req->peternakan_id, 3, $req->id);
            Alert::success('success',  'Peternakan ' . $req->name . ' telah berhenti bermitra');
            return redirect()->back();
        } catch (\Exception $ex) {
            DB::rollBack();
            Alert::error('Error', $ex->getMessage());
            return redirect()->back();
        }
    }
    /* #endregion */

    public function permintaan()
    {
        $listpeternakan = Peternakan::leftjoin('mitra_peternakan', function ($join) {
            $join->on('peternakan.id', '=', 'mitra_peternakan.peternakan_id');
        })
            ->leftjoin('mitra_peternakan_invited', function ($join) {
                $join->on('peternakan.id', '=', 'mitra_peternakan_invited.peternakan_id');
            })
            ->whereNull('mitra_peternakan.peternakan_id')
            ->whereNull('mitra_peternakan_invited.peternakan_id')
            ->select('peternakan.id', 'peternakan.nama_peternakan')
            ->get();
        $mitra = MitraPeternakan::leftjoin('peternakan', function ($join) {
            $join->on('mitra_peternakan.peternakan_id', '=', 'peternakan.id');
        })
            ->select('peternakan.id', 'peternakan.nama_peternakan')
            ->where('status', 0)
            ->get();
        $mitraInvited = MitraPeternakanInvited::leftjoin('peternakan', function ($join) {
            $join->on('mitra_peternakan_invited.peternakan_id', '=', 'peternakan.id');
        })
            ->select('peternakan.id', 'peternakan.nama_peternakan')
            ->where('decline', 1)
            ->get();
        $mitraPeternak = $listpeternakan->merge($mitra);

        $peternakan = $mitraPeternak->merge($mitraInvited);
        $peternakan->unique();
        $permintaan = MitraPeternakanInvited::where('mitra_id', session('idKemitraan'))
            ->orderBy('created_at', 'Desc')
            ->get();
        return view('user.kemitraan.permintaan.Dashboard', compact('peternakan', 'permintaan'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'peternakan' => ['required', 'string'],
            'kesepakatan' => ['required', 'text'],
        ]);
        if ($validate->failed()) {
            Alert::error('Error', $validate);
            return redirect()->back();
        } else {
            $post = DB::transaction(function () use ($request) {
                try {
                    $mitra_id = Mitra::where('user_id', Auth::user()->id)
                        ->value('id');
                    $notifikasi_id = MitraPeternakanInvited::insertGetId([
                        'mitra_id' => $mitra_id,
                        'peternakan_id' => $request->peternakan,
                        'desc_pengajuan' => $request->kesepakatan,
                        'status' => 'Menunggu Persetujuan',
                        'created_at' => Carbon::now()
                    ]);
                    notifible($request->peternakan, 1, $notifikasi_id);
                    Alert::success('Success', 'Peternakan Berhasil di-Undang, Tunggu Persetujuan dari Peternakan Tersebut!');
                    return redirect()->back();
                } catch (\Exception $e) {
                    DB::rollBack();
                    Alert::error('Error', $e->getMessage());
                    return redirect()->back();
                }
            });
            return $post;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
