<?php

namespace App\Http\Controllers\teknisi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Session\Session;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\Models\User;
use Alert;
use App\Http\Middleware\peternakan;
use Carbon\Carbon;

class TeknisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->session()->has('idPeternakan')){
            $request->session()->forget('idPeternakan');
        }
        $token = DB::table('history_inspect')
                ->select('peternakan.id','peternakan.nama_peternakan','peternakan.nama_pemilik','peternakan.alamat','peternakan.no_hp','history_inspect.token_time')
                ->where('teknisi_id',Auth::user()->id)
                ->leftjoin('peternakan','peternakan.id','history_inspect.peternakan_id')
                ->get();
        return view('user.teknisi.dashboard',compact('token'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->has('idPeternakan')){
                $check = DB::table('peternakan')
                    ->select('id','nama_peternakan','token_time')
                    ->where('id', $request->idPeternakan)
                    ->first();
            }else{
                $check = DB::table('peternakan')
                    ->select('id','nama_peternakan','token_time')
                    ->where('token', $request->token)
                    ->first();
            }
            if($check != null){
                session(['idPeternakan' => $check->id]);
                $temp = DB::table('history_inspect')
                            ->where('teknisi_id' , Session('idTeknisi'))
                            ->where('peternakan_id' , $check->id)
                            ->first();
                $input = Session('idUserTeknisi');
                if($temp == null){
                    DB::table('history_inspect')->insert([
                            'teknisi_id'=> Session('idTeknisi'),
                            'peternakan_id'=> $check->id,
                            'token_time'=> $check->token_time,
                            'created_at'=>Carbon::now('+07:00'),
                        ]);
                    $notifikasi_id = DB::table('history_event')->insertGetId([
                        'kode_cowcard' => "-",
                        'activity' => "Teknisi Check in di Peternakan " .$check->nama_peternakan,
                        'peternakan_id' => $check->id,
                        'input_by' => $input,
                        'created_at' => Carbon::now('+07:00'),
                    ]);
                    notifible($check->id, 2, $notifikasi_id);
                    Alert::success('Success',  "Selamat Datang di Peternakan ". $check->nama_peternakan ." !");
                }else if($temp != null){
                    DB::table('history_inspect')
                            ->where('teknisi_id' , Session('idTeknisi'))
                            ->where('peternakan_id' , $check->id)
                            ->update([
                                'token_time'=> $check->token_time,
                                'created_at'=>Carbon::now('+07:00'),
                            ]);
                    $notifikasi_id = DB::table('history_event')->insertGetId([
                        'kode_cowcard' => "-",
                        'activity' => "Teknisi Check in Kembali ke Peternakan " .$check->nama_peternakan,
                        'peternakan_id' => $check->id,
                        'input_by' => $input,
                        'created_at' => Carbon::now('+07:00'),
                    ]);
                    notifible($check->id, 2, $notifikasi_id);
                    Alert::success('Success',  "Selamat Datang Kembali di Peternakan ". $check->nama_peternakan ." !");
                }else{
                    if($request->session()->has('idPeternakan')){
                        $request->session()->forget('idPeternakan');
                    }
                    Alert::error('Error',  "Masuk Peternakan Bermasalah! Ulangi Kembali!");
                    return redirect('/teknisi');
                }
                return redirect('/peternakan');
            }else{
                Alert::warning('Warning',  "Token Tidak Tersedia/Tidak Aktif!");
                return redirect()->back();
            }
        }catch (\Exception $error) {
            if($request->session()->has('idPeternakan')){
                $request->session()->forget('idPeternakan');
            }
            DB::rollback();
            Alert::error('Error',  $error->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function token_show(Request $request)
    {
        return view('user.teknisi.token');
    }


    public function cek(Request $request)
    {
        dd($request->session()->all());
    }
}
