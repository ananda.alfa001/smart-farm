<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Peternakan
 * 
 * @property int $id
 * @property int $user_id
 * @property string $nama_peternakan
 * @property Carbon $tahun_berdiri
 * @property string $nama_pemilik
 * @property string|null $lembar_persetujuan
 * @property string $alamat
 * @property string $no_hp
 * @property int $limit
 * @property string|null $token
 * @property string|null $token_time
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property User $user
 * @property Collection|Dmi[] $dmis
 * @property Collection|HistoryEvent[] $history_events
 * @property Collection|HistoryInspect[] $history_inspects
 * @property Collection|Kandang[] $kandangs
 * @property Collection|MilkProduction[] $milk_productions
 * @property Collection|Mitra[] $mitras
 * @property Collection|Program[] $programs
 * @property Collection|Tempcowcount[] $tempcowcounts
 *
 * @package App\Models
 */
class Peternakan extends Model
{
	use SoftDeletes;
	protected $table = 'peternakan';

	protected $casts = [
		'user_id' => 'int',
		'limit' => 'int'
	];

	protected $dates = [
		'tahun_berdiri'
	];

	protected $hidden = [
		'token'
	];

	protected $fillable = [
		'user_id',
		'nama_peternakan',
		'tahun_berdiri',
		'nama_pemilik',
		'lembar_persetujuan',
		'alamat',
		'no_hp',
		'limit',
		'token',
		'token_time'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function dmis()
	{
		return $this->hasMany(Dmi::class);
	}

	public function history_events()
	{
		return $this->hasMany(HistoryEvent::class);
	}

	public function history_inspects()
	{
		return $this->hasMany(HistoryInspect::class);
	}

	public function kandangs()
	{
		return $this->hasMany(Kandang::class);
	}

	public function milk_productions()
	{
		return $this->hasMany(MilkProduction::class);
	}

	public function mitras()
	{
		return $this->belongsToMany(Mitra::class, 'mitra_peternakan_invited')
					->withPivot('id', 'approval', 'decline', 'desc_pengajuan', 'desc_decline', 'status', 'input_by', 'deleted_at')
					->withTimestamps();
	}

	public function programs()
	{
		return $this->hasMany(Program::class);
	}

	public function tempcowcounts()
	{
		return $this->hasMany(Tempcowcount::class);
	}
}
