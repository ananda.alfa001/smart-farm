<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Sold
 * 
 * @property int $id
 * @property int $sapi_id
 * @property string $date
 * @property int $price
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Cowcard $cowcard
 *
 * @package App\Models
 */
class Sold extends Model
{
	use SoftDeletes;
	protected $table = 'sold';

	protected $casts = [
		'sapi_id' => 'int',
		'price' => 'int',
		'input_by' => 'int'
	];

	protected $fillable = [
		'sapi_id',
		'date',
		'price',
		'input_by'
	];

	public function cowcard()
	{
		return $this->belongsTo(Cowcard::class, 'sapi_id');
	}
}
