<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Mitra
 * 
 * @property int $id
 * @property int $user_id
 * @property string $nama_instansi
 * @property string $alamat
 * @property string|null $no_telp
 * @property string $skala
 * @property string|null $file_notaris
 * @property string|null $file_identitas
 * @property string|null $file_pernyataan
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property User $user
 * @property Collection|Peternakan[] $peternakans
 *
 * @package App\Models
 */
class Mitra extends Model
{
	use SoftDeletes;
	protected $table = 'mitra';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'nama_instansi',
		'alamat',
		'no_telp',
		'skala',
		'file_notaris',
		'file_identitas',
		'file_pernyataan'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function peternakans()
	{
		return $this->belongsToMany(Peternakan::class, 'mitra_peternakan_invited')
					->withPivot('id', 'approval', 'decline', 'desc_pengajuan', 'desc_decline', 'status', 'input_by', 'deleted_at')
					->withTimestamps();
	}
}
