<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Kandang
 * 
 * @property int $id
 * @property int $peternakan_id
 * @property string|null $nama_kandang
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Peternakan $peternakan
 * @property Collection|Cowcard[] $cowcards
 *
 * @package App\Models
 */
class Kandang extends Model
{
	use SoftDeletes;
	protected $table = 'kandang';

	protected $casts = [
		'peternakan_id' => 'int'
	];

	protected $fillable = [
		'peternakan_id',
		'nama_kandang'
	];

	public function peternakan()
	{
		return $this->belongsTo(Peternakan::class);
	}

	public function cowcards()
	{
		return $this->hasMany(Cowcard::class);
	}
}
