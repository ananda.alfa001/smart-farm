<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HistoryEvent
 * 
 * @property int $id
 * @property string $activity
 * @property string $kode_cowcard
 * @property int|null $peternakan_id
 * @property string|null $DIM
 * @property string|null $ket_1
 * @property string|null $ket_2
 * @property string|null $ket_3
 * @property string|null $ket_4
 * @property string|null $ket_5
 * @property string|null $ket_6
 * @property string|null $ket_7
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Peternakan|null $peternakan
 *
 * @package App\Models
 */
class HistoryEvent extends Model
{
	use SoftDeletes;
	protected $table = 'history_event';

	protected $casts = [
		'peternakan_id' => 'int',
		'input_by' => 'int'
	];

	protected $fillable = [
		'activity',
		'kode_cowcard',
		'peternakan_id',
		'DIM',
		'ket_1',
		'ket_2',
		'ket_3',
		'ket_4',
		'ket_5',
		'ket_6',
		'ket_7',
		'input_by'
	];

	public function peternakan()
	{
		return $this->belongsTo(Peternakan::class);
	}
}
