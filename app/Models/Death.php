<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Death
 * 
 * @property int $id
 * @property int $sapi_id
 * @property string $date
 * @property string $reason
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Cowcard $cowcard
 *
 * @package App\Models
 */
class Death extends Model
{
	use SoftDeletes;
	protected $table = 'death';

	protected $casts = [
		'sapi_id' => 'int',
		'input_by' => 'int'
	];

	protected $fillable = [
		'sapi_id',
		'date',
		'reason',
		'input_by'
	];

	public function cowcard()
	{
		return $this->belongsTo(Cowcard::class, 'sapi_id');
	}
}
