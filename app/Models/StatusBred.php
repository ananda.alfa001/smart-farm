<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StatusBred
 *
 * @property int $id
 * @property int $sapi_id
 * @property Carbon $tanggal
 * @property Carbon $CDAT
 * @property int $TBRD
 * @property int $DSLH
 * @property int $DCC
 * @property Carbon $HDAT
 * @property string $STRAW
 * @property string $status_bred
 * @property string $status
 * @property int $LACT
 * @property int|null $dim_masuk
 * @property int|null $dim_keluar
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Cowcard $cowcard
 *
 * @package App\Models
 */
class StatusBred extends Model
{
	use SoftDeletes;
	protected $table = 'status_bred';

	protected $casts = [
		'sapi_id' => 'int',
		'TBRD' => 'int',
		'DSLH' => 'int',
		'DCC' => 'int',
		'LACT' => 'int',
		'dim_masuk' => 'int',
		'dim_keluar' => 'int',
		'input_by' => 'int'
	];

	protected $dates = [
		'tanggal',
		'CDAT',
        'HDAT'
	];

	protected $fillable = [
		'sapi_id',
		'tanggal',
		'CDAT',
		'TBRD',
        'DSLH',
        'DCC',
        'HDAT',
        'STRAW',
		'status_bred',
		'status',
		'LACT',
		'dim_masuk',
		'dim_keluar',
		'input_by'
	];

	public function cowcard()
	{
		return $this->belongsTo(Cowcard::class, 'sapi_id');
	}
}
