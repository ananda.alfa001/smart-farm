<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cowcard
 *
 * @property int $id
 * @property string $kode_cowcard
 * @property int $kandang_id
 * @property string $PEN
 * @property Carbon $tanggal_lahir
 * @property string $CALF
 * @property string $SEX
 * @property int $LACT
 * @property string $Status
 * @property string $status_vaksin
 * @property int|null $DIM
 * @property int|null $DMI
 * @property int|null $DIH
 * @property string|null $berat
 * @property string|null $sakit
 * @property string|null $pakan
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Kandang $kandang
 * @property Collection|Death[] $deaths
 * @property Collection|Dmi[] $dmis
 * @property Collection|Penyakit[] $penyakits
 * @property Collection|Sold[] $solds
 * @property Collection|StatusBred[] $status_breds
 * @property Collection|StatusDry[] $status_dries
 * @property Collection|StatusFresh[] $status_freshes
 * @property Collection|StatusNobred[] $status_nobreds
 * @property Collection|StatusOpen[] $status_opens
 * @property Collection|StatusPreg[] $status_pregs
 * @property Collection|StatusVaksin[] $status_vaksins
 * @property Collection|Syncron[] $syncrons
 *
 * @package App\Models
 */
class Cowcard extends Model
{
	use SoftDeletes;
	protected $table = 'cowcard';

	protected $casts = [
		'kandang_id' => 'int',
		'LACT' => 'int',
		'DIM' => 'int',
		'DMI' => 'int',
		'DIH' => 'int',
		'input_by' => 'int'
	];

	protected $dates = [
		'tanggal_lahir'
	];

	protected $fillable = [
		'kode_cowcard',
		'induk_sapi_id',
		'kandang_id',
		'PEN',
		'tanggal_lahir',
		'CALF',
		'SEX',
		'LACT',
		'Status',
		'status_vaksin',
		'DIM',
		'DMI',
		'DIH',
		'berat',
		'sakit',
		'pakan',
		'input_by'
	];

	public function kandang()
	{
		return $this->belongsTo(Kandang::class);
	}

	public function deaths()
	{
		return $this->hasMany(Death::class, 'sapi_id');
	}

	public function dmis()
	{
		return $this->hasMany(Dmi::class, 'sapi_id');
	}

	public function penyakits()
	{
		return $this->hasMany(Penyakit::class, 'sapi_id');
	}

	public function solds()
	{
		return $this->hasMany(Sold::class, 'sapi_id');
	}

	public function status_breds()
	{
		return $this->hasMany(StatusBred::class, 'sapi_id');
	}

	public function status_dries()
	{
		return $this->hasMany(StatusDry::class, 'sapi_id');
	}

	public function status_freshes()
	{
		return $this->hasMany(StatusFresh::class, 'sapi_id');
	}

	public function status_nobreds()
	{
		return $this->hasMany(StatusNobred::class, 'sapi_id');
	}

	public function status_opens()
	{
		return $this->hasMany(StatusOpen::class, 'sapi_id');
	}

	public function status_pregs()
	{
		return $this->hasMany(StatusPreg::class, 'sapi_id');
	}

	public function status_vaksins()
	{
		return $this->hasMany(StatusVaksin::class, 'sapi_id');
	}

	public function syncrons()
	{
		return $this->hasMany(Syncron::class, 'sapi_id');
	}
}
