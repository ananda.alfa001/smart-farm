<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tempcowcount
 * 
 * @property int $id
 * @property int $peternakan_id
 * @property int $current
 * @property int $max
 * @property string|null $Status
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Peternakan $peternakan
 *
 * @package App\Models
 */
class Tempcowcount extends Model
{
	use SoftDeletes;
	protected $table = 'tempcowcount';

	protected $casts = [
		'peternakan_id' => 'int',
		'current' => 'int',
		'max' => 'int',
		'input_by' => 'int'
	];

	protected $fillable = [
		'peternakan_id',
		'current',
		'max',
		'Status',
		'input_by'
	];

	public function peternakan()
	{
		return $this->belongsTo(Peternakan::class);
	}
}
