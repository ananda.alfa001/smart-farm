<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Notifible
 * 
 * @property int $id
 * @property int $roles_target_id
 * @property int $type_notif
 * @property int $notifikasi_id
 * @property bool $read
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @package App\Models
 */
class Notifible extends Model
{
	use SoftDeletes;
	protected $table = 'notifible';

	protected $casts = [
		'roles_target_id' => 'int',
		'type_notif' => 'int',
		'notifikasi_id' => 'int',
		'read' => 'bool'
	];

	protected $fillable = [
		'roles_target_id',
		'type_notif',
		'notifikasi_id',
		'read'
	];
}
