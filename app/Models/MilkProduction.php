<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MilkProduction
 * 
 * @property int $id
 * @property int $peternakan_id
 * @property int $production
 * @property float $ADM
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Peternakan $peternakan
 *
 * @package App\Models
 */
class MilkProduction extends Model
{
	use SoftDeletes;
	protected $table = 'milk_production';

	protected $casts = [
		'peternakan_id' => 'int',
		'production' => 'int',
		'ADM' => 'float',
		'input_by' => 'int'
	];

	protected $fillable = [
		'peternakan_id',
		'production',
		'ADM',
		'input_by'
	];

	public function peternakan()
	{
		return $this->belongsTo(Peternakan::class);
	}
}
