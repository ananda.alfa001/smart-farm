<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Teknisi
 * 
 * @property int $id
 * @property int $user_id
 * @property string $nama_instansi
 * @property string $profesi
 * @property string|null $activity
 * @property Carbon $tgl_lahir
 * @property string $alamat
 * @property string $no_hp
 * @property string|null $lvl
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property User $user
 * @property Collection|HistoryInspect[] $history_inspects
 *
 * @package App\Models
 */
class Teknisi extends Model
{
	use SoftDeletes;
	protected $table = 'teknisi';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $dates = [
		'tgl_lahir'
	];

	protected $fillable = [
		'user_id',
		'nama_instansi',
		'profesi',
		'activity',
		'tgl_lahir',
		'alamat',
		'no_hp',
		'lvl'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function history_inspects()
	{
		return $this->hasMany(HistoryInspect::class);
	}
}
