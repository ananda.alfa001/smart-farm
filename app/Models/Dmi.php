<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Dmi
 * 
 * @property int $id
 * @property int $peternakan_id
 * @property int $sapi_id
 * @property int $DMI
 * @property string|null $pakan
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Peternakan $peternakan
 * @property Cowcard $cowcard
 *
 * @package App\Models
 */
class Dmi extends Model
{
	use SoftDeletes;
	protected $table = 'dmi';

	protected $casts = [
		'peternakan_id' => 'int',
		'sapi_id' => 'int',
		'DMI' => 'int',
		'input_by' => 'int'
	];

	protected $fillable = [
		'peternakan_id',
		'sapi_id',
		'DMI',
		'pakan',
		'input_by'
	];

	public function peternakan()
	{
		return $this->belongsTo(Peternakan::class);
	}

	public function cowcard()
	{
		return $this->belongsTo(Cowcard::class, 'sapi_id');
	}
}
