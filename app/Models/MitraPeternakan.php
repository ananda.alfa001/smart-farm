<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MitraPeternakan
 * 
 * @property int $id
 * @property int $mitra_id
 * @property int $peternakan_id
 * @property bool $status
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Mitra $mitra
 * @property Peternakan $peternakan
 *
 * @package App\Models
 */
class MitraPeternakan extends Model
{
	use SoftDeletes;
	protected $table = 'mitra_peternakan';

	protected $casts = [
		'mitra_id' => 'int',
		'peternakan_id' => 'int',
		'status' => 'bool',
		'input_by' => 'int'
	];

	protected $fillable = [
		'mitra_id',
		'peternakan_id',
		'status',
		'input_by'
	];

	public function mitra()
	{
		return $this->belongsTo(Mitra::class);
	}

	public function peternakan()
	{
		return $this->belongsTo(Peternakan::class);
	}
}
