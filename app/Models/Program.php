<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Program
 * 
 * @property int $id
 * @property int $peternakan_id
 * @property string $namaprogram
 * @property string|null $previous_program
 * @property string|null $program_1
 * @property string|null $program_2
 * @property string|null $program_3
 * @property string|null $program_4
 * @property string|null $program_5
 * @property string|null $program_6
 * @property string|null $program_7
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Peternakan $peternakan
 *
 * @package App\Models
 */
class Program extends Model
{
	use SoftDeletes;
	protected $table = 'program';

	protected $casts = [
		'peternakan_id' => 'int',
		'input_by' => 'int'
	];

	protected $fillable = [
		'peternakan_id',
		'namaprogram',
		'previous_program',
		'program_1',
		'program_2',
		'program_3',
		'program_4',
		'program_5',
		'program_6',
		'program_7',
		'input_by'
	];

	public function peternakan()
	{
		return $this->belongsTo(Peternakan::class);
	}
}
