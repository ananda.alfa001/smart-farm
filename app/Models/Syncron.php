<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Syncron
 * 
 * @property int $id
 * @property int $sapi_id
 * @property string $curent_prog
 * @property string $previous_prog
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Cowcard $cowcard
 *
 * @package App\Models
 */
class Syncron extends Model
{
	use SoftDeletes;
	protected $table = 'syncron';

	protected $casts = [
		'sapi_id' => 'int',
		'input_by' => 'int'
	];

	protected $fillable = [
		'sapi_id',
		'curent_prog',
		'previous_prog',
		'input_by'
	];

	public function cowcard()
	{
		return $this->belongsTo(Cowcard::class, 'sapi_id');
	}
}
