<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HistoryInspect
 *
 * @property int $id
 * @property int $teknisi_id
 * @property int $peternakan_id
 * @property Carbon $token_time
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Peternakan $peternakan
 * @property Teknisi $teknisi
 *
 * @package App\Models
 */
class HistoryInspect extends Model
{
	use SoftDeletes;
	protected $table = 'history_inspect';

	protected $casts = [
		'teknisi_id' => 'int',
		'peternakan_id' => 'int'
	];

	protected $dates = [
		'token_time'
	];

	protected $fillable = [
		'teknisi_id',
		'peternakan_id',
		'token_time'
	];

	public function peternakan()
	{
		return $this->belongsTo(Peternakan::class);
	}

	public function teknisi()
	{
		return $this->belongsTo(Teknisi::class);
	}
}
