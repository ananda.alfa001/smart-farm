<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StatusPreg
 *
 * @property int $id
 * @property int $sapi_id
 * @property Carbon $tanggal
 * @property int $DCCD
 * @property int $DCC
 * @property Carbon $SYNC
 * @property string $SDESC
 * @property string $status
 * @property int $LACT
 * @property int|null $dim_masuk
 * @property int|null $dim_keluar
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Cowcard $cowcard
 *
 * @package App\Models
 */
class StatusPreg extends Model
{
	use SoftDeletes;
	protected $table = 'status_preg';

	protected $casts = [
		'sapi_id' => 'int',
		'DCCD' => 'int',
		'DCC' => 'int',
		'LACT' => 'int',
		'dim_masuk' => 'int',
		'dim_keluar' => 'int',
		'input_by' => 'int'
	];

	protected $dates = [
		'tanggal',
		'SYNC'
	];

	protected $fillable = [
		'sapi_id',
		'tanggal',
		'DCCD',
		'DCC',
		'SYNC',
		'SDESC',
		'status',
		'LACT',
		'dim_masuk',
		'dim_keluar',
		'input_by'
	];

	public function cowcard()
	{
		return $this->belongsTo(Cowcard::class, 'sapi_id');
	}
}
