<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MitraPeternakanInvited
 * 
 * @property int $id
 * @property int $mitra_id
 * @property int $peternakan_id
 * @property bool $approval
 * @property bool $decline
 * @property string|null $desc_pengajuan
 * @property string|null $desc_decline
 * @property string $status
 * @property int|null $input_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Mitra $mitra
 * @property Peternakan $peternakan
 *
 * @package App\Models
 */
class MitraPeternakanInvited extends Model
{
	use SoftDeletes;
	protected $table = 'mitra_peternakan_invited';

	protected $casts = [
		'mitra_id' => 'int',
		'peternakan_id' => 'int',
		'approval' => 'bool',
		'decline' => 'bool',
		'input_by' => 'int'
	];

	protected $fillable = [
		'mitra_id',
		'peternakan_id',
		'approval',
		'decline',
		'desc_pengajuan',
		'desc_decline',
		'status',
		'input_by'
	];

	public function mitra()
	{
		return $this->belongsTo(Mitra::class);
	}

	public function peternakan()
	{
		return $this->belongsTo(Peternakan::class);
	}
}
