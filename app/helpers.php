<?php

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\MitraPeternakanInvited;
use App\Models\MitraPeternakan;
use App\Models\HistoryEvent;
use App\Models\Notifible;
use App\Models\Peternakan;
use Illuminate\Http\Request;



// PRIMARY FIRST CHECK
function DIM()
{
    $cow = DB::table('cowcard')
        ->get();
    foreach ($cow as $create) {
        $tanggal_fresh = DB::table('status_fresh')
            ->where('sapi_id', $create->id)
            ->orderBy('created_at', 'desc')
            ->limit(1)
            ->value('tanggal');
        if ($tanggal_fresh == null || $create->Status == "Sold" || $create->Status == "Death") {
            continue;
        } else {
            $akhir = Carbon::now();
            $dtoday = strtotime($akhir->toDateString());
            $tgl_fresh = strtotime($tanggal_fresh);
            $diff  = ($dtoday - $tgl_fresh) / 60 / 60 / 24;
            // UPDATE DCC DOPN
            $dim_lama = $create->DIM;
            $dim_baru = $diff;
            $selisih = $dim_baru - $dim_lama;
            $temp1 = DB::table('status_preg')
                ->where('sapi_id', $create->id)
                ->where('status', 'Active')
                ->limit(1)
                ->value('DCC');
            $temp2 = DB::table('status_dry')
                ->where('sapi_id', $create->id)
                ->where('status', 'Active')
                ->limit(1)
                ->value('DDRY');
            $DCC = $temp1 + $selisih;
            $DDRY = $temp2 + $selisih;
            DB::table('status_preg')->where('sapi_id', $create->id)->where('status', "Active")->update(['DCC' => $DCC]);
            DB::table('status_dry')->where('sapi_id', $create->id)->where('status', "Active")->update(['DDRY' => $DDRY]);
            //UPDATE DIM
            DB::table('cowcard')
                ->where('id', $create->id)
                ->update(['DIM' => $diff]);
        }
    }
}
function notifible($roles_target_id, $type_notif, $notifikasi_id)
{
    try {
            //check invitation -> Kemitraan to Peternakan (type_notif = 1 )
            DB::table('notifible')->insert([
                'roles_target_id' => $roles_target_id,
                'type_notif' => $type_notif,
                'notifikasi_id' => $notifikasi_id,
                'created_at' => Carbon::now('+07:00'),
            ]);
    } catch (\Exception $error) {
        DB::rollback();
        throw new \ErrorException($error->getMessage());
    }
}
function notifible_data($type, $id){
    switch($type){
        case "1":
            $notif_type_1 = MitraPeternakanInvited::where('id',$id)
                ->get();
            return $notif_type_1;
            break;
        case "2":
            $notif_type_2 = HistoryEvent::where('id',$id)
                ->where('input_by','!=', Session('idUserPeternakan'))
                ->get();
            return $notif_type_2;
            break;
        case "3":
            $notif_type_3 = MitraPeternakan::where('id',$id)
                ->get();
            return $notif_type_3;
            break;
    }
}
function notifible_read($id){
    $notifible = Notifible::find($id);
    $notifible->update([
        'read' => 1,
        'update_at' => Carbon::now('+07:00')
    ]);
}
// ================================================
function cow_card($id)
{
    return DB::table('cowcard as a')
        ->select('a.*', 'b.nama_kandang', 'b.peternakan_id')
        ->leftJoin('kandang as b', 'a.kandang_id', 'b.id')
        ->leftJoin('peternakan as c', 'b.peternakan_id', 'c.id')
        ->where('c.id', $id)
        ->orderBy('a.id', 'DESC')
        ->get();
}

function activity($id)
{
    $activity = HistoryEvent::with('Peternakan')
    ->where('peternakan_id',  $id)
    ->orderBy('created_at','DESC')
    ->get();
    return $activity;
}
function ExplodeVariable($text)
{
    try {
        $data = explode(" ", $text);
        if ($data[0] == "LACT" || $data[0] == "PEN" || $data[0] == "Status") {
            return "cowcard." . $data[0];
        } else {
            return $data[0];
        }
    } catch (\Exception $e) {
        return null;
    }
}

function ExplodeVariableBlade($text)
{
    try {
        $data = explode(" ", $text);
        return $data[0];
    } catch (\Exception $e) {
        return null;
    }
}

function ExplodeOperator($text)
{
    try {
        $data = explode(" ", $text);
        return $data[1];
    } catch (\Exception $e) {
        return null;
    }
}
function ExplodeNilai($text)
{
    try {
        $data = explode(" ", $text);
        return $data[2];
    } catch (\Exception $e) {
        return null;
    }
}
function DIH()
{
    $cow = DB::table('cowcard')
        ->get();
    foreach ($cow as $create) {
        $tanggal_sakit = DB::table('penyakit')
            ->where('sapi_id', $create->id)
            ->orderBy('created_at', 'desc')
            ->limit(1)
            ->value('tglsakit');
        if ($tanggal_sakit == null || $create->Status == "Sold" || $create->Status == "Death" || $create->sakit != "Sakit") {
            continue;
        } else {
            $akhir = Carbon::now();
            $dtoday = strtotime($akhir->toDateString());
            $tgl_sakit = strtotime($tanggal_sakit);
            $diff  = ($dtoday - $tgl_sakit) / 60 / 60 / 24;
            //UPDATE DIH
            DB::table('cowcard')
                ->where('id', $create->id)
                ->update(['DIH' => $diff]);
        }
    }
}
function PEN($idKandang, $PenName)
{
    return DB::table('cowcard')
        ->where('kandang_id', $idKandang)
        ->where('PEN', $PenName)
        ->orderBy('updated_at', 'ASC')
        ->get();
}
function FormDate($date)
{
    $dates = date('d-m-Y', strtotime($date));
    return $dates;
}
function status_update($status, $id)
{
    if ($status == "Bred") {
        DB::table('status_bred')->where('sapi_id', $id)->where('status', 'Active')->update([
            'status' => "Deactive",
            'updated_at' => Carbon::now('+07:00')
        ]);
    } elseif ($status == "No Bred") {
        DB::table('status_nobred')->where('sapi_id', $id)->where('status', 'Active')->update([
            'status' => "Deactive",
            'updated_at' => Carbon::now('+07:00')
        ]);
    } elseif ($status == "Open") {
        DB::table('status_open')->where('sapi_id', $id)->where('status', 'Active')->update([
            'status' => "Deactive",
            'updated_at' => Carbon::now('+07:00')
        ]);
    } elseif ($status == "Preg") {
        DB::table('status_preg')->where('sapi_id', $id)->where('status', 'Active')->update([
            'status' => "Deactive",
            'updated_at' => Carbon::now('+07:00')
        ]);
    } elseif ($status == "Fresh") {
        DB::table('status_fresh')->where('sapi_id', $id)->where('status', 'Active')->update([
            'status' => "Deactive",
            'updated_at' => Carbon::now('+07:00')
        ]);
    } elseif ($status == "Dry") {
        DB::table('status_dry')->where('sapi_id', $id)->where('status', 'Active')->update([
            'status' => "Deactive",
            'updated_at' => Carbon::now('+07:00')
        ]);
    }
}
function uniqueuser($email)
{
    $check = DB::table('users')
        ->select('email')
        ->get();
    foreach ($check as $case) {
        if ($case->email === $email) {
            return true;
        } else {
            continue;
        }
    }
}
function getactivestatus($id_sapi)
{
    $fresh = DB::table('status_fresh')
        ->where('sapi_id', $id_sapi)
        ->orderBy('created_at', 'desc')
        ->limit(1)
        ->value('status');
    $bred = DB::table('status_bred')
        ->where('sapi_id', $id_sapi)
        ->orderBy('created_at', 'desc')
        ->limit(1)
        ->value('status');
    $dry = DB::table('status_dry')
        ->where('sapi_id', $id_sapi)
        ->orderBy('created_at', 'desc')
        ->limit(1)
        ->value('status');
    $nobred = DB::table('status_nobred')
        ->where('sapi_id', $id_sapi)
        ->orderBy('created_at', 'desc')
        ->limit(1)
        ->value('status');
    $open = DB::table('status_open')
        ->where('sapi_id', $id_sapi)
        ->orderBy('created_at', 'desc')
        ->limit(1)
        ->value('status');
    $preg = DB::table('status_preg')
        ->where('sapi_id', $id_sapi)
        ->orderBy('created_at', 'desc')
        ->limit(1)
        ->value('status');
    if ($fresh == "Active") {
        return "Fresh";
    } elseif ($bred == "Active") {
        return "Bred";
    } elseif ($dry == "Active") {
        return "Dry";
    } elseif ($nobred == "Active") {
        return "No Bred";
    } elseif ($open == "Active") {
        return "Open";
    } elseif ($preg == "Active") {
        return "Preg";
    } else {
        return "Belum Ada Status";
    }
}
function token($idPeternakan)
{
    $time = Carbon::now()->format('Y-m-d');
    $check = DB::table('peternakan')
        ->where('id', $idPeternakan)
        ->value('token_time');
}
function token_generate($input, $idPeternakan)
{
    $post = DB::transaction(function () use ($input, $idPeternakan) {
        try {
            $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $generate1 = substr(str_shuffle(str_repeat($char, 5)), 0, 10);
            $generate2 = substr(str_shuffle(str_repeat($char, 5)), 0, 10);
            $carbon = Carbon::now()->getPreciseTimestamp(3);
            $token = $generate1 . $carbon . $generate2;

            DB::table('peternakan')
                ->where('id', $idPeternakan)
                ->update([
                    'token' => $token,
                    'token_time' => Carbon::now()->format('Y-m-d')
                ]);
            $notifikasi_id =  DB::table('history_event')->insertGetId([
                'kode_cowcard' => "-",
                'activity' => 'Generate Token',
                'peternakan_id' => $idPeternakan,
                'input_by' => $input,
                'created_at' => Carbon::now('+07:00'),
            ]);
            notifible(Session('idPeternakan'), 2, $notifikasi_id);
        } catch (\Exception $error) {
            DB::rollback();
            throw new \ErrorException($error->getMessage());
        }
    });
    return $post;
}
function token_all_generate()
{
    $now = Carbon::now()->format('Y-m-d');
    $get = DB::table('peternakan')->select('id', 'token_time')->get();
    foreach ($get as $get_token_time) {
        if ($get_token_time->token_time != $now) {
            $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $generate1 = substr(str_shuffle(str_repeat($char, 5)), 0, 10);
            $generate2 = substr(str_shuffle(str_repeat($char, 5)), 0, 10);
            $carbon = Carbon::now()->getPreciseTimestamp(3);
            $token = $generate1 . $carbon . $generate2;

            DB::table('peternakan')
                ->where('id', $get_token_time->id)
                ->update([
                    'token' => $token,
                    'token_time' => Carbon::now()->format('Y-m-d')
                ]);
        }
    }
}
//Set Status
function setstatusbred(Request $request)
{
    $cowcard = DB::table('cowcard')->where('id', $request->id)->get();
    $dim = 0;
    $LACT = 0;
    $status = 'null';
    $idsapi = 'null';
    foreach ($cowcard as $p) {
        $dim =  $p->DIM;
        $LACT =  $p->LACT;
        $status = $p->Status;
        $idsapi = $p->kode_cowcard;
    }
    status_update($status, $request->id);
    DB::table('cowcard')->where('id', $request->id)->update([
        'Status' => "Bred",
        'kandang_id' => $request->selectKandang,
        'PEN' => $request->PEN,
        'updated_at' => Carbon::now('+07:00')
    ]);

    $bred = DB::table('status_bred')->where('sapi_id', $request->id)->get();
    $count = 1;
    foreach ($bred as $p) {
        $count++;
    }
    if ($request->session()->has('idUserTeknisi')) {
        $input = Session('idUserTeknisi');
    } else {
        $input = Session('idUserPeternakan');
    };
    $today = Carbon::now();
    $dtoday = strtotime($today->toDateString());
    $tgl_bred = strtotime($request->CDAT);
    $DSLH = ($dtoday - $tgl_bred) / 60 / 60 / 24;
    DB::table('status_bred')->insert([
        'sapi_id' => $request->id,
        'tanggal' => Carbon::now('+07:00'),
        'CDAT' => $request->CDAT,
        'TBRD' => $count,
        'HDAT' => $request->HDAT,
        'DSLH' => $DSLH,
        'status_bred' => $request->status_bred,
        'status' => "Active",
        'LACT' => $LACT,
        'dim_masuk' => $dim,
        'created_at' => Carbon::now('+07:00'),
        'STRAW' => $request->straw
    ]);
    $notifikasi_id = DB::table('history_event')->insertGetId([
        'input_by' => $input,
        'kode_cowcard' => $idsapi,
        'activity' => 'Bred',
        'ket_1' => "LACT : " . strval($LACT),
        'ket_2' => "CDAT : " . strval($request->CDAT),
        'ket_3' => "TBRD : " . strval($count),
        'ket_4' => "Sts-Bred : " . strval($request->status_bred),
        'DIM' => $dim,
        'peternakan_id' => Session('idPeternakan'),
        'created_at' => Carbon::now('+07:00')
    ]);
    notifible(Session('idPeternakan'), 2, $notifikasi_id);
}
function setstatusdry(Request $request)
{
    $cowcard = DB::table('cowcard')->where('id', $request->id)->get();
    $dim = 0;
    $LACT = 0;
    $status = 'null';
    $idsapi = 'null';
    foreach ($cowcard as $p) {
        $dim =  $p->DIM;
        $LACT =  $p->LACT;
        $status = $p->Status;
        $idsapi = $p->kode_cowcard;
    }
    status_update($status, $request->id);
    DB::table('cowcard')->where('id', $request->id)->update([
        'Status' => "Dry",
        'kandang_id' => $request->selectKandang,
        'PEN' => $request->PEN,
        'updated_at' => Carbon::now('+07:00')
    ]);
    $today = Carbon::now();
    $dtoday = strtotime($today->toDateString());
    $DUDRY = strtotime($request->DUDRY);
    $ddiff = ($dtoday - $DUDRY) / 60 / 60 / 24;
    DB::table('status_dry')->insert([
        'sapi_id' => $request->id,
        'tanggal' => Carbon::now('+07:00'),
        'DUDRY' => $request->DUDRY,
        'DDRY' => $ddiff,
        'status' => "Active",
        'LACT' => $LACT,
        'dim_masuk' => $dim,
        'created_at' => Carbon::now('+07:00')
    ]);
    if ($request->session()->has('idUserTeknisi')) {
        $input = Session('idUserTeknisi');
    } else {
        $input = Session('idUserPeternakan');
    };
    $notifikasi_id = DB::table('history_event')->insertGetId([
        'kode_cowcard' => $idsapi,
        'activity' => 'Dry',
        'ket_1' => "LACT : " . strval($LACT),
        'ket_2' => "DUDRY : " . strval($request->DUDRY),
        'ket_3' => "DDRY : " . strval($ddiff),
        'DIM' => $dim,
        'input_by' => $input,
        'peternakan_id' => Session('idPeternakan'),
        'created_at' => Carbon::now('+07:00')
    ]);
    notifible(Session('idPeternakan'), 2, $notifikasi_id);
}
function setstatusfresh(Request $request)
{
    $cowcard = DB::table('cowcard')->where('id', $request->id)->get();
    $dim = 0;
    $LACT = 0;
    $status = 'null';
    $idsapi = 'null';
    foreach ($cowcard as $p) {
        $dim =  $p->DIM;
        $LACT =  $p->LACT;
        $status = $p->Status;
        $idsapi = $p->kode_cowcard;
    }
    status_update($status, $request->id);
    DB::table('cowcard')->where('id', $request->id)->update([
        'Status' => "Fresh",
        'DIM' => 0,
        'kandang_id' => $request->selectKandang,
        'PEN' => $request->PEN,
        'updated_at' => Carbon::now('+07:00')
    ]);
    DB::table('cowcard')->where('id', $request->id)->increment('LACT');
    DB::table('status_fresh')->insert([
        'sapi_id' => $request->id,
        'tanggal' => $request->tgl_fresh,
        'EASE' => $request->EASE,
        'status' => "Active",
        'LACT' => $LACT,
        'dim_masuk' => $dim,
        'created_at' => Carbon::now('+07:00')
    ]);
    if ($request->session()->has('idUserTeknisi')) {
        $input = Session('idUserTeknisi');
    } else {
        $input = Session('idUserPeternakan');
    };
    $notifikasi_id = DB::table('history_event')->insertGetId([
        'kode_cowcard' => $idsapi,
        'activity' => 'Fresh',
        'ket_1' => "LACT : " . strval($LACT),
        'ket_2' => "EASE : " . strval($request->EASE),
        'ket_3' => "FDAT : " . strval($request->tgl_fresh),
        'DIM' => $dim,
        'peternakan_id' => Session('idPeternakan'),
        'input_by' => $input,
        'created_at' => Carbon::now('+07:00')
    ]);
    notifible(Session('idPeternakan'), 2, $notifikasi_id);
}
function setstatusnobred(Request $request)
{
    $cowcard = DB::table('cowcard')->where('id', $request->id)->get();
    $dim = 0;
    $LACT = 0;
    $status = 'null';
    $idsapi = 'null';
    foreach ($cowcard as $p) {
        $dim =  $p->DIM;
        $LACT =  $p->LACT;
        $status = $p->Status;
        $idsapi = $p->kode_cowcard;
    }
    status_update($status, $request->id);
    DB::table('cowcard')->where('id', $request->id)->update([
        'Status' => "No Bred",
        'kandang_id' => $request->selectKandang,
        'PEN' => $request->PEN,
        'updated_at' => Carbon::now('+07:00')
    ]);
    DB::table('status_nobred')->insert([
        'sapi_id' => $request->id,
        'tanggal' => $request->tgl_nobred,
        'status' => "Active",
        'LACT' => $LACT,
        'dim_masuk' => $dim,
        'created_at' => Carbon::now('+07:00')
    ]);
    if ($request->session()->has('idUserTeknisi')) {
        $input = Session('idUserTeknisi');
    } else {
        $input = Session('idUserPeternakan');
    };
    $notifikasi_id = DB::table('history_event')->insertGetId([
        'kode_cowcard' => $idsapi,
        'activity' => 'No Bred',
        'ket_1' => "LACT : " . strval($LACT),
        'ket_2' => "NbDAT : " . strval($request->tgl_nobred),
        'DIM' => $dim,
        'input_by' => $input,
        'peternakan_id' => Session('idPeternakan'),
        'created_at' => Carbon::now('+07:00')
    ]);
    notifible(Session('idPeternakan'), 2, $notifikasi_id);
}
function setstatusopen(Request $request)
{
    $cowcard = DB::table('cowcard')->where('id', $request->id)->get();
    $dim = 0;
    $LACT = 0;
    $status = 'null';
    $idsapi = 'null';
    foreach ($cowcard as $p) {
        $dim =  $p->DIM;
        $LACT =  $p->LACT;
        $status = $p->Status;
        $idsapi = $p->kode_cowcard;
    }
    status_update($status, $request->id);
    DB::table('cowcard')->where('id', $request->id)->update([
        'Status' => "Open",
        'kandang_id' => $request->selectKandang,
        'PEN' => $request->PEN,
        'updated_at' => Carbon::now('+07:00')
    ]);

    $today = Carbon::now();
    $dtoday = strtotime($today->toDateString());
    $tgl_open = strtotime($request->tgl_open);
    $DOPN = ($dtoday - $tgl_open) / 60 / 60 / 24;
    DB::table('status_open')->insert([
        'sapi_id' => $request->id,
        'tanggal' => $request->tgl_open,
        'DOPN' => $DOPN,
        'ABDAT' => $request->ABDAT,
        'status' => "Active",
        'LACT' => $LACT,
        'dim_masuk' => $dim,
        'created_at' => Carbon::now('+07:00')
    ]);
    if ($request->session()->has('idUserTeknisi')) {
        $input = Session('idUserTeknisi');
    } else {
        $input = Session('idUserPeternakan');
    };
    $notifikasi_id = DB::table('history_event')->insertGetId([
        'kode_cowcard' => $idsapi,
        'activity' => 'Open',
        'ket_1' => "LACT : " . strval($LACT),
        'ket_2' => "OpDAT : " . strval($request->tgl_open),
        'ket_3' => "DOPN : " . strval($DOPN),
        'ket_4' => "ABDAT : " . strval($request->ABDAT),
        'DIM' => $dim,
        'peternakan_id' => Session('idPeternakan'),
        'input_by' => $input,
        'created_at' => Carbon::now('+07:00')
    ]);
    notifible(Session('idPeternakan'), 2, $notifikasi_id);
}
function setstatuspreg(Request $request)
{
    $cowcard = DB::table('cowcard')->where('id', $request->id)->get();
    $dim = 0;
    $LACT = 0;
    $status = 'null';
    $idsapi = 'null';
    foreach ($cowcard as $p) {
        $dim =  $p->DIM;
        $LACT =  $p->LACT;
        $status = $p->Status;
        $idsapi = $p->kode_cowcard;
    }
    status_update($status, $request->id);
    $id = $request->id;
    DB::table('cowcard')->where('id', $request->id)->update([
        'Status' => "Preg",
        'kandang_id' => $request->selectKandang,
        'PEN' => $request->PEN,
        'updated_at' => Carbon::now('+07:00')
    ]);
    $today = Carbon::now();
    $dtoday = strtotime($today->toDateString());
    $tgl_preg = strtotime($request->tgl_preg);
    $tgl_bred = strtotime($request->bred);
    $DCCD = ($dtoday - $tgl_preg) / 60 / 60 / 24;
    $DCC = ($tgl_preg - $tgl_bred) / 60 / 60 / 24;
    $DSLH = ($dtoday - $tgl_bred) / 60 / 60 / 24;
    DB::table('status_preg')->insert([
        'sapi_id' => $request->id,
        'tanggal' => $request->tgl_preg,
        'DCC' => $DCC,
        'DCCD' => $DCCD,
        'SYNC' => $request->SYNC,
        'SDESC' => $request->SDESC,
        'status' => "Active",
        'LACT' => $LACT,
        'dim_masuk' => $dim,
        'created_at' => Carbon::now('+07:00')
    ]);
    if ($request->session()->has('idUserTeknisi')) {
        $input = Session('idUserTeknisi');
    } else {
        $input = Session('idUserPeternakan');
    };
    $notifikasi_id = DB::table('history_event')->insertGetId([
        'kode_cowcard' => $idsapi,
        'activity' => 'Preg',
        'ket_1' => "LACT : " . strval($LACT),
        'ket_2' => "PrDAT : " . strval($request->tgl_preg),
        'ket_3' => "DCC : " . strval($DCC),
        'ket_4' => "DCCD : " . strval($DCCD),
        'ket_5' => "SYNC : " . strval($request->SYNC),
        'DIM' => $dim,
        'peternakan_id' => Session('idPeternakan'),
        'input_by' => $input,
        'created_at' => Carbon::now('+07:00')
    ]);
    notifible(Session('idPeternakan'), 2, $notifikasi_id);
}
function programresult($id)
{
    $query = DB::table('program')->where('id', $id)->first();
    if ($query) {
        $var1 = $query->program_1 ? ExplodeVariable($query->program_1) : null;
        $var2 = $query->program_2 ? ExplodeVariable($query->program_2) : null;
        $var3 = $query->program_3 ? ExplodeVariable($query->program_3) : null;
        $var4 = $query->program_4 ? ExplodeVariable($query->program_4) : null;
        $var5 = $query->program_5 ? ExplodeVariable($query->program_5) : null;
        $var6 = $query->program_6 ? ExplodeVariable($query->program_6) : null;
        $var7 = $query->program_7 ? ExplodeVariable($query->program_7) : null;

        $op1 = $query->program_1 ? ExplodeOperator($query->program_1) : null;
        $op2 = $query->program_2 ? ExplodeOperator($query->program_2) : null;
        $op3 = $query->program_3 ? ExplodeOperator($query->program_3) : null;
        $op4 = $query->program_4 ? ExplodeOperator($query->program_4) : null;
        $op5 = $query->program_5 ? ExplodeOperator($query->program_5) : null;
        $op6 = $query->program_6 ? ExplodeOperator($query->program_6) : null;
        $op7 = $query->program_7 ? ExplodeOperator($query->program_7) : null;

        $nilai1 = $query->program_1 ? ExplodeNilai($query->program_1) : null;
        $nilai2 = $query->program_2 ? ExplodeNilai($query->program_2) : null;
        $nilai3 = $query->program_3 ? ExplodeNilai($query->program_3) : null;
        $nilai4 = $query->program_4 ? ExplodeNilai($query->program_4) : null;
        $nilai5 = $query->program_5 ? ExplodeNilai($query->program_5) : null;
        $nilai6 = $query->program_6 ? ExplodeNilai($query->program_6) : null;
        $nilai7 = $query->program_7 ? ExplodeNilai($query->program_7) : null;
        $data = DB::table('cowcard')
            ->select(
                'cowcard.kode_cowcard',
                'cowcard.PEN',
                'cowcard.SEX',
                'cowcard.DIM',
                'cowcard.LACT',
                'cowcard.Status',
                'status_bred.CDAT',
                'status_bred.TBRD',
                'status_dry.DUDRY',
                'status_dry.DDRY',
                'status_fresh.EASE',
                'status_open.DOPN',
                'status_open.ABDAT',
                'status_preg.DCC',
                'status_preg.DCCD',
                'status_preg.HDAT',
                'status_preg.DSLH',
                'status_preg.SYNC',
                'status_preg.SDESC',
                'status_vaksin.nama_vaksin',
            )
            ->leftjoin('kandang', 'kandang.id', '=', 'cowcard.kandang_id')
            ->leftjoin('status_bred', 'status_bred.sapi_id', '=', 'cowcard.id')
            ->leftjoin('status_dry', 'status_dry.sapi_id', '=', 'cowcard.id')
            ->leftjoin('status_fresh', 'status_fresh.sapi_id', '=', 'cowcard.id')
            ->leftjoin('status_nobred', 'status_nobred.sapi_id', '=', 'cowcard.id')
            ->leftjoin('status_open', 'status_open.sapi_id', '=', 'cowcard.id')
            ->leftjoin('status_preg', 'status_preg.sapi_id', '=', 'cowcard.id')
            ->leftjoin('status_vaksin', 'status_vaksin.sapi_id', '=', 'cowcard.id')
            ->where('kandang.peternakan_id', Session('idPeternakan'))
            ->where([
                [$var1, $op1, $nilai1],
                [$var2, $op2, $nilai2],
                [$var3, $op3, $nilai3],
                [$var4, $op4, $nilai4],
                [$var5, $op5, $nilai5],
                [$var6, $op6, $nilai6],
                [$var7, $op7, $nilai7],
            ])
            ->distinct()
            ->get();
        $data->map(function ($check) {

            if ($check->kode_cowcard == null) {
                $check->kode_cowcard = "Tidak Ada Kode Sapi";
            } else {
                $check->kode_cowcard;
            }
            if ($check->PEN == null) {
                $check->PEN = "PEN Kosong";
            } else {
                $check->PEN;
            }
            if ($check->DIM == null) {
                $check->DIM = "0";
            } else {
                $check->DIM;
            }
            if ($check->LACT == null) {
                $check->LACT = "0";
            } else {
                $check->LACT;
            }
            if ($check->TBRD == null) {
                $check->TBRD = "0";
            } else {
                $check->TBRD;
            }
            if ($check->DUDRY == null) {
                $check->DUDRY = "0";
            } else {
                $check->DUDRY;
            }
            if ($check->DDRY == null) {
                $check->DDRY = "0";
            } else {
                $check->DDRY;
            }
            if ($check->EASE == null) {
                $check->EASE = "0";
            } else {
                $check->EASE;
            }
            if ($check->DOPN == null) {
                $check->DOPN = "0";
            } else {
                $check->DOPN;
            }
            if ($check->DCC == null) {
                $check->DCC = "0";
            } else {
                $check->DCC;
            }
            if ($check->DCCD == null) {
                $check->DCCD = "0";
            } else {
                $check->DCCD;
            }
            if ($check->DSLH == null) {
                $check->DSLH = "0";
            } else {
                $check->DSLH;
            }
            if ($check->SYNC == null) {
                $check->SYNC = "0";
            } else {
                $check->SYNC;
            }
            if ($check->SDESC == null) {
                $check->SDESC = "0";
            } else {
                $check->SDESC;
            }
            if ($check->nama_vaksin == null) {
                $check->nama_vaksin = "Belum Vaksin";
            } else {
                $check->nama_vaksin;
            }
            if ($check->Status == null) {
                $check->Status = "Tidak ada Status";
            } else {
                $check->Status;
            }
            //DATE
            if ($check->CDAT == null) {
                $check->CDAT = "Tidak ada Tanggal";
            } else {
                $check->CDAT;
            }
            if ($check->ABDAT == null) {
                $check->ABDAT = "Tidak ada Tanggal";
            } else {
                $check->ABDAT;
            }
            if ($check->HDAT == null) {
                $check->HDAT = "Tidak ada Tanggal";
            } else {
                $check->HDAT;
            }
            return $check;
        });
        return $data;
    }
}
function durasi($in, $out)
{
    $begin = new DateTime($in);
    $end = new DateTime($out);
    $diff = $begin->diff($end);
    return $diff->format("%y tahun %m bulan %d hari");
}
function durasi_now($out)
{
    $begin = new DateTime();
    $end = new DateTime($out);
    $diff = $begin->diff($end);
    if($diff->format("%i") == 0){
        return "Baru Saja";
    }elseif($diff->format("%d") == 0 &&  $diff->format("%h") == 0 ){
        return $diff->format("%i menit");
    }elseif($diff->format("%d") == 0 && $diff->format("%h") > 0){
        return $diff->format(" %h jam %i menit");
    }else{
        return $diff->format("%d hari %h jam %i menit");
    }
}

function pemeriksa($iduser)
{
    $name  = DB::table('users')
        ->where('id', $iduser)
        ->value('name');
    $roles  = DB::table('users')
        ->where('id', $iduser)
        ->value('roles');
    return $name . " (" . $roles . ")";
}

function countSapiByKandang($id)
{
    $result = DB::table('cowcard')
        ->where('kandang_id', $id)
        ->count();
    return $result;
}

function DIMSYnc($id)
{
    $temp = false;
    $Dim = DB::table('program')
        ->where('id', $id)
        ->get();
    //GET DIM
    foreach ($Dim as $get) {
        if (isset($get->program_1)) {
            $set1 = explode(" ", $get->program_1);
            if ($set1[0] == "DIM") {
                $temp = true;
            };
        };
        if (isset($get->program_2)) {
            $set2 = explode(" ", $get->program_2);
            if ($set2[0] == "DIM") {
                $temp = true;
            };
        };
        if (isset($get->program_3)) {
            $set3 = explode(" ", $get->program_3);
            if ($set3[0] == "DIM") {
                $temp = true;
            };
        };
        if (isset($get->program_4)) {
            $set4 = explode(" ", $get->program_4);
            if ($set4[0] == "DIM") {
                $temp = true;
            };
        };
        if (isset($get->program_5)) {
            $set5 = explode(" ", $get->program_5);
            if ($set5[0] == "DIM") {
                $temp = true;
            };
        };
    }
    return $temp;
}
function DatesIdn($dates)
{
    if ($dates == '' or $dates == null or $dates == '0000-00-00') {
        return $dates;
    } else {
        try {
            $dates = Carbon::parse($dates)->format('d F Y ,H:i:s');
            return $dates;
        } catch (\Exception $error) {
            return "-";
        }
    }
}



/* #region Get List Kandang */
function ListKandang($id)
{
    $kandang = DB::table('kandang')
        ->where('peternakan_id', $id)
        ->get();
    return $kandang;
}
/* #endregion */
