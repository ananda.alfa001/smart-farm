<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class ProgramExport implements FromCollection, WithHeadings, WithColumnWidths
{
    protected $id;

    function __construct($id) {
            $this->id = $id;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return programresult($this->id);
    }

    public function headings(): array
    {
        return ["KODE SAPI","PEN","JENIS KELAMIN","DIM","LACT","CDAT","TBRD","DUDRY","DDRY","EASE","DOPN","ABDAT","DCC","DCCD","HDAT","DSLH","SYNC","SDESC","PERVENTIF TERAKHIR","STATUS"];

    }
    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'C' => 20,
            'S' => 25,
            'T' => 20,
            'F' => 18,
            'L' => 18,
            'O' => 18,
        ];
    }
}
