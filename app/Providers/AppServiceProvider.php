<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Session;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Jakarta');

        View::composer('*', function($view)
        {
            $all = 0;
            $sessid = null;
            if(Session::has('idPeternakan')){
                $sessid = Session('idPeternakan');
            }elseif(Session::has('your_key')){
                $sessid = Session('idTeknisi');
            }elseif(Session::has('idKemitraan')){
                $sessid = Session('idKemitraan');
            }
            $notifible_count = DB::table('notifible')
                ->where('roles_target_id', $sessid)
                ->where('read',0)
                ->get();
            foreach($notifible_count as $value){
                foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $notif){
                    if ($value->type_notif == '1'){
                        foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $a){
                            $all++;
                        }
                    }elseif($value->type_notif == '2'){
                        foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $b){
                            $all++;
                        }
                    }elseif($value->type_notif == '3'){
                        foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $c){
                            $all++;
                        }
                    }
                }
            }
            $notifible = DB::table('notifible')
                ->where('roles_target_id', $sessid)
                ->where('read',0)
                ->orderBy('created_at', 'DESC')
                ->paginate(4);
            $view->with('notifible', $notifible)->with('all', $all );
        });

    }
}
