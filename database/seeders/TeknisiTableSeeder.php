<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TeknisiTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('teknisi')->delete();
        
        \DB::table('teknisi')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 4,
                'nama_instansi' => 'Ananda Putra Alfa Robby',
                'profesi' => 'Teknisi',
                'activity' => NULL,
                'tgl_lahir' => '2022-07-14',
                'alamat' => '081337001001',
                'no_hp' => '85234005',
                'lvl' => NULL,
                'created_at' => '2022-07-22 00:21:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 5,
                'user_id' => 8,
                'nama_instansi' => 'PT Dhanistha Surya Pertiwi',
                'profesi' => 'Dokter Hewan',
                'activity' => NULL,
                'tgl_lahir' => '1991-03-15',
                'alamat' => 'Kota Malang',
                'no_hp' => '081332431539',
                'lvl' => NULL,
                'created_at' => '2022-08-07 13:02:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 6,
                'user_id' => 11,
                'nama_instansi' => 'FSGR Dairy Farm',
                'profesi' => 'Programmer',
                'activity' => NULL,
                'tgl_lahir' => '1998-01-31',
                'alamat' => 'Surabaya, Jawa Timur',
                'no_hp' => '085850508387',
                'lvl' => NULL,
                'created_at' => '2022-08-15 12:23:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 7,
                'user_id' => 19,
                'nama_instansi' => 'GDA',
                'profesi' => 'Dokter',
                'activity' => NULL,
                'tgl_lahir' => '2022-08-26',
                'alamat' => 'Jakarta',
                'no_hp' => '081337001001',
                'lvl' => NULL,
                'created_at' => '2022-08-26 16:44:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 8,
                'user_id' => 23,
                'nama_instansi' => 'Bintang vet',
                'profesi' => 'Doktee hewan praktisi',
                'activity' => NULL,
                'tgl_lahir' => '1992-01-24',
                'alamat' => 'Perum maiya 2 residence b9 pekalongan',
                'no_hp' => '08562523549',
                'lvl' => NULL,
                'created_at' => '2022-08-31 08:12:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 9,
                'user_id' => 24,
                'nama_instansi' => 'dinas pertanian',
                'profesi' => 'dokter hewan',
                'activity' => NULL,
                'tgl_lahir' => '1983-02-22',
                'alamat' => 'jl merdeka by pass koba bangka tengah',
                'no_hp' => '085267670989',
                'lvl' => NULL,
                'created_at' => '2022-09-06 12:05:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 10,
                'user_id' => 30,
                'nama_instansi' => 'Dairy farm',
                'profesi' => 'Paramedis',
                'activity' => NULL,
                'tgl_lahir' => '1989-09-20',
                'alamat' => 'Kp.bantargadung ,kab sukabumi',
                'no_hp' => '082113170466',
                'lvl' => NULL,
                'created_at' => '2022-09-13 19:29:23',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 11,
                'user_id' => 33,
                'nama_instansi' => 'Ananda Putra Alfa Robby',
                'profesi' => 'Dokter',
                'activity' => NULL,
                'tgl_lahir' => '2022-10-05',
                'alamat' => '081337001001',
                'no_hp' => '081337001001',
                'lvl' => NULL,
                'created_at' => '2022-10-16 13:57:10',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}