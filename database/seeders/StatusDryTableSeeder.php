<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StatusDryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('status_dry')->delete();
        
        \DB::table('status_dry')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sapi_id' => 21,
                'tanggal' => '2022-08-31',
                'DUDRY' => '2022-06-12',
                'DDRY' => 80,
                'status' => 'Deactive',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 11:54:48',
                'updated_at' => '2022-08-31 11:56:26',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'sapi_id' => 21,
                'tanggal' => '2022-08-31',
                'DUDRY' => '2022-06-07',
                'DDRY' => 85,
                'status' => 'Deactive',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 11:56:48',
                'updated_at' => '2022-09-29 10:54:37',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'sapi_id' => 19,
                'tanggal' => '2022-08-31',
                'DUDRY' => '2022-06-19',
                'DDRY' => 73,
                'status' => 'Active',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 11:59:25',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'sapi_id' => 13,
                'tanggal' => '2022-08-31',
                'DUDRY' => '2022-07-17',
                'DDRY' => 45,
                'status' => 'Active',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 12:21:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'sapi_id' => 15,
                'tanggal' => '2022-08-31',
                'DUDRY' => '2022-08-31',
                'DDRY' => 0,
                'status' => 'Active',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 16:45:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'sapi_id' => 28,
                'tanggal' => '2022-08-31',
                'DUDRY' => '2022-08-31',
                'DDRY' => 0,
                'status' => 'Active',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 17:21:34',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'sapi_id' => 24,
                'tanggal' => '2022-09-01',
                'DUDRY' => '2022-09-01',
                'DDRY' => 0,
                'status' => 'Deactive',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-09-01 06:41:42',
                'updated_at' => '2022-09-01 06:45:29',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'sapi_id' => 24,
                'tanggal' => '2022-09-01',
                'DUDRY' => '2022-09-01',
                'DDRY' => 0,
                'status' => 'Deactive',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-09-01 06:45:29',
                'updated_at' => '2022-09-11 16:21:16',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'sapi_id' => 23,
                'tanggal' => '2022-09-01',
                'DUDRY' => '2022-09-01',
                'DDRY' => 0,
                'status' => 'Deactive',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-09-01 06:45:54',
                'updated_at' => '2022-09-11 16:19:30',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}