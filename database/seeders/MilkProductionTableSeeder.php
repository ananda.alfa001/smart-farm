<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MilkProductionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('milk_production')->delete();
        
        \DB::table('milk_production')->insert(array (
            0 => 
            array (
                'id' => 6,
                'peternakan_id' => 1,
                'production' => 12,
                'ADM' => 0.0,
                'input_by' => NULL,
                'created_at' => '2022-08-10 04:47:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 7,
                'peternakan_id' => 1,
                'production' => 11,
                'ADM' => 0.0,
                'input_by' => NULL,
                'created_at' => '2022-08-11 22:11:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 8,
                'peternakan_id' => 11,
                'production' => 20,
                'ADM' => 20.0,
                'input_by' => NULL,
                'created_at' => '2022-08-26 17:14:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 9,
                'peternakan_id' => 2,
                'production' => 35,
                'ADM' => 35.0,
                'input_by' => NULL,
                'created_at' => '2022-08-30 19:10:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 10,
                'peternakan_id' => 1,
                'production' => 45,
                'ADM' => 0.0,
                'input_by' => NULL,
                'created_at' => '2022-08-31 20:06:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 11,
                'peternakan_id' => 11,
                'production' => 67,
                'ADM' => 67.0,
                'input_by' => NULL,
                'created_at' => '2022-09-01 11:15:28',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 12,
                'peternakan_id' => 12,
                'production' => 75,
                'ADM' => 15.0,
                'input_by' => NULL,
                'created_at' => '2022-09-01 15:40:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 13,
                'peternakan_id' => 12,
                'production' => 75,
                'ADM' => 15.0,
                'input_by' => NULL,
                'created_at' => '2022-09-04 18:26:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 14,
                'peternakan_id' => 2,
                'production' => 24,
                'ADM' => 4.0,
                'input_by' => NULL,
                'created_at' => '2022-09-04 19:27:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 15,
                'peternakan_id' => 12,
                'production' => 70,
                'ADM' => 14.0,
                'input_by' => NULL,
                'created_at' => '2022-09-07 08:56:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 16,
                'peternakan_id' => 12,
                'production' => 70,
                'ADM' => 7.7777777777778,
                'input_by' => NULL,
                'created_at' => '2022-09-11 16:25:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 17,
                'peternakan_id' => 12,
                'production' => 95,
                'ADM' => 10.555555555556,
                'input_by' => NULL,
                'created_at' => '2022-09-16 05:53:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 18,
                'peternakan_id' => 5,
                'production' => 2,
                'ADM' => 1.0,
                'input_by' => NULL,
                'created_at' => '2022-09-16 17:15:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 19,
                'peternakan_id' => 12,
                'production' => 105,
                'ADM' => 11.666666666667,
                'input_by' => NULL,
                'created_at' => '2022-09-18 05:44:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 20,
                'peternakan_id' => 14,
                'production' => 20,
                'ADM' => 20.0,
                'input_by' => NULL,
                'created_at' => '2022-09-21 17:53:20',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 21,
                'peternakan_id' => 13,
                'production' => 90,
                'ADM' => 3.3333333333333,
                'input_by' => NULL,
                'created_at' => '2022-09-28 10:34:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 22,
                'peternakan_id' => 12,
                'production' => 115,
                'ADM' => 11.5,
                'input_by' => NULL,
                'created_at' => '2022-10-08 16:14:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}