<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DmiTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('dmi')->delete();
        
        \DB::table('dmi')->insert(array (
            0 => 
            array (
                'id' => 1,
                'peternakan_id' => 10,
                'sapi_id' => 27,
                'DMI' => 6,
                'pakan' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-09 18:27:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'peternakan_id' => 12,
                'sapi_id' => 24,
                'DMI' => 20,
                'pakan' => 'Fresh',
                'input_by' => NULL,
                'created_at' => '2022-09-11 16:25:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}