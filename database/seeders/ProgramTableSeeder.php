<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProgramTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('program')->delete();
        
        \DB::table('program')->insert(array (
            0 => 
            array (
                'id' => 1,
                'peternakan_id' => 11,
                'namaprogram' => 'PROGRAM DRY',
                'previous_program' => NULL,
                'program_1' => 'LACT >= 0',
                'program_2' => 'LACT < 10',
                'program_3' => 'PEN = b',
                'program_4' => 'DCC > 213',
                'program_5' => 'DCC < 225',
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-26 17:03:40',
                'updated_at' => NULL,
                'deleted_at' => '2022-08-26 17:06:42',
            ),
            1 => 
            array (
                'id' => 2,
                'peternakan_id' => 11,
                'namaprogram' => 'PROGRAM DRY 2',
                'previous_program' => NULL,
                'program_1' => 'LACT > 0',
                'program_2' => 'DCC > 190',
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-26 17:06:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'peternakan_id' => 11,
                'namaprogram' => 'PROGRAM DRY 3',
                'previous_program' => NULL,
                'program_1' => 'LACT < 10',
                'program_2' => 'DCC > 180',
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-26 17:09:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'peternakan_id' => 1,
                'namaprogram' => 'Ad',
                'previous_program' => NULL,
                'program_1' => 'DIM > 18',
                'program_2' => NULL,
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-28 17:08:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'peternakan_id' => 12,
                'namaprogram' => 'dim',
                'previous_program' => NULL,
                'program_1' => 'DIM >= 60',
                'program_2' => NULL,
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 16:16:56',
                'updated_at' => NULL,
                'deleted_at' => '2022-08-31 16:17:35',
            ),
            5 => 
            array (
                'id' => 6,
                'peternakan_id' => 12,
                'namaprogram' => 'laktasi',
                'previous_program' => NULL,
                'program_1' => 'DIM >= 45',
                'program_2' => 'STATUS = Open',
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 16:18:54',
                'updated_at' => NULL,
                'deleted_at' => '2022-08-31 16:19:09',
            ),
            6 => 
            array (
                'id' => 7,
                'peternakan_id' => 12,
                'namaprogram' => 'laktasi',
                'previous_program' => NULL,
                'program_1' => 'DIM >= 10',
                'program_2' => NULL,
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 16:19:52',
                'updated_at' => NULL,
                'deleted_at' => '2022-08-31 16:20:25',
            ),
            7 => 
            array (
                'id' => 8,
                'peternakan_id' => 12,
                'namaprogram' => 'laktasi',
                'previous_program' => NULL,
                'program_1' => 'DCC > 90',
                'program_2' => NULL,
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 16:21:12',
                'updated_at' => NULL,
                'deleted_at' => '2022-08-31 16:21:42',
            ),
            8 => 
            array (
                'id' => 9,
                'peternakan_id' => 12,
                'namaprogram' => '7',
                'previous_program' => NULL,
                'program_1' => 'DCCD < 1',
                'program_2' => NULL,
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 16:53:28',
                'updated_at' => NULL,
                'deleted_at' => '2022-08-31 16:53:36',
            ),
            9 => 
            array (
                'id' => 10,
                'peternakan_id' => 13,
                'namaprogram' => 'dry',
                'previous_program' => NULL,
                'program_1' => 'LACT > 220',
                'program_2' => NULL,
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-09-09 20:33:48',
                'updated_at' => NULL,
                'deleted_at' => '2022-09-09 20:34:07',
            ),
            10 => 
            array (
                'id' => 11,
                'peternakan_id' => 5,
                'namaprogram' => '12',
                'previous_program' => NULL,
                'program_1' => 'DIM < 22',
                'program_2' => NULL,
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-09-16 17:16:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'peternakan_id' => 14,
                'namaprogram' => 'bunting',
                'previous_program' => NULL,
                'program_1' => 'STATUS = Preg',
                'program_2' => NULL,
                'program_3' => NULL,
                'program_4' => NULL,
                'program_5' => NULL,
                'program_6' => NULL,
                'program_7' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-09-21 17:26:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}