<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class KandangTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('kandang')->delete();
        
        \DB::table('kandang')->insert(array (
            0 => 
            array (
                'id' => 1,
                'peternakan_id' => 1,
                'nama_kandang' => 'Barn 1',
                'created_at' => '2022-03-09 22:01:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'peternakan_id' => 1,
                'nama_kandang' => 'Barn 2',
                'created_at' => '2022-03-09 22:01:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'peternakan_id' => 2,
                'nama_kandang' => 'Kandang produksi',
                'created_at' => '2022-07-18 09:58:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'peternakan_id' => 3,
                'nama_kandang' => 'Pen 5A1',
                'created_at' => '2022-08-02 16:56:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'peternakan_id' => 3,
                'nama_kandang' => 'Pen W',
                'created_at' => '2022-08-02 16:56:19',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'peternakan_id' => 4,
                'nama_kandang' => 'Pule',
                'created_at' => '2022-08-15 09:13:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'peternakan_id' => 5,
                'nama_kandang' => 'Cenamat',
                'created_at' => '2022-08-15 12:11:59',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'peternakan_id' => 6,
                'nama_kandang' => 'Imamsufii',
                'created_at' => '2022-08-15 15:36:17',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'peternakan_id' => 7,
                'nama_kandang' => 'Imamsufii',
                'created_at' => '2022-08-15 15:37:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'peternakan_id' => 8,
                'nama_kandang' => 'Purwotengah',
                'created_at' => '2022-08-16 12:00:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'peternakan_id' => 8,
                'nama_kandang' => NULL,
                'created_at' => '2022-08-16 12:00:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'peternakan_id' => 9,
                'nama_kandang' => 'One',
                'created_at' => '2022-08-18 18:01:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'peternakan_id' => 10,
                'nama_kandang' => 'Arif 1',
                'created_at' => '2022-08-26 16:15:05',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'peternakan_id' => 11,
                'nama_kandang' => 'kandang 1',
                'created_at' => '2022-08-26 16:23:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'peternakan_id' => 11,
                'nama_kandang' => 'kandang 2',
                'created_at' => '2022-08-26 16:23:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'peternakan_id' => 11,
                'nama_kandang' => 'kandang 3',
                'created_at' => '2022-08-26 16:23:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'peternakan_id' => 12,
                'nama_kandang' => 'Gigih Iman',
                'created_at' => '2022-08-27 19:29:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'peternakan_id' => 13,
                'nama_kandang' => 'PCF lact',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'peternakan_id' => 13,
                'nama_kandang' => 'PCF dry',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'peternakan_id' => 13,
                'nama_kandang' => 'PCF bull',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'peternakan_id' => 13,
                'nama_kandang' => 'PCF heifer',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'peternakan_id' => 13,
                'nama_kandang' => 'PCF heifer preg',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'peternakan_id' => 13,
                'nama_kandang' => 'Hj Didin',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'peternakan_id' => 13,
                'nama_kandang' => 'Eman',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'peternakan_id' => 13,
                'nama_kandang' => 'Tika',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'peternakan_id' => 13,
                'nama_kandang' => 'Susan',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'peternakan_id' => 13,
                'nama_kandang' => 'Anda',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'peternakan_id' => 13,
                'nama_kandang' => 'Gugun',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'peternakan_id' => 13,
                'nama_kandang' => 'Uba',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'peternakan_id' => 13,
                'nama_kandang' => 'Oyon',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'peternakan_id' => 13,
                'nama_kandang' => 'Jidan',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'peternakan_id' => 13,
                'nama_kandang' => 'Raiz',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'peternakan_id' => 13,
                'nama_kandang' => 'Asep',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'peternakan_id' => 13,
                'nama_kandang' => 'Alya',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'peternakan_id' => 13,
                'nama_kandang' => 'Naripan',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'peternakan_id' => 13,
                'nama_kandang' => 'Supriatna',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'peternakan_id' => 13,
                'nama_kandang' => 'Solihin',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'peternakan_id' => 13,
                'nama_kandang' => 'Yana',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'peternakan_id' => 13,
                'nama_kandang' => 'Wahyudi',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'peternakan_id' => 13,
                'nama_kandang' => 'Kelp. Talaga',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'peternakan_id' => 13,
                'nama_kandang' => 'Kelp. Cimeong Talaga',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'peternakan_id' => 13,
                'nama_kandang' => 'Kelp. Cipari',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'peternakan_id' => 13,
                'nama_kandang' => 'Tasik',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'peternakan_id' => 13,
                'nama_kandang' => 'Wasim',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'peternakan_id' => 13,
                'nama_kandang' => 'H Rahidin',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'peternakan_id' => 13,
                'nama_kandang' => 'Eka',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'peternakan_id' => 13,
                'nama_kandang' => 'Oman',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'peternakan_id' => 13,
                'nama_kandang' => 'Rosid',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'peternakan_id' => 13,
                'nama_kandang' => 'Dani',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'peternakan_id' => 13,
                'nama_kandang' => 'Oyon',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'peternakan_id' => 13,
                'nama_kandang' => 'Deva',
                'created_at' => '2022-08-30 09:44:00',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'peternakan_id' => 14,
                'nama_kandang' => 'aribowo',
                'created_at' => '2022-09-06 12:19:37',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'peternakan_id' => 15,
                'nama_kandang' => '3',
                'created_at' => '2022-09-07 08:22:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'peternakan_id' => 15,
                'nama_kandang' => 'A',
                'created_at' => '2022-09-07 08:22:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'peternakan_id' => 15,
                'nama_kandang' => 'B',
                'created_at' => '2022-09-07 08:22:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'peternakan_id' => 15,
                'nama_kandang' => 'C',
                'created_at' => '2022-09-07 08:22:50',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'peternakan_id' => 16,
                'nama_kandang' => 'Dairy Goat',
                'created_at' => '2022-09-08 15:24:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'peternakan_id' => 16,
                'nama_kandang' => 'Dorper cage',
                'created_at' => '2022-09-08 15:24:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'peternakan_id' => 16,
                'nama_kandang' => 'Texel Cage',
                'created_at' => '2022-09-08 15:24:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'peternakan_id' => 16,
                'nama_kandang' => 'Suffolk cage',
                'created_at' => '2022-09-08 15:24:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'peternakan_id' => 5,
                'nama_kandang' => 'Jozz Mas',
                'created_at' => '2022-09-16 18:03:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'peternakan_id' => 17,
                'nama_kandang' => 'Ananda Putra Alfa Robby',
                'created_at' => '2022-10-16 13:55:53',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}