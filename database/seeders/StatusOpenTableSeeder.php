<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StatusOpenTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('status_open')->delete();
        
        \DB::table('status_open')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sapi_id' => 18,
                'tanggal' => '2022-06-14',
                'DOPN' => 78,
                'ABDAT' => '2022-06-14',
                'status' => 'Deactive',
                'LACT' => 3,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 12:01:46',
                'updated_at' => '2022-08-31 16:49:27',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'sapi_id' => 17,
                'tanggal' => '2022-07-10',
                'DOPN' => 52,
                'ABDAT' => '2022-07-10',
                'status' => 'Deactive',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 12:15:11',
                'updated_at' => '2022-09-16 05:55:41',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'sapi_id' => 18,
                'tanggal' => '2022-08-31',
                'DOPN' => 0,
                'ABDAT' => NULL,
                'status' => 'Active',
                'LACT' => 4,
                'dim_masuk' => 426,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 16:50:02',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'sapi_id' => 61,
                'tanggal' => '2022-03-20',
                'DOPN' => 184,
                'ABDAT' => NULL,
                'status' => 'Active',
                'LACT' => 1,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-09-20 12:30:13',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}