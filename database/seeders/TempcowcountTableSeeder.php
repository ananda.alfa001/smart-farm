<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TempcowcountTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tempcowcount')->delete();
        
        \DB::table('tempcowcount')->insert(array (
            0 => 
            array (
                'id' => 1,
                'peternakan_id' => 14,
                'current' => 0,
                'max' => 1,
                'Status' => 'Preg',
                'input_by' => NULL,
                'created_at' => '2022-09-21 17:54:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}