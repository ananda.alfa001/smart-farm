<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PenyakitTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('penyakit')->delete();
        
        \DB::table('penyakit')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sapi_id' => 2,
                'tglsakit' => '2022-08-26',
                'gejala' => 'Feses keras',
                'diagnosis' => 'Milk Fever',
                'penanganan' => 'B1',
                'LACT' => 3,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 2,
                'created_at' => '2022-08-26 17:16:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'sapi_id' => 2,
                'tglsakit' => '2022-08-26',
                'gejala' => 'Feses keras',
                'diagnosis' => 'Milk Fever',
                'penanganan' => 'B1',
                'LACT' => 3,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 2,
                'created_at' => '2022-08-26 17:16:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'sapi_id' => 5,
                'tglsakit' => '2022-08-26',
                'gejala' => 'Mencret',
                'diagnosis' => 'Milk Fever',
                'penanganan' => 'Poop',
                'LACT' => 3,
                'dim_masuk' => 56,
                'dim_keluar' => NULL,
                'input_by' => 5,
                'created_at' => '2022-08-26 17:22:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'sapi_id' => 4,
                'tglsakit' => '2022-08-31 09:54:58',
                'gejala' => 'Sembuh',
                'diagnosis' => 'Sembuh',
                'penanganan' => 'Sembuh',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 18,
                'created_at' => '2022-08-31 09:54:58',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}