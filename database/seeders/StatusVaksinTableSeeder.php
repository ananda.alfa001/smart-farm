<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StatusVaksinTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('status_vaksin')->delete();
        
        \DB::table('status_vaksin')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sapi_id' => 4,
                'tanggal' => '2022-08-26',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => '2cc',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 7,
                'created_at' => '2022-08-26 16:47:46',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'sapi_id' => 6,
                'tanggal' => '2022-06-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaksin 1',
                'LACT' => 4,
                'dim_masuk' => 108,
                'dim_keluar' => NULL,
                'input_by' => 6,
                'created_at' => '2022-08-27 20:03:51',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'sapi_id' => 6,
                'tanggal' => '2022-07-27',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaksin 2',
                'LACT' => 4,
                'dim_masuk' => 108,
                'dim_keluar' => NULL,
                'input_by' => 6,
                'created_at' => '2022-08-27 20:05:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'sapi_id' => 25,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks1',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 13:06:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'sapi_id' => 24,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 13:07:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'sapi_id' => 23,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks2',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 13:07:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'sapi_id' => 22,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 3,
                'dim_masuk' => 57,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:09:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'sapi_id' => 21,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:10:16',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'sapi_id' => 20,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:10:41',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'sapi_id' => 19,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:11:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'sapi_id' => 18,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 3,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:11:40',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'sapi_id' => 17,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:12:07',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'sapi_id' => 16,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 4,
                'dim_masuk' => 6,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:12:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'sapi_id' => 15,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:13:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'sapi_id' => 14,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 3,
                'dim_masuk' => 59,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:14:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'sapi_id' => 13,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:15:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'sapi_id' => 7,
                'tanggal' => '2022-07-28',
                'nama_vaksin' => 'Aftopor',
                'keterangan' => 'vaks 2',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-08-31 16:15:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'sapi_id' => 24,
                'tanggal' => '2022-09-11',
                'nama_vaksin' => 'Antipiretik',
                'keterangan' => 'post partus',
                'LACT' => 1,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-09-11 16:22:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'sapi_id' => 23,
                'tanggal' => '2022-09-11',
                'nama_vaksin' => 'Pain killer',
                'keterangan' => 'post partus',
                'LACT' => 1,
                'dim_masuk' => 1,
                'dim_keluar' => NULL,
                'input_by' => 20,
                'created_at' => '2022-09-11 16:22:38',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}