<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StatusPregTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('status_preg')->delete();
        
        \DB::table('status_preg')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sapi_id' => 4,
                'tanggal' => '2022-08-26',
                'DCC' => 197,
                'DCCD' => 0,
                'HDAT' => '2022-02-10',
                'DSLH' => 197,
                'SYNC' => '2022-08-26',
                'SDESC' => 'SYNC',
                'status' => 'Deactive',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-26 16:33:45',
                'updated_at' => '2022-08-26 16:53:05',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'sapi_id' => 24,
                'tanggal' => '2022-03-01',
                'DCC' => 80,
                'DCCD' => 183,
                'HDAT' => '2021-12-11',
                'DSLH' => 263,
                'SYNC' => '2021-12-11',
                'SDESC' => 'no synch',
                'status' => 'Deactive',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 11:48:23',
                'updated_at' => '2022-09-01 06:22:58',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'sapi_id' => 23,
                'tanggal' => '2022-02-01',
                'DCC' => 51,
                'DCCD' => 211,
                'HDAT' => '2021-12-12',
                'DSLH' => 262,
                'SYNC' => '2021-12-12',
                'SDESC' => 'no sinkron',
                'status' => 'Deactive',
                'LACT' => 0,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 11:49:37',
                'updated_at' => '2022-09-01 06:23:21',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'sapi_id' => 21,
                'tanggal' => '2022-03-12',
                'DCC' => 90,
                'DCCD' => 172,
                'HDAT' => '2021-12-12',
                'DSLH' => 262,
                'SYNC' => '2021-12-12',
                'SDESC' => 'no sync',
                'status' => 'Deactive',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 11:56:26',
                'updated_at' => '2022-08-31 11:56:48',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'sapi_id' => 20,
                'tanggal' => '2022-04-25',
                'DCC' => 59,
                'DCCD' => 128,
                'HDAT' => '2022-02-25',
                'DSLH' => 187,
                'SYNC' => '2022-02-25',
                'SDESC' => 'no syn',
                'status' => 'Active',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 11:57:49',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'sapi_id' => 19,
                'tanggal' => '2022-03-19',
                'DCC' => 59,
                'DCCD' => 165,
                'HDAT' => '2022-01-19',
                'DSLH' => 224,
                'SYNC' => '2022-01-19',
                'SDESC' => 'ni',
                'status' => 'Deactive',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 11:58:58',
                'updated_at' => '2022-08-31 11:59:25',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'sapi_id' => 15,
                'tanggal' => '2022-06-06',
                'DCC' => 730546,
                'DCCD' => 86,
                'HDAT' => '2022-04-06',
                'DSLH' => 730632,
                'SYNC' => '2022-04-06',
                'SDESC' => 'no',
                'status' => 'Deactive',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 12:17:29',
                'updated_at' => '2022-08-31 16:45:03',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'sapi_id' => 13,
                'tanggal' => '2022-02-17',
                'DCC' => 62,
                'DCCD' => 195,
                'HDAT' => '2021-12-17',
                'DSLH' => 257,
                'SYNC' => '2021-12-17',
                'SDESC' => 'no',
                'status' => 'Deactive',
                'LACT' => 2,
                'dim_masuk' => 0,
                'dim_keluar' => NULL,
                'input_by' => NULL,
                'created_at' => '2022-08-31 12:21:27',
                'updated_at' => '2022-08-31 12:21:57',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}