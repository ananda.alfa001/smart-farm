<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UsersTableSeeder::class);
        // $this->call(PeternakanTableSeeder::class);
        // $this->call(MitraTableSeeder::class);
        // $this->call(KandangTableSeeder::class);
        // $this->call(TeknisiTableSeeder::class);
        // $this->call(CowCardTableSeeder::class);
        // $this->call(SyncronTableSeeder::class);
        // $this->call(DeathTableSeeder::class);
        // $this->call(DmiTableSeeder::class);
        // $this->call(HistoryEventTableSeeder::class);
        // $this->call(HistoryInspectTableSeeder::class);
        // $this->call(MilkProductionTableSeeder::class);
        // $this->call(PenyakitTableSeeder::class);
        // $this->call(ProgramTableSeeder::class);
        // $this->call(SoldTableSeeder::class);
        // $this->call(StatusBredTableSeeder::class);
        // $this->call(StatusDryTableSeeder::class);
        // $this->call(StatusFreshTableSeeder::class);
        // $this->call(StatusNobredTableSeeder::class);
        // $this->call(StatusOpenTableSeeder::class);
        // $this->call(StatusPregTableSeeder::class);
        // $this->call(StatusVaksinTableSeeder::class);
        // $this->call(TempcowcountTableSeeder::class);
    }
}
