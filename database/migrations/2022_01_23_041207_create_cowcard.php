<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCowCard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cowcard', function (Blueprint $table) {
            $table->id()->index('id');
            $table->string('kode_cowcard')->index('kode_cowcard');
            $table->unsignedBigInteger('kandang_id')->index('kandang_id');
            $table->string('PEN')->index('PEN');
            $table->date('tanggal_lahir');
            $table->string('CALF')->index('CALF');
            $table->string('SEX')->index('SEX');
            $table->integer('LACT')->index('LACT');
            $table->string('Status')->index('Status');
            $table->string('status_vaksin')->default('Belum Vaksin')->index('status_vaksin');
            $table->integer('DIM')->nullable()->index('DIM');
            $table->integer('DMI')->nullable()->index('DMI');
            $table->integer('DIH')->nullable()->index('DIH');
            $table->string('berat')->nullable()->index('berat');
            $table->string('sakit')->nullable()->index('sakit');
            $table->string('pakan')->nullable()->index('pakan');
            $table->unsignedBigInteger('input_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
             * Run the Reference.
             *
             * @return void
             */
            $table->foreign('kandang_id')->references('id')->on('kandang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cowcard');
    }
}
