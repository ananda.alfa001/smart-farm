<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeternakan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peternakan', function (Blueprint $table) {
            $table->id()->index('id');
            $table->unsignedBigInteger('user_id');
            $table->string('nama_peternakan');
            $table->date('tahun_berdiri');
            $table->string('nama_pemilik');
            $table->string('lembar_persetujuan')->nullable();
            $table->string('alamat');
            $table->string('no_hp');
            $table->integer('limit')->default(2);
            $table->string('token')->nullable();
            $table->string('token_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
             * Run the Reference.
             *
             * @return void
             */
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peternakan');
    }
}
