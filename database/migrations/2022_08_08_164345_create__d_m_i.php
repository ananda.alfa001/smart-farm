<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDMI extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dmi', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('peternakan_id');
            $table->unsignedBigInteger('sapi_id');
            $table->integer('DMI');
            $table->string('pakan')->nullable();
            $table->unsignedBigInteger('input_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
             * Run the Reference.
             *
             * @return void
             */
            $table->foreign('peternakan_id')->references('id')->on('peternakan')->onDelete('cascade');
            $table->foreign('sapi_id')->references('id')->on('cowcard')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dmi');
    }
}
