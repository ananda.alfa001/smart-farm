<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKandangPeternakan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kandang', function (Blueprint $table) {
            $table->id()->index('id');
            $table->unsignedBigInteger('peternakan_id');
            $table->string('nama_kandang')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
             * Run the Reference.
             *
             * @return void
             */
            $table->foreign('peternakan_id')->references('id')->on('peternakan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kandang');
    }
}
