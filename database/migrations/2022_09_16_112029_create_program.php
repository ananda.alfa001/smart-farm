<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('peternakan_id')->index();
            $table->string('namaprogram');
            $table->string('previous_program')->nullable();
            $table->string('program_1')->nullable();
            $table->string('program_2')->nullable();
            $table->string('program_3')->nullable();
            $table->string('program_4')->nullable();
            $table->string('program_5')->nullable();
            $table->string('program_6')->nullable();
            $table->string('program_7')->nullable();
            $table->unsignedBigInteger('input_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
             * Run the Reference.
             *
             * @return void
             */
            $table->foreign('peternakan_id')->references('id')->on('peternakan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program');
    }
}
