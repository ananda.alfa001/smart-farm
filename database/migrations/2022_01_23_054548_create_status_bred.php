<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusBred extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_bred', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sapi_id')->index('sapi_id');
            $table->date('tanggal');
            $table->date('CDAT')->index('CDAT');
            $table->integer('TBRD')->index('TBRD');
            $table->string('status_bred')->index('status_bred');
            $table->string('status')->index('status');
            $table->integer('LACT')->index('LACT');
            $table->integer('dim_masuk')->nullable();
            $table->integer('dim_keluar')->nullable();
            $table->unsignedBigInteger('input_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
             * Run the Reference.
             *
             * @return void
             */
            $table->foreign('sapi_id')->references('id')->on('cowcard')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_bred');
    }
}
