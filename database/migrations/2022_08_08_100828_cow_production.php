<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CowProduction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('milk_production', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('peternakan_id');
            $table->integer('production');
            $table->double('ADM');
            $table->unsignedBigInteger('input_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
             * Run the Reference.
             *
             * @return void
             */
            $table->foreign('peternakan_id')->references('id')->on('peternakan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('milk_production');
    }
}
