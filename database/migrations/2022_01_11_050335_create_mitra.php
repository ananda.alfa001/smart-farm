<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMitra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mitra', function (Blueprint $table) {
            $table->id()->index('id');
            $table->unsignedBigInteger('user_id');
            $table->string('nama_instansi');
            $table->string('alamat');
            $table->string('no_telp')->nullable();
            $table->string('skala');
            $table->string('file_notaris')->nullable();
            $table->string('file_identitas')->nullable();
            $table->string('file_pernyataan')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
             * Run the Reference.
             *
             * @return void
             */
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mitra');
    }
}
