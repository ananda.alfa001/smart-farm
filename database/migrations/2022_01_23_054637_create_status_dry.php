<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusDry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_dry', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sapi_id');
            $table->date('tanggal');
            $table->date('DUDRY')->index('DUDRY');
            $table->integer('DDRY')->index('DDRY');
            $table->string('status')->index('status');
            $table->integer('LACT')->index('LACT');
            $table->integer('dim_masuk')->nullable();
            $table->integer('dim_keluar')->nullable();
            $table->unsignedBigInteger('input_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
             * Run the Reference.
             *
             * @return void
             */
            $table->foreign('sapi_id')->references('id')->on('cowcard')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_dry');
    }
}
