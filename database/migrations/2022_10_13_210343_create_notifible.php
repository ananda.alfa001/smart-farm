<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotifible extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifible', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('roles_target_id')->index(); //notif buat ke siapa
            $table->integer('type_notif')->index(); //type fungsi buat 1,2 3, 4
            $table->unsignedBigInteger('notifikasi_id')->index(); // id untuk history event / mitra request
            $table->boolean('read')->default(false); //sudah terbaca atau belum
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifible');
    }
}
