<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_event', function (Blueprint $table) {
            $table->id();
            $table->string('activity');
            $table->string('kode_cowcard');
            $table->unsignedBigInteger('peternakan_id')->nullable();
            $table->string('DIM')->nullable();
            $table->string('ket_1')->nullable();
            $table->string('ket_2')->nullable();
            $table->string('ket_3')->nullable();
            $table->string('ket_4')->nullable();
            $table->string('ket_5')->nullable();
            $table->string('ket_6')->nullable();
            $table->string('ket_7')->nullable();
            $table->unsignedBigInteger('input_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
             * Run the Reference.
             *
             * @return void
             */
            $table->foreign('peternakan_id')->references('id')->on('peternakan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_event');
    }
}
