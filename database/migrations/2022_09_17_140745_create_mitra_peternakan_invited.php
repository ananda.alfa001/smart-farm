<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMitraPeternakanInvited extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mitra_peternakan_invited', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mitra_id')->index('mitra_id');
            $table->unsignedBigInteger('peternakan_id')->index('peternakan_id');
            $table->boolean('approval')->default(false)->index('approval');
            $table->boolean('decline')->default(false);
            $table->text('desc_pengajuan')->nullable();
            $table->text('desc_decline')->nullable();
            $table->string('status')->default('0');
            $table->unsignedBigInteger('input_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /**
                * Run the Reference.
                *
                * @return void
            */
            $table->foreign('mitra_id')->references('id')->on('mitra')->onDelete('cascade');
            $table->foreign('peternakan_id')->references('id')->on('peternakan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mitra_peternakan_invited');
    }
}
