<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\auth\LoginController;
use App\Http\Controllers\auth\GuardController;
use App\Http\Controllers\auth\RegisterController;
use App\Http\Controllers\kemitraan\KemitraanController;
use App\Http\Controllers\peternakan\NotifikasiController;
use App\Http\Controllers\peternakan\PeternakanController;
use App\Http\Controllers\peternakan\SycnController;
use App\Http\Controllers\teknisi\TeknisiController;
use App\Http\Controllers\usercomponent\UserController;
use Illuminate\Contracts\Session\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//halaman awal (UI/UX)
Route::get('/', [GuestController::class, 'homepage'])->name('login');
// Route::fallback(function () {
//     Alert::error('Error',  "Link Tidak Tersedia!");
//     return redirect()->back();
// });

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login', [GuestController::class, 'login'])->name('login');
    Route::post('/log-up', [LoginController::class, 'authenticate'])->name('authenticate');
    Route::get('/register', [GuestController::class, 'register'])->name('register');
    Route::post('/reg-up', [RegisterController::class, 'store'])->name('form_1');
});

Route::middleware(['auth'])->group(function () {
    //Session Guard
    Route::get('/Guard', [GuardController::class, 'AuthGuard'])->name('AuthGuard');
    Route::get('/Auth-Roles', [GuardController::class, 'AuthRoles'])->name('AuthRoles');
    Route::get('/logout', [LoginController::class, 'logout'])->name('dashboard-logout');
    //User Component
    Route::get('/notifible-update-1', [UserController::class, 'navbar_1'])->name('notifible-update-1');
    Route::get('/notifible-update-2', [UserController::class, 'navbar_2'])->name('notifible-update-2');
    Route::get('/notifible-update-3', [UserController::class, 'navbar_3'])->name('notifible-update-3');
    Route::get('/User-detils', [UserController::class, 'User'])->name('User-detils');
    Route::post('/User-detils/update', [UserController::class, 'Update'])->name('User-detils-update');
    Route::post('/User-detils/ChangePassword', [UserController::class, 'ChangePassword'])->name('User-detils-ChangePassword');
    Route::post('/User-detils/TambahKandang', [UserController::class, 'TambahKandang'])->name('User-detils-TambahKandang');
    Route::get('/notifikasi', [NotifikasiController::class, 'notifikasi'])->name('notifikasi');
    Route::get('/notifikasi/approve/{id}', [NotifikasiController::class, 'notifikasi_approve'])->name('notifikasi-approve');
});

Route::middleware(['auth'])->group(function () {
        Route::get('/peternakan', [PeternakanController::class, 'index'])->name('dashboard-peternakan');
        Route::get('/register/form/first', [RegisterController::class, 'form_1'])->name('form1');
        Route::post('/register/form/first/up', [RegisterController::class, 'form_1_up'])->name('form1-up');
        Route::get('/peternakan/cowcard', [PeternakanController::class, 'cow_card'])->name('cocard-peternakan');
        Route::get('/peternakan/regcowcard', [PeternakanController::class, 'redirecttoregister'])->name('register-cowcard');
        Route::post('/peternakan/regcowcard/sent', [PeternakanController::class, 'signcow'])->name('cowcard-sign');
        Route::post('/peternakan/setbred', [PeternakanController::class, 'set_bred'])->name('set-status-bred');
        Route::post('/peternakan/setdry', [PeternakanController::class, 'set_dry'])->name('set-status-dry');
        Route::post('/peternakan/setfresh', [PeternakanController::class, 'set_fresh'])->name('set-status-fresh');
        Route::post('/peternakan/setbull', [PeternakanController::class, 'set_bull'])->name('set-status-bull');
        Route::post('/peternakan/setnobred', [PeternakanController::class, 'set_nobred'])->name('set-status-nobred');
        Route::post('/peternakan/setopen', [PeternakanController::class, 'set_open'])->name('set-status-open');
        Route::post('/peternakan/setpreg', [PeternakanController::class, 'set_preg'])->name('set-status-preg');
        Route::post('/peternakan/setsold', [PeternakanController::class, 'sold'])->name('set-status-sold');
        Route::post('/peternakan/setdead', [PeternakanController::class, 'death'])->name('set-status-death');
        Route::get('/peternakan/viewcowcard/{id}', [PeternakanController::class, 'view_selection'])->name('view-cow-card');
        Route::get('/peternakan/bred/{id}', [PeternakanController::class, 'view_bred'])->name('view-status-bred');
        Route::get('/peternakan/dry/{id}', [PeternakanController::class, 'view_dry'])->name('view-status-dry');
        Route::get('/peternakan/fresh/{id}', [PeternakanController::class, 'view_fresh'])->name('view-status-fresh');
        Route::get('/peternakan/nobred/{id}', [PeternakanController::class, 'view_nobred'])->name('view-status-nobred');
        Route::get('/peternakan/open/{id}', [PeternakanController::class, 'view_open'])->name('view-status-open');
        Route::get('/peternakan/preg/{id}', [PeternakanController::class, 'view_preg'])->name('view-status-preg');
        Route::get('/peternakan/viewstatusdetail/{id}', [PeternakanController::class, 'view_status_details'])->name('view-status-details');
        Route::get('/peternakan/program', [PeternakanController::class, 'gotoprogram'])->name('dashboard-program');
        Route::get('/peternakan/makeprogram/{id}', [PeternakanController::class, 'set_program'])->name('make-program');
        Route::post('/peternakan/makeprogram/setnew', [PeternakanController::class, 'insert_program'])->name('set-program');
        Route::get('/peternakan/showprogram/{id}', [PeternakanController::class, 'ProgramResult'])->name('show-program');
        Route::get('/peternakan/editprogram/{id}', [PeternakanController::class, 'edit_program'])->name('edit-program');
        Route::get('/peternakan/deleteprogram/{id}', [PeternakanController::class, 'delete_program'])->name('delete-program');
        Route::post('/peternakan/editprogram/post', [PeternakanController::class, 'insert_edited_program'])->name('save-new-program');
        Route::post('/peternakan/vaksinasi/post', [PeternakanController::class, 'vaksin'])->name('vaksin');
        Route::get('/peternakan/hospitalize/cure/{id}', [PeternakanController::class, 'sembuh'])->name('cured');
        Route::get('/peternakan/hospitalize/open/{id}', [PeternakanController::class, 'hospitalize'])->name('penanganan-sakit');
        Route::post('/peternakan/hospitalize/post/{id}', [PeternakanController::class, 'penyakit'])->name('hospitalize');
        Route::get('/peternakan/hospitalize/riwayatpenyakit/{id}', [PeternakanController::class, 'riwayatpenyakit'])->name('riwayat-sakit');
        Route::get('/peternakan/hospitalize/riwayatvaksin/{id}', [PeternakanController::class, 'riwayatvaksin'])->name('riwayat-vaksin');
        Route::post('/peternakan/batch/first', [PeternakanController::class, 'registerbatch'])->name('batch-first-input');
        Route::get('/peternakan/batch/second', [PeternakanController::class, 'startbatch'])->name('batch-second-input');
        Route::post('/peternakan/batch/last', [PeternakanController::class, 'insertbatch'])->name('batch-last-input');
        Route::get('/dashboard', [PeternakanController::class, 'dashboard'])->name('dashboard');
        Route::post('/peternakan/ubahbb', [PeternakanController::class, 'ubahberat'])->name('ubah-berat-badan');
        Route::post('/peternakan/addremark', [PeternakanController::class, 'remark'])->name('remark');
        Route::post('/peternakan/pakan', [PeternakanController::class, 'feeder'])->name('feed');
        Route::get('/cekk', [PeternakanController::class, 'cek']);
        Route::post('/peternakan/pindahKandang/post', [PeternakanController::class, 'pindahKandang'])->name('pindahKandang');
        Route::get('/peternakan/export/{id}', [PeternakanController::class, 'export'])->name('exportProgram');
        Route::get('/peternakan/produksi', [PeternakanController::class, 'gotoproduksi'])->name('dashboard-produksi');
        Route::post('/peternakan/produksi/post', [PeternakanController::class, 'produksisusu'])->name('produksi');
        Route::post('/peternakan/produksi/edit', [PeternakanController::class, 'editproduksisusu'])->name('edit-produksi');
        Route::get('/peternakan/kemitraan', [PeternakanController::class, 'IndexKemitraan'])->name('mitra-peternakan');
        Route::post('/peternakan/kemitraan/tolak-mitra', [PeternakanController::class, 'TolakMitra'])->name('tolak-mitra');
        Route::resource('sync', SycnController::class);
        Route::get('/peternakan/sync/tindakan/{id}/{idprog}', [SycnController::class, 'updatetindakan'])->name('tindakan');
});

Route::group(['middleware' => 'peternakan'], function () {
    Route::get('/token', [PeternakanController::class, 'token_show'])->name('token');
    Route::get('/token-generate', [PeternakanController::class, 'token_generate'])->name('token-generate');
    Route::get('/approve-kemitraan/{id0}/{id1}/{id2}', [PeternakanController::class, 'TerimaMitra']);
});
Route::group(['middleware' => 'teknisi'], function () {
    Route::get('/register-teknisi/form/first', [RegisterController::class, 'form_teknisi_1'])->name('form1-teknisi');
    Route::post('/register-teknisi/form/first/up', [RegisterController::class, 'form_teknisi_1_up'])->name('form1-teknisi-up');
});
Route::resource('teknisi', TeknisiController::class)->middleware('teknisi');

Route::group(['middleware' => 'kemitraan'], function () {
    Route::get('/register-kemitraan/form/first', [RegisterController::class, 'form_kemitraan_1'])->name('form1-kemitraan');
    Route::post('/register-kemitraan/form/first/up', [RegisterController::class, 'form_kemitraan_1_up'])->name('form1-kemitraan-up');

    Route::get('/kemitraan/daftar/mitra', [KemitraanController::class, 'DaftarKemitraan'])->name('daftar-kemitraan');
    Route::get('/kemitraan/MitraTernak/', [KemitraanController::class, 'ternak'])->name('mitra-ternak');
    Route::get('/kemitraan/MitraTernak/GetAllPeternak', [KemitraanController::class, 'GetAllPeternak'])->name('mitra-ternak-getallpeternak');
    Route::get('/kemitraan/MitraTernak/GetOnePeternak', [KemitraanController::class, 'GetOnePeternak'])->name('mitra-ternak-getonepeternak');
    Route::get('/kemitraan/MitraTernak/Dashboard', [KemitraanController::class, 'Dashboard'])->name('mitra-ternak-Dashboard');
    Route::get('/kemitraan/produksi', [KemitraanController::class, 'produksi'])->name('kemitraan-produksi');
    Route::get('/kemitraan/permintaan/mitra', [KemitraanController::class, 'permintaan'])->name('permintaan-kemitraan');
    Route::get('/kemitraan/status/mitra', [KemitraanController::class, 'IndexKemitraan'])->name('status-kemitraan');
    Route::post('/kemitraan/daftar/BerhentiBermitra', [KemitraanController::class, 'BerhentiBermitra'])->name('BerhentiBermitra');
});
Route::resource('kemitraan', KemitraanController::class)->middleware('kemitraan');


Route::group(['middleware' => 'admin'], function () {
    Route::get('/dashboard/farm', [AdminController::class, 'farm'])->name('farm');
    Route::get('/dashboard/user', [AdminController::class, 'user'])->name('user');
});
Route::resource('admin', AdminController::class)->middleware('admin');

