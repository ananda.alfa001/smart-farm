<!-- Vendor JS Files -->
<script src="{{ asset('assets/roles-panel/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('assets/roles-panel/vendor/php-email-form/validate.js') }}"></script>
<script src="{{ asset('assets/roles-panel/vendor/quill/quill.min.js') }}"></script>
<script src="{{ asset('assets/roles-panel/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/roles-panel/vendor/simple-datatables/simple-datatables.js') }}"></script>
<script src="{{ asset('assets/roles-panel/vendor/chart.js/chart.min.js') }}"></script>
<script src="{{ asset('assets/roles-panel/vendor/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/roles-panel/vendor/echarts/echarts.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function copytoken() {
        /* Get the text field */
        var copyText = document.getElementById("token");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        /* Copy the text inside the text field */
        navigator.clipboard.writeText(copyText.value);

        Swal.fire(
        'Token Di Copy!',
        '',
        'success'
    )
}
setInterval(function() {
    $('#header_1').load('/notifible-update-1');
    $('#header_2').load('/notifible-update-2');
    $('#header_3').load('/notifible-update-3');
}, 30000);
</script>
<!-- Template Main JS File -->
<script src="{{ asset('assets/roles-panel/js/main.js') }}"></script>
@if(Auth::user()->roles == 'kemitraan')
    <script src="{{ asset('assets/roles-panel/js/kemitraan.js') }}"></script>
@endif
