<header id="header" class="header fixed-top d-flex align-items-center">
    <div class="d-flex align-items-center justify-content-between">
        <a href="/peternakan" class="logo d-flex align-items-center">
            <h4 class="card-title"><span> <img src="{{asset('assets/front-page/img/logo/logo_wht.png')}}" style="width: 142px;"></span></h4>
        </a>
        <i class="bi bi-list toggle-sidebar-btn"></i>
    </div>

    <nav class="header-nav ms-auto">
        <ul class="d-flex align-items-center">

            <li class="nav-item d-block d-lg-none">
                <a class="nav-link nav-icon search-bar-toggle " href="#">
                    <i class="bi bi-search"></i>
                </a>
            </li>
            <li class="nav-item dropdown">
                    <a class="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
                        <i class="bi bi-bell"></i>
                        <div id="header_1">
                            @if ($all != 0)
                                <span class="badge bg-danger badge-number">{{ $all }}</span>
                            @endif
                        </div>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">
                        <li class="dropdown-header">
                            <div id="header_2">
                                @if ($all == 0)
                                    Tidak Ada Notifikasi Baru
                                @else
                                    Terdapat {{ $all }} Notifikasi Baru Belum Terbaca
                                @endif
                            </div>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <div id="header_3">
                            @foreach ($notifible as $value)
                                @foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $notif)
                                    @if ($value->type_notif == '1')
                                        @foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $getval1)
                                            <li class="notification-item">
                                                <i class="bi bi-info-circle text-warning"></i>
                                                <div>
                                                    <h4>Permintaan Bermitra <br> dari Kemitraan <u>{{ $getval1->mitra->nama_instansi }}</u></h4>
                                                    <p>{{ durasi_now($getval1->created_at)}}</p>
                                                </div>
                                            </li>
                                        @endforeach
                                    @elseif($value->type_notif == '2')
                                        @foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $getval2)
                                            <li class="notification-item">
                                                <i class="bi bi-info-circle text-primary"></i>
                                                <div>
                                                    <h4>Update Aktifitas {{ $getval2->activity }}</h4>
                                                    <p>{{ pemeriksa($getval2->input_by) }}</p>
                                                    <p>{{  durasi_now($getval2->created_at) }}</p>
                                                </div>
                                            </li>
                                        @endforeach
                                    @elseif($value->type_notif == '3')
                                        @foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $getval3)
                                            <li class="notification-item">
                                                <i class="bi bi-info-circle text-primary"></i>
                                                <div>
                                                    <h4>{{ $getval3->Desc }}</h4>
                                                    <p>{{ pemeriksa($getval3->input_by) }}</p>
                                                    <p>{{  durasi_now($getval3->updated_at) }}</p>
                                                </div>
                                            </li>
                                        @endforeach
                                    @endif
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                @endforeach
                            @endforeach
                        </div>
                        <li class="dropdown-footer">
                            <a href="{{route('notifikasi')}}">Lihat Semua Notifikasi</a>
                        </li>
                    </ul>
            </li>
            <li class="nav-item dropdown pe-3">
                <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                    <img src="{{ asset('assets/auth/img/user.jpg') }}" alt="Profile" class="rounded-circle">
                    <span class="d-none d-md-block dropdown-toggle ps-2">{{ Auth::user()->name }}</span>
                </a>

                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                    <li class="dropdown-header">
                        <h6>{{ Auth::user()->name }}</h6>
                        <span>{{ Auth::user()->roles }}</span>
                    </li>
                    @if (Session('idPeternakan') != null && Session('idUserTeknisi') != null)
                        <li>
                            <a class="dropdown-item d-flex align-items-center" href="/teknisi">
                                <i class="bi bi-box-arrow-right"></i>
                                <span>Sign Out Peternakan</span>
                            </a>
                        </li>
                    @endif
                    <li>
                        <a class="dropdown-item d-flex align-items-center" href="/logout">
                            <i class="bi bi-box-arrow-right"></i>
                            <span>Sign Out Account</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</header>
