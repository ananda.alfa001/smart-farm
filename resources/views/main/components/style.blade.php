  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="{{asset('assets/roles-panel/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/roles-panel/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('assets/roles-panel/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{asset('assets/roles-panel/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/roles-panel/vendor/quill/quill.snow.css')}}" rel="stylesheet">
  <link href="{{asset('assets/roles-panel/vendor/quill/quill.bubble.css')}}" rel="stylesheet">
  <link href="{{asset('assets/roles-panel/vendor/simple-datatables/style.css')}}" rel="stylesheet">
  <link rel=”stylesheet” href=" https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <!-- Template Main CSS File -->
  <link href="{{asset('assets/roles-panel/css/style.css')}}" rel="stylesheet">


