@foreach ($notifible as $value)
    @foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $notif)
        @if ($value->type_notif == '1')
            @foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $getval1)
                <li class="notification-item">
                    <i class="bi bi-info-circle text-warning"></i>
                    <div>
                        <h4>Permintaan Bermitra <br> dari Kemitraan <u>{{ $getval1->mitra->nama_instansi }}</u></h4>
                        <p>{{ durasi_now($getval1->created_at) }}</p>
                    </div>
                </li>
            @endforeach
        @elseif($value->type_notif == '2')
            @foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $getval2)
                <li class="notification-item">
                    <i class="bi bi-info-circle text-primary"></i>
                    <div>
                        <h4>Update Aktifitas {{ $getval2->activity }}</h4>
                        <p>{{ pemeriksa($getval2->input_by) }}</p>
                        <p>{{ durasi_now($getval2->created_at) }}</p>
                    </div>
                </li>
            @endforeach
        @elseif($value->type_notif == '3')
            @foreach (notifible_data($value->type_notif, $value->notifikasi_id) as $getval3)
                <li class="notification-item">
                    <i class="bi bi-info-circle text-primary"></i>
                    <div>
                        <h4>{{ $getval3->Desc }}</h4>
                        <p>{{ pemeriksa($getval3->input_by) }}</p>
                        <p>{{ durasi_now($getval3->updated_at) }}</p>
                    </div>
                </li>
            @endforeach
        @endif
        <li>
            <hr class="dropdown-divider">
        </li>
    @endforeach
@endforeach
