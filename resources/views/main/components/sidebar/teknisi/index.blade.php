 <!-- ======= Sidebar ======= -->
 <aside id="sidebar" class="sidebar">
     <ul class="sidebar-nav" id="sidebar-nav">

        @if (Session('idPeternakan') != null)
            <li class="nav-item">
                <a class="nav-link collapsed" href="/peternakan">
                    <i class="bi bi-grid"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="/peternakan/cowcard">
                    <i class="bi bi-card-list"></i>
                    <span>Cow Card</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="/peternakan/program">
                    <i class="bi bi-bounding-box"></i>
                    <span>Program</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="/peternakan/produksi">
                    <i class="bi ri-scales-3-line"></i>
                    <span>Produksi</span>
                </a>
            </li>
        @elseif (Session('idPeternakan') != null && Session('idUserTeknisi') != null)
            <li class="nav-item">
                <a class="nav-link collapsed" href="/teknisi">
                    <i class="bi bi-grid"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="/peternakan/cowcard">
                    <i class="bi bi-card-list"></i>
                    <span>Cow Card</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="/peternakan/program">
                    <i class="bi bi-bounding-box"></i>
                    <span>Program</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="/peternakan/produksi">
                    <i class="bi ri-scales-3-line"></i>
                    <span>Produksi</span>
                </a>
            </li>
        @endif
        @if (Auth::user()->roles == 'teknisi')
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{route('teknisi.index')}}">
                    <i class="bi bi-person"></i>
                    <span>Dashboard Teknisi</span>
                </a>
            </li>
        @endif


        <li class="nav-heading">Account Management</li>
        @if (Auth::user()->roles == 'peternakan')
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{route('token')}}">
                    <i class="bi bi-person"></i>
                    <span>Token</span>
                </a>
            </li>
        @endif

         <li class="nav-item">
             <a class="nav-link collapsed" href="{{route('User-detils')}}">
                 <i class="bi bi-person"></i>
                 <span>Profile</span>
             </a>
         </li>
         @if (Auth::user()->roles == 'admin')
             <li class="nav-item">
                 <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
                     <i class="bi bi-layout-text-window-reverse"></i><span>Data Admin</span><i
                         class="bi bi-chevron-down ms-auto"></i>
                 </a>
                 <ul id="tables-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                     <li>
                         <a href="/peternakan">
                             <i class="bi bi-circle"></i><span>Daftar Peternakan</span>
                         </a>
                     </li>
                     <li>
                         <a href="/dashboard-admin/patienttable">
                             <i class="bi bi-circle"></i><span>Daftar Teknisi</span>
                         </a>
                     </li>
                     <li>
                         <a href="#">
                             <i class="bi bi-circle"></i><span>Daftar Kemitraan</span>
                         </a>
                     </li>
                 </ul>
             </li><!-- End Tables Nav -->
         @endif
     </ul>
 </aside><!-- End Sidebar-->
