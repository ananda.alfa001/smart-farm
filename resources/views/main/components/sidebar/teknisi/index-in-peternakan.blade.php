    {{-- Peternakan --}}
    <li class="nav-item">
        <a class="nav-link collapsed" href="/peternakan">
            <i class="bi bi-grid"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/peternakan/cowcard">
            <i class="bi bi-card-list"></i>
            <span>Cow Card</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/peternakan/program">
            <i class="bi bi-bounding-box"></i>
            <span>Program</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/peternakan/produksi">
            <i class="bi ri-scales-3-line"></i>
            <span>Produksi</span>
        </a>
    </li>
    <li class="nav-heading">Account Management</li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('User-detils') }}">
            <i class="bi bi-person"></i>
            <span>Profile</span>
        </a>
    </li>
