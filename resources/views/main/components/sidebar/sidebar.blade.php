 <!-- ======= Sidebar ======= -->
 <aside id="sidebar" class="sidebar">
    <ul class="sidebar-nav" id="sidebar-nav">
        @if(Auth::user()->roles == 'peternakan')
            @include('main.components.sidebar.peternakan.index')
        @elseif(Auth::user()->roles == 'teknisi')
            @if(Session('idPeternakan') != null && Session('idUserTeknisi') != null)
                @include('main.components.sidebar.teknisi.index-in-peternakan')
            @else
                @include('main.components.sidebar.teknisi.index')
            @endif
        @elseif(Auth::user()->roles == 'kemitraan')
                @include('main.components.sidebar.kemitraan.index')
        @elseif(Auth::user()->roles == 'admin')
            @include('main.components.sidebar.admin.index')
        @endif
    </ul>
</aside>

