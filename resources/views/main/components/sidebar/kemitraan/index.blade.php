 <!-- ======= Sidebar ======= -->
 <aside id="sidebar" class="sidebar">
     <ul class="sidebar-nav" id="sidebar-nav">
        <li class="nav-item">
            <a class="nav-link collapsed" href="/kemitraan">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li>
         <li class="nav-item">
             <a class="nav-link collapsed" href="{{route('daftar-kemitraan')}}">
                 <i class="bi bi-grid"></i>
                 <span>Daftar Kemitraan</span>
             </a>
         </li>
         <li class="nav-item">
            <a class="nav-link collapsed" href="{{route('status-kemitraan')}}">
                <i class="bi ri-scales-3-line"></i>
                <span>Status Kemitraan</span>
            </a>
        </li>
         <li class="nav-item">
             <a class="nav-link collapsed" href="{{route('permintaan-kemitraan')}}">
                 <i class="bi ri-scales-3-line"></i>
                 <span>Permintaan Mitra</span>
             </a>
         </li>
         <li class="nav-heading">Account Management</li>
         <li class="nav-item">
             <a class="nav-link collapsed" href="{{ route('User-detils') }}">
                 <i class="bi bi-person"></i>
                 <span>Profile</span>
             </a>
         </li>
     </ul>
 </aside><!-- End Sidebar-->
