<div class="container mt-4">
    <div class="copyright">
        &copy; Copyright <strong><span>Zantara.id</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
        Developt by <a href="https://zantara.id/">Zantara Team</a> & Designed by <a
            href="https://www.instagram.com/ourproject.creative/"> Our Project.Creative</a>
    </div>
</div>
