<div class="modal fade" id="mdlTambahKandang" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Kandang
                </h5>
                <button type="button" class="btn-close"
                    data-bs-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form action="/User-detils/TambahKandang" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row px-3">
                        <input class=" form-control mb-4" type="text"
                            name="id_pet" value="{{ $id_pet }}"
                            hidden>
                    </div>
                    <div class="row mb-3">
                        <label for="Nama Kandang" class="col-md-4 col-lg-3 col-form-label">Nama Kandang</label>
                        <div class="col-md-8 col-lg-9">
                            <input name="namaKandang" required="required" type="text" class="form-control" id="namaKandang" placeholder="Isi Nama Kandang">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                        data-bs-dismiss="modal">Tutup
                    </button>
                    <button type="submit" class="btn btn-success"><i
                        class="bi bi-eyedropper"></i> &nbsp;Tambah Kandang
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
