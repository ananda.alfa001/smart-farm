@extends('main.main')
@section('content')
  <div class="pagetitle">
    <h1>Profile</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/peternakan">Account Profile</a></li>
        <li class="breadcrumb-item">Profile</li>
        <li class="breadcrumb-item active">Profile {{Auth::user()->roles}} Detil</li>
      </ol>
    </nav>
  </div><!-- End Page Title -->
@foreach($peternakan as $pet)
  <section class="section profile">
    <div class="row">
      <div class="col-xl-4">

        <div class="card">
          <div class="card-body profile-card pt-4 d-flex flex-column align-items-center d-flex justify-content-center">
            <img src="{{asset('assets/auth/img/user.jpg')}}" alt="Profile" class="rounded-circle">
            <h2>{{Auth::user()->name}}</h2>
            <h3>
              <div class="small fst-italic">
                {{Auth::user()->email}}
              </div>
            </h3>
            <h3>
              <div class="small">
              Sebagai {{Auth::user()->roles}}
              </div>
            
            </h3>
          </div>
        </div>
      </div>
      <div class="col-xl-8">
        <div class="card">
          <div class="card-body pt-3">
            <!-- Bordered Tabs -->
            <ul class="nav nav-tabs nav-tabs-bordered">

              <li class="nav-item">
                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Overview</button>
              </li>

              <li class="nav-item">
                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit Profile</button>
              </li>

              <li class="nav-item">
                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-change-password">Change Password</button>
              </li>

              <li class="nav-item">
                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-list-kandang">List Kandang</button>
              </li>

            </ul>
            <div class="tab-content pt-2">

              <div class="tab-pane fade show active profile-overview" id="profile-overview">
                <h5 class="card-title">About</h5>
                <p class="small fst-italic">{{Auth::user()->roles}}</p>

                <h5 class="card-title">Profile Details</h5>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Full Name</div>
                  <div class="col-lg-9 col-md-8">{{Auth::user()->name}}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label">Roles</div>
                  <div class="col-lg-9 col-md-8">{{Auth::user()->roles}}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label">Email</div>
                  <div class="col-lg-9 col-md-8">{{Auth::user()->email}}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label">Nama Peternakan</div>
                  <div class="col-lg-9 col-md-8">{{$pet->nama_peternakan}}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label">Telepon Peternakan</div>
                  <div class="col-lg-9 col-md-8">{{$pet->no_hp}}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label">Tahun Berdiri</div>
                  <div class="col-lg-9 col-md-8">{{$pet->tahun_berdiri}}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label">Nama Pemilik Peternakan</div>
                  <div class="col-lg-9 col-md-8">{{$pet->nama_pemilik}}</div>
                </div>

              </div>

              <div class="tab-pane fade profile-edit pt-3" id="profile-edit">

                <!-- Profile Edit Form -->
                <form action="/User-detils/update" method="POST">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" value="{{Auth::user()->id}}"><br/>
                  <div class="row mb-3">
                    <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Profile Image</label>
                    <div class="col-md-8 col-lg-9">
                      <img src="{{asset('assets/auth/img/user.jpg')}}" alt="Profile">
                      <div class="pt-2">
                        <a href="/peternakan" class="btn btn-primary btn-sm" title="Upload new profile image"><i class="bi bi-upload"></i></a>
                        <a href="/peternakan" class="btn btn-danger btn-sm" title="Remove my profile image"><i class="bi bi-trash"></i></a>
                      </div>
                    </div>
                  </div>
                  
                  <div class="row mb-3">
                    <label for="name" class="col-md-4 col-lg-3 col-form-label">Full Name</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="name" required="required" type="text" class="form-control" id="name" value="{{Auth::user()->name}}">
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="email" class="col-md-4 col-lg-3 col-form-label">Email</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="email" required="required" type="text" class="form-control" id="email" value="{{Auth::user()->email}}">
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="nama_peternakan" class="col-md-4 col-lg-3 col-form-label">Nama Peternakan</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="nama_peternakan" required="required" type="text" class="form-control" id="nama_peternakan" value="{{$pet->nama_peternakan}}">
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="no_hp" class="col-md-4 col-lg-3 col-form-label">Telepon Peternakan</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="no_hp" required="required" type="number" class="form-control" id="no_hp" value="{{$pet->no_hp}}">
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="tahun_berdiri" class="col-md-4 col-lg-3 col-form-label">Tahun Berdiri</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="tahun_berdiri" required="required" type="date" class="form-control" id="tahun_berdiri" value="{{$pet->tahun_berdiri}}">
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="nama_pemilik" class="col-md-4 col-lg-3 col-form-label">Nama Pemilik Peternakan</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="nama_pemilik" required="required" type="text" class="form-control" id="nama_pemilik" value="{{$pet->nama_pemilik}}">
                    </div>
                  </div>

                  <div class="text-center">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                  </div>
                </form><!-- End Profile Edit Form -->

              </div>

              {{-- <div class="tab-pane fade pt-3" id="profile-settings">

                <!-- Settings Form -->
                <form>

                  <div class="row mb-3">
                    <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Email Notifications</label>
                    <div class="col-md-8 col-lg-9">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="changesMade" checked>
                        <label class="form-check-label" for="changesMade">
                          Changes made to your account
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="newProducts" checked>
                        <label class="form-check-label" for="newProducts">
                          Information on new products and services
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="proOffers">
                        <label class="form-check-label" for="proOffers">
                          Marketing and promo offers
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="securityNotify" checked disabled>
                        <label class="form-check-label" for="securityNotify">
                          Security alerts
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="text-center">
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                  </div>
                </form><!-- End settings Form -->

              </div> --}}

              <div class="tab-pane fade pt-3" id="profile-change-password">
                <!-- Change Password Form -->
                <form action="/User-detils/ChangePassword" method="POST">
                  @csrf
                  <input type="hidden" name="id" value="{{Auth::user()->id}}"><br/>
                  <div class="row mb-3">
                    <label for="oldPassword" class="col-md-4 col-lg-3 col-form-label">Old Password</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="oldPassword" required="required" type="password" class="form-control" id="oldPassword" placeholder="Password">
                    </div>
                  </div>
                  <div class="row mb-3">
                    <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">New Password</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="newPassword" required="required" type="password" class="form-control" id="newPassword" placeholder="Password">
                    </div>
                  </div>
                  <div class="row mb-3">
                    <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">Re-enter New Password</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="renewPassword" required="required" type="password" class="form-control" id="renewPassword" placeholder="Password">
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-primary">Change Password</button>
                  </div>
                </form><!-- End Change Password Form -->
              </div>

              <div class="tab-pane fade pt-3" id="profile-list-kandang">
                <div class="d-grid gap-2 mt-3">
                  <button type="button" class="btn btn-success"
                    data-bs-toggle="modal"
                    data-bs-target="#mdlTambahKandang">
                    <i class="bi bi-palette2"></i>
                    &nbsp; Tambah Kandang
                </button>
                </div>
                
                <div class="table-responsive">
                  <table id="tblKandang" class="table table-borderless datatable">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Kandang</th>
                        <th scope="col">Total Sapi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      ?>
                      @foreach ($kandang as $p)
                        <tr>
                          <td> {{ $no++ }} </td>
                          <td> {{ $p->nama_kandang }} </td>
                          <td> {{ countSapiByKandang($p->id) }} </td>
                        </tr>
                      @endforeach                                      
                    </tbody>
                  </table>
                </div>

              </div>

            </div><!-- End Bordered Tabs -->
          </div>
        </div>
      </div>
    </div>
  </section>
@endforeach
@endsection
@include('main.user-component.ModalTambahKandang')