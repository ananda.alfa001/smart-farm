<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ZANTARA - Empowering Farmers With Zantara!</title>
    <meta name="description" content="ZANTARA adalah platform inovatif yang mendorong Recording Sapi Peternakan dengan solusi cerdas. Temukan layanan rekaman hewan, manajemen peternakan, dan lebih banyak lagi dengan ZANTARA.">
    <meta name="keywords" content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">

    <!-- SOCIAL MEDIA META -->
    <meta property="og:title" content="ZANTARA - Recording Sapi Peternakan dengan Zantara!">
    <meta property="og:description" content="ZANTARA adalah platform inovatif yang mendorong Recording Sapi Peternakan dengan solusi cerdas. Temukan layanan rekaman hewan, manajemen peternakan, dan lebih banyak lagi dengan ZANTARA.">
    <meta property="og:image" content="{{asset('assets/front-page/img/logo/logo.png')}}">
    <meta property="og:url" content="https://zantara.id">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="ZANTARA">
    <meta name="twitter:card" content="summary_large_image">

    <!-- SEO META -->
    <meta name="google-site-verification" content="">

    <!-- Additional Meta Tags -->
    <meta name="robots" content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">
    <meta name="author" content="Ciptanesia.id">
    <meta name="googlebot" content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">

	<!-- Favicons -->
    <link href="{{asset('assets/logo_zantara.png')}}" rel="icon">
    <link href="{{asset('assets/logo_zantara.png')}}" rel="apple-touch-icon">
	@include('main.components.style')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

</head>
<body>
	<!-- ======= Header ======= -->
	@include('main.components.navbar')
	<!-- ======= Sidebar ======= -->
	@include('main.components.sidebar.sidebar')
	<!-- ======= Alert ======= -->
	@include('sweetalert::alert')
	<!-- ============== -->
	 <main id="main" class="main">
		<section class="section dashboard">
			@yield('content')
		</section>
	 </main>
	<!-- ======= Footer ======= -->
	<footer id="footer" class="footer">
		@include('main.components.footer')
	</footer><!-- End Footer -->
	<!-- /page container -->
	@include('main.components.script')
</body>
</html>
