@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Dashboard Token Peternakan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/peternakan">Dashboard</a></li>
                <li class="breadcrumb-item active">Token</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Masukkan Token <span>| Token Peternakan</span></h5>
                            <div class="alert alert-success  alert-dismissible fade show" role="alert">
                                <h4 class="alert-heading">Token</h4>
                                <p>Token Peternakan harus medapat Persetujuan dari Penanggungjawab Peternakan, Silahkan
                                    hubungi Peternakan yang ingin di Inspeksi!</p>
                                <hr>
                                <p class="mb-0">Bila sudah memiliki Token, Silahkan Masukan di Bawah Ini! &nbsp; <span
                                        class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> (Token
                                        Bersifat Case Sensitif)</span></p>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-12">
                                    <form method="POST" class="form-group" action="{{ route('set-status-fresh') }}">
                                        @csrf
                                        <input type="text" placeholder="Masukan Token Peternakan" class="form-control"
                                            required>
                                        <div style="padding-top: 10px">
                                            <button type="submit" class="btn btn-warning"> Submit </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">List Akun Belum Divalidasi <span>| Today</span></h5>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
