@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Dashboard Teknisi</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Teknisi</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Masukkan Token <span>| Token Peternakan</span></h5>
                            <div class="alert alert-success  alert-dismissible fade show" role="alert">
                                <h4 class="alert-heading">Token</h4>
                                <p>Token Peternakan harus medapat Persetujuan dari Penanggungjawab Peternakan, Silahkan
                                    hubungi Peternakan yang ingin di Inspeksi!</p>
                                <hr>
                                <p class="mb-0">Bila sudah memiliki Token, Silahkan Masukan di Bawah Ini! &nbsp; <span
                                        class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> (Token
                                        Bersifat Case Sensitif)</span></p>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-12">

                                    <form method="POST" class="form-group" action="{{ route('teknisi.store') }}">
                                        @csrf
                                        <input type="text" name="token" placeholder="Masukan Token Peternakan . . "
                                            class="form-control" required>
                                        <div style="padding-top: 10px">
                                            <button type="submit" class="btn btn-warning"> Submit </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Recent Sales -->
        <div class="col-24">
            <div class="card recent-sales">
                <div class="card-body">
                    <h5 class="card-title">Histori Pengerjaan Inspeksi</h5>
                    <div class="table-responsive">
                        <table class="table table-striped datatable">
                            <thead>
                                <tr>
                                    <th scope="col">Nama Peternakan</th>
                                    <th scope="col">Nama Pemilik</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">No. Telepon</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($token as $drop)
                                    <tr>
                                        <td>{{ $drop->nama_peternakan }}</td>
                                        <td>{{ $drop->nama_pemilik }}</td>
                                        <td>{{ $drop->alamat }}</td>
                                        <td>{{ $drop->no_hp }}</td>
                                        <td>
                                            @if ($drop->token_time == Carbon\Carbon::now()->format('Y-m-d'))
                                                <span class="badge bg-success"><i
                                                        class="bi bi-exclamation-octagon me-1"></i>
                                                    Aktif</span>
                                            @else
                                                <span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i>
                                                    Token Kadaluarsa</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($drop->token_time == Carbon\Carbon::now()->format('Y-m-d'))
                                            <form method="POST" class="form-group" action="{{ route('teknisi.store') }}">
                                                @csrf
                                                <input name="idPeternakan" value="{{ $drop->id }}" hidden>
                                                    <button type="submit" class="btn btn-sm btn-warning"> Masuk Peternakan </button>
                                                </form>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- End Recent Sales -->
    </div>
@endsection
