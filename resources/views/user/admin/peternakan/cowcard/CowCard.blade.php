@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Cow Card All Details</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item">Peternakan</li>
                <li class="breadcrumb-item active">Detil</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <div class="row">
        <div class="col-lg-16">
            <div class="row">
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            @foreach ($cow as $p)
                                <h5 class="card-title">{{ $p->kode_cowcard }}</h5>
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    PEN
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td style="width: 20%;">
                                                    {{ $p->PEN }}
                                                </td>
                                                <td>
                                                    {{ $p->Status == "Bull" ? "DOF" : "DIM"}}
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td style="width: 20%;">
                                                    {{ $p->DIM }}
                                                </td>
                                                <td>
                                                    RPRO
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td style="width: 20%;">
                                                    {{ $p->Status }}
                                                </td>
                                            </tr>
                                            @if ($p->Status != "Bull")
                                            <tr>
                                                <td>
                                                    LACT
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    {{ $p->LACT }}
                                                </td>
                                                <td>
                                                    DCC
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    {{ $DCC != null ? $DCC : '-' }}
                                                </td>
                                                <td>
                                                    TBRD
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    {{ $TBRD != null ? $TBRD : '-' }}
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    BB
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    {{ $p->berat }} Kg
                                                </td>
                                                <td>
                                                    DUDRY
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    {{ $DUDRY != null ? $DUDRY : '-' }}
                                                </td>
                                                <td>
                                                    DSLH
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    {{ $DSLH != null ? $DSLH : '-' }}
                                                </td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    MILK
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    <?php
                                                    $status = $p->Status;
                                                    ?>
                                                    @if ($status == 'Fresh')
                                                        Yes
                                                    @else
                                                        NO
                                                    @endif
                                                </td>
                                                <td>
                                                    DDAT
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    {{ $DDAT != null ? $DDAT : '-' }}
                                                </td>
                                            </tr>
                                            @else
                                            <tr>
                                                <td>
                                                    Usia
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td style="width: 20%;">
                                                    {{ durasi($p->tanggal_lahir, \Carbon\Carbon::now('+07:00'))}}
                                                </td>
                                                <td>
                                                    Berat
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td style="width: 20%;">
                                                    {{ $p->berat == null ? '-' : $p->berat }}
                                                </td>
                                                <td>
                                                    DIH
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td style="width: 20%;">
                                                    {{ $p->DIH == null ? '0' : $p->DIH}}
                                                </td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Recent Sales -->
        <div class="col-24">
            <div class="card recent-sales">
                <div class="card-body">
                    <h5 class="card-title">Activity {{ $kodesapi }}</h5>
                    <div class="table-responsive">
                        <table class="table table-striped datatable">
                            <thead>
                                <tr>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">Aktivitas</th>
                                    <th scope="col">{{ $p->Status == "Bull" ? "DOF" : "DIM"}}</th>
                                    <th scope="col">Ket 1</th>
                                    <th scope="col">Ket 2</th>
                                    <th scope="col">Ket 3</th>
                                    <th scope="col">Ket 4</th>
                                    <th scope="col">Ket 5</th>
                                    <th scope="col">Ket 6</th>
                                    <th scope="col">Ket 7</th>
                                    <th scope="col">Diproses Oleh</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($activity as $p)
                                    <tr>
                                        <td> {{ DatesIdn($p->created_at) }} </td>
                                        <td> {{ $p->activity }} </td>
                                        <td> {{ $p->DIM }} </td>
                                        <td> {{ $p->ket_1 != null ? $p->ket_1 : '-' }} </td>
                                        <td> {{ $p->ket_2 != null ? $p->ket_2 : '-' }} </td>
                                        <td> {{ $p->ket_3 != null ? $p->ket_3 : '-' }} </td>
                                        <td> {{ $p->ket_4 != null ? $p->ket_4 : '-' }} </td>
                                        <td> {{ $p->ket_5 != null ? $p->ket_5 : '-' }} </td>
                                        <td> {{ $p->ket_6 != null ? $p->ket_6 : '-' }} </td>
                                        <td> {{ $p->ket_7 != null ? $p->ket_7 : '-' }} </td>
                                        <td> {{ pemeriksa($p->input_by)}} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
