@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Dashboard Admin Utama</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Admin Utama Zantara</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Data Seluruh Cow Card Zantara</h5>
                            <div class="table-responsive">
                                <table class="table table-striped datatable">
                                    <thead>
                                        <tr>
                                            <th scope="col">No.</th>
                                            <th scope="col">Kode CowCard</th>
                                            <th scope="col">Jenis Kelamin</th>
                                            <th scope="col">DIM</th>
                                            <th scope="col">Berat Sapi</th>
                                            <th scope="col">Kesehatan</th>
                                            <th scope="col">Nama Peternakan</th>
                                            <th scope="col" style="text-align:center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($cowcard as $cow)
                                            <tr>
                                                <td>{{ $no }}</td>
                                                <td>{{ $cow->kode_cowcard }}</td>
                                                <td>{{ $cow->SEX }}</td>
                                                <td>{{ $cow->DIM }}</td>
                                                <td>{{ $cow->berat }} kg</td>
                                                <td>{{ $cow->sakit }}</td>
                                                <td>{{ $cow->nama_peternakan }}</td>
                                                <td style="text-align:center;">
                                                    <a class="btn btn-sm btn-primary" href="{{route('admin.show', $cow->id )}}">
                                                        <span>Lihat CowCard</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            @php
                                                $no++;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Recent Sales -->
        <div class="col-24">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Seluruh Peternakan Zantara</h5>
                    <div class="table-responsive">
                        <table class="table table-striped datatable">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Nama Peternakan</th>
                                    <th scope="col">Nama Pemilik</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">No. Telepon</th>
                                    <th scope="col" style="text-align:center;">Limit Cow Card</th>
                                    <th scope="col" style="text-align:center;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $nos = 1;
                                @endphp
                                @foreach ($peternakan as $drop)
                                    <tr>
                                        <td>{{ $nos }}</td>
                                        <td>{{ $drop->nama_peternakan }}</td>
                                        <td>{{ $drop->nama_pemilik }}</td>
                                        <td>{{ $drop->alamat }}</td>
                                        <td>{{ $drop->no_hp }}</td>
                                        <td style="text-align:center;">{{ $drop->limit }}</td>
                                        <td style="text-align:center;">
                                            <button type="button" class="btn btn-sm btn-warning" data-bs-toggle="modal"
                                                data-bs-target="#id{{ $drop->id }}"><i class="bi bi-gear-fill"></i><br>Ubah Limit
                                            </button>
                                            <button type="button" class="btn btn-sm btn-primary" data-bs-toggle="modal"
                                                data-bs-target="#kandang{{ $drop->id }}">Lihat Kandang
                                            </button>
                                        </td>
                                    </tr>
                                    @php
                                        $nos++;
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- End Recent Sales -->
    </div>
    <div class="row">
        <!-- Recent Sales -->
        <div class="col-24">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Seluruh Aktivitas Peternakan Zantara</h5>
                    <div class="table-responsive">
                        <div class="card-body">
                            <table class="table table-striped datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">Tanggal</th>
                                        <th scope="col">Aktivitas</th>
                                        <th scope="col">Kode Sapi</th>
                                        <th scope="col">DIM</th>
                                        <th scope="col">Ket 1</th>
                                        <th scope="col">Ket 2</th>
                                        <th scope="col">Ket 3</th>
                                        <th scope="col">Ket 4</th>
                                        <th scope="col">Ket 5</th>
                                        <th scope="col">Ket 6</th>
                                        <th scope="col">Ket 7</th>
                                        <th scope="col">Diproses Oleh</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($activity as $p)
                                        <tr>
                                            <td> {{DatesIdn($p->created_at) }} </td>
                                            <td> {{ $p->activity }} </td>
                                            <td> {{ $p->kode_cowcard }} </td>
                                            <td> {{ $p->DIM != null ? $p->DIM : '-' }} </td>
                                            <td> {{ $p->ket_1 }} </td>
                                            <td> {{ $p->ket_2 }} </td>
                                            <td> {{ $p->ket_3 }} </td>
                                            <td> {{ $p->ket_4 }} </td>
                                            <td> {{ $p->ket_5 }} </td>
                                            <td> {{ $p->ket_6 }} </td>
                                            <td> {{ $p->ket_7 }} </td>
                                            <td> {{ pemeriksa($p->input_by) }} </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Recent Sales -->
    </div>
    @foreach ($peternakan as $drop)
    <div class="modal fade" id="id{{ $drop->id }}" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Limit Cowcard Peternakan {{ $drop->nama_peternakan }}
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                </div>
                <form action="{{route('admin.update', $drop->id)}}" method="POST">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="modal-body">
                        <div class="row px-3">
                            <label class="mr-sm-2">
                                <h6 class="mb-0 text-sm">Jumlah Cow Card
                                </h6>
                            </label>
                            <input class=" form-control mb-4 form-control" name="limit" type="number" value="{{$drop->limit}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                            data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success"><i
                                class="bi bi-gear"></i> &nbsp;Rubah Limit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="kandang{{ $drop->id }}" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">List Kandang {{ $drop->nama_peternakan }}
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="cobaKandang" class="table table-borderless datatable">
                          <thead>
                            <tr>
                              <th scope="col">No</th>
                              <th scope="col">Nama Kandang</th>
                              <th scope="col">Total Sapi</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $no = 1;
                            ?>
                            @foreach (ListKandang($drop->id) as $dum)
                              <tr>
                                <td> {{ $no++ }} </td>
                                <td> {{ $dum->nama_kandang }} </td>
                                <td> {{ countSapiByKandang($dum->id) }} </td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                        data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@endsection
