@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Dashboard Admin Utama</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Admin Utama Zantara</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Data Seluruh Cow Card Zantara</h5>
                            <div class="table-responsive">
                                <table class="table table-striped datatable">
                                    <thead>
                                        <tr>
                                            <th scope="col">No.</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Role</th>
                                            <th scope="col" style="text-align:center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($user as $user)
                                            <tr>
                                                <td>{{ $no }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{{ $user->roles }}</td>
                                                <td style="text-align:center;">
                                                    <a class="btn btn-sm btn-danger" href="/dashboard/farm">
                                                        <span>Menu</span>
                                                    </a>
                                                </td>
                                            </tr>
                                            @php
                                                $no++;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
