@extends('main.main')
@section('content')
<div class="pagetitle">
        <h1>Dashboard Token Peternakan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/peternakan">Dashboard</a></li>
                <li class="breadcrumb-item active">Token</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Token <span>| Token Peternakan</span></h5>
                            <div class="alert alert-warning  alert-dismissible fade show" role="alert">
                                <h4 class="alert-heading">Generate Token</h4>
                                <p>Token Peternakan digunakan Dokter/Teknisi untuk membantu Inspeksi Peternakan, Token akan berganti otomatis 1 Hari Sekali! </p>
                                <hr>
                                <p class="mb-0"> <span class="badge bg-danger"><i class="bi bi-exclamation-octagon me-1"></i> Harap Rahasiakan</span> Token berikut, Token digunakan untuk masuk ke dalam Sistem Peternakan Anda oleh Dokter/Teknisi yang sudah mendapat persetujuan dari Anda untuk membantu mengakomodir Sistem Peternakan Zantara Anda!</p>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-12">
                                <input type="text" value = "{{$token}}" class="form-control" id="token" disabled>
                                <div style="padding-top:10px">
                                    <button type="button" class="btn btn-warning btn-clipboard"  onclick="copytoken()"><i class="bi bi-clipboard"></i> &nbsp; Copy Token</button>
                                    <a href="{{route('token-generate')}}" class="btn btn-info"><i class="bi bi-collection"></i> &nbsp;Generate New Token</a>
                                </div>
                            </div>
                              </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Pemilik Token <span>| Today</span></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
