@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Dashboard Cow Card Peternakan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/peternakan">Dashboard</a></li>
                <li class="breadcrumb-item active">Cow Card</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <div class="card-body">
                    <div class="btn-group col-md-12" role="group" style="text-align:center;">
                        <a class="btn btn-lg btn-success col-md-6" href="{{ route('register-cowcard') }}">
                            <i class="bi bi-palette2"></i>
                            Tambah Cow Card Baru
                        </a>
                        <a class="btn btn-lg btn-primary col-md-6" data-bs-toggle="modal" data-bs-target="#batchinput">
                            <i class="bi bi-palette2"></i>
                            Tambah Multi Cow Card Baru
                        </a>
                    </div>
                </div>
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <h2 class="card-title text-uppercase">Data-data Cow Card Terdaftar</h2>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="cowcards" class="table table-borderless datatable">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Kode Sapi</th>
                                            <th scope="col">Kandang</th>
                                            <th scope="col">PEN</th>
                                            <th style="text-align:center;" scope="col">Status</th>
                                            <th style="text-align:center;" scope="col">Tentang Sapi</th>
                                            <th style="text-align:center;" scope="col">Menu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        ?>
                                        @foreach ($cow as $p)
                                            <tr>
                                                <td> {{ $no++ }} </td>
                                                <td> {{ $p->kode_cowcard }} </td>
                                                <td> {{ $p->nama_kandang }} </td>
                                                <td> {{ $p->PEN }} </td>
                                                <td>
                                                    <div style="text-align:center;">
                                                        @if ($p->Status == 'Sold')
                                                            <button type="button" class="btn btn-success btn-sm"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#cowcard{{ $p->id }}">{{ $p->Status }}
                                                            </button>
                                                        @elseif ($p->Status == 'Death')
                                                            <button type="button" class="btn btn-danger btn-sm"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#cowcard{{ $p->id }}">{{ $p->Status }}
                                                            </button>
                                                        @else
                                                            <button type="button"
                                                                class="btn {{ $p->Status == 'Belum Ada Status' ? 'btn-light' : 'btn-primary' }} btn-sm"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#cowcard{{ $p->id }}">{{ $p->Status }}
                                                            </button>
                                                        @endif
                                                        @if( $p->sakit == 'Sakit')
                                                            <button type="button" class="btn btn-danger btn-sm">{{ 'Sapi ' .$p->sakit }}
                                                            </button>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td style="text-align:center;">
                                                    <div class="btn-group" role="group"
                                                        aria-label="Basic mixed styles example">
                                                        <a class="btn btn-secondary btn-sm" type="button"
                                                            href="{{ route('view-status-details', $p->id) }}">
                                                            <i class="bi bi-info-circle"></i>
                                                            &nbsp; Detail Sapi
                                                        </a>
                                                        @if ($p->Status == 'Sold' || $p->Status == 'Death')
                                                        @else
                                                            <button type="button" class="btn btn-warning btn-sm"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#updatestatus{{ $p->id }}">
                                                                <i class="bi bi-arrow-repeat"></i>
                                                                &nbsp; Ubah Status
                                                            </button>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td style="text-align:center;">
                                                    @if ($p->Status == 'Sold' || $p->Status == 'Death')
                                                    @else
                                                        <div class="btn-group" role="group"
                                                            aria-label="Basic mixed styles example">

                                                            <button type="button" class="btn btn-info btn-sm"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#mainmenu{{ $p->id }}">
                                                                <i class="bi bi-clipboard-plus"></i>
                                                                &nbsp; Menu
                                                            </button>
                                                        </div>
                                                    @endif
                                                </td>

                                                {{-- Modal Base Cow Card --}}
                                                <div class="modal fade" id="cowcard{{ $p->id }}" tabindex="-1">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Cow Card
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row px-3">
                                                                    <input class=" form-control mb-4" type="text"
                                                                        name="id" value="{{ $p->id }}" hidden>
                                                                </div>
                                                                <div class="row px-3">
                                                                    <label class="mr-sm-2">
                                                                        <h6 class="mb-0 text-sm">Nama Kandang</h6>
                                                                    </label>
                                                                    <input class=" form-control mb-4" type="text"
                                                                        name="kandang" value="{{ $p->nama_kandang }}"
                                                                        disabled>
                                                                </div>
                                                                <div class="row px-3">
                                                                    <label class="mr-sm-2">
                                                                        <h6 class="mb-0 text-sm">Nama PEN</h6>
                                                                    </label>
                                                                    <input class=" form-control mb-4" type="text"
                                                                        name="pen" value="{{ $p->PEN }}"
                                                                        disabled>
                                                                </div>
                                                                <div class="row px-3">
                                                                    <label class="mr-sm-2">
                                                                        <h6 class="mb-0 text-sm">Tanggal Lahir</h6>
                                                                    </label>
                                                                    <input class=" form-control mb-4" type="date"
                                                                        name="tgl_lhr" max="<?= date('Y-m-d') ?>"
                                                                        value="{{ $p->tanggal_lahir }}" disabled>
                                                                </div>
                                                                <div class="row px-3">
                                                                    <label class="mr-sm-2">
                                                                        <h6 class="mb-0 text-sm">SEX</h6>
                                                                    </label>
                                                                    <input class=" form-control mb-4" type="text"
                                                                        name="pen" value="{{ $p->SEX }}"
                                                                        disabled>
                                                                </div>

                                                                <div class="row px-3">
                                                                    <label class="mr-sm-2">
                                                                        <h6 class="mb-0 text-sm">Laktasi</h6>
                                                                    </label>
                                                                    <input class=" form-control mb-4" type="text"
                                                                        name="pen" value="{{ $p->LACT }}"
                                                                        disabled>
                                                                </div>
                                                                <div class="row px-3">
                                                                    <label class="mr-sm-2">
                                                                        <h6 class="mb-0 text-sm">DIM</h6>
                                                                    </label>
                                                                    <input class=" form-control mb-4" type="text"
                                                                        name="pen" value="{{ $p->DIM }}"
                                                                        disabled>
                                                                </div>
                                                                <div class="row px-3">
                                                                    <label class="mr-sm-2">
                                                                        <h6 class="mb-0 text-sm">Status</h6>
                                                                    </label>
                                                                    <a href="{{ route('set-status-preg', $p->id) }}">
                                                                        <div class="d-grid gap-2 mt-3">
                                                                            <button
                                                                                class="btn {{ $p->Status == 'Belum Ada Status' ? 'btn-light' : 'btn-primary' }}"
                                                                                type="button">{{ $p->Status }}</button>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Tutup</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- Modal Button Status --}}
                                                <div class="modal fade" id="updatestatus{{ $p->id }}"tabindex="-1">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Update Status Sapi
                                                                </h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="d-grid gap-2 mt-3" style="text-align:center;">
                                                                    <label class="col-md-12 float-center h6">
                                                                        <i class="bi bi-moisture"></i> &nbsp;Keterangan
                                                                        Sapi
                                                                    </label>
                                                                </div>
                                                                <div class="row">
                                                                    <label class="col-sm-6">
                                                                        <span
                                                                            class="badge rounded-pill bg-success text-white">
                                                                            <h6 class="mb-0 text-sm">Jenis Kelamin Sapi :
                                                                                &nbsp; {{ $p->SEX }}
                                                                            </h6>
                                                                        </span>
                                                                    </label>
                                                                    <label class="col-sm-6">
                                                                        <span
                                                                            class="badge rounded-pill bg-warning text-dark">
                                                                            <h6 class="mb-0 text-sm">
                                                                                Status : &nbsp; {{ $p->Status }}
                                                                            </h6>
                                                                        </span>
                                                                    </label>

                                                                </div>
                                                                <hr>
                                                                <div class="d-grid gap-2 mt-3" style="text-align:center;">
                                                                    <label class="col-md-12 float-center h6">
                                                                        <i class="bi bi-signpost-split"></i> &nbsp; Pilih
                                                                        Status Sapi
                                                                    </label>
                                                                </div>
                                                                @if ($p->Status == 'Bull')
                                                                @else
                                                                    <div style="text-align:center;">
                                                                        <button type="button" class="btn btn-primary"
                                                                            data-bs-toggle="modal"
                                                                            data-bs-target="#status_fresh{{ $p->id }}">Fresh
                                                                        </button>
                                                                        <button type="button" class="btn btn-primary"
                                                                            data-bs-toggle="modal"
                                                                            data-bs-target="#status_nobred{{ $p->id }}">No
                                                                            Bred
                                                                        </button>
                                                                        <button type="button" class="btn btn-primary"
                                                                            data-bs-toggle="modal"
                                                                            data-bs-target="#status_open{{ $p->id }}">Open
                                                                        </button>
                                                                        <button type="button" class="btn btn-primary"
                                                                            data-bs-toggle="modal"
                                                                            data-bs-target="#status_preg{{ $p->id }}">Preg
                                                                        </button>
                                                                        <button type="button" class="btn btn-primary"
                                                                            data-bs-toggle="modal"
                                                                            data-bs-target="#status_bred{{ $p->id }}">Bred
                                                                        </button>
                                                                        <button type="button" class="btn btn-primary"
                                                                            data-bs-toggle="modal"
                                                                            data-bs-target="#status_dry{{ $p->id }}">Dry
                                                                        </button>
                                                                    </div>
                                                                @endif

                                                                <hr>
                                                                <div style="text-align:center;">
                                                                    @if ($p->Status == 'Bull')
                                                                    @else
                                                                        <button type="button" class="btn btn-primary"
                                                                            data-bs-toggle="modal"
                                                                            data-bs-target="#status_bull{{ $p->id }}">Bull
                                                                        </button>
                                                                    @endif
                                                                    <button type="button" class="btn btn-success"
                                                                        data-bs-toggle="modal"
                                                                        data-bs-target="#sold{{ $p->id }}"><i
                                                                            class="bi bi-currency-exchange"></i> &nbsp;
                                                                        SOLD
                                                                    </button>
                                                                    <button type="button" class="btn btn-danger"
                                                                        data-bs-toggle="modal"
                                                                        data-bs-target="#death{{ $p->id }}"><i
                                                                            class="bi bi-shield-slash"></i> &nbsp; DEAD
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Tutup</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    {{-- Modal Form Update Status --}}
                                                    {{-- Fresh --}}
                                                    <div class="modal fade" id="status_fresh{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Set Status Fresh
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('set-status-fresh') }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Tanggal Fresh</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" name="tgl_fresh"
                                                                                max="<?= date('Y-m-d') ?>"required>
                                                                        </div>
                                                                        <div>
                                                                            <label for="customRange2"
                                                                                class="form-label">EASE
                                                                                (Tingkat Kemudahan Calving)
                                                                            </label>
                                                                            <input type="range" class="form-range"
                                                                                value="0" min="1"
                                                                                max="5" step="1"
                                                                                name="EASE">
                                                                        </div>
                                                                        <br />
                                                                        @include('user.peternakan.ModalKandang')
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-primary">Set
                                                                            Status
                                                                            Fresh</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- No Bred --}}
                                                    <div class="modal fade" id="status_nobred{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Set Status No Bred
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('set-status-nobred') }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="text-sm">Masukan Tanggal No Bred
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="tgl_nobred" required>
                                                                        </div>
                                                                        @include('user.peternakan.ModalKandang')
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-primary">Set
                                                                            Status No
                                                                            Bred</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- Open --}}
                                                    <div class="modal fade" id="status_open{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Set Status Open
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('set-status-open') }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="text-sm">Masukan Tanggal Open
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" name="tgl_open"
                                                                                max="<?= date('Y-m-d') ?>" required>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">ABDAT (Jika
                                                                                    Aborsi/Gagal
                                                                                    PREG)</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="ABDAT">
                                                                        </div>
                                                                        @include('user.peternakan.ModalKandang')
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-primary">Set
                                                                            Status
                                                                            Open</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- PREG --}}
                                                    <div class="modal fade" id="status_preg{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Set Status Preg
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('set-status-preg') }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="text-sm">Masukan Tanggal Preg
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="tgl_preg" required>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="text-sm">Masukan Tanggal Bred
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="tgl_bred" required>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="text-sm">SYNC</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="SYNC" required>
                                                                        </div>
                                                                        <div class="row px-3" hidden>
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="text-sm">Deskripsi SYNC</h6>
                                                                            </label>
                                                                            <textarea class="mb-4" type="text" name="SDESC"></textarea>
                                                                        </div>
                                                                        @include('user.peternakan.ModalKandang')
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-primary">Set
                                                                            Status
                                                                            PREG</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- Bred --}}
                                                    <div class="modal fade" id="status_bred{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Set Status Bred
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('set-status-bred') }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="text-sm">Status Bred</h6>
                                                                            </label>
                                                                            <select name="status_bred"
                                                                                class="form-select">
                                                                                <option selected>Choose...</option>
                                                                                <option value="ET">ET</option>
                                                                                <option value="Inseminasi Buatan">
                                                                                    Inseminasi Buatan</option>
                                                                                <option value="Join Bull">Join Bull
                                                                                </option>
                                                                                <option value="Super Ovulasi">Super Ovulasi
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="text-sm">HDAT</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="HDAT" required>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Tanggal Conception
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="CDAT" required>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Kode STRAW
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="text"
                                                                                name="straw" required>
                                                                        </div>
                                                                        @include('user.peternakan.ModalKandang')
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-primary">Set
                                                                            Status
                                                                            Bred</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- Dry --}}
                                                    <div class="modal fade" id="status_dry{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Set Status Dry
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('set-status-dry') }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Tanggal Dry</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="DUDRY" required>
                                                                        </div>
                                                                        @include('user.peternakan.ModalKandang')
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-primary">Set
                                                                            Status
                                                                            Dry</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- DEAD --}}
                                                    <div class="modal fade" id="death{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Set Status Death
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('set-status-death') }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Tanggal Mati</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="deathdate" required>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Penyebab kematian
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="deathreason"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-danger">Set
                                                                            Status
                                                                            Dead</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- SOLD --}}
                                                    <div class="modal fade" id="sold{{ $p->id }}" tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Set Status Sold
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('set-status-sold') }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidder>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Tanggal Jual</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="solddate" required>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Harga Jual</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="soldprice" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-success">Set
                                                                            Status
                                                                            Sold</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- Modal Vaksinasi --}}
                                                    <div class="modal fade" id="vaksin{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Perlakuan terhadap Sapi
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('vaksin') }}" method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Nama Pemeriksa
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="text"
                                                                                placeholder="{{ Session('idUserTeknisi') != null ? 'Teknisi' : 'Peternakan' }}"
                                                                                readonly>
                                                                            <input type="text" name="teknisi"
                                                                                value="{{ Session('idUserTeknisi') != null ? Session('idUserTeknisi') : Session('idUserPeternakan')}}"
                                                                                hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Tanggal perlakuan
                                                                                    diberikan</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="tvaksin" required>
                                                                        </div>
                                                                            <div class="row g-3">
                                                                                <div class="col-md-6">
                                                                                <label class="mr-sm-2">
                                                                                    <h6 class="mb-0 text-sm">Perlakuan yang
                                                                                        diberikan :</h6>
                                                                                </label>
                                                                                <div class="form-check">
                                                                                  <input class="form-check-input opo" type="radio" name="nvaksin" id="MilkF" value="B1" >
                                                                                  <label class="form-check-label" >
                                                                                    B1
                                                                                  </label>
                                                                                </div>
                                                                                <div class="form-check">
                                                                                  <input class="form-check-input opo" type="radio" name="nvaksin" id="RFM" value="Multivit" >
                                                                                  <label class="form-check-label" >
                                                                                    Multivit
                                                                                  </label>
                                                                                </div>
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input opo" type="radio" name="nvaksin" id="Antipiretik" value="Antipiretik" >
                                                                                  <label class="form-check-label" >
                                                                                    Antipiretik
                                                                                  </label>
                                                                                </div>
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input opo" type="radio" name="nvaksin" id="Pain" value="Pain killer" >
                                                                                  <label class="form-check-label" >
                                                                                    Pain killer
                                                                                  </label>
                                                                                </div>
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input opo" type="radio" name="nvaksin" id="Deworming" value="Deworming" >
                                                                                  <label class="form-check-label" >
                                                                                    Deworming
                                                                                  </label>
                                                                                </div>
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input opo" type="radio" name="nvaksin" id="Aftopor" value="Aftopor" >
                                                                                  <label class="form-check-label" >
                                                                                    Aftopor
                                                                                  </label>
                                                                                </div>
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input opo" type="radio" name="nvaksin" id="Ultravact" value="Ultravact" >
                                                                                  <label class="form-check-label" >
                                                                                    Ultravact
                                                                                  </label>
                                                                                </div>
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input opo" type="radio" name="nvaksin" id="Viras" value="Vira shield" >
                                                                                  <label class="form-check-label" >
                                                                                    Vira shield
                                                                                  </label>
                                                                                </div>
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input _teat" type="radio" name="nvaksin" id="Teat" value="Teat seal hooftrimming" >
                                                                                  <label class="form-check-label" >
                                                                                    Teat seal hooftrimming LL/LF/RF/RR
                                                                                  </label>

                                                                                </div>
                                                                                <div class="form-check radbox">
                                                                                    <input type="checkbox" id="LF" name="narray[]" value="LF">
                                                                                    <input type="checkbox" id="LR" name="narray[]" value="LR">
                                                                                    <input type="checkbox" id="RF" name="narray[]" value="RF">
                                                                                    <input type="checkbox" id="RR" name="narray[]" value="RR">
                                                                                </div>
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input opo" type="radio" name="nvaksin" id="Magnet" value="Magnet" >
                                                                                  <label class="form-check-label" >
                                                                                    Magnet
                                                                                  </label>
                                                                                </div>
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input lain" type="radio" name="nvaksin" value="Lainnya" >
                                                                                    <label class="form-check-label" >
                                                                                        Lainnya
                                                                                    </label>
                                                                                  </div>
                                                                                  <input class=" form-control mb-4 other" type="text" name="nvaksin">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Keterangan
                                                                                    perlakuan</h6>
                                                                            </label>
                                                                            <textarea class=" form-control mb-4" type="text" name="keterangan" style="height: 100px" required></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-success"><i
                                                                                class="bi bi-eyedropper"></i> &nbsp;Tambah
                                                                            Perlakuan</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- Modal Button Menu main --}}
                                                    <div class="modal fade" id="mainmenu{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Aktivitas apa yang akan anda
                                                                        lakukan?
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="modal-body">
                                                                        <div class="d-grid gap-2 mt-3"
                                                                            style="text-align:center;">
                                                                            <label class="col-md-12 float-center h6">
                                                                                <i class="bi bi-moisture"></i>
                                                                                &nbsp;Kesehatan
                                                                                Sapi : {{ $p->sakit }}
                                                                            </label>
                                                                        </div>
                                                                        <div style="text-align:center;">
                                                                            <div class="row">
                                                                                <label class="col-sm-12">
                                                                                    <span
                                                                                        class="badge rounded-pill bg-warning text-dark">
                                                                                        <h6 class="mb-0 text-sm">
                                                                                            Status : &nbsp;
                                                                                            {{ $p->Status }}
                                                                                        </h6>
                                                                                    </span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                        <div style="text-align:center;">
                                                                            <a
                                                                                href="{{ route('riwayat-sakit', $p->id) }}"><button
                                                                                    type="button"
                                                                                    class="btn btn-success">
                                                                                    <i class="bi bi-calendar-plus"></i></i>
                                                                                    &nbsp;
                                                                                    Riwayat Sakit Sapi
                                                                                </button>
                                                                            </a>
                                                                            @if ($p->sakit == 'Sakit')
                                                                                <a
                                                                                    href="{{ route('penanganan-sakit', $p->id) }}"><button
                                                                                        type="button"
                                                                                        class="btn btn-warning">
                                                                                        <i
                                                                                            class="bi bi-calendar-plus"></i></i>
                                                                                        &nbsp;
                                                                                        Penanganan
                                                                                    </button>
                                                                                </a>
                                                                            @else
                                                                                <button type="button"
                                                                                    class="btn btn-info"
                                                                                    data-bs-toggle="modal"
                                                                                    data-bs-target="#sapisakit{{ $p->id }}">
                                                                                    <i class="bi bi-bag-plus"></i>
                                                                                    &nbsp; Sapi Sakit
                                                                                </button>
                                                                            @endif

                                                                            <hr>
                                                                            @if ($p->status_vaksin == 'Sudah Vaksin')
                                                                                <a
                                                                                    href="{{ route('riwayat-vaksin', $p->id) }}"><button
                                                                                        type="button"
                                                                                        class="btn btn-success">
                                                                                        <i
                                                                                            class="bi bi-calendar-plus"></i></i>
                                                                                        &nbsp;
                                                                                        Riwayat Perventif/Perlakuan
                                                                                    </button>
                                                                                </a>
                                                                            @endif
                                                                            <button class="btn btn-primary" type="button"
                                                                                data-bs-toggle="modal"
                                                                                data-bs-target="#vaksin{{ $p->id }}">
                                                                                <i class="bi bi-eyedropper"></i> &nbsp;
                                                                                Perventif/Perlakuan terhadap Sapi
                                                                            </button>

                                                                            <hr>
                                                                            <button class="btn btn-primary" type="button"
                                                                                data-bs-toggle="modal"
                                                                                data-bs-target="#remark{{ $p->id }}">
                                                                                <i class="ri ri-sticky-note-fill"></i>
                                                                                &nbsp;
                                                                                Masukan lain (Remark)
                                                                            </button>
                                                                            <button class="btn btn-info" type="button"
                                                                                data-bs-toggle="modal"
                                                                                data-bs-target="#berat{{ $p->id }}">
                                                                                <i class="ri ri-scales-3-line"></i> &nbsp;
                                                                                Update Berat Badan
                                                                            </button>

                                                                            <hr>
                                                                            <button class="btn btn-primary" type="button"
                                                                                data-bs-toggle="modal"
                                                                                data-bs-target="#pindahKandang{{ $p->id }}">
                                                                                <i class="bi bi-box-arrow-right"></i>
                                                                                &nbsp;
                                                                                Pindah Kandang
                                                                            </button>
                                                                            <button class="btn btn-success" type="button"
                                                                                data-bs-toggle="modal"
                                                                                data-bs-target="#feed{{ $p->id }}">
                                                                                <i class="ri ri-restaurant-fill"></i>
                                                                                &nbsp;
                                                                                Ubah Jenis Pakan
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- Modal Hospitalize --}}
                                                    <div class="modal fade" id="sapisakit{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Sapi Sakit
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('hospitalize', $p->id) }}" method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Nama Pemeriksa
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4 form-control"
                                                                                type="text"
                                                                                placeholder="{{ Session('idUserTeknisi') != null ? 'Teknisi' : 'Peternakan' }}"
                                                                                readonly>
                                                                            <input type="text" name="teknisi"
                                                                                value="{{ Session('idUserTeknisi') != null ? Session('idUserTeknisi') : Session('idUserPeternakan')}}"
                                                                                hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Tanggal Sakit</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" name="tsakit" required>
                                                                        </div>
                                                                        <div class="row g-3">
                                                                            <div class="col-md-6">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Diagnosis penyakit :</h6>
                                                                            </label>
                                                                            <div class="form-check">
                                                                              <input class="form-check-input opo" type="radio" name="diagnosis" id="MilkF" value="Milk Fever" required >
                                                                              <label class="form-check-label" >
                                                                                Milk Fever
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                              <input class="form-check-input opo" type="radio" name="diagnosis" id="RFM" value="Return Foetal Membrane" >
                                                                              <label class="form-check-label" >
                                                                                Return Foetal Membrane (RFM)
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="Downner" value="Downner" >
                                                                              <label class="form-check-label" >
                                                                                Downner
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="Metritis" value="Metritis" >
                                                                              <label class="form-check-label" >
                                                                                Metritis
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="Enteritis" value="Enteritis" >
                                                                              <label class="form-check-label" >
                                                                                Enteritis
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="Indigesty" value="Indigesty" >
                                                                              <label class="form-check-label" >
                                                                                Indigesty
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="Dermatitis" value="Dermatitis" >
                                                                              <label class="form-check-label" >
                                                                                Dermatitis
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="Ketosis" value="Ketosis" >
                                                                              <label class="form-check-label" >
                                                                                Ketosis
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="Myasis" value="Myasis" >
                                                                              <label class="form-check-label" >
                                                                                Myasis
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="Pinke" value="Pink eye" >
                                                                              <label class="form-check-label" >
                                                                                Pink eye
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="LDA" value="Left Displaced abomasum" >
                                                                              <label class="form-check-label" >
                                                                                Left Displaced abomasum
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="RDA" value="Right Displaced abomasum" >
                                                                              <label class="form-check-label" >
                                                                                Right Displaced abomasum
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="Pyometra" value="Pyometra" >
                                                                              <label class="form-check-label" >
                                                                                Pyometra
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input mast" type="radio" name="diagnosis" id="Mastitis" value="Mastitis" >
                                                                              <label class="form-check-label" >
                                                                                Mastitis <br>LF/RF/LR/RR
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check radbox2">
                                                                                <input type="checkbox" id="LF" name="narray[]" value="LF">
                                                                                <input type="checkbox" id="LR" name="narray[]" value="LR">
                                                                                <input type="checkbox" id="RF" name="narray[]" value="RF">
                                                                                <input type="checkbox" id="RR" name="narray[]" value="RR">
                                                                            </div>
                                                                        </div>

                                                                            <div class="col-md-6">
                                                                                <br>
                                                                            <div class="form-check">
                                                                              <input class="form-check-input lame" type="radio" name="diagnosis" id="Lame" value="Lame" >
                                                                              <label class="form-check-label" >
                                                                                Lame <br> LF/RF/LR/RR
                                                                              </label>
                                                                            </div>
                                                                            <div class="form-check radbox3">
                                                                                <input type="checkbox" id="LF" name="narray[]" value="LF">
                                                                                <input type="checkbox" id="LR" name="narray[]" value="LR">
                                                                                <input type="checkbox" id="RF" name="narray[]" value="RF">
                                                                                <input type="checkbox" id="RR" name="narray[]" value="RR">
                                                                            </div>
                                                                            <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="WL" value="White Line Diseases" >
                                                                                <label class="form-check-label" >
                                                                                    White Line Diseases (WL)
                                                                                </label>
                                                                              </div>
                                                                              <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="ID" value="Interdigitalisasi dermatitis" >
                                                                                <label class="form-check-label" >
                                                                                    Interdigitalisasi dermatitis
                                                                                </label>
                                                                              </div>
                                                                              <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="SU" value="Sole Ulcer" >
                                                                                <label class="form-check-label" >
                                                                                    Sole Ulcer
                                                                                </label>
                                                                              </div>
                                                                              <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="SH" value="Sole Haemoraghea" >
                                                                                <label class="form-check-label" >
                                                                                    Sole Haemoraghea
                                                                                </label>
                                                                              </div>
                                                                              <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="ST" value="Sole Thin" >
                                                                                <label class="form-check-label" >
                                                                                    Sole Thin
                                                                                </label>
                                                                              </div>
                                                                              <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="FR" value="Footh roth" >
                                                                                <label class="form-check-label" >
                                                                                    Footh roth
                                                                                </label>
                                                                              </div>
                                                                              <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="MilkF" value="Luxaxii" >
                                                                                <label class="form-check-label" >
                                                                                    Luxaxii
                                                                                </label>
                                                                              </div>
                                                                              <div class="form-check">
                                                                                <input class="form-check-input opo" type="radio" name="diagnosis" id="MilkF" value="Dislocation" >
                                                                                <label class="form-check-label" >
                                                                                    Dislocation
                                                                                </label>
                                                                              </div>
                                                                              <div class="form-check">
                                                                                <input class="form-check-input lain2" type="radio" name="diagnosis" value="Lainnya" >
                                                                                <label class="form-check-label" >
                                                                                    Lainnya
                                                                                </label>
                                                                              </div>
                                                                              <input class=" form-control mb-4 other2" type="text" name="diagnosis">
                                                                        </div>
                                                                    </div>

                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Gejala Sapi</h6>
                                                                            </label>
                                                                            <textarea class="form-control mb-4" type="text" name="gejala" style="height: 100px" required></textarea>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Penanganan</h6>
                                                                            </label>
                                                                            <textarea class="form-control mb-4" type="text" name="penanganan" style="height: 100px"></textarea>
                                                                        </div>
                                                                        @include('user.peternakan.ModalKandang')
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-danger"><i
                                                                                class="bi bi-eyedropper"></i> &nbsp;Simpan
                                                                            Penyakit</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- Modal Batch --}}
                                                    <div class="modal fade" id="batchinput" tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Batch Input
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('batch-first-input') }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id_Peternakan"
                                                                                value="{{ session('idPeternakan') }}"
                                                                                hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Jumlah Sapi yang
                                                                                    didaftarkan</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="number" name="max" required>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Status Sapi yang
                                                                                    didaftarkan</h6>
                                                                            </label>
                                                                            <select class="form-select" name="Status"
                                                                                required>
                                                                                <option selected>Choose...</option>
                                                                                <option value="Fresh">Fresh</option>
                                                                                <option value="Bred">Bred</option>
                                                                                <option value="No Bred">No Bred</option>
                                                                                <option value="Dry">Dry</option>
                                                                                <option value="Open">Open</option>
                                                                                <option value="Preg">Preg</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-primary"><i
                                                                                class="bi bi-eyedropper"></i>
                                                                            &nbsp;Daftar</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- Modal Ubah Berat Badan --}}
                                                    <div class="modal fade" id="berat{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Update Berat Badan Sapi
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('ubah-berat-badan') }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="kode"
                                                                                value="{{ $p->kode_cowcard }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Berat Badan Baru (Satuan Kilogram)
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="number" name="berat" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-success"><i
                                                                                class="bi bi-eyedropper"></i> &nbsp;Update
                                                                            Berat Badan</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- Modal Remark --}}
                                                    <div class="modal fade" id="remark{{ $p->id }}"
                                                        tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Masukan tambahan (remark)
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('remark') }}" method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Nama Pemeriksa
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4 form-control"
                                                                                type="text"
                                                                                placeholder="{{ Session('idUserTeknisi') != null ? 'Teknisi' : 'Peternakan' }}"
                                                                                readonly>
                                                                            <input type="text" name="teknisi"
                                                                                value="{{ Session('idUserTeknisi') != null ? Session('idUserTeknisi') : Session('idUserPeternakan')}}"
                                                                                hidden>
                                                                            <input type="text" name="kode"
                                                                                value="{{ $p->kode_cowcard }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Tanggal Aktivitas
                                                                                </h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="date" max="<?= date('Y-m-d') ?>"
                                                                                name="tremark" required>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Aktivitas yang
                                                                                    dilakukan</h6>
                                                                            </label>
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="remarktitle"
                                                                                required>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Keterangan
                                                                                    aktivitas</h6>
                                                                            </label>
                                                                            <textarea class="form-control mb-4" type="text" name="keterangan" style="height: 100px" required></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-success"><i
                                                                                class="bi bi-eyedropper"></i> &nbsp;Tambah
                                                                            Keterangan Tambahan</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- Modal Ubah Pakan --}}
                                                    <div class="modal fade" id="feed{{ $p->id }}" tabindex="-1">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Penggantian Pakan Sapi
                                                                    </h5>
                                                                    <button type="button" class="btn-close"
                                                                        data-bs-dismiss="modal"
                                                                        aria-label="Close"></button>
                                                                </div>
                                                                <form action="{{ route('feed') }}" method="post">
                                                                    @csrf
                                                                    <div class="modal-body">
                                                                        <div class="row px-3">
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="id"
                                                                                value="{{ $p->id }}" hidden>
                                                                            <input class=" form-control mb-4"
                                                                                type="text" name="kode"
                                                                                value="{{ $p->kode_cowcard }}" hidden>
                                                                        </div>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">Nama pakan baru
                                                                                </h6>
                                                                            </label>
                                                                            <select name="pakan" class="form-select">
                                                                                <option selected>Choose...</option>
                                                                                <option value="Fresh">Fresh</option>
                                                                                <option value="Pick">High</option>
                                                                                <option value="Late">Late</option>
                                                                                <option value="Dry">Dry</option>
                                                                                <option value="Transisi">Transisi</option>
                                                                                <option value="Prestarter">(Bull)
                                                                                    Prestarter</option>
                                                                                <option value="Starter">(Bull) Starter
                                                                                </option>
                                                                                <option value="Grower">(Bull) Grower
                                                                                </option>
                                                                                <option value="Finisher">(Bull) Finisher
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                        <br>
                                                                        <div class="row px-3">
                                                                            <label class="mr-sm-2">
                                                                                <h6 class="mb-0 text-sm">DMI</h6>
                                                                            </label>
                                                                            <input class="form-control mb-4" type="number" name="DMI"  required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                            data-bs-dismiss="modal">Tutup</button>
                                                                        <button type="submit" class="btn btn-success"><i
                                                                                class="ri ri-restaurant-fill"></i> &nbsp;Ganti Pakan Sapi</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @include('user.peternakan.ModalPindahKandang')
                                                </div>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->
            </div>
        </div><!-- End Left side columns -->
    </div>

    <script type="module">
        //id
        $('.radbox').hide()
        $('.radbox2').hide()
        $('.radbox3').hide()
        $('.other').prop("disabled", "true")
        $('.other2').prop("disabled", "true")
        $('.lain').on('click', function(){
            $('.other').prop( "disabled", false )
            $('.radbox').hide()
            $('.radbox').removeAttr('checked')
        })
        $('.lain2').on('click', function(){
            $('.other2').prop( "disabled", false )
            $('.radbox2').hide()
            $('.radbox3').hide()
            $('.radbox2').removeAttr('checked')
            $('.radbox3').removeAttr('checked')
        })
        $('._teat').on('click', function(){
            $('.radbox').show()
            $('.other').prop("disabled", "true")
            $('.other').val("")
        })
        $('.mast').on('click', function(){
            $('.radbox2').show()
            $('.radbox3').hide()
            $('.other2').prop("disabled", "true")
            $('.radbox3').removeAttr('checked')
            $('.other2').val("")
        })
        $('.lame').on('click', function(){
            $('.radbox3').show()
            $('.radbox2').hide()
            $('.other2').prop("disabled", "true")
            $('.radbox2').removeAttr('checked')
            $('.other2').val("")
        })
        $('.opo').on('click', function(){
            $('.other').prop("disabled", "true")
            $('.radbox').hide()
            $('.other').val("")
            $('.radbox').removeAttr('checked')
        })
        $('.opo').on('click', function(){
            $('.other2').prop("disabled", "true")
            $('.radbox2').hide()
            $('.radbox3').hide()
            $('.other2').val("")
            $('.radbox2').removeAttr('checked')
            $('.radbox3').removeAttr('checked')
        })

        //class
        // $('.')
    </script>
@endsection
