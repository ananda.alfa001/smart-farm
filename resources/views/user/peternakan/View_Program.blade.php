@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Dashboard Peternakan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Program</li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                          <hr>
                          <div class = "row">
                            <div class ="col-md-10">
                              <h3 class="card-title text-uppercase">Program Zantara</h3>
                            </div>
                            <div class ="col-md-2">
                            <a href="{{route('exportProgram',$id)}}">
                              <button class="btn btn-warning text-uppercase pull-right">Export Excel</button>
                            </a>
                            </div>
                          </div>
                          <div class="col-sm-12 form-group">
                            <input class="form-control" placeholder="{{$StringProgram}}" disabled>
                          </div>
                            <hr style="margin-top: 10px;margin-bottom: 10px;">
                          <div class="table-responsive">
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th class="col-md-1">No</th>
                                        <th class="col-md-3">KODE SAPI</th>
                                        <th class="col-md-3">PEN</th>
                                        <th class="col-md-3">JENIS KELAMIN</th>
                                        <th class="col-md-3">DIM</th>
                                        <th class="col-md-3">LACT</th>
                                        <th class="col-md-3">CDAT</th>
                                        <th class="col-md-3">TBRD</th>
                                        <th class="col-md-3">DUDRY</th>
                                        <th class="col-md-3">DDRY</th>
                                        <th class="col-md-3">EASE</th>
                                        <th class="col-md-3">DOPN</th>
                                        <th class="col-md-3">ABDAT</th>
                                        <th class="col-md-3">DCC</th>
                                        <th class="col-md-3">DCCD</th>
                                        <th class="col-md-3">HDAT</th>
                                        <th class="col-md-3">DSLH</th>
                                        <th class="col-md-3">SYNC</th>
                                        <th class="col-md-3">SDESC</th>
                                      	<th class="col-md-3">PERVENTIF TERAKHIR</th>
                                        <th class="col-md-3">STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    ?>
                                  @foreach($shares as $p)
                                  <tr>
                                    <td> {{ $no++ }} </td>
                                    <td> {{$p->kode_cowcard}} </td>
                                    <td> {{$p->PEN}} </td>
                                    <td> {{$p->SEX}} </td>
                                    <td> {{$p->DIM}} </td>
                                    <td> {{($p->LACT != null) ? $p->LACT :  0   }} </td>
                                    <td> {{($p->CDAT != "Tidak ada Tanggal") ? FormDate($p->CDAT) :  0   }} </td>
                                    <td> {{($p->TBRD != null) ? $p->TBRD :  0   }} </td>
                                    <td> {{($p->DUDRY != null) ? $p->DUDRY :  0   }} </td>
                                    <td> {{($p->DDRY != null) ? $p->DDRY :  0   }} </td>
                                    <td> {{($p->EASE != null) ? $p->EASE :  0   }} </td>
                                    <td> {{($p->DOPN != null) ? $p->DOPN :  0   }} </td>
                                    <td> {{($p->ABDAT != "Tidak ada Tanggal") ? FormDate($p->ABDAT) :  0   }} </td>
                                    <td> {{($p->DCC != null) ? $p->DCC :  0   }} </td>
                                    <td> {{($p->DCCD != null) ? $p->DCCD :  0   }} </td>
                                    <td> {{($p->HDAT != "Tidak ada Tanggal") ?  FormDate($p->HDAT) :  0   }} </td>
                                    <td> {{($p->DSLH != null) ? $p->DSLH :  0   }} </td>
                                    <td> {{($p->SYNC != null) ? $p->SYNC :  0   }} </td>
                                    <td> {{($p->SDESC != null) ? $p->SDESC :  0   }} </td>
                                    <td> {{($p->nama_vaksin != null) ? $p->nama_vaksin :  "Belum Vaksinasi"   }} </td>
                                    <td> <span class="{{$p->Status == "Belum Ada Status" ? :  "badge rounded-pill bg-warning text-dark"  }}">{{($p->Status == "Belum Ada Status") ? "-" :   $p->Status  }}</span> </td>

                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->
            </div>
        </div><!-- End Left side columns -->
    </div>
@endsection
