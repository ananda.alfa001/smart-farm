@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Dashboard Notifikasi Peternakan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Auth-Roles">Dashboard</a></li>
                <li class="breadcrumb-item active">Semua Notifikasi</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-16">
            <div class="row">
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <hr>
                            @foreach ($unread as $unread_pluck)
                                @if ($unread_pluck->type_notif == '1')
                                    @foreach (notifible_data($unread_pluck->type_notif, $unread_pluck->notifikasi_id) as $unread_pluck_get)
                                        <div class="alert alert-warning fade show" role="alert">
                                            <h5 class="alert-heading">Permintaan Bermitra</h5>
                                            <h6><b><span
                                                        class="badge rounded-pill bg-primary">{{ $unread_pluck_get->mitra->nama_instansi }}</span></b>
                                            </h6>
                                            <p><u><b>Hal yang ditawarkan:</b></u></p>
                                            <p>&nbsp;"{{ $unread_pluck_get->desc_pengajuan }}"</p>
                                            <br>
                                            <p class="small text-muted align-right">
                                                {{ durasi_now($unread_pluck_get->created_at) }}
                                                <hr>
                                            <div class="text-right">
                                                <a href="{{ route('mitra-peternakan') }}">
                                                    <button class="btn btn-warning btn-sm">Lihat Detail Pengajuan</button>
                                                </a>
                                            </div>
                                            </p>
                                        </div>
                                        <hr style="margin-top: 0px;margin-bottom: 10px;">
                                    {{notifible_read($unread_pluck->id)}}
                                    @endforeach
                                @elseif($unread_pluck->type_notif == '2')
                                    @foreach (notifible_data($unread_pluck->type_notif, $unread_pluck->notifikasi_id) as $unread_pluck_gut)
                                        <div class="alert alert-info fade show" role="alert" style="padding-bottom: 0px;">
                                            <h5 class="alert-heading">Pemberitahuan Aktivitas</h5>
                                            <h6><b><span
                                                        class="badge rounded-pill bg-primary">{{ $unread_pluck_gut->activity }}</span></b>
                                            </h6>
                                            <p>Diproses oleh : {{ pemeriksa($unread_pluck_gut->input_by) }}</p>
                                            <hr>
                                            <p class="small text-muted">{{ durasi_now($unread_pluck_gut->created_at) }}
                                            </p>
                                        </div>
                                        <hr style="margin-top: 0px;margin-bottom: 10px;">
                                    {{notifible_read($unread_pluck->id)}}
                                    @endforeach
                                @elseif($unread_pluck->type_notif == '3')
                                    @foreach (notifible_data($unread_pluck->type_notif, $unread_pluck->notifikasi_id) as $unread_pluck_3)
                                        <div class="alert alert-info fade show" role="alert">
                                            <h5 class="alert-heading">Pemberitahuan Aktivitas</h5>
                                            <h6><b><span
                                                        class="badge rounded-pill bg-primary">{{ $unread_pluck_3->Desc }}</span></b>
                                            </h6>
                                            <p>Diproses oleh : {{ pemeriksa($unread_pluck_3->input_by) }}</p>
                                            <br>
                                            <hr>
                                            <p class="small text-muted">{{ durasi_now($unread_pluck_3->updated_at) }}
                                            </p>
                                        </div>
                                        <hr style="margin-top: 0px;margin-bottom: 10px;">
                                        {{notifible_read($unread_pluck->id)}}
                                    @endforeach
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
