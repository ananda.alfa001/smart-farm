@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Dashboard Program Peternakan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Auth-Roles">Dashboard</a></li>
                <li class="breadcrumb-item active">Program</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-16">
            <div class="row">
                <div class="card-body">
                        <div class="d-grid gap-2 mt-3" style="text-align:center;">
                            <button class="btn btn-lg btn-primary"
                            data-bs-toggle="modal"
                            data-bs-target="#add_susu">
                                <i class="bi bi-bar-chart-steps"></i>
                                Masukkan Produksi Susu Baru
                            </button>
                        </div>
                </div>
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <hr>
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title text-uppercase">Produksi Susu</h3>
                                </div>
                            </div>
                            <hr style="margin-top: 0px;margin-bottom: 10px;">
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th class="col-md-1">No</th>
                                        <th class="col-md-3">Tanggal</th>
                                        <th class="col-md-3">Jumlah susu (L)</th>
                                        <th class="col-md-3">ADM</th>
                                        <th class="col-md-5">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    ?>
                                    @foreach ($produksusu as $p)
                                        <tr>
                                            <td> {{ $no++ }} </td>
                                            <td> {{ DatesIdn($p->created_at) }} </td>
                                            <td> {{ $p->production }} </td>
                                            <td> {{ $p->ADM }} </td>
                                            <td>
                                                    <button class="btn btn-warning btn-sm"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#edit_susu{{ $p->id }}">Edit
                                                        Jumlah Produksi</button>
                                            </td>
                                            {{-- Modal Start --}}
                                            <div class="modal fade" id="edit_susu{{ $p->id }}"
                                                tabindex="-1">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Update Berat Badan Sapi
                                                            </h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal"
                                                                aria-label="Close"></button>
                                                        </div>
                                                        <form action="{{ route('edit-produksi') }}"
                                                            method="post">
                                                            @csrf
                                                            <div class="modal-body">
                                                                <div class="row px-3">
                                                                    <input class=" form-control mb-4"
                                                                        type="text" name="id"
                                                                        value="{{ $p->id }}" hidden>
                                                                    <label class="mr-sm-2">
                                                                        <h6 class="mb-0 text-sm">Produksi susu hari ini (L)
                                                                        </h6>
                                                                    </label>
                                                                    <input class=" form-control mb-4"
                                                                        type="number" name="susu" required>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Tutup</button>
                                                                <button type="submit" class="btn btn-success"><i
                                                                        class="bi bi-eyedropper"></i> &nbsp;Edit Jumlah Susu</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- Modal End --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- Start Modal --}}
                            <div class="modal fade" id="add_susu"
                                tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Update Berat Badan Sapi
                                            </h5>
                                            <button type="button" class="btn-close"
                                                data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <form action="{{ route('produksi') }}"
                                            method="post">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="row px-3">
                                                    <label class="mr-sm-2">
                                                        <h6 class="mb-0 text-sm">Produksi susu hari ini (L)
                                                        </h6>
                                                    </label>
                                                    <input class=" form-control mb-4"
                                                        type="number" name="susu" required>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Tutup</button>
                                                <button type="submit" class="btn btn-success"><i
                                                        class="bi bi-eyedropper"></i> &nbsp;Masukkan Susu</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            {{-- End Modal --}}
                        </div>
                    </div>
                </div><!-- End Recent Sales -->
            </div>
        </div><!-- End Left side columns -->
    </div>
@endsection
