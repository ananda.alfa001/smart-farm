@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Cow Card All Details</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item">Peternakan</li>
                <li class="breadcrumb-item active">Riwayat Sakit</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <div class="row">
        <!-- Recent Sales -->
        <div class="col-24">
            <div class="card recent-sales">
                <div class="card-body">
                    <h5 class="card-title">Riwayat sakit {{ $kodesapi }}</h5>
                    <div class="table-responsive">
                        <table class="table table-striped datatable">
                            <thead>
                                <tr>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">LACT</th>
                                    <th scope="col">Gejala</th>
                                    <th scope="col">Diagnosis</th>
                                    <th scope="col">Penanganan/Obat</th>
                                    <th scope="col">Pemeriksa</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($penyakit as $p)
                                    <tr>
                                        <td> {{ DatesIdn($p->created_at) }} </td>
                                        <td> {{ $p->LACT }} </td>
                                        <td> {{ $p->gejala == 'Sembuh' ? '-' :  $p->gejala}} </td>
                                        <td> {{ $p->diagnosis == 'Sembuh' ? '-' :  $p->diagnosis}} </td>
                                        <td> {{ $p->penanganan != '0' ? ($p->penanganan == 'Sembuh' ? 'Dinyatakan Sembuh' :  $p->penanganan) : 'Belum Ditangani' }} </td>
                                        <td> {{ pemeriksa($p->input_by)}} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- End Recent Sales -->
    </div>

    <div class="row">
        <!-- Recent Sales -->
        <div class="col-24">
            <div class="card recent-sales">
                <div class="card-body">
                    <h5 class="card-title">Penanganan {{ $kodesapi }}</h5>
                    <form class="row g-3" action="{{ route('hospitalize', $id_sapi) }}" method="post">
                        @csrf
                        <div class="col-12">
                            <label class="form-label">
                                Nama Pemeriksa
                            </label>
                                <input  class="form-control" type="text" placeholder="{{Session('idUserTeknisi') != null ? "Teknisi" : "Peternakan" }}"
                                 readonly>
                             <input type="text" name="PEN"  value="{{$PEN}}" hidden>
                             <input type="text" name="selectKandang"  value="{{$idkandang}}" hidden>
                        </div>
                        <div class="col-12">
                            <label class="form-label">
                               Tanggal Sakit
                            </label>
                            <input class="form-control" type="date"
                                name="tsakit" value = {{$tgl_sakit}} readonly>
                        </div>
                        <div class="col-12">
                            <label class="form-label">
                                Gejala yang dihadapi oleh Sapi
                            </label>
                            <textarea class="form-control" type="text" name="gejala" style="height: 100px" required>{{$gejala}}</textarea>

                        </div>
                        <div class="col-12">
                            <label class="form-label">
                               Diagnosis
                            </label>
                            <textarea class="form-control" type="text" name="diagnosis" style="height: 100px" required>{{$diagnosis}}</textarea>
                        </div>
                        <div class="col-12">
                            <label class="form-label">
                               Penanganan
                            </label>
                            <textarea class="form-control" type="text" name="penanganan" style="height: 100px"></textarea>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-warning">Tambahkan Penanganan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ route('cured', $id_sapi) }}"> <button type="button" class="btn btn-primary">Sapi Sembuh</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
