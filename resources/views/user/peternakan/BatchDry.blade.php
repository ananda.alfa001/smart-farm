@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Form Registrasi Cow {{$current+1}}/{{$max}}</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item">Peternakan</li>
                <li class="breadcrumb-item active">Form Registrasi Sapi</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <form class="row g-3" action="{{ route('batch-last-input') }}" method="post">
        @csrf
        <div class="col-12">
            <label class="form-label">Kode Sapi</label>
            <input type="text" name="id_sapi" class="form-control" required>
        </div>
        <div class="col-md-6">
            <label class="form-label">Nama Kandang</label>
            <select name="kandang" class="form-select">
                <option selected>Choose...</option>
                @foreach ($kandang as $kand)
                    <option value="{{ $kand->id }}">{{ $kand->nama_kandang }}</option>
                @endforeach
            </select>

        </div>
        <div class="col-md-6">
            <label class="form-label">Nama PEN</label>
            <input type="text" class="form-control" name="pen" style="text-transform:uppercase">
        </div>
        <div class="col-md-6">
            <label class="form-label">Tanggal Lahir</label>
            <input type="date" name="tgl_lhr" class="form-control">
        </div>
        <div class="col-md-6">
            <label class="form-label">Berat Badan</label>
            <input type="number" name="bb" class="form-control">
        </div>

        <div class="col-md-4">
            <label for="inputCity" class="form-label">SEX</label>
            <select name="jenis_kelamin" class="form-select">
                <option selected>Choose...</option>
                <option value="Jantan">Jantan</option>
                <option value="Betina">Betina</option>
            </select>
        </div>
        <div class="col-md-4">
            <label class="form-label">CALF</label>
            <select class="form-select" name="calf">
                <option selected>Choose...</option>
                <option value="Calf 1">Calf 1</option>
                <option value="Calf 2">Calf 2</option>
                <option value="Calf 3">Calf 3</option>
                <option value="Calf 4">Calf 4</option>
                <option value="Calf 5">Calf 5</option>
                <option value="Calf 6">Calf 6</option>
                <option value="Calf 7">Calf 7</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputZip" class="form-label">LAKTASI</label>
            <input type="text" class="form-control" name="LACT" placeholder="Laktasi Saat Ini">
        </div>

        {{-- SET STATUS AREA --}}
            <label for="inputZip" class="form-label" >STATUS DRY</label>
        
            <div class="col-12">
                <input class=" form-control mb-4" type="text"
                    name="id" value="{{ $p->id }}"
                    hidden>
            </div>
            <div class="col-12">
                <label for="inputZip" class="form-label">
                    Tanggal Dry
                </label>
                <input class=" form-control mb-4" type="date"
                    max="<?= date('Y-m-d') ?>" name="DUDRY"
                    required>
            </div>
        {{-- END SET STATUS AREA --}}

        <div class="text-center">
            <button type="submit" class="btn btn-success">Daftar Cow Baru</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </div>
    </form>
@endsection
