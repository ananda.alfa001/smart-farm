<div class="modal fade" id="pindahKandang{{ $p->id }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pindah Kandang
                </h5>
                <button type="button" class="btn-close"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('pindahKandang') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="row px-3">
                        <input class=" form-control mb-4" type="text"
                            name="id" value="{{ $p->id }}"
                            hidden>
                        <input class=" form-control mb-4" type="text"
                            name="kode" value="{{ $p->kode_cowcard }}"
                            hidden>
                    </div>
                    @include('user.peternakan.ModalKandang')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                        data-bs-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success"><i
                            class="bi bi-eyedropper"></i> &nbsp;Update Pindah Kandang</button>
                </div>
            </form>
        </div>
    </div>
</div>
