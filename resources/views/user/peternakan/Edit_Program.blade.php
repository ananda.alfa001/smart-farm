@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Buat program baru</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/peternakan">Dashboard</a></li>
                <li class="breadcrumb-item">Peternakan</li>
                <li class="breadcrumb-item active">Program</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <form class="row g-3" action="{{ route('save-new-program') }}" method="post">
        @csrf
        <div class="col-12">
            <label class="form-label">Nama Program</label>
            <input type="text" name="nprog" class="form-control" value="{{ $data->namaprogram }}" required>
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select id="1" onchange="PEN()" name="variabel_1" class="form-select">
                <option value="" {{ ExplodeVariableBlade($data->program_1) == null ? 'selected' : '' }}>Pilih
                    Variable..</option>
                <option value="LACT" {{ ExplodeVariableBlade($data->program_1) == 'LACT' ? 'selected' : '' }}>LACT
                </option>
                <option value="STATUS" {{ ExplodeVariableBlade($data->program_1) == 'STATUS' ? 'selected' : '' }}>STATUS
                </option>
                <option value="PEN" {{ ExplodeVariableBlade($data->program_1) == 'PEN' ? 'selected' : '' }}>PEN</option>
                <option value="DIM" {{ ExplodeVariableBlade($data->program_1) == 'DIM' ? 'selected' : '' }}>DIM</option>
                <option value="TBRD" {{ ExplodeVariableBlade($data->program_1) == 'TBRD' ? 'selected' : '' }}>TBRD
                </option>
                <option value="DUDRY" {{ ExplodeVariableBlade($data->program_1) == 'DUDRY' ? 'selected' : '' }}>DUDRY
                </option>
                <option value="DDRY" {{ ExplodeVariableBlade($data->program_1) == 'DDRY' ? 'selected' : '' }}>DDRY
                </option>
                <option value="EASE" {{ ExplodeVariableBlade($data->program_1) == 'EASE' ? 'selected' : '' }}>EASE
                </option>
                <option value="FRAGE" {{ ExplodeVariableBlade($data->program_1) == 'FRAGE' ? 'selected' : '' }}>FRAGE
                </option>
                <option value="DOPN" {{ ExplodeVariableBlade($data->program_1) == 'DOPN' ? 'selected' : '' }}>DOPN
                </option>
                <option value="DCC" {{ ExplodeVariableBlade($data->program_1) == 'DCC' ? 'selected' : '' }}>DCC
                </option>
                <option value="DCCD" {{ ExplodeVariableBlade($data->program_1) == 'DCCD' ? 'selected' : '' }}>DCCD
                </option>
                <option value="DSLH" {{ ExplodeVariableBlade($data->program_1) == 'DSLH' ? 'selected' : '' }}>DSLH
                </option>
                <option value="SYNC" {{ ExplodeVariableBlade($data->program_1) == 'SYNC' ? 'selected' : '' }}>SYNC
                </option>
            </select>
        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_1">
                <option value="" selected>Choose...</option>
                <option value=">" {{ ExplodeOperator($data->program_1) == '>' ? 'selected' : '' }}> > </option>
                <option value="<" {{ ExplodeOperator($data->program_1) == '<' ? 'selected' : '' }}>
                    < </option>
                <option value="=" {{ ExplodeOperator($data->program_1) == '=' ? 'selected' : '' }}> = </option>
            </select>
        </div>
        <div class="col-md-4">
            {{-- 1 --}}
            <label for="inputZip" class="form-label">Nilai</label>
            <input type="number" id="nilai_1" class="form-control" name="nilai_1" placeholder="Nilai yang dicari"
                {{ ExplodeVariableBlade($data->program_1) != 'PEN' || ExplodeVariableBlade($data->program_1) != 'STATUS' ? 'hidden' : '' }}>
            {{-- 2 --}}
            <select class="form-select" id="nilai_1i" name="nilai_1i"
                {{ ExplodeVariableBlade($data->program_1) == 'PEN' ? '' : 'hidden' }}>
                <option selected disabled hidden>Pilih PEN ...</option>
                @foreach ($pen as $pen1)
                    <option value="{{ $pen1->PEN }}" {{ $nilai1 == $pen1->PEN ? 'selected' : '' }}>{{ $pen1->PEN }}
                    </option>
                @endforeach
            </select>
            {{-- 3 --}}
            <select class="form-select" id="nilai_1ii" name="nilai_1ii"
                {{ ExplodeVariableBlade($data->program_1) == 'STATUS' ? '' : 'hidden' }}>
                <option selected disabled hidden>Pilih Status ...</option>
                @foreach ($status as $stat)
                    <option value="{{ $stat }}" {{ $nilai1 == $stat ? 'selected' : '' }}>{{ $stat }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select name="variabel_2" id="2" onchange="PEN()" class="form-select">
                <option value="" {{ ExplodeVariableBlade($data->program_2) == null ? 'selected' : '' }}>Pilih
                    Variable..</option>
                <option value="LACT" {{ ExplodeVariableBlade($data->program_2) == 'LACT' ? 'selected' : '' }}>LACT
                </option>
                <option value="STATUS" {{ ExplodeVariableBlade($data->program_2) == 'STATUS' ? 'selected' : '' }}>STATUS
                </option>
                <option value="PEN" {{ ExplodeVariableBlade($data->program_2) == 'PEN' ? 'selected' : '' }}>PEN
                </option>
                <option value="DIM" {{ ExplodeVariableBlade($data->program_2) == 'DIM' ? 'selected' : '' }}>DIM
                </option>
                <option value="TBRD" {{ ExplodeVariableBlade($data->program_2) == 'TBRD' ? 'selected' : '' }}>TBRD
                </option>
                <option value="DUDRY" {{ ExplodeVariableBlade($data->program_2) == 'DUDRY' ? 'selected' : '' }}>DUDRY
                </option>
                <option value="DDRY" {{ ExplodeVariableBlade($data->program_2) == 'DDRY' ? 'selected' : '' }}>DDRY
                </option>
                <option value="EASE" {{ ExplodeVariableBlade($data->program_2) == 'EASE' ? 'selected' : '' }}>EASE
                </option>
                <option value="FRAGE" {{ ExplodeVariableBlade($data->program_2) == 'FRAGE' ? 'selected' : '' }}>FRAGE
                </option>
                <option value="DOPN" {{ ExplodeVariableBlade($data->program_2) == 'DOPN' ? 'selected' : '' }}>DOPN
                </option>
                <option value="DCC" {{ ExplodeVariableBlade($data->program_2) == 'DCC' ? 'selected' : '' }}>DCC
                </option>
                <option value="DCCD" {{ ExplodeVariableBlade($data->program_2) == 'DCCD' ? 'selected' : '' }}>DCCD
                </option>
                <option value="DSLH" {{ ExplodeVariableBlade($data->program_2) == 'DSLH' ? 'selected' : '' }}>DSLH
                </option>
                <option value="SYNC" {{ ExplodeVariableBlade($data->program_2) == 'SYNC' ? 'selected' : '' }}>SYNC
                </option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_2">
                <option value="" selected>Choose...</option>
                <option value=">" {{ ExplodeOperator($data->program_2) == '>' ? 'selected' : '' }}> > </option>
                <option value="<" {{ ExplodeOperator($data->program_2) == '<' ? 'selected' : '' }}>
                    < </option>
                <option value="=" {{ ExplodeOperator($data->program_2) == '=' ? 'selected' : '' }}> = </option>
            </select>
        </div>
        <div class="col-md-4">
            {{-- 1 --}}
            <label for="inputZip" class="form-label">Nilai</label>
            <input type="number" id="nilai_2" class="form-control" name="nilai_2" placeholder="Nilai yang dicari"
                {{ ExplodeVariableBlade($data->program_2) == 'PEN' || ExplodeVariableBlade($data->program_2) == 'STATUS' ? 'hidden' : '' }}>
            {{-- 2 --}}
            <select class="form-select" id="nilai_2i" name="nilai_2i"
                {{ ExplodeVariableBlade($data->program_2) == 'PEN' ? '' : 'hidden' }}>
                <option selected disabled hidden>Pilih PEN ...</option>
                @foreach ($pen as $pen1)
                    <option value="{{ $pen1->PEN }}" {{ $nilai2 == $pen1->PEN ? 'selected' : '' }}>
                        {{ $pen1->PEN }}</option>
                @endforeach
            </select>
            {{-- 3 --}}
            <select class="form-select" id="nilai_2ii" name="nilai_2ii"
                {{ ExplodeVariableBlade($data->program_2) == 'STATUS' ? '' : 'hidden' }}>
                <option selected disabled hidden>Pilih Status ...</option>
                @foreach ($status as $stat)
                    <option value="{{ $stat }}" {{ $nilai2 == $stat ? 'selected' : '' }}>{{ $stat }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select name="variabel_3" id="3" onchange="PEN()" class="form-select">
                <option value="" {{ ExplodeVariableBlade($data->program_3) == null ? 'selected' : '' }}>Pilih
                    Variable..</option>
                <option value="LACT" {{ ExplodeVariableBlade($data->program_3) == 'LACT' ? 'selected' : '' }}>LACT
                </option>
                <option value="STATUS" {{ ExplodeVariableBlade($data->program_3) == 'STATUS' ? 'selected' : '' }}>STATUS
                </option>
                <option value="PEN" {{ ExplodeVariableBlade($data->program_3) == 'PEN' ? 'selected' : '' }}>PEN
                </option>
                <option value="DIM" {{ ExplodeVariableBlade($data->program_3) == 'DIM' ? 'selected' : '' }}>DIM
                </option>
                <option value="TBRD" {{ ExplodeVariableBlade($data->program_3) == 'TBRD' ? 'selected' : '' }}>TBRD
                </option>
                <option value="DUDRY" {{ ExplodeVariableBlade($data->program_3) == 'DUDRY' ? 'selected' : '' }}>DUDRY
                </option>
                <option value="DDRY" {{ ExplodeVariableBlade($data->program_3) == 'DDRY' ? 'selected' : '' }}>DDRY
                </option>
                <option value="EASE" {{ ExplodeVariableBlade($data->program_3) == 'EASE' ? 'selected' : '' }}>EASE
                </option>
                <option value="FRAGE" {{ ExplodeVariableBlade($data->program_3) == 'FRAGE' ? 'selected' : '' }}>FRAGE
                </option>
                <option value="DOPN" {{ ExplodeVariableBlade($data->program_3) == 'DOPN' ? 'selected' : '' }}>DOPN
                </option>
                <option value="DCC" {{ ExplodeVariableBlade($data->program_3) == 'DCC' ? 'selected' : '' }}>DCC
                </option>
                <option value="DCCD" {{ ExplodeVariableBlade($data->program_3) == 'DCCD' ? 'selected' : '' }}>DCCD
                </option>
                <option value="DSLH" {{ ExplodeVariableBlade($data->program_3) == 'DSLH' ? 'selected' : '' }}>DSLH
                </option>
                <option value="SYNC" {{ ExplodeVariableBlade($data->program_3) == 'SYNC' ? 'selected' : '' }}>SYNC
                </option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_3">
                <option value="" selected>Choose...</option>
                <option value=">" {{ ExplodeOperator($data->program_3) == '>' ? 'selected' : '' }}> > </option>
                <option value="<" {{ ExplodeOperator($data->program_3) == '<' ? 'selected' : '' }}>
                    < </option>
                <option value="=" {{ ExplodeOperator($data->program_3) == '=' ? 'selected' : '' }}> = </option>
            </select>
        </div>
        <div class="col-md-4">
            {{-- 1 --}}
            <label for="inputZip" class="form-label">Nilai</label>
            <input type="number" id="nilai_3" class="form-control" name="nilai_3" placeholder="Nilai yang dicari"
                {{ ExplodeVariableBlade($data->program_3) == 'PEN' || ExplodeVariableBlade($data->program_3) == 'STATUS' ? 'hidden' : '' }}>
            {{-- 2 --}}
            <select class="form-select" id="nilai_3i" name="nilai_3i"
                {{ ExplodeVariableBlade($data->program_3) == 'PEN' ? '' : 'hidden' }}>
                <option selected disabled hidden>Pilih PEN ...</option>
                @foreach ($pen as $pen1)
                    <option value="{{ $pen1->PEN }}" {{ $nilai3 == $pen1->PEN ? 'selected' : '' }}>
                        {{ $pen1->PEN }}</option>
                @endforeach
            </select>
            {{-- 3 --}}
            <select class="form-select" id="nilai_3ii" name="nilai_3ii"
                {{ ExplodeVariableBlade($data->program_3) == 'STATUS' ? '' : 'hidden' }}>
                <option selected disabled hidden>Pilih Status ...</option>
                @foreach ($status as $stat)
                    <option value="{{ $stat }}" {{ $nilai3 == $stat ? 'selected' : '' }}>{{ $stat }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select name="variabel_4" id="4" onchange="PEN()" class="form-select">
                <option value="" {{ ExplodeVariableBlade($data->program_4) == null ? 'selected' : '' }}>Pilih
                    Variable..</option>
                <option value="LACT" {{ ExplodeVariableBlade($data->program_4) == 'LACT' ? 'selected' : '' }}>LACT
                </option>
                <option value="STATUS" {{ ExplodeVariableBlade($data->program_4) == 'STATUS' ? 'selected' : '' }}>STATUS
                </option>
                <option value="PEN" {{ ExplodeVariableBlade($data->program_4) == 'PEN' ? 'selected' : '' }}>PEN
                </option>
                <option value="DIM" {{ ExplodeVariableBlade($data->program_4) == 'DIM' ? 'selected' : '' }}>DIM
                </option>
                <option value="TBRD" {{ ExplodeVariableBlade($data->program_4) == 'TBRD' ? 'selected' : '' }}>TBRD
                </option>
                <option value="DUDRY" {{ ExplodeVariableBlade($data->program_4) == 'DUDRY' ? 'selected' : '' }}>DUDRY
                </option>
                <option value="DDRY" {{ ExplodeVariableBlade($data->program_4) == 'DDRY' ? 'selected' : '' }}>DDRY
                </option>
                <option value="EASE" {{ ExplodeVariableBlade($data->program_4) == 'EASE' ? 'selected' : '' }}>EASE
                </option>
                <option value="FRAGE" {{ ExplodeVariableBlade($data->program_4) == 'FRAGE' ? 'selected' : '' }}>FRAGE
                </option>
                <option value="DOPN" {{ ExplodeVariableBlade($data->program_4) == 'DOPN' ? 'selected' : '' }}>DOPN
                </option>
                <option value="DCC" {{ ExplodeVariableBlade($data->program_4) == 'DCC' ? 'selected' : '' }}>DCC
                </option>
                <option value="DCCD" {{ ExplodeVariableBlade($data->program_4) == 'DCCD' ? 'selected' : '' }}>DCCD
                </option>
                <option value="DSLH" {{ ExplodeVariableBlade($data->program_4) == 'DSLH' ? 'selected' : '' }}>DSLH
                </option>
                <option value="SYNC" {{ ExplodeVariableBlade($data->program_4) == 'SYNC' ? 'selected' : '' }}>SYNC
                </option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_4">
                <option value="" selected>Choose...</option>
                <option value=">" {{ ExplodeOperator($data->program_4) == '>' ? 'selected' : '' }}> > </option>
                <option value="<" {{ ExplodeOperator($data->program_4) == '<' ? 'selected' : '' }}>
                    < </option>
                <option value="=" {{ ExplodeOperator($data->program_4) == '=' ? 'selected' : '' }}> = </option>
            </select>
        </div>
        <div class="col-md-4">
            {{-- 1 --}}
            <label for="inputZip" class="form-label">Nilai</label>
            <input type="number" id="nilai_4" class="form-control" name="nilai_4" placeholder="Nilai yang dicari"
                {{ ExplodeVariableBlade($data->program_4) == 'PEN' || ExplodeVariableBlade($data->program_4) == 'STATUS' ? 'hidden' : '' }}>
            {{-- 2 --}}
            <select class="form-select" id="nilai_4i" name="nilai_4i"
                {{ ExplodeVariableBlade($data->program_4) == 'PEN' ? '' : 'hidden' }}>
                <option selected disabled hidden>Pilih PEN ...</option>
                @foreach ($pen as $pen1)
                    <option value="{{ $pen1->PEN }}" {{ $nilai4 == $pen1->PEN ? 'selected' : '' }}>
                        {{ $pen1->PEN }}</option>
                @endforeach
            </select>
            {{-- 3 --}}
            <select class="form-select" id="nilai_4ii" name="nilai_4ii"
                {{ ExplodeVariableBlade($data->program_4) == 'STATUS' ? '' : 'hidden' }}>
                <option selected disabled hidden>Pilih Status ...</option>
                @foreach ($status as $stat)
                    <option value="{{ $stat }}" {{ $nilai4 == $stat ? 'selected' : '' }}>{{ $stat }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select name="variabel_5" id="5" onchange="PEN()" class="form-select">
                <option value="" {{ ExplodeVariableBlade($data->program_5) == null ? 'selected' : '' }}>Pilih
                    Variable..</option>
                <option value="LACT" {{ ExplodeVariableBlade($data->program_5) == 'LACT' ? 'selected' : '' }}>LACT
                </option>
                <option value="DIM" {{ ExplodeVariableBlade($data->program_5) == 'DIM' ? 'selected' : '' }}>DIM
                </option>
                <option value="STATUS" {{ ExplodeVariableBlade($data->program_5) == 'STATUS' ? 'selected' : '' }}>STATUS
                </option>
                <option value="PEN" {{ ExplodeVariableBlade($data->program_5) == 'PEN' ? 'selected' : '' }}>PEN
                </option>
                <option value="TBRD" {{ ExplodeVariableBlade($data->program_5) == 'TBRD' ? 'selected' : '' }}>TBRD
                </option>
                <option value="DUDRY" {{ ExplodeVariableBlade($data->program_5) == 'DUDRY' ? 'selected' : '' }}>DUDRY
                </option>
                <option value="DDRY" {{ ExplodeVariableBlade($data->program_5) == 'DDRY' ? 'selected' : '' }}>DDRY
                </option>
                <option value="EASE" {{ ExplodeVariableBlade($data->program_5) == 'EASE' ? 'selected' : '' }}>EASE
                </option>
                <option value="FRAGE" {{ ExplodeVariableBlade($data->program_5) == 'FRAGE' ? 'selected' : '' }}>FRAGE
                </option>
                <option value="DOPN" {{ ExplodeVariableBlade($data->program_5) == 'DOPN' ? 'selected' : '' }}>DOPN
                </option>
                <option value="DCC" {{ ExplodeVariableBlade($data->program_5) == 'DCC' ? 'selected' : '' }}>DCC
                </option>
                <option value="DCCD" {{ ExplodeVariableBlade($data->program_5) == 'DCCD' ? 'selected' : '' }}>DCCD
                </option>
                <option value="DSLH" {{ ExplodeVariableBlade($data->program_5) == 'DSLH' ? 'selected' : '' }}>DSLH
                </option>
                <option value="SYNC" {{ ExplodeVariableBlade($data->program_5) == 'SYNC' ? 'selected' : '' }}>SYNC
                </option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_5">
                <option value="" selected>Choose...</option>
                <option value=">" {{ ExplodeOperator($data->program_5) == '>' ? 'selected' : '' }}> > </option>
                <option value="<" {{ ExplodeOperator($data->program_5) == '<' ? 'selected' : '' }}>
                    < </option>
                <option value="=" {{ ExplodeOperator($data->program_5) == '=' ? 'selected' : '' }}> = </option>
            </select>
        </div>
        <div class="col-md-4">
            {{-- 1 --}}
            <label for="inputZip" class="form-label">Nilai</label>
            <input type="number" id="nilai_5" class="form-control" name="nilai_5" placeholder="Nilai yang dicari"
                {{ ExplodeVariableBlade($data->program_5) == 'PEN' || ExplodeVariableBlade($data->program_5) == 'STATUS' ? 'hidden' : '' }}>
            {{-- 2 --}}
            <select class="form-select" id="nilai_5i" name="nilai_5i"
                {{ ExplodeVariableBlade($data->program_5) == 'PEN' ? '' : 'hidden' }}>
                <option selected disabled hidden>Pilih PEN ...</option>
                @foreach ($pen as $pen1)
                    <option value="{{ $pen1->PEN }}" {{ $nilai5 == $pen1->PEN ? 'selected' : '' }}>
                        {{ $pen1->PEN }}</option>
                @endforeach
            </select>
            {{-- 3 --}}
            <select class="form-select" id="nilai_5ii" name="nilai_5ii"
                {{ ExplodeVariableBlade($data->program_5) == 'STATUS' ? '' : 'hidden' }}>
                <option selected disabled hidden>Pilih Status ...</option>
                @foreach ($status as $stat)
                    <option value="{{ $stat }}" {{ $nilai5 == $stat ? 'selected' : '' }}>{{ $stat }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select name="variabel_6" class="form-select">
                <option value="" {{ ExplodeVariableBlade($data->program_6) == null ? 'selected' : '' }}>Pilih
                    Variable..</option>
                <option value="CDAT" {{ ExplodeVariableBlade($data->program_6) == 'CDAT' ? 'selected' : '' }}>CDAT
                </option>
                <option value="BREDAT" {{ ExplodeVariableBlade($data->program_6) == 'BREDAT' ? 'selected' : '' }}>BREDAT
                </option>
                <option value="ABDAT" {{ ExplodeVariableBlade($data->program_6) == 'ABDAT' ? 'selected' : '' }}>ABDAT
                </option>
                <option value="HDAT" {{ ExplodeVariableBlade($data->program_6) == 'HDAT' ? 'selected' : '' }}>HDAT
                </option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_6">
                <option value="" selected>Choose...</option>
                <option value=">" {{ ExplodeOperator($data->program_6) == '>' ? 'selected' : '' }}> > </option>
                <option value="<" {{ ExplodeOperator($data->program_6) == '<' ? 'selected' : '' }}>
                    < </option>
                <option value="=" {{ ExplodeOperator($data->program_6) == '=' ? 'selected' : '' }}> = </option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputZip" class="form-label">Nilai</label>
            <input type="date" class="form-control" name="nilai_6" value="{{ $nilai6 }}"
                placeholder="Nilai yang dicari">
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select name="variabel_7" class="form-select">
                <option value="" {{ ExplodeVariableBlade($data->program_7) == null ? 'selected' : '' }}>Pilih
                    Variable..</option>
                <option value="CDAT" {{ ExplodeVariableBlade($data->program_7) == 'CDAT' ? 'selected' : '' }}>CDAT
                </option>
                <option value="BREDAT" {{ ExplodeVariableBlade($data->program_7) == 'BREDAT' ? 'selected' : '' }}>BREDAT
                </option>
                <option value="ABDAT" {{ ExplodeVariableBlade($data->program_7) == 'ABDAT' ? 'selected' : '' }}>ABDAT
                </option>
                <option value="HDAT" {{ ExplodeVariableBlade($data->program_7) == 'HDAT' ? 'selected' : '' }}>HDAT
                </option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_7">
                <option value="" selected>Choose...</option>
                <option value=">" {{ ExplodeOperator($data->program_7) == '>' ? 'selected' : '' }}> > </option>
                <option value="<" {{ ExplodeOperator($data->program_7) == '<' ? 'selected' : '' }}>
                    < </option>
                <option value="=" {{ ExplodeOperator($data->program_7) == '=' ? 'selected' : '' }}> = </option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputZip" class="form-label">Nilai</label>
            <input type="date" class="form-control" name="nilai_7" value="{{ $nilai7 }}"
                placeholder="Nilai yang dicari">
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">Simpan Program</button>
            <a href="{{ route('dashboard-program') }}"><button type="button"
                    class="btn btn-danger">Cancel</button></a>
        </div>
    </form>
    <script src="{{ asset('assets/roles-panel/js/program.js') }}"></script>
@endsection
