@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Form Registrasi Cow</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item">Peternakan</li>
                <li class="breadcrumb-item active">Form Registrasi Sapi</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <form class="row g-3" action="regcowcard/sent" method="post">
        @csrf
        <div class="col-6">
            <label class="form-label">Kode Sapi</label>
            <input type="text" name="id_sapi" placeholder="Masukan Kode Sapi" class="form-control" required>
        </div>
        <div class="col-md-6">
            <label class="form-label">Nama Induk Sapi</label>
            <select name="induk_sapi" class="form-select">
                <option selected disabled>Pilih Induk Sapi</option>
                @foreach($IndukSapi as $data)
                    @if($data->id != null )
                        <option value="{{$data->id}}">{{$data->kode_cowcard}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="col-md-6">
            <label class="form-label">Nama Kandang</label>
            <select name="kandang" class="form-select" required>
                <option value="" selected disabled>Pilih Kandang</option>
                @foreach($kandang as $kand)
                <option value="{{$kand->id}}">{{$kand->nama_kandang}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6">
            <label class="form-label">Nama PEN</label>
            <input type="text" class="form-control" name="pen" placeholder="Masukan PEN Kandang" oninput="this.value = this.value.toUpperCase()" required>
        </div>
        <div class="col-md-6">
            <label class="form-label">Tanggal Lahir</label>
            <input type="date" name="tgl_lhr" class="form-control" required>
        </div>
        <div class="col-md-6">
            <label class="form-label">Berat Badan</label>
            <input type="number" placeholder="Masukan Berat Badan Sapi" name="bb" class="form-control" required>
        </div>

        <div class="col-md-4">
            <label for="inputCity" class="form-label">Jenis Kelamin</label>
            <select name="jenis_kelamin" class="form-select" required>
                <option value="" selected disabled hidden>Jenis Kelamin Sapi...</option>
                <option value="Betina">Betina</option>
                <option value="Jantan">Jantan</option>
            </select>
        </div>
        <div class="col-md-4">
            <label class="form-label">CALF</label>
            <select class="form-select" name="calf" required>
                <option value="" selected disabled hidden>CALF Sapi...</option>
                <option value="Calf 1">Calf 1</option>
                <option value="Calf 2">Calf 2</option>
                <option value="Calf 3">Calf 3</option>
                <option value="Calf 4">Calf 4</option>
                <option value="Calf 5">Calf 5</option>
                <option value="Calf 6">Calf 6</option>
                <option value="Calf 7">Calf 7</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputZip" class="form-label">LAKTASI</label>
            <input type="number" class="form-control" name="LACT" placeholder="Laktasi Saat Ini" required>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-success">Daftar Cow Baru</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </div>
    </form>
@endsection
