@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Buat Program Sync baru</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/peternakan">Dashboard</a></li>
                <li class="breadcrumb-item">Peternakan</li>
                <li class="breadcrumb-item active">Program Sync</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <form class="row g-3" action="{{ route('sync.store') }}" method="POST">
        @csrf
        <div class="col-12">
            <label class="form-label">Nama Program</label>
            <input name="prev_program" value="{{ $idprev }}" hidden>
            <input name="main_program" value="{{ $idmain }}" hidden>
            <input type="text" name="nprog" class="form-control" required>
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select id="1" onchange="PEN()" name="variabel_1" class="form-select">
                <option value="" selected disabled hidden>Pilih Variable..</option>
                <option value="LACT">LACT</option>
                <option value="STATUS">STATUS</option>
                <option value="PEN">PEN</option>
                @if (DIMSYnc($idprev))
                    <option value="DIM">DIM</option>
                @endif
                <option value="TBRD">TBRD</option>
                <option value="DUDRY">DUDRY</option>
                <option value="DDRY">DDRY</option>
                <option value="EASE">EASE</option>
                <option value="FRAGE">FRAGE</option>
                <option value="DOPN">DOPN</option>
                <option value="DCC">DCC</option>
                <option value="DCCD">DCCD</option>
                <option value="DSLH">DSLH</option>
                <option value="SYNC">SYNC</option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_1">
                <option class="hiddens1" selected disabled hidden>Pilih Operator ...</option>
                <option class="hiddens1" value=">">Lebih dari </option>
                <option class="hiddens1" value="<">Kurang dari </option>
                <option class="hiddens1" value=">=">Lebih dari sama dengan</option>
                <option class="hiddens1" value="<=">Kurang dari sama dengan</option>
                <option value="=">Sama dengan</option>
                <option class="sync1" value="+" hidden>(SYNC) Hari setelah DIM Ke...</option>
            </select>
        </div>
        <div class="col-md-4">
            {{-- 1 --}}
            <label for="inputZip" class="form-label">Nilai</label>
            <input type="number" id="nilai_1" class="form-control" name="nilai_1" placeholder="Nilai yang dicari">
            {{-- 2 --}}
            <select class="form-select" id="nilai_1i" name="nilai_1i" hidden>
                <option selected disabled hidden>Pilih PEN ...</option>
                @foreach ($pen as $pen1)
                    <option value="{{ $pen1->PEN }}">{{ $pen1->PEN }}</option>
                @endforeach
            </select>
            {{-- 3 --}}
            <select class="form-select" id="nilai_1ii" name="nilai_1ii" hidden>
                <option selected disabled hidden>Pilih Status ...</option>
                @foreach ($status as $stat)
                    <option value="{{ $stat }}">{{ $stat }}</option>
                @endforeach
            </select>
        </div>



        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select id="2" onchange="PEN()"name="variabel_2" class="form-select">
                <option selected disabled hidden>Pilih Variable..</option>
                <option value="LACT">LACT</option>
                <option value="STATUS">STATUS</option>
                <option value="PEN">PEN</option>
                @if (DIMSYnc($idprev))
                    <option value="DIM">DIM</option>
                @endif
                <option value="TBRD">TBRD</option>
                <option value="DUDRY">DUDRY</option>
                <option value="DDRY">DDRY</option>
                <option value="EASE">EASE</option>
                <option value="FRAGE">FRAGE</option>
                <option value="DOPN">DOPN</option>
                <option value="DCC">DCC</option>
                <option value="DCCD">DCCD</option>
                <option value="DSLH">DSLH</option>
                <option value="SYNC">SYNC</option>
            </select>
        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_2">
                <option class="hiddens2" selected disabled hidden>Pilih Operator . . .</option>
                <option class="hiddens2" value=">">Lebih dari </option>
                <option class="hiddens2" value="<">Kurang dari </option>
                <option class="hiddens2" value=">=">Lebih dari sama dengan</option>
                <option class="hiddens2" value="<=">Kurang dari sama dengan</option>
                <option value="=">Sama dengan</option>
                <option class="sync2" value="+" hidden>(SYNC) Hari setelah DIM Ke...</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputZip" class="form-label">Nilai</label>
            {{-- 1 --}}
            <input type="number" id="nilai_2" class="form-control" name="nilai_2" placeholder="Nilai yang dicari">
            {{-- 2 --}}
            <select class="form-select" id="nilai_2i" name="nilai_2i" hidden>
                <option selected disabled hidden>Pilih PEN ...</option>
                @foreach ($pen as $pen2)
                    <option value="{{ $pen2->PEN }}">{{ $pen2->PEN }}</option>
                @endforeach
            </select>
            {{-- 3 --}}
            <select class="form-select" id="nilai_2ii" name="nilai_2ii" hidden>
                <option selected disabled hidden>Pilih Status ...</option>
                @foreach ($status as $stat)
                    <option value="{{ $stat }}">{{ $stat }}</option>
                @endforeach
            </select>

        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select id="3" onchange="PEN()" name="variabel_3" class="form-select">
                <option value="" selected disabled hidden>Pilih Variable..</option>
                <option value="LACT">LACT</option>
                <option value="STATUS">STATUS</option>
                <option value="PEN">PEN</option>
                @if (DIMSYnc($idprev))
                    <option value="DIM">DIM</option>
                @endif
                <option value="TBRD">TBRD</option>
                <option value="DUDRY">DUDRY</option>
                <option value="DDRY">DDRY</option>
                <option value="EASE">EASE</option>
                <option value="FRAGE">FRAGE</option>
                <option value="DOPN">DOPN</option>
                <option value="DCC">DCC</option>
                <option value="DCCD">DCCD</option>
                <option value="DSLH">DSLH</option>
                <option value="SYNC">SYNC</option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_3">
                <option class="hiddens3" selected disabled hidden>Pilih Operator . . .</option>
                <option class="hiddens3" value=">">Lebih dari </option>
                <option class="hiddens3" value="<">Kurang dari </option>
                <option class="hiddens3" value=">=">Lebih dari sama dengan</option>
                <option class="hiddens3" value="<=">Kurang dari sama dengan</option>
                <option value="=">Sama dengan</option>
                <option class="sync3" value="+" hidden>(SYNC) Hari setelah DIM Ke...</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputZip" class="form-label">Nilai</label>
            {{-- 1 --}}
            <input type="number" id="nilai_3" class="form-control" name="nilai_3" placeholder="Nilai yang dicari">
            {{-- 2 --}}
            <select class="form-select" id="nilai_3i" name="nilai_3i" hidden>
                <option selected disabled hidden>Pilih PEN ...</option>
                @foreach ($pen as $pen3)
                    <option value="{{ $pen3->PEN }}">{{ $pen3->PEN }}</option>
                @endforeach
            </select>
            {{-- 3 --}}
            <select class="form-select" id="nilai_3ii" name="nilai_3ii" hidden>
                <option selected disabled hidden>Pilih Status ...</option>
                @foreach ($status as $stat)
                    <option value="{{ $stat }}">{{ $stat }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select id="4" onchange="PEN()" name="variabel_4" class="form-select">
                <option value="" selected disabled hidden>Pilih Variable..</option>
                <option value="LACT">LACT</option>
                <option value="PEN">PEN</option>
                <option value="STATUS">STATUS</option>
                @if (DIMSYnc($idprev))
                    <option value="DIM">DIM</option>
                @endif
                <option value="TBRD">TBRD</option>
                <option value="DUDRY">DUDRY</option>
                <option value="DDRY">DDRY</option>
                <option value="EASE">EASE</option>
                <option value="FRAGE">FRAGE</option>
                <option value="DOPN">DOPN</option>
                <option value="DCC">DCC</option>
                <option value="DCCD">DCCD</option>
                <option value="DSLH">DSLH</option>
                <option value="SYNC">SYNC</option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_4">
                <option class="hiddens4" selected disabled hidden>Pilih Operator . . .</option>
                <option class="hiddens4" value=">">Lebih dari </option>
                <option class="hiddens4" value="<">Kurang dari </option>
                <option class="hiddens4" value=">=">Lebih dari sama dengan</option>
                <option class="hiddens4" value="<=">Kurang dari sama dengan</option>
                <option value="=">Sama dengan</option>
                <option class="sync4" value="+" hidden>(SYNC) Hari setelah DIM Ke...</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputZip" class="form-label">Nilai</label>
            {{-- 1 --}}
            <input type="number" id="nilai_4" class="form-control" name="nilai_4" placeholder="Nilai yang dicari">
            {{-- 2 --}}
            <select class="form-select" id="nilai_4i" name="nilai_4i" hidden>
                <option selected disabled hidden>Pilih PEN ...</option>
                @foreach ($pen as $pen4)
                    <option value="{{ $pen4->PEN }}">{{ $pen4->PEN }}</option>
                @endforeach
            </select>
            {{-- 3 --}}
            <select class="form-select" id="nilai_4ii" name="nilai_4ii" hidden>
                <option selected disabled hidden>Pilih Status ...</option>
                @foreach ($status as $stat)
                    <option value="{{ $stat }}">{{ $stat }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select id="5" onchange="PEN()" name="variabel_5" class="form-select">
                <option value="" selected disabled hidden>Pilih Variable..</option>
                <option value="LACT">LACT</option>
                @if (DIMSYnc($idprev))
                    <option value="DIM">DIM</option>
                @endif
                <option value="STATUS">STATUS</option>
                <option value="PEN">PEN</option>
                <option value="TBRD">TBRD</option>
                <option value="DUDRY">DUDRY</option>
                <option value="DDRY">DDRY</option>
                <option value="EASE">EASE</option>
                <option value="FRAGE">FRAGE</option>
                <option value="DOPN">DOPN</option>
                <option value="DCC">DCC</option>
                <option value="DCCD">DCCD</option>
                <option value="DSLH">DSLH</option>
                <option value="SYNC">SYNC</option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_5">
                <option class="hiddens5" selected disabled hidden>Pilih Operator . . .</option>
                <option class="hiddens5" value=">">Lebih dari </option>
                <option class="hiddens5" value="<">Kurang dari </option>
                <option class="hiddens5" value=">=">Lebih dari sama dengan</option>
                <option class="hiddens5" value="<=">Kurang dari sama dengan</option>
                <option value="=">Sama dengan</option>
                <option class="sync5" value="+" hidden>(SYNC) Hari setelah DIM Ke...</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputZip" class="form-label">Nilai</label>
            {{-- 1 --}}
            <input type="number" id="nilai_5" class="form-control" name="nilai_5" placeholder="Nilai yang dicari">
            {{-- 2 --}}
            <select class="form-select" id="nilai_5i" name="nilai_5i" hidden>
                <option selected disabled hidden>Pilih PEN ...</option>
                @foreach ($pen as $pen5)
                    <option value="{{ $pen5->PEN }}">{{ $pen5->PEN }}</option>
                @endforeach
            </select>
            {{-- 3 --}}
            <select class="form-select" id="nilai_5ii" name="nilai_5ii" hidden>
                <option selected disabled hidden>Pilih Status ...</option>
                @foreach ($status as $stat)
                    <option value="{{ $stat }}">{{ $stat }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select name="variabel_6" class="form-select">
                <option value="" selected disabled hidden>Pilih Variable..</option>
                <option value="CDAT">CDAT</option>
                <option value="BREDAT">BREDAT</option>
                <option value="ABDAT">ABDAT</option>
                <option value="HDAT">HDAT</option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_6">
                <option selected disabled hidden>Pilih Operator . . .</option>
                <option value=">">Lebih dari </option>
                <option value="<">Kurang dari </option>
                <option value=">=">Lebih dari sama dengan</option>
                <option value="<=">Kurang dari sama dengan</option>
                <option value="=">Sama dengan</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputZip" class="form-label">Nilai</label>
            <input type="date" class="form-control" name="nilai_6" placeholder="Nilai yang dicari">
        </div>
        <div class="col-md-4">
            <label for="inputCity" class="form-label">Variabel</label>
            <select name="variabel_7" class="form-select">
                <option value="" selected disabled hidden>Pilih Variable..</option>
                <option value="CDAT">CDAT</option>
                <option value="BREDAT">BREDAT</option>
                <option value="ABDAT">ABDAT</option>
                <option value="HDAT">HDAT</option>
            </select>

        </div>
        <div class="col-md-4">
            <label class="form-label">Operator</label>
            <select class="form-select" name="operator_7">
                <option selected disabled hidden>Pilih Operator . . .</option>
                <option value=">">Lebih dari </option>
                <option value="<">Kurang dari </option>
                <option value=">=">Lebih dari sama dengan</option>
                <option value="<=">Kurang dari sama dengan</option>
                <option value="=">Sama dengan</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="inputZip" class="form-label">Nilai</label>
            <input type="date" class="form-control" name="nilai_7" placeholder="Nilai yang dicari">
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-warning">Simpan Program</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </div>
    </form>
    <script src="{{ asset('assets/roles-panel/js/sync.js') }}"></script>
@endsection
