@extends('main.main')
@section('content')
<div class="pagetitle">
    <h1>Cow Card Details Status : Bred</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item">Peternakan</li>
        <li class="breadcrumb-item active">Status Bred</li>
      </ol>
    </nav>
</div><!-- End Page Title -->
<form action="regcowcard/sent" method="post">
    @csrf
    @foreach ($cow as $p)
    <div class="row px-3">
        <label class="mr-sm-2">
            <h6 class="mb-0 text-sm">Tanggal Status Diberikan</h6>
        </label>
        <input class="mb-4" type="text" name="created"
            value="{{ DatesIdn($p->created_at) }}" disabled>
    </div>
    <div class="row px-3">
        <label class="mr-sm-2">
            <h6 class="mb-0 text-sm">ID Sapi</h6>
        </label>
        <input class="mb-4" type="text" name="id"
            value="{{ $p->sapi_id }}" disabled>
    </div>
    <div class="row px-3">
        <label class="mr-sm-2">
            <h6 class="mb-0 text-sm">Nama Kandang</h6>
        </label>
        <input class="mb-4" type="text" name="kandang"
            value="{{ $p->kandang_id }}" disabled>
    </div>
    <div class="row px-3">
        <label class="mr-sm-2">
            <h6 class="mb-0 text-sm">Nama PEN</h6>
        </label>
        <input class="mb-4" type="text" name="pen"
            value="{{ $p->PEN }}" disabled>
    </div>
    <div class="row px-3">
        <label class="mr-sm-2">
            <h6 class="mb-0 text-sm">Tanggal Lahir</h6>
        </label>
        <input class="mb-4" type="date" name="tgl_lhr"
            value="{{ $p->tanggal_lahir }}" disabled>
    </div>
    <div class="row px-3">
        <label class="mr-sm-2">
            <h6 class="mb-0 text-sm">SEX</h6>
        </label>
        <input class="mb-4" type="text" name="pen"
            value="{{ $p->SEX }}" disabled>
    </div>

    <div class="row px-3">
        <label class="mr-sm-2">
            <h6 class="mb-0 text-sm">Laktasi</h6>
        </label>
        <input class="mb-4" type="text" name="pen"
            value="{{ $p->LACT }}" disabled>
    </div>
    <div class="row px-3">
        <label class="mr-sm-2">
            <h6 class="mb-0 text-sm">DIM</h6>
        </label>
        <input class="mb-4" type="text" name="pen"
            value="{{ $p->DIM }}" disabled>
    </div>
    <div class="row px-3">
        <label class="mr-sm-2">
            <h6 class="mb-0 text-sm">CDAT</h6>
        </label>
        <input class="mb-4" type="date" name="CDAT"
            value="{{ $p->CDAT }}" disabled>
    </div>
    <div class="row px-3">
        <label class="mr-sm-2">
            <h6 class="mb-0 text-sm">TBRD</h6>
        </label>
        <input class="mb-4" type="text" name="TBRD"
            value="{{ $p->TBRD }}" disabled>
    </div>
    @endforeach



    <div class="row mb-3 px-3">
        <button type="submit" class="btn btn-primary text-center" >
            Update Informasi
        </button>
    </div>
</form>
@endsection
