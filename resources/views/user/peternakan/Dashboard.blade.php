@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Dashboard Peternakan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/peternakan">Dashboard</a></li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <div class="row">
        {{-- JENIS KELAMIN --}}
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">JENIS KELAMIN SAPI</h5>

                    <!-- Pie Chart -->
                    <canvas id="pieChart" style="max-height: 400px;"></canvas>
                    <!-- End Pie CHart -->

                </div>
            </div>
        </div>
        {{-- STATUS --}}
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">STATUS SAPI</h5>

                    <!-- Polar Area Chart -->
                    <canvas id="polarAreaChart" style="max-height: 400px;"></canvas>
                    <!-- End Polar Area Chart -->

                </div>
            </div>
        </div>
        {{-- KESEHATAN SAPI --}}
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">KESEHATAN SAPI</h5>

                    <!-- Doughnut Chart -->
                    <canvas id="doughnutChart" style="max-height: 400px;"></canvas>

                    <!-- End Doughnut CHart -->

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Produksi</h5>

                    <!-- Doughnut Chart -->
                    <canvas id="lineChart" style="max-height: 400px;"></canvas>

                    <!-- End Doughnut CHart -->

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Kandang dan PEN Peternakan</h5>

                    <?php
                    $a = 0;
                    ?>
                    <div class="accordion" id="accordionExample">
                        @foreach ($pen as $pens)
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse{{ $a }}" aria-expanded="true"
                                        aria-controls="collapseOne">
                                        Kandang {{ $pens->nama_kandang }} {{ $pens->PEN == null ? '- Tidak ada PEN' :  '- PEN '. $pens->PEN }}
                                    </button>
                                </h2>
                                <div id="collapse{{ $a }}" class="accordion-collapse collapse"
                                    aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">

                                        <div class="row">
                                            <div class="card recent-sales">
                                                <div class="card-body">
                                                    <h5 class="card-title">Activity Peternakan</h5>
                                                    <div class="table-responsive">
                                                        <table class="table table-striped datatable">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Kode Sapi</th>
                                                                    <th scope="col">DIM</th>
                                                                    <th scope="col" style="text-align:center;">Status
                                                                    </th>
                                                                    <th scope="col">Aksi</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach (PEN($pens->id, $pens->PEN) as $cow)
                                                                    <tr>
                                                                        <td> {{ $cow->kode_cowcard }} </td>
                                                                        <td> {{ $cow->DIM }} </td>
                                                                        <td>
                                                                            <div style="text-align:center;">
                                                                                @if ($cow->Status == 'Sold')
                                                                                    <button type="button"
                                                                                        class="btn btn-success btn-sm"
                                                                                        data-bs-toggle="modal"
                                                                                        data-bs-target="#cowcard{{ $cow->id }}">{{ $cow->Status }}
                                                                                    </button>
                                                                                @elseif ($cow->Status == 'Death')
                                                                                    <button type="button"
                                                                                        class="btn btn-danger btn-sm"
                                                                                        data-bs-toggle="modal"
                                                                                        data-bs-target="#cowcard{{ $cow->id }}">{{ $cow->Status }}
                                                                                    </button>
                                                                                @else
                                                                                    <button type="button"
                                                                                        class="btn {{ $cow->Status == 'Belum Ada Status' ? 'btn-light' : 'btn-primary' }} btn-sm"
                                                                                        data-bs-toggle="modal"
                                                                                        data-bs-target="#cowcard{{ $cow->id }}">{{ $cow->Status }}
                                                                                    </button>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-success btn-sm" type="button"
                                                                                href="{{ route('view-status-details', $cow->id) }}">
                                                                                <i class="bi bi-info-circle"></i>
                                                                                &nbsp; Detail Sapi
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $a++;
                            ?>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Activity Peternakan</h5>
                            <div class="table-responsive">
                                <table class="table table-striped datatable">
                                    <thead>
                                        <tr>
                                            <th scope="col">Tanggal</th>
                                            <th scope="col">Aktivitas</th>
                                            <th scope="col">Kode Sapi</th>
                                            <th scope="col">DIM</th>
                                            <th scope="col">Ket 1</th>
                                            <th scope="col">Ket 2</th>
                                            <th scope="col">Ket 3</th>
                                            <th scope="col">Ket 4</th>
                                            <th scope="col">Ket 5</th>
                                            <th scope="col">Ket 6</th>
                                            <th scope="col">Ket 7</th>
                                            <th scope="col">Diproses Oleh</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($activity as $p)
                                            <tr>
                                                <td> {{ DatesIdn($p->created_at) }} </td>
                                                <td> {{ $p->activity }} </td>
                                                <td> {{ $p->kode_cowcard }} </td>
                                                <td> {{ $p->DIM != NULL ? $p->DIM : "-" }} </td>
                                                <td> {{ $p->ket_1 }} </td>
                                                <td> {{ $p->ket_2 }} </td>
                                                <td> {{ $p->ket_3 }} </td>
                                                <td> {{ $p->ket_4 }} </td>
                                                <td> {{ $p->ket_5 }} </td>
                                                <td> {{ $p->ket_6 }} </td>
                                                <td> {{ $p->ket_7 }} </td>
                                                <td> {{ pemeriksa($p->input_by) }} </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->
            </div>
        </div><!-- End Left side columns -->
    </div>
    {{-- COMMING SOON --}}
    {{-- <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Program</h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Tanggal Dibuat</th>
                                        <th scope="col">Nama Program</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no_L = 1;
                                    $no_P = 1;
                                    ?>
                                    <td> $no_L++ </td>
                                    <td> $p->created_at </td>
                                    <td> $p->nama </td>
                                    <td> $p->status </td>
                                </tbody>
                            </table>
                            <button type="button" class="btn btn-success">Register NET</button>
                            <button type="button" class="btn btn-success">Register Event</button>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->
            </div>
        </div><!-- End Left side columns -->
    </div>
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Summary</h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Kandang</th>
                                        <th scope="col">Jumlah PEN</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no_L = 1;
                                    $no_P = 1;
                                    ?>
                                    <td> $no_L++ </td>
                                    <td> $p->nama_kandang </td>
                                    <td> count : $p->kandang->pen </td>
                                    <td> $p->status </td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->
            </div>
        </div><!-- End Left side columns -->
    </div>
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Set Up</h5>
                            <button type="button" class="btn btn-warning">Register PEN</button>
                            <button type="button" class="btn btn-warning">Register SIR</button>
                            <button type="button" class="btn btn-warning">Register Vet</button>
                            <button type="button" class="btn btn-warning">Tambah</button>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->
            </div>
        </div><!-- End Left side columns -->
    </div> --}}
    <script src="{{ asset('assets/roles-panel/js/dashboard.js') }}"></script>
@endsection
