<div class="row px-3">
    <label for="kandang" class="form-label"> Kandang
    </label>
    <select name="selectKandang" id="selectKandang" class="form-control mb-4">
        <option value="" disabled selected hidden>Select Kandang</option>
        @foreach ($kandangs as $kandang)
        <option value="{{ $kandang->id }}" {{ ($p->nama_kandang == $kandang->nama_kandang ? 'selected' : "") }}>{{ $kandang->nama_kandang }}</option>
        @endforeach
    </select>
</div>
<div class="row px-3">
    <label for="pen" class="form-label"> PEN
    </label>
    <input name="PEN" required="required" type="text" class="form-control" id="PEN" value="{{ $p->PEN }}" placeholder="PEN">
</div>
<hr />
<label for="pen" class="form-label"> PEN Yang Tersedia
</label>
@foreach ($pens as $pen)
<span class="badge bg-info text-dark">{{ $pen->PEN }}</span>
@endforeach