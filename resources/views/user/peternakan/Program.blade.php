@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Dashboard Program Peternakan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Auth-Roles">Dashboard</a></li>
                <li class="breadcrumb-item active">Program</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-16">
            <div class="row">
                <div class="card-body">
                    <a href="{{ route('make-program', $idpeternakan) }}">
                        <div class="d-grid gap-2 mt-3" style="text-align:center;">
                            <button class="btn btn-lg btn-primary ">
                                <i class="bi bi-bar-chart-steps"></i>
                                Buat Program Filtering Baru
                            </button>
                        </div>
                    </a>
                </div>
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <hr>
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title text-uppercase">Program Filtering Zantara</h3>
                                </div>
                            </div>
                            <hr style="margin-top: 0px;margin-bottom: 10px;">
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th class="col-md-1">No</th>
                                        <th class="col-md-3">Tanggal Dibuat</th>
                                        <th class="col-md-3">Nama Program</th>
                                        <th class="col-md-5">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    ?>
                                    @foreach ($program as $p)
                                        <tr>
                                            <td> {{ $no++ }} </td>
                                            <td> {{ DatesIdn($p->created_at) }} </td>
                                            <td> {{ $p->namaprogram }} </td>
                                            <td>
                                                <a href="{{ route('show-program', $p->id) }}">
                                                    <button class="btn btn-info btn-sm" type="button">Jalankan
                                                        Program</button>
                                                </a>
                                                {{-- <a href="{{ route('edit-program', $p->id) }}">
                                                    <button class="btn btn-warning btn-sm" type="button">Edit
                                                        Program</button>
                                                </a> --}}
                                                <a href="{{ route('delete-program', $p->id) }}">
                                                    <button class="btn btn-danger btn-sm" type="button">Hapus
                                                        Program</button>
                                                </a>
                                                {{-- <a href="{{ route('sync.show', $p->id) }}">
                                                    <button class="btn btn-warning btn-sm" type="button">Buat Sync</button>
                                                </a> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->
            </div>
        </div><!-- End Left side columns -->
    </div>
@endsection
