@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Daftar Kemitraan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('kemitraan.index') }}">Dashboard</a></li>
                <li class="breadcrumb-item">Daftar Kemitraan</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Daftar Kemitraan</h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Tanggal Bergabung</th>
                                        <th scope="col">Nama Mitra</th>
                                        <th scope="col" style="text-align: center">Status</th>
                                        <th scope="col" style="text-align: center">Keterangan</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                        $no = 1;
                                        ?>
                                    @foreach($MitraPeternakan as $vMitra)
                                    <tr>
                                        <td> {{$no++}}. </td>
                                        <td> {{DatesIdn($vMitra->created_at)}} </td>
                                        <td> {{$vMitra->mitra->nama_instansi}} </td>
                                        <td  style="text-align: center;pointer-events: none;">
                                            @if($vMitra->approval == 1)
                                            <button class="btn btn-outline-success btn-sm">{{ $vMitra->status }}
                                            </button>
                                            @elseif($vMitra->decline == 1)
                                            <button class="btn btn-outline-danger btn-sm">{{ $vMitra->status }}
                                            </button>
                                            @else
                                            <button class="btn btn-outline-warning btn-sm">{{ $vMitra->status }}
                                            </button>
                                            @endif
                                        </td>
                                        <td>{{ $vMitra->desc_pengajuan }}</td>
                                        <td>
                                            @if ($vMitra->approval == 0 && $vMitra->decline == 0)
                                        <a href="/approve-kemitraan/{{$vMitra->id}}/{{$vMitra->mitra_id}}/{{$vMitra->peternakan_id}}">
                                            <button type="button" class="btn btn-success btn-sm">
                                            Setujui
                                            </button>
                                        </a>

                                            <button type="button"
                                            class="btn btn-danger btn-sm"
                                            data-bs-toggle="modal"
                                            data-bs-target="#ModalTolakMitra{{$vMitra->id}}">
                                            Tolak Pengajuan
                                            </button>
                                            @endif
                                        </td>
                                        @include('user.peternakan.kemitraan.ModalTolakMitra')
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->

            </div>
        </div>
    </div>
@endsection
