<div class="modal fade" id="ModalTolakMitra{{ $vMitra->id }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tolak Pengajuan Mitra {{ $vMitra->mitra->nama_instansi }}
                </h5>
                <button type="button" class="btn-close"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('tolak-mitra') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="row px-3">
                        <input class=" form-control mb-4" type="text"
                            name="id" value="{{ $vMitra->id }}"
                            hidden>
                            <input class=" form-control mb-4" type="text"
                            name="name" value="{{ $vMitra->mitra->nama_instansi }}"
                            hidden>
                    </div>
                    <div class="row px-3">
                        <label for="Desc"
                            class="form-label">Alasan Menolak Pengajuan
                        </label>
                        <textarea name="Desc"
                            required="required"
                            type="text"
                            class="form-control"
                            id="Desc" value=""
                            placeholder="Keterangan">
                    </textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-secondary"
                        data-bs-dismiss="modal">Tutup
                    </button>
                    <button type="submit"
                        class="btn btn-success">&nbsp;Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
