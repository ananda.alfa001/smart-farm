@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Cow Card All Details</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item">Peternakan</li>
                <li class="breadcrumb-item active">Riwayat Sakit</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <div class="row">
        <!-- Recent Sales -->
        <div class="col-24">
            <div class="card recent-sales">
                <div class="card-body">
                    <h5 class="card-title">Riwayat sakit {{ $kodesapi }}</h5>
                    <div class="table-responsive">
                        <table class="table table-striped datatable">
                            <thead>
                                <tr>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">LACT</th>
                                    <th scope="col">Nama Vaksin</th>
                                    <th scope="col">Keterangan</th>
                                    <th scope="col">Penyuntik Vaksin</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($vaksin as $p)
                                    <tr>
                                        <td> {{ $p->tanggal }} </td>
                                        <td> {{ $p->LACT }} </td>
                                        <td> {{ $p->nama_vaksin }} </td>
                                        <td> {{ $p->keterangan }} </td>
                                        <td> {{ pemeriksa($p->input_by) }} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- End Recent Sales -->
    </div>

@endsection
