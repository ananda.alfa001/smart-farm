@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Cow Card All Details</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item">Peternakan</li>
                <li class="breadcrumb-item active">Riwayat Sakit</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <div class="row">
        <!-- Recent Sales -->
        <div class="col-24">
            <div class="card recent-sales">
                <div class="card-body">
                    <h5 class="card-title">Riwayat sakit {{ $kodesapi }}</h5>
                    <div class="table-responsive">
                        <table class="table table-striped datatable">
                            <thead>
                                <tr>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">LACT</th>
                                    <th scope="col">Gejala</th>
                                    <th scope="col">Diagnosis</th>
                                    <th scope="col">Penanganan/Obat</th>
                                    <th scope="col">Pemeriksa</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($penyakit as $p)
                                    <tr>
                                        <td> {{ DatesIdn($p->created_at) }} </td>
                                        <td> {{ $p->LACT }} </td>
                                        <td> {{ $p->gejala == 'Sembuh' ? '-' :  $p->gejala}} </td>
                                        <td> {{ $p->diagnosis == 'Sembuh' ? '-' :  $p->diagnosis}} </td>
                                        <td> {{ $p->penanganan != '0' ? ($p->penanganan == 'Sembuh' ? 'Dinyatakan Sembuh' :  $p->penanganan) : 'Belum Ditangani' }} </td>
                                        <td> {{ pemeriksa($p->input_by)}} </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- End Recent Sales -->
    </div>

@endsection
