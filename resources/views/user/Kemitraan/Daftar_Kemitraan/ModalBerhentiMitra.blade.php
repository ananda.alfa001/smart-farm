<div class="modal fade" id="ModalBerhentiMitra{{ $vPeternak->peternakan_id }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Berhenti Bermitra Dengan {{ $vPeternak->peternakan->nama_peternakan }}
                </h5>
                <button type="button" class="btn-close"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="route{{ ('BerhentiBermitra') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="row px-3">
                        <input class=" form-control mb-4" type="text"
                            name="id" value="{{ $vPeternak->id }}"
                            hidden>
                            <input class=" form-control mb-4" type="text"
                            name="name" value="{{ $vPeternak->peternakan->nama_peternakan }}"
                            hidden>
                            <input class=" form-control mb-4" type="text"
                            name="peternakan_id" value="{{ $vPeternak->peternakan_id }}"
                            hidden>
                    </div>
                    <div class="row px-3">
                        <label for="Desc"
                            class="form-label">Alasan Berhenti Bermitra
                        </label>
                        <textarea name="Desc"
                            required="required"
                            type="text"
                            class="form-control"
                            id="Desc" value=""
                            placeholder="Keterangan">
                    </textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-secondary"
                        data-bs-dismiss="modal">Tutup
                    </button>
                    <button type="submit"
                        class="btn btn-success">&nbsp;Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
