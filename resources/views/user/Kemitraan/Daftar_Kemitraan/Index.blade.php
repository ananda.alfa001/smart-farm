@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Status Kemitraan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('kemitraan.index') }}">Dashboard</a></li>
                <li class="breadcrumb-item">Daftar Kemitraan</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Daftar Kemitraan</h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Tanggal Bergabung</th>
                                        <th scope="col">Nama Peternakan</th>
                                        <th scope="col" style="text-align: center">Status</th>
                                        <th scope="col" style="text-align: center">Keterangan</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                        $no = 1;
                                        ?>
                                    @foreach($MitraPeternakan as $vPeternak)
                                    <tr>
                                        <td> {{$no++}}. </td>
                                        <td> {{DatesIdn($vPeternak->created_at)}} </td>
                                        <td> {{$vPeternak->peternakan->nama_peternakan}} </td>
                                        <td  style="text-align: center;pointer-events: none;">
                                            @if($vPeternak->status == 1)
                                            <button class="btn btn-outline-success btn-sm">Bermitra
                                            </button>
                                            @else
                                            <button class="btn btn-outline-danger btn-sm">Berhenti
                                            </button>
                                            @endif
                                        </td>
                                        <td>{{ $vPeternak->Desc }}</td>
                                        <td>
                                            @if ($vPeternak->status == 1)
                                            <button type="button"
                                            class="btn btn-primary btn-sm"
                                            data-bs-toggle="modal"
                                            data-bs-target="#ModalBerhentiMitra{{$vPeternak->peternakan_id}}">
                                            Berhenti Bermitra
                                            </button>
                                            @endif
                                        </td>
                                        @include('user.Kemitraan.Daftar_Kemitraan.ModalBerhentiMitra')
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->

            </div>
        </div>
    </div>
@endsection
