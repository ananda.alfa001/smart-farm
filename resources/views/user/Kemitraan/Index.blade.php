@extends('main.main')
@section('content')
<div class="pagetitle">
    <h1>Dashboard</h1>
    {{-- <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item" active>Dashboard</a></li>
        </ol>
    </nav> --}}
</div><!-- End Page Title -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"></h5>
                <div class="row">
                    {{-- JENIS KELAMIN --}}
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">JENIS KELAMIN SAPI</h5>

                                <!-- Pie Chart -->
                                <canvas id="pieChart" style="max-height: 400px;"></canvas>
                                <!-- End Pie CHart -->

                            </div>
                        </div>
                    </div>
                    {{-- STATUS --}}
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">STATUS SAPI</h5>

                                <!-- Polar Area Chart -->
                                <canvas id="polarAreaChart" style="max-height: 400px;"></canvas>
                                <!-- End Polar Area Chart -->

                            </div>
                        </div>
                    </div>
                    {{-- KESEHATAN SAPI --}}
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">KESEHATAN SAPI</h5>

                                <!-- Doughnut Chart -->
                                <canvas id="doughnutChart" style="max-height: 400px;"></canvas>

                                <!-- End Doughnut CHart -->

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Produksi</h5>

                                <!-- Doughnut Chart -->
                                <canvas id="lineChart" style="max-height: 400px;"></canvas>

                                <!-- End Doughnut CHart -->

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-24">
                        <div class="card recent-sales">
                            <div class="card-body">
                                <h5 class="card-title">Aktifitas Peternakan</h5>
                                <div class="table-responsive">
                                    <table class="table table-striped datatable">
                                        <thead>
                                            <tr>
                                                <th scope="col">Nama Peternakan</th>
                                                <th scope="col">Tanggal</th>
                                                <th scope="col">Aktivitas</th>
                                                <th scope="col">Kode Sapi</th>
                                                <th scope="col">DIM</th>
                                                <th scope="col">Ket 1</th>
                                                <th scope="col">Ket 2</th>
                                                <th scope="col">Ket 3</th>
                                                <th scope="col">Ket 4</th>
                                                <th scope="col">Ket 5</th>
                                                <th scope="col">Ket 6</th>
                                                <th scope="col">Ket 7</th>
                                                <th scope="col">Diproses Oleh</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($MitraPeternakan as $vPeternakan)
                                                @foreach (activity($vPeternakan->peternakan_id) as $activitys)
                                                    <tr>
                                                        <td> {{ $vPeternakan->peternakan->nama_peternakan }}</td>
                                                        <td> {{ date('D, d/m/Y', strtotime($activitys->created_at)) }} </td>
                                                        <td> {{ $activitys->activity }} </td>
                                                        <td> {{ $activitys->kode_cowcard }} </td>
                                                        <td> {{ $activitys->DIM != NULL ? $activitys->DIM : "-" }} </td>
                                                        <td> {{ $activitys->ket_1 }} </td>
                                                        <td> {{ $activitys->ket_2 }} </td>
                                                        <td> {{ $activitys->ket_3 }} </td>
                                                        <td> {{ $activitys->ket_4 }} </td>
                                                        <td> {{ $activitys->ket_5 }} </td>
                                                        <td> {{ $activitys->ket_6 }} </td>
                                                        <td> {{ $activitys->ket_7 }} </td>
                                                        <td> {{ pemeriksa($activitys->input_by) }} </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('assets/roles-panel/js/Mitra/Index.js') }}"></script>
@endsection
