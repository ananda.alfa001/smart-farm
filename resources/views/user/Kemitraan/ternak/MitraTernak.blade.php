@extends('main.main')
@section('content')
<div class="pagetitle">
    <h1>Daftar Kemitraan</h1>
    {{-- <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item" active>Dashboard</a></li>
        </ol>
    </nav> --}}
</div><!-- End Page Title -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Daftar Mitra Peternakan</h5>

                <?php
                $a = 1;
                ?>
                <div class="accordion" id="accordionExample">
                    @foreach ($MitraPeternakan as $MitraPeternaks)
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button cek" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse{{ $a }}" aria-expanded="true"
                                    data-id="{{ $MitraPeternaks->peternakan_id }}"
                                    data-row="{{ $a }}"
                                    aria-controls="collapseOne">
                                        Peternak {{$MitraPeternaks->peternakan->nama_peternakan}}
                                </button>
                            </h2>
                            <div id="collapse{{ $a }}" class="accordion-collapse collapse"
                                aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">

                                    <div class="row">
                                        {{-- JENIS KELAMIN --}}
                                        <div class="col-lg-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title">JENIS KELAMIN SAPI</h5>

                                                    <!-- Pie Chart -->
                                                    <canvas id="pieChart{{ $a }}" style="max-height: 400px;"></canvas>
                                                    <!-- End Pie CHart -->

                                                </div>
                                            </div>
                                        </div>
                                        {{-- STATUS --}}
                                        <div class="col-lg-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title">STATUS SAPI</h5>

                                                    <!-- Polar Area Chart -->
                                                    <canvas id="polarAreaChart{{ $a }}" style="max-height: 400px;"></canvas>
                                                    <!-- End Polar Area Chart -->

                                                </div>
                                            </div>
                                        </div>
                                        {{-- KESEHATAN SAPI --}}
                                        <div class="col-lg-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title">KESEHATAN SAPI</h5>

                                                    <!-- Doughnut Chart -->
                                                    <canvas id="doughnutChart{{ $a }}" style="max-height: 400px;"></canvas>

                                                    <!-- End Doughnut CHart -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title">Produksi</h5>

                                                    <!-- Doughnut Chart -->
                                                    <canvas id="lineChart{{ $a }}" style="max-height: 400px;"></canvas>

                                                    <!-- End Doughnut CHart -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <!-- Recent Sales -->
                                        <div class="col-24">
                                            <div class="card recent-sales">
                                                <div class="card-body">
                                                    <h5 class="card-title">Activity Peternakan</h5>
                                                    <div class="table-responsive">
                                                        <table class="table table-striped datatable">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Tanggal</th>
                                                                    <th scope="col">Aktivitas</th>
                                                                    <th scope="col">Kode Sapi</th>
                                                                    <th scope="col">DIM</th>
                                                                    <th scope="col">Ket 1</th>
                                                                    <th scope="col">Ket 2</th>
                                                                    <th scope="col">Ket 3</th>
                                                                    <th scope="col">Ket 4</th>
                                                                    <th scope="col">Ket 5</th>
                                                                    <th scope="col">Ket 6</th>
                                                                    <th scope="col">Ket 7</th>
                                                                    <th scope="col">Diproses Oleh</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach (activity($MitraPeternaks->peternakan->id) as $activitys)
                                                                    <tr>
                                                                        <td> {{ date('D, d/m/Y', strtotime($activitys->created_at)) }} </td>
                                                                        <td> {{ $activitys->activity }} </td>
                                                                        <td> {{ $activitys->kode_cowcard }} </td>
                                                                        <td> {{ $activitys->DIM != NULL ? $activitys->DIM : "-" }} </td>
                                                                        <td> {{ $activitys->ket_1 }} </td>
                                                                        <td> {{ $activitys->ket_2 }} </td>
                                                                        <td> {{ $activitys->ket_3 }} </td>
                                                                        <td> {{ $activitys->ket_4 }} </td>
                                                                        <td> {{ $activitys->ket_5 }} </td>
                                                                        <td> {{ $activitys->ket_6 }} </td>
                                                                        <td> {{ $activitys->ket_7 }} </td>
                                                                        <td> {{ pemeriksa($activitys->input_by) }} </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- End Recent Sales -->
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php
                        $a++;
                        ?>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>
<script src="{{ asset('assets/roles-panel/js/Mitra/mitraTernak.js') }}"></script>
@endsection
