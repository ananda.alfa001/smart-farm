@extends('main.main')
@section('content')
    <div class="pagetitle">
        <h1>Permintaan Mitra</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('kemitraan.index') }}">Dashboard</a></li>
                <li class="breadcrumb-item">Permintaan Kemitraan</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Tambah Relasi Mitra Peternakan</h5>
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse" aria-expanded="true" aria-controls="collapseOne">
                                    Form Tambah Relasi Mitra Peternakan
                                </button>
                            </h2>
                            <div id="collapse" class="accordion-collapse collapse" aria-labelledby="headingOne"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <div class="row">
                                        <div class="table-responsive">
                                            <br>
                                            <form method="POST" action="{{ route('kemitraan.store') }}">
                                                @csrf
                                                <div class="row mb-3"> <label class="col-sm-2 col-form-label">Pilih
                                                        Peternakan</label>
                                                    <div class="col-sm-10">
                                                        <select name="peternakan" class="form-select invite-peternakan"
                                                            aria-label="Default select example" style="width: 100%"
                                                            required>
                                                            <option selected disabled hidden value="">
                                                                Pilih Peternakan</option>
                                                            @foreach ($peternakan as $key)
                                                                <option value="{{ $key->id }}">
                                                                    {{ $key->nama_peternakan }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row mb-3"> <label for="kesepakatan"
                                                        class="col-sm-2 col-form-label">Kesepakatan Kemitraan</label>
                                                    <div class="col-sm-10">
                                                        <textarea class="form-control" name="kesepakatan" style="height: 100px"></textarea>
                                                    </div>
                                                </div>
                                                <div class="d-grid gap-2 mt-3">
                                                    <button type="submit" class="btn btn-warning">
                                                        Kirim Permintaan Bergabung
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="card-body">
                            <h5 class="card-title">Daftar Permintaan Peternakan</h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Tanggal Kirim</th>
                                        <th scope="col">Nama Peternakan</th>
                                        <th scope="col" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                        $no = 1;
                                        ?>
                                    @foreach($permintaan as $key)
                                    <tr>
                                        <td> {{$no++}}. </td>
                                        <td> {{DatesIdn($key->created_at)}} </td>
                                        <td> {{$key->peternakan->nama_peternakan}} </td>
                                        <td  style="text-align: center;pointer-events: none;">
                                            <button class="btn btn-outline-danger btn-sm">
                                            {{$key->status}}
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- End Recent Sales -->

            </div>
        </div>
    </div>
@endsection
