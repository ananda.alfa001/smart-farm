@extends('main.main')
@section('content')
<div class="pagetitle">
        <h1>Dashboard Kemitraan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('kemitraan.index')}}">Dashboard</a></li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="row">
        <!-- Left side columns -->
        <div class="col-lg-16">
            <div class="row">
                <!-- Recent Sales -->
                <div class="col-24">
                    <div class="card recent-sales">
                        <div class="filter">
                            <a class="icon" href="#" data-bs-toggle="dropdown"><i
                                    class="bi bi-three-dots"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                <li class="dropdown-header text-start">
                                    <h6>Filter</h6>
                                </li>
                                <li><a class="dropdown-item" href="#">Today</a></li>
                                <li><a class="dropdown-item" href="#">This Month</a></li>
                                <li><a class="dropdown-item" href="#">This Year</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">List Akun Belum Divalidasi <span>| Today</span></h5>
                            <table class="table table-borderless datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Tanggal Daftar</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Roles</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no_L = 1;
                                    $no_P = 1;
                                    ?>
                                    <td> $no_L++ </td>
                                    <td> $p->created_at </td>
                                    <td> $p->nama </td>
                                    <td> $p->role </td>
                                    <td> $p->mail </td>
                                    <td> <button type="button" class="btn btn-warning btn-sm"> Validasi Akun
                                    </button> </td>
                                </tbody>
                            </table>

                        </div>

                        

                    </div>
                </div><!-- End Recent Sales -->

            </div>
        </div><!-- End Left side columns -->

        <div class="row">
            <!-- Left side columns -->
            <div class="col-lg-16">
                <div class="row">
                    <!-- Recent Sales -->
                    <div class="col-24">
                        <div class="card recent-sales">
                            <div class="filter">
                                <a class="icon" href="#" data-bs-toggle="dropdown"><i
                                        class="bi bi-three-dots"></i></a>
                                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                                    <li class="dropdown-header text-start">
                                        <h6>Filter</h6>
                                    </li>
                                    <li><a class="dropdown-item" href="#">Today</a></li>
                                    <li><a class="dropdown-item" href="#">This Month</a></li>
                                    <li><a class="dropdown-item" href="#">This Year</a></li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">List Akun Yang Sudah Divalidasi <span>| Today</span></h5>
                                <table class="table table-borderless datatable">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Tanggal Divalidasi</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Roles</th>
                                            <th scope="col">Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no_L = 1;
                                        $no_P = 1;
                                        ?>
                                         <tr>
                                            <td> $no_L++ </td>
                                            <td> $p->updated_at </td>
                                            <td> $p->nama </td>
                                            <td> $p->role </td>
                                            <td> $p->mail </td>
                                            
                                         </tr>
                                    </tbody>
                                </table>
    
                            </div>
    
                            
    
                        </div>
                    </div><!-- End Recent Sales -->
    
                </div>
            </div><!-- End Left side columns -->

        
        <!-- Right side columns -->
        <div class="col-lg-4">
            <!-- Website Traffic -->
            <div class="card">
                <div class="filter">
                    <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                        <li class="dropdown-header text-start">
                            <h6>Filter</h6>
                        </li>
                        <li><a class="dropdown-item" href="#">Today</a></li>
                        <li><a class="dropdown-item" href="#">This Month</a></li>
                        <li><a class="dropdown-item" href="#">This Year</a></li>
                    </ul>
                </div>
                
            </div><!-- End Website Traffic -->
        </div><!-- End Right side columns -->
    </div>
@endsection