<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ZANTARA - Empowering Farmers With Zantara!</title>
    <meta name="description" content="ZANTARA adalah platform inovatif yang mendorong Recording Sapi Peternakan dengan solusi cerdas. Temukan layanan rekaman hewan, manajemen peternakan, dan lebih banyak lagi dengan ZANTARA.">
    <meta name="keywords" content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">

    <!-- SOCIAL MEDIA META -->
    <meta property="og:title" content="ZANTARA - Recording Sapi Peternakan dengan Zantara!">
    <meta property="og:description" content="ZANTARA adalah platform inovatif yang mendorong Recording Sapi Peternakan dengan solusi cerdas. Temukan layanan rekaman hewan, manajemen peternakan, dan lebih banyak lagi dengan ZANTARA.">
    <meta property="og:image" content="{{asset('assets/front-page/img/logo/logo.png')}}">
    <meta property="og:url" content="https://zantara.id">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="ZANTARA">
    <meta name="twitter:card" content="summary_large_image">

    <!-- SEO META -->
    <meta name="google-site-verification" content="">

    <!-- Additional Meta Tags -->
    <meta name="robots" content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">
    <meta name="author" content="Ciptanesia.id">
    <meta name="googlebot" content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('assets/front-page/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/front-page/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{asset('assets/front-page/vendor/aos/aos.css')}}" rel="stylesheet">
    <link href="{{asset('assets/front-page/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/front-page/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('assets/front-page/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('assets/logo_zantara.png')}}" rel="icon">
    <link href="{{asset('assets/logo_zantara.png')}}" rel="apple-touch-icon">
    <!-- =======================================================
  * Template Name: Impact
  * Updated: Mar 10 2023 with Bootstrap v5.2.3
  * Template URL: https://bootstrapmade.com/impact-bootstrap-business-website-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <section id="topbar" class="topbar d-flex align-items-center" style="border-bottom: 2px solid #bebebe;">
        <div class="container d-flex justify-content-center justify-content-md-between">
            <div class="contact-info d-flex align-items-center">
                <i class="bi bi-hash d-flex align-items-center ms-4"
                    style="color:#ffbf00;">EmpoweringFarmersWithZantara</i>
            </div>
            <div class="social-links d-none d-md-flex align-items-center">
                <i class="bi bi-envelope d-flex align-items-center ms-4"><a
                        href="zantara.smartfarm@zantara.id">zantara.smartfarm@zantara.id</a></i>
                <i class="bi bi-phone d-flex align-items-center ms-4"><span>+62 822-3479-0991</span></i>
                {{-- <a href="#" class="twitter"><i class="bi bi-twitter"></i></a> --}}
                {{-- <a href="#" class="facebook"><i class="bi bi-facebook"></i></a> --}}
                <a href="https://www.instagram.com/zantara.id" class="instagram"><i class="bi bi-instagram"></i></a>
                {{-- <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a> --}}
            </div>
        </div>
    </section><!-- End Top Bar -->

    <header id="header" class="header d-flex align-items-center">
        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
            <a href="index.html" class="logo d-flex align-items-center">
                <!-- Uncomment the line below if you also wish to use an image logo -->
                <img src="{{asset('assets/front-page/img/logo/logo.png')}}" alt="">
                <!-- <h1>Zantara<span>.</span></h1> -->
            </a>
            <nav id="navbar" class="navbar">
                <ul>
                    <li><a href="#hero">Beranda</a></li>
                    <li><a href="#about">Tentang Kami</a></li>
                    <li><a href="#services">Layanan</a></li>
                    <li><a href="#portfolio">Mitra</a></li>
                    <li><a href="#team">Developer</a></li>
                    <li><a href="#">Berita</a></li>
                    <li class="dropdown"><a href="#"><span>Peternak Indonesia</span> <i
                                class="bi bi-chevron-down dropdown-indicator"></i></a>
                        <ul>
                            <li class="dropdown"><a href="#"><span>Sapi Indonesia</span> <i
                                        class="bi bi-chevron-down dropdown-indicator"></i></a>
                                <ul>
                                    <li><a href="#">Sapi Limosin</a></li>
                                    <li><a href="#">Sapi Madura</a></li>
                                    <li><a href="#">Sapi Simental</a></li>
                                    <li><a href="#">Sapi Brahman</a></li>
                                    <li><a href="#">Sapi Bali</a></li>
                                    <li><a href="#">Sapi Peranakan Ongole (PO)</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a href="#"><span>Kambing Indonesia</span> <i
                                        class="bi bi-chevron-down dropdown-indicator"></i></a>
                                <ul>
                                    <li><a href="#">Kambing Etawa</a></li>
                                    <li><a href="#">Kambing Muara</a></li>
                                    <li><a href="#">Kambing Saanen</a></li>
                                    <li><a href="#">Kambing Gembrong</a></li>
                                    <li><a href="#">Kambing Jawarandu</a></li>
                                    <li><a href="#">Kambing Boer</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a href="#"><span>Domba Indonesia</span> <i
                                        class="bi bi-chevron-down dropdown-indicator"></i></a>
                                <ul>
                                    <li><a href="#">Domba Garut</a></li>
                                    <li><a href="#">Domba lokal ekor tebal</a></li>
                                    <li><a href="#">Domba lokal ekor tipis</a></li>
                                    <li><a href="#">Domba Texel</a></li>
                                    <li><a href="#">Domba Merino</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#contact">Hubungi Kami</a></li>
                </ul>
            </nav><!-- .navbar -->
            <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
            <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="hero" style="background: linear-gradient(to right, #0f5825, #1f883e);">
        <div class="container position-relative">
            <div class="row gy-5" data-aos="fade-in">
                <div
                    class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
                    <h2>Welcome to <span> <img src="{{asset('assets/front-page/img/logo/logo.png')}}" style="width: 200px;"></span></h2>
                    <p>Mewujudkan Revolusi Peternakan Melalui Kemajuan Teknologi yang Luar Biasa, termasuk Pencatatan
                        Data yang Mendalam tentang Kesehatan dan Performa Hewan Peternakan.</p>
                    <div class="d-flex justify-content-center justify-content-lg-start">
                        <a href="{{route('login')}}" class="btn-get-started">Sign In Now!</a>
                        <a class="glightbox btn-watch-video d-flex align-items-center"><i
                                class="bi bi-play-circle"></i><span>Watch Video</span></a>
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2">
                    <img src="{{asset('assets/front-page/img/hero-img.png')}}" class="img-fluid animate-img-hero" alt=""
                        data-aos="zoom-out" data-aos-delay="100">
                </div>
            </div>
        </div>
        <div class="icon-boxes position-relative">
            <div class="container position-relative">
                <div class="row gy-4 mt-5">

                    <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
                        <div class="icon-box">
                            <div class="icon"><i class="bi bi-credit-card-2-front"></i></div>
                            <h4 class="title"><a href="" class="stretched-link">Record CowCard</a></h4>
                            <div class="text-minibox">Wawasan mendalam tentang performa hewan,
                                identifikasi pola-pola yang bermanfaat, dan implementasikan strategi yang tepat untuk
                                meningkatkan hasil produksi.</div>
                        </div>
                    </div>
                    <!--End Icon Box -->

                    <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="200">
                        <div class="icon-box">
                            <div class="icon"><i class="bi bi-graph-up"></i></div>
                            <h4 class="title"><a href="" class="stretched-link">Data Produksi</a></h4>
                            <div  class="text-minibox">Akses terhadap informasi yang
                                akurat dan terperinci tentang setiap langkah produksi, Anda dapat mengambil keputusan
                                yang lebih cerdas dan strategis.</div>
                        </div>
                    </div>
                    <!--End Icon Box -->

                    <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="300">
                        <div class="icon-box">
                            <div class="icon"><i class="bi bi-grid-1x2"></i></div>
                            <h4 class="title"><a href="" class="stretched-link">Filtering Data</a></h4>
                            <div  class="text-minibox">Mudah menyaring data
                                berdasarkan kriteria yang ditentukan, seperti tanggal, jenis, lokasi, atau parameter
                                lainnya. Nikmati hasil yang tepat dan relevan dalam sekejap!</div>
                        </div>
                    </div>
                    <!--End Icon Box -->

                    <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="500">
                        <div class="icon-box">
                            <div class="icon"><i class="bi bi-motherboard"></i>
                            </div>
                            <h4 class="title"><a href="" class="stretched-link">Status CowCard</a></h4>
                            <div  class="text-minibox">Mencatat dan memantau
                                informasi penting tentang ternak Anda, seperti riwayat kesehatan, berat badan, dan
                                sejarah reproduksi.</div>
                        </div>
                    </div>
                    <!--End Icon Box -->

                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- End Hero Section -->

    <main id="main">

        <!-- ======= About Us Section ======= -->
        <section id="about" class="about">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Tentang <span> <img src="{{asset('assets/front-page/img/logo/logo_wht.png')}}" style="width: 150px;"></span> </h2>
                    <p>"Transformasi Digital dalam Merekam dan Mengelola Data Sapi Anda" <i
                            style="color:#d4a107;">#EmpoweringFarmersWithZantara</i></p>
                </div>

                <div class="row gy-4">
                    <div class="col-lg-6">
                        <h3>Zantara adalah solusi peternakan yang menggabungkan teknologi webApps dengan industri
                            peternakan.</h3>
                        <img src="{{asset('assets/front-page/img/about.jpg')}}" class="img-fluid rounded-4 mb-4" alt="">
                        <p>Dengan Zantara, masa depan peternakan adalah lebih cerdas, efisien, dan berkelanjutan.
                            Teknologi yang canggih ini tidak hanya membantu peternak meningkatkan profitabilitas mereka,
                            tetapi juga menjaga kesehatan dan kesejahteraan hewan, serta berkontribusi pada pertumbuhan
                            industri peternakan secara keseluruhan.</p>
                        <p>Zantara juga membantu peternak untuk memantau dan mengontrol berbagai aspek peternakan secara
                            real-time. Sistem pemantauan cerdas mengumpulkan dan menganalisis data lingkungan, kesehatan
                            hewan, pakan, dan performa individu. Hal ini memungkinkan peternak untuk mengambil keputusan
                            yang tepat waktu dan tepat dalam meningkatkan efisiensi dan kesehatan ternak.</p>
                    </div>
                    <div class="col-lg-6">
                        <div class="content ps-0 ps-lg-5">
                            <ul>
                                <li><i class="bi bi-check-circle-fill"></i> Efisiensi Pencatatan: Dengan Zantara, Anda
                                    dapat merekam dan mengelola data sapi secara digital, menghemat waktu dan usaha
                                    dalam pencatatan manual tradisional..</li>
                                <li><i class="bi bi-check-circle-fill"></i> Pengelolaan Informasi yang Mudah: Dengan
                                    fitur pengelolaan data yang intuitif, Anda dapat dengan mudah mengakses,
                                    memperbarui, dan menganalisis informasi mengenai sapi Anda, seperti riwayat
                                    kesehatan, reproduksi, dan performa.</li>
                                <li><i class="bi bi-check-circle-fill"></i> Peningkatan Efisiensi Reproduksi: Dengan
                                    melacak data reproduksi sapi, Zantara membantu mengidentifikasi periode kebuntingan,
                                    waktu birahi, dan performa reproduksi, memungkinkan perencanaan pembiakan yang lebih
                                    efektif.</li>
                                <li><i class="bi bi-check-circle-fill"></i> Peningkatan Keuntungan: Dengan menggunakan
                                    Zantara untuk mengoptimalkan pengelolaan dan performa sapi, Anda dapat meningkatkan
                                    efisiensi peternakan dan potensi keuntungan.</li>
                            </ul>
                            <p>
                                Dengan menghadirkan inovasi perumusan CowCard, Zantara membantu peternak mengoptimalkan
                                operasi mereka, meningkatkan produktivitas, dan meningkatkan kesejahteraan hewan.
                            </p>

                            <div class="position-relative mt-4">
                                <img src="{{asset('assets/auth/img/bg_zantara.jpg')}}" class="img-fluid rounded-4" alt="">
                                <a class="glightbox play-btn"></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End About Us Section -->

        <!-- ======= Clients Section ======= -->
        <section id="clients" class="clients">
            <div class="container" data-aos="zoom-out">

                <div class="clients-slider swiper">
                    <div class="swiper-wrapper align-items-center">
                        <div class="swiper-slide"><img src="{{asset('assets/front-page/img/clients/client-1.png')}}" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{asset('assets/front-page/img/clients/client-2.png')}}" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{asset('assets/front-page/img/clients/client-3.png')}}" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{asset('assets/front-page/img/clients/client-4.png')}}" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{asset('assets/front-page/img/clients/client-5.png')}}" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{asset('assets/front-page/img/clients/client-6.png')}}" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{asset('assets/front-page/img/clients/client-7.png')}}" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{asset('assets/front-page/img/clients/client-8.png')}}" class="img-fluid"
                                alt=""></div>
                    </div>
                </div>

            </div>
        </section><!-- End Clients Section -->

        <!-- ======= Stats Counter Section ======= -->
        <section id="stats-counter" class="stats-counter">
            <div class="container" data-aos="fade-up">

                <div class="row gy-4 align-items-center">

                    <div class="col-lg-6">
                        <img src="{{asset('assets/front-page/img/stats-img.svg')}}" alt="" class="img-fluid">
                    </div>

                    <div class="col-lg-6">

                        <div class="stats-item d-flex align-items-center">
                            <span data-purecounter-start="0" data-purecounter-end="{{$cowcard}}" data-purecounter-duration="1"
                                class="purecounter"></span>
                            <p><strong>cowcard</strong> Sapi yang terdaftar pada kami.</p>
                        </div><!-- End Stats Item -->

                        <div class="stats-item d-flex align-items-center">
                            <span data-purecounter-start="0" data-purecounter-end="{{$peternakan}}" data-purecounter-duration="1"
                                class="purecounter"></span>
                            <p><strong>Peternakan</strong> percaya dengan kami di Indonesia.</p>
                        </div><!-- End Stats Item -->

                        <div class="stats-item d-flex align-items-center">
                            <span data-purecounter-start="0" data-purecounter-end="{{$kandang}}" data-purecounter-duration="1"
                                class="purecounter"></span>
                            <p><strong>Kandang</strong>dalam peternakan-peternakan kami di Indonesia</p>
                        </div><!-- End Stats Item -->

                    </div>

                </div>

            </div>
        </section><!-- End Stats Counter Section -->
    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer" class="footer">

        <div class="container">
            <div class="row gy-4">
                <div class="col-lg-5 col-md-12 footer-info">
                    <a href="index.html" class="logo d-flex align-items-center">
                        <span> <img src="{{asset('assets/front-page/img/logo/logo.png')}}" style="width: 200px;"></span>
                    </a>
                    <p>Mewujudkan Revolusi Peternakan Melalui Kemajuan Teknologi yang Luar Biasa, termasuk Pencatatan
                        Data yang Mendalam tentang Kesehatan dan Performa Hewan Peternakan.</p>
                    <div class="social-links d-flex mt-4">
                        <a href="https://www.instagram.com/zantara.id"class="instagram"><i class="bi bi-instagram"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><a href="#">Beranda</a></li>
                        <li><a href="#">Tentang Kami</a></li>
                        <li><a href="#">Layanan</a></li>
                        <li><a href="#">Mitra</a></li>
                    </ul>
                </div>

                <div class="col-lg-2 col-6 footer-links">
                    <h4>Peternakan Indonesia</h4>
                    <ul>
                        <li>Sapi</li>
                        <li>Kambing</li>
                        <li>Domba</li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
                    <h4>Alamat Kantor</h4>
                    <p>Head Office: <br>
                        Jl. Raya Narogong<br>
                        Kota Bekasi<br>17117<br>
                        Jawa Barat
                    </p>
                    <p>Site 1 Office: <br>
                        Driyorejo<br>
                        Kabupaten Gresik<br>61177<br>
                        Jawa Timur
                    </p>
                    <p>Site 2 Office: <br>
                        Jl. Gajah Timur, Magersari<br>
                        Kota Sidoarjo<br>61212<br>
                        Jawa Timur
                    </p>

                </div>

            </div>
        </div>

        <div class="container mt-4">
            <div class="copyright">
                &copy; Copyright <strong><span>Zantara.id</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/impact-bootstrap-business-website-template/ -->
                Developt by <a href="https://zantara.id/">Zantara Team</a> & Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </div>
    </footer>
    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
    <div id="preloader"></div>
    <!-- Vendor JS Files -->
    <script src="{{asset('assets/front-page/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/front-page/vendor/aos/aos.js')}}"></script>
    <script src="{{asset('assets/front-page/vendor/glightbox/js/glightbox.min.js')}}"></script>
    <script src="{{asset('assets/front-page/vendor/purecounter/purecounter_vanilla.js')}}"></script>
    <script src="{{asset('assets/front-page/vendor/swiper/swiper-bundle.min.js')}}"></script>
    <script src="{{asset('assets/front-page/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('assets/front-page/vendor/php-email-form/validate.js')}}"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('assets/front-page/js/main.js')}}"></script>

</body>

</html>
