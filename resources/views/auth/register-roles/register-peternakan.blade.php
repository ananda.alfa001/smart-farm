<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ZANTARA - Empowering Farmers With Zantara!</title>
    <meta name="description"
        content="ZANTARA adalah platform inovatif yang mendorong Recording Sapi Peternakan dengan solusi cerdas. Temukan layanan rekaman hewan, manajemen peternakan, dan lebih banyak lagi dengan ZANTARA.">
    <meta name="keywords"
        content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">

    <!-- SOCIAL MEDIA META -->
    <meta property="og:title" content="ZANTARA - Recording Sapi Peternakan dengan Zantara!">
    <meta property="og:description"
        content="ZANTARA adalah platform inovatif yang mendorong Recording Sapi Peternakan dengan solusi cerdas. Temukan layanan rekaman hewan, manajemen peternakan, dan lebih banyak lagi dengan ZANTARA.">
    <meta property="og:image" content="{{ asset('assets/front-page/img/logo/logo.png') }}">
    <meta property="og:url" content="https://zantara.id">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="ZANTARA">
    <meta name="twitter:card" content="summary_large_image">

    <!-- SEO META -->
    <meta name="google-site-verification" content="">

    <!-- Additional Meta Tags -->
    <meta name="robots"
        content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">
    <meta name="author" content="Ciptanesia.id">
    <meta name="googlebot"
        content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">


    <!-- Favicons -->
    <link href="{{asset('assets/logo_zantara.png')}}" rel="icon">
    <link href="{{asset('assets/logo_zantara.png')}}" rel="apple-touch-icon">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/auth/css/my-login.css') }}">
</head>

<body class="my-login-page">
    @include('sweetalert::alert')
    <section class="h-100">
        <div class="container h-100">
            <div class="row justify-content-md-center h-100" style="
            margin-top: 200px;
        ">
                <div class="card-wrapper"
                    style="
                    width: 922px;
                    padding-left: 15px;
                    padding-right: 15px;">
                    <form method="POST" class="my-login-validation" action="{{ route('form1-up') }}"
                        enctype="multipart/form-data" novalidate="">
                        @csrf
                        <div class="card fat" style="border-radius: 2.25rem;">
                            <div class="card-body">
                                <h4 class="card-title text-center"> <span><img
                                            src="{{ asset('assets/front-page/img/logo/logo_wht.png') }}"
                                            style="width: 150px;"></span> <br> Registrasi Peternakan</h4>
                                <div class="form-group">
                                    <label for="name">Farm Name</label>
                                    <input id="name" type="text" class="form-control" name="name"
                                        placeholder="Boy Farm's" required>
                                    <div class="invalid-feedback">
                                        Farm Name invalid
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="roles">Farm Owner's Name</label>
                                    <input id="farm_owner" type="name" placeholder="Boy" class="form-control"
                                        name="farm_owner" required>
                                    <div class="invalid-feedback">
                                        Farm Owner's Name invalid
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="year">Since</label>
                                    <input id="year" type="date" class="form-control" max="<?= date('Y-m-d') ?>"
                                        name="year" required>
                                    <div class="invalid-feedback">
                                        Since invalid
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">
                                        Name of Cages</label>
                                    <div class="field_wrapper">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <input class="form-control" placeholder="Name of Cage"
                                                        type="text" name="cage[]" required data-eye />
                                                </div>
                                                <div class="col-md-2">
                                                    <a class="btn btn-success" href="javascript:void(0);"
                                                        id="add_button" title="Add field">Add Cage</a>
                                                </div>
                                            </div>
                                            <div class="invalid-feedback">
                                                Number of Cages is invalid!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Farm Address</label>
                                    <input id="alamat" type="alamat" class="form-control"
                                        placeholder="East Java, Indonesian" name="address" required data-eye>
                                    <div class="invalid-feedback">
                                        Farm Address is required
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Telephone Number</label>
                                    <input id="alamat" type="number" class="form-control"
                                        name="telephone_number" placeholder="(+62)" required data-eye>
                                    <div class="invalid-feedback">
                                        Telephone Number is required
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group m-0">
                            <button type="submit" class="btn btn-success btn-block">
                                Next Registration
                            </button>
                        </div>
                        {{-- <div class="card fat" style="border-radius: 2.25rem;">
                            <div class="card-body">
                                <h4 class="card-title">Farm Approval Sheet</h4>
                                <div class="form-group">
                                    <div class="custom-file">
                                        <label class="form-label" for="customFile">Choose file Approval
                                            Sheet</label>
                                        <input type="file" name="file" class="form-control" id="customFile" style="height: 46.59376px;" required data-eye />
                                        <div class="invalid-feedback">
                                            file Approval invalid
                                        </div>
                                    </div>
                                </div>
                                <hr>

                            </div>
                        </div> --}}
                    </form>
                    <div class="footer">
                        <div class="container mt-4">
                            <div class="copyright">
                                &copy; Copyright <strong><span>Zantara.id</span></strong>. All Rights Reserved
                            </div>
                            <div class="credits">
                                Developt by <a href="https://zantara.id/">Zantara Team</a> & Designed by <a
                                    href="https://www.instagram.com/ourproject.creative/"> Our Project.Creative</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="{{ asset('assets/auth/js/my-login.js') }}"></script>
</body>
</html>
