<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ZANTARA - Empowering Farmers With Zantara!</title>
    <meta name="description"
        content="ZANTARA adalah platform inovatif yang mendorong Recording Sapi Peternakan dengan solusi cerdas. Temukan layanan rekaman hewan, manajemen peternakan, dan lebih banyak lagi dengan ZANTARA.">
    <meta name="keywords"
        content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">

    <!-- SOCIAL MEDIA META -->
    <meta property="og:title" content="ZANTARA - Recording Sapi Peternakan dengan Zantara!">
    <meta property="og:description"
        content="ZANTARA adalah platform inovatif yang mendorong Recording Sapi Peternakan dengan solusi cerdas. Temukan layanan rekaman hewan, manajemen peternakan, dan lebih banyak lagi dengan ZANTARA.">
    <meta property="og:image" content="{{ asset('assets/front-page/img/logo/logo.png') }}">
    <meta property="og:url" content="https://zantara.id">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="ZANTARA">
    <meta name="twitter:card" content="summary_large_image">

    <!-- SEO META -->
    <meta name="google-site-verification" content="">

    <!-- Additional Meta Tags -->
    <meta name="robots"
        content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">
    <meta name="author" content="Ciptanesia.id">
    <meta name="googlebot"
        content="ZANTARA, Recording Sapi Peternakan, rekaman hewan, manajemen peternakan, peternakan cerdas, inovasi peternakan,  recording, animal, cowcard, smartfarm, farmer, cow, record, farming, cowcard, smart farm, zantara">


    <!-- Favicons -->
    <link href="{{asset('assets/logo_zantara.png')}}" rel="icon">
    <link href="{{asset('assets/logo_zantara.png')}}" rel="apple-touch-icon">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/auth/css/my-login.css') }}">
</head>

<body class="my-login-page">
    @include('sweetalert::alert')
    <section class="h-100">
        <div class="container h-100">
            <div class="row justify-content-md-center h-100" style="
            margin-top: 200px;
        ">
                <div class="card-wrapper">
                    <div class="card fat">
                        <div class="card-body">
                            <h4 class="card-title text-center"> <span><img
                                        src="{{ asset('assets/front-page/img/logo/logo_wht.png') }}"
                                        style="width: 150px;"></span> <br> Registrasi Dokter Hewan/Teknisi</h4>
                            <form method="POST" class="my-login-validation" action="{{ route('form1-teknisi-up') }}"
                                novalidate="">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Nama Instansi</label>
                                    <input id="name" type="text" class="form-control" name="name_instansi"
                                        required autofocus>
                                    <div class="invalid-feedback">
                                        Asal Instansi Anda?
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="profesi">Profesi</label>
                                    <input id="profesi" type="text" class="form-control" name="profesi" required>
                                    <div class="invalid-feedback">
                                        Profesi Anda?
                                    </div>
                                </div>
                                <h5> Data Pribadi </h5>
                                <div class="form-group">
                                    <label for="tanggal">Tanggal Lahir</label>
                                    <input id="tanggal" type="date" max="<?= date('Y-m-d') ?>" class="form-control"
                                        name="tanggal" required>
                                    <div class="invalid-feedback">
                                        Tanggal Lahir Salah
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat Tinggal</label>
                                    <input id="alamat" type="text" class="form-control" name="alamat" required>
                                    <div class="invalid-feedback">
                                        Alamat anda belum lengkap
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Nomor Telepon Aktif</label>
                                    <input id="nomor" type="number" class="form-control" name="nomor_telp"
                                        required>
                                    <div class="invalid-feedback">
                                        Nomor Telepon anda belum lengkap
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-checkbox custom-control">
                                        <input type="checkbox" name="agree" id="agree"
                                            class="custom-control-input" required="">
                                        <label for="agree" class="custom-control-label">Data yang Dikirim Tidak Akan
                                            disebarluaskan </label>
                                        <div class="invalid-feedback">
                                            You must agree with this option!
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-0">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Lengkapi Data
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="footer">
                        <div class="container mt-4">
                            <div class="copyright">
                                &copy; Copyright <strong><span>Zantara.id</span></strong>. All Rights Reserved
                            </div>
                            <div class="credits">
                                Developt by <a href="https://zantara.id/">Zantara Team</a> & Designed by <a
                                    href="https://www.instagram.com/ourproject.creative/"> Our Project.Creative</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="{{ asset('assets/auth/js/my-login.js') }}"></script>
</body>

</html>
